/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SRC_GUI_H_
#define _SRC_GUI_H_

#include <gdk/gdk.h>

#include "src/frontend/mvc_types.h"
#include "src/frontend/view_generic.h"

/**
 * The structure stores data that need to be supplied to the event handling loop
 * in order to work correctly.
 */
typedef struct {
		/** \brief Storage for glib event loop pointer. */
		GMainLoop *loop;

		/** \brief Storage for pointer to the application window. */
		GdkWindow *window;

		/** \brief Storage for main gui view pointer. */
		ViewGeneric *application;
	} GuiData;

/** \brief Create gui structures and initialize main loop. */
extern RCode gui_init(GuiData *guiData);

/** \brief Deallocate gui structures and main loop. */
extern RCode gui_finish(GuiData *guiData);

/** \brief Initiate main application loop. */
extern RCode gui_loop(GuiData *guiData);

#endif
