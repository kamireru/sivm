/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file image.h
 *
 * \brief Declares structure for storing information about images.
 *
 * The Image structure is one of basic structures in SIVM. It stores all
 * information about image that is known to the SIVM.
 *
 * Apart from image (file) specific stuff we store information about groups into
 * which image belongs here. When the image is added to the group, group tag is
 * added to the image and reference to the image is created.  This information
 * is used for recreation of SIVM session from textual dump.
 */
#ifndef __BACKEND_IMAGE_H__
#define __BACKEND_IMAGE_H__

#include <time.h>
#include <glib.h>

#include "src/common/common_def.h"
#include "src/backend/taglist.h"

/**
 * \brief The structure holds information about one image.
 *
 * It represents image on the file system, so it contains path to the file and
 * variables for caching information about the image (like dimensions, size,
 * ...)
 */
typedef struct {
		/** \brief Path to the file (relative or absolute) in the file system */
		gchar* name;

		/**
		 * \brief Structure holding information about groups containing the
		 * image.
		 */
		TagList* group;

		/** \brief Size of the image in the bytes as reported by file system. */
		guint size;

		/** \brief  Timestamp of the image as reported by file system. */
		time_t timestamp;

		/** \brief Number of references to the image instance. */
		guint ref;
	} Image;

/** \brief Allocate and initialize image structure. */
extern Image* image_create(const gchar *filename);

/** \brief Deallocate image structure. */
extern RCode image_destroy(Image* image);

/** \brief Increase reference count of the image. */
extern RCode image_ref(Image* image);

/** \brief Decrese reference count and possibly deallocate. */
extern RCode image_unref(Image* image);

/** \brief Return number of references to the image. */
extern guint image_ref_count(Image* image);

/** \brief Return taglist with group tags. */
extern  TagList* image_get_groups(Image* image);

#endif
