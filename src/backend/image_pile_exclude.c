/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include "src/common/common_def.h"
#include "src/backend/image_pile_exclude.h"


/**
 * The function takes NULL terminated list of character strings and compiles
 * NULL terminated exclude pattern list from them.
 *
 * \param excludeList NULL terminated string list
 * \return NULL terminated pattern list
 */
GPatternSpec** pile_exclude_create(gchar **excludeList) {

	GPatternSpec** result = NULL;
	guint i;

	if ( NULL == excludeList ) {
		return result;
	}

	// check number of patterns in the exclude list
	for ( i = 0; NULL != excludeList[i]; i++ );

	// allocate memory for pattern list
	result = g_try_malloc( (i+1) * sizeof(GPatternSpec*) );
	if ( NULL == result ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return NULL;
	}

	// compile pattern list
	for ( i = 0; NULL != excludeList[i]; i++ ) {
		result[i] = g_pattern_spec_new( excludeList[i] );
	}
	result[i] = NULL;

	return result;
}

/**
 * The function deallocate NULL terminate list of exclude patterns.
 *
 * \param patternList NULL terminated list of patterns
 */
void pile_exclude_destroy(GPatternSpec **patternList) {
	guint i;

	if ( NULL == patternList ) {
		return;
	}

	// free allocated patterns
	for ( i = 0; NULL != patternList[i] ; i++ ) {
		g_pattern_spec_free( patternList[i] );
	}

	// free allocated list
	g_free( (gpointer)patternList);

}

/**
 * Check supplied string agains NULL terminated list of GLIB patterns and return
 * true if any pattern match to the string.
 *
 * \param patternList NULL terminated list of patterns
 * \param string string to match
 * \return TRUE if string match to some pattern
 */
gboolean pile_exclude_match(GPatternSpec **patternList, const gchar *string) {
	guint i;
	gchar* reverse_string = NULL;
	gboolean match_found = FALSE;

	if ( NULL == patternList || NULL == string ) {
		return FALSE;
	}

	// precompute values used in the cycle
	reverse_string = g_utf8_strreverse(string, -1);

	// try to match some pattern from list
	for ( i = 0; NULL != patternList[i] && FALSE == match_found; i++ ) {
		match_found = g_pattern_match( patternList[i], strlen(string), string, reverse_string);
	}

	g_free( (gpointer)reverse_string );
	return match_found;
}
