/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include "src/backend/image_group_buffer.h"

/**
 * Pointer to space allocated for yank buffer.
 */
Image** group_yank_buffer = NULL;

/**
 * Variable for keeping track how many elements are present in yank buffer.
 */
guint group_yank_size = 0;

/**
 * Variable for keeping track what is the maximum capacity of yank buffer.
 */
guint group_yank_max_size = 0;


/**
 * Default tag for a yank buffer (to mark that image is present in yank buffer).
 */
#define YANK_BUFFER_TAG '*'

/**
 * The function allocates or reallocates internal yank buffer to hold up to
 * supplied size of elements.
 *
 * \todo Keep pointer to old buffer so we can revert to it in case reallocation
 * fails.
 *
 * \param newSize new size of the yank buffer
 * \return OK on success
 */
RCode group_buffer_resize(guint newSize) {

	if ( group_yank_max_size >= newSize ) {
		return OK;
	}

	TRACE_MESSAGE(IGRP, TL_DEBUG, "Resize yank buffer (old=%d,new=%d)", group_yank_max_size, newSize);

	group_yank_buffer = (Image**)g_try_realloc(group_yank_buffer, newSize*sizeof(Image*));
	if ( NULL == group_yank_buffer ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return FAIL;
	}

	group_yank_max_size = newSize;
	return OK;
}

/**
 * This function deallocates all internal buffers available to image_group
 * module. Ie  insert buffer and yank buffer.
 *
 * \return OK on success
 */
RCode group_buffer_destroy() {

	TRACE_MESSAGE(IGRP, TL_MINOR, "Deallocating yank buffer");

	if ( FAIL == group_buffer_clear() ) {
		return FAIL;
	}

	g_free((gpointer)group_yank_buffer);
	group_yank_buffer = NULL;
	group_yank_size = 0;
	group_yank_max_size = 0;

	return OK;
}


/**
 * This function will iterate though all records in yank buffer and will call
 * unref to the image pointer (to allow deletion of such pointer). The it will
 * set number of record to zero, but will not deallocate the buffer.
 *
 * \return OK on success
 */
RCode group_buffer_clear() {
	guint i = 0;

	TRACE_MESSAGE(IGRP, TL_MINOR, "Clearing yank buffer");
	if ( NULL == group_yank_buffer || 0 == group_yank_size ) {
		return OK;
	}

	/* all image pointers stored in yank buffer have own reference to image
	 * instance and these references need to be cleared before pointer is erased
	 */
	for ( i = 0; i < group_yank_size; i++ ) {
		RCode result;

		result = taglist_remove(
				image_get_groups(group_yank_buffer[i]), YANK_BUFFER_TAG
			);
		if ( FAIL == result ) {
			ERROR_LOG();
		}

		result = image_unref(group_yank_buffer[i]);
		if ( FAIL == result ) {
			ERROR_LOG();
		}
	}

	// clear the yank buffer
	memset( group_yank_buffer, 0, group_yank_max_size*sizeof(Image*));
	group_yank_size = 0;
	return OK;
}

/**
 * The function returns pointer to the current yank buffer.
 *
 * \return pointer to yank buffer
 */
Image** group_buffer_get_pointer() {
	return group_yank_buffer;
}

/**
 * The function returns number of elements stored in the yank buffer.
 *
 * \return number of elements in buffer
 */
guint group_buffer_size() {
	return group_yank_size;
}

/**
 * The function returns maximum capacity of the yank buffer
 *
 * \return maximum capacity of buffer
 */
guint group_buffer_max_size() {
	return group_yank_max_size;
}

/**
 * The function inserts image into the buffer and references it. If the size of
 * buffer is too small to accomodate new element, it is resized.
 *
 * \param image pointer to image to insert into buffer
 * \return OK on success
 */
RCode group_buffer_insert(Image* image) {
	ASSERT( NULL != image);

	// check if there is enough space in buffer and resize if not
	if ( group_yank_size == group_yank_max_size ) {
		if ( FAIL == group_buffer_resize(group_yank_max_size + 1) ) {
			ERROR_NOTE("Image group yank buffer reallocation failed");
			return FAIL;
		}
	}

	taglist_insert(image_get_groups(image), YANK_BUFFER_TAG);
	image_ref(image);
	group_yank_buffer[group_yank_size++] = image;

	return OK;
}
