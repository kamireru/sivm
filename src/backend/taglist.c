/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>
#include "src/backend/taglist.h"

/**
 * Allocate memory for taglist structure and initialize it to contain no tags at
 * all.
 *
 * \return pointer to TagList or NULL on error
 */
TagList* taglist_create() {
	TagList* result = NULL;

	result = (TagList*) g_try_malloc0(sizeof(TagList));
	if ( NULL == result ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return NULL;
	}

	result->list = NULL;
	result->size = 0;
	result->max_size = 0;

	return result;
}

/**
 * Deallocate taglist structure along with the internal structures.
 *
 * \param taglist pointer to the TagList structure
 * \return OK on success
 */
RCode taglist_destroy(TagList *taglist) {
	ASSERT( NULL != taglist );

	if ( NULL != taglist->list ) {
		g_free( (gpointer) taglist->list );
	}

	g_free( (gpointer) taglist );
	return OK;
}

/**
 * The function inserts tag character into the list, reallocating the list size
 * if needed. The tag character is added to the list only if it is not present
 * yet.
 *
 * \param taglist pointer to the TagList structure
 * \param tag character to add into the list
 * \return OK on success
 */
RCode taglist_insert(TagList *taglist, gchar tag) {
	guint i;

	ASSERT( NULL != taglist );

	// check tag validity
	if ( ! taglist_is_tag(tag) ) {
		ERROR_SET(ERROR.INVALID_PARAM);
		return FAIL;
	}

	// check if the tag is already present
	if ( NULL != taglist->list ) {
		for (i = 0; i < taglist->size; i++) {
			if ( tag == taglist->list[i] ) {
				return OK;
			}
		}
	}

	// check if there is enough space in list and reallocate if not
	if ( taglist->max_size < taglist_get_count(taglist) + 1 ) {
		gchar* new_pointer = NULL;

		// the size increase is maximum of half of current size and minimum
		// increment
		guint new_size = taglist->max_size / 2;
		new_size = ( new_size < TAGLIST_MIN_INCREMENT ) ? TAGLIST_MIN_INCREMENT : new_size;
		new_size = taglist->max_size + new_size;

		// try to reallocate the list
		new_pointer = (gchar*) g_try_realloc(taglist->list, new_size * sizeof(gchar) );
		if ( NULL == new_pointer ) {
			ERROR_SET(ERROR.MEMORY_FAILURE);
			return FAIL;
		}

		taglist->list = new_pointer;
		taglist->max_size = new_size;
	}

	// add tag character into the list
	taglist->list[taglist->size++] = tag;

	return OK;
}

/**
 * Remove tag from the taglist and reallocate the taglist if there is too much
 * of empty space.
 *
 * If the tag is not present, then nothing is removed and success is noted (the
 * tag is not in the taglist anymore).
 *
 * The same is done when an invalid character is supplied - since it is invalid,
 * it can't be in the list and so a success is noted.
 *
 * \param taglist pointer to the TagList structure
 * \param tag character to remove from list
 * \return OK on success
 */
RCode taglist_remove(TagList *taglist, gchar tag) {
	guint i;

	ASSERT( NULL != taglist );

	// check for invalid character
	if ( ! taglist_is_tag(tag) ) {
		return OK;
	}

	// check if the list is empty
	if ( NULL == taglist->list || 0 == taglist->size ) {
		return OK;
	}

	// try to find and remove the tag character
	for (i = 0; i < taglist->size; i++ ) {
		// if tag is found, it is replaced by tag from the end of the list and
		// size is decreased
		if ( tag == taglist->list[i] ) {
			taglist->list[i] = taglist->list[--taglist->size];
		}
	}

	// check if reallocation is needed
	if ( 2 * taglist->size >= taglist->max_size ) {
		gchar* new_pointer = NULL;

		// the size decrease is the half of the empty space in list
		guint new_size = (taglist->max_size / 4) * 3;
		new_size = ( TAGLIST_MIN_INCREMENT > new_size ) ? TAGLIST_MIN_INCREMENT : new_size;

		// do not reallocate under minimum increment size
		if ( new_size == taglist->max_size ) {
			return OK;
		}

		// try to reallocate the list
		new_pointer = (gchar*) g_try_realloc(taglist->list, new_size * sizeof(gchar) );
		if ( NULL == new_pointer ) {
			ERROR_SET(ERROR.MEMORY_FAILURE);
			return FAIL;
		}

		taglist->list = new_pointer;
		taglist->max_size = new_size;
	}

	return OK;
}

/**
 * Check if tag is present in the TagList structure.
 *
 * \param taglist pointer to the TagList structure
 * \param tag character to check for presence
 * \return TRUE if tag found in the TagList
 */
gboolean taglist_contains(TagList *taglist, gchar tag) {
	guint i;

	ASSERT( NULL != taglist );

	// check for invalid character
	if ( ! taglist_is_tag(tag) ) {
		return FALSE;
	}

	// check if the list is empty
	if ( NULL == taglist->list || 0 == taglist->size ) {
		return FALSE;
	}

	// try to find a tag in the list
	for (i = 0; i < taglist->size; i++ ) {
		if ( tag == taglist->list[i] ) {
			return TRUE;
		}
	}

	return FALSE;
}

/**
 * Get number of tags in the TagList structure
 *
 * \param taglist pointer to the TagList structure
 * \return tag count
 */
guint taglist_get_count(TagList* taglist) {
	ASSERT( NULL != taglist );

	return taglist->size;
}

/**
 * The function returns true if supplied character is valid tag character.
 *
 * The tag characters are limited to lowercase and uppercase letters and
 * numbers.
 *
 * Special tag character '*' is reserved for yank buffer.
 *
 * \param tag character to check for validity
 * \return TRUE if character is valid
 */
gboolean taglist_is_tag(gchar tag) {

	if ( 'a' <= tag && tag <= 'z' ) {
		return TRUE;

	} else if ( 'A' <= tag && tag <= 'Z' ) {
		return TRUE;

	} else if ( '0' <= tag && tag <= '9' ) {
		return TRUE;

	} else if ( '*' == tag ) {
		return TRUE;
	}

	return FALSE;
}
