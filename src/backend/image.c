/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>
#include "src/backend/image.h"

/**
 * Allocate memory for Image structure and initializes {name} with supplied
 * file name.
 *
 * The remaining structure members are initialized to the default values.
 *
 * \param filename path to the file
 * \return pointer to initialize Image structure or NULL on error
 */
Image* image_create(const gchar *filename) {
	TagList* group_list = NULL;
	Image* tmp_image = NULL;

	ASSERT(NULL != filename);

	if ( NULL == ( tmp_image = (Image*) g_try_malloc(sizeof(Image))) ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return NULL;
	}

	if ( NULL == (tmp_image->name = g_strdup(filename)) ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		ERROR_NOTE("Image name allocation failed");
		g_free((gpointer)tmp_image);
		return NULL;
	}

	group_list = taglist_create();
	if ( NULL == group_list ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		ERROR_NOTE("Image group list allocation failed");
		free( (void*)tmp_image->name );
		g_free((gpointer)tmp_image);
		return NULL;
	}

	tmp_image->group = group_list;
	tmp_image->ref = 1;
	tmp_image->size = 0;
	tmp_image->timestamp = 0;
	return tmp_image;
}


/**
 * Function checks reference count and if it is zero, the it deallocates the
 * image. If it is non-zero, then someone called the function directly without
 * checking reference count and failure need to be reported.
 *
 * \param image pointer to Image structure
 * \return OK on success
 */
RCode image_destroy(Image* image) {
	ASSERT( NULL != image );

	if ( 0 != image->ref ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Image structure has %d references", image->ref);
		return FAIL;
	}

	taglist_destroy(image->group);
	free( (void*)image->name);
	g_free((gpointer)(image));
	return OK;
}


/**
 * Increase reference count. The maximum count is not limited.
 *
 * \param image pointer to Image structure
 * \return OK
 */
RCode image_ref(Image* image) {
	ASSERT(NULL != image);

	image->ref++;
	return OK;
}


/**
 * Decrease reference count for given image. This function could not be used to
 * decrease reference count below 1. The last reference can be removed only by
 * call to image_destroy() function.
 *
 * \param image pointer to Image structure
 *
 * \return OK if refcount > 1, result of image_destroy() otherwise
 */
RCode image_unref(Image* image) {
	ASSERT(NULL != image);

	if ( 0 != (--image->ref) ) {
		return OK;
	}

	return image_destroy(image);
}

/**
 * Function returns number of references the Image structure has.
 *
 * \param image pointer to Image structure
 * \return number of references
 */
guint image_ref_count( Image* image) {
 	ASSERT( NULL != image );

	return image->ref;
}

/**
 * Return TagList structure containing tags of all groups into which the image
 * belongs.
 *
 * \param image pointer to Image structure
 * \return TagList pointer
 */
TagList* image_get_groups(Image* image) {
	ASSERT( NULL != image );

	return image->group;
}
