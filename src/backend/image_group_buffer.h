/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file image_group_buffer.h
 * \brief API for accessing temporary buffer for moving images between image
 * groups.
 *
 * When moving images between various groups (ie yanking and cutting) it is
 * necessary to use array of Image* variables.
 *
 * To prevent frequent allocation of arrays for that, we provide internal 'yank'
 * buffer into which we will move all elements we want to move froma and to
 * ImageGroup.
 *
 * This buffer is destroyed at the end of program execution and during runtime
 * it onle increases its size.
 */

#ifndef _BACKEND_IMAGE_GROUP_BUFFER_H_
#define _BACKEND_IMAGE_GROUP_BUFFER_H_

#include <glib.h>
#include "src/common/common_def.h"
#include "src/backend/image.h"


/** \brief Allocate/reallocate memory for yank buffer. */
extern RCode group_buffer_resize(guint newSize);

/** \brief Deallocate yank buffer. */
extern RCode group_buffer_destroy();

/** \brief Remove content from buffer. */
extern RCode group_buffer_clear();

/** \brief Get pionter to the yank buffer. */
extern Image** group_buffer_get_pointer();

/** \brief Get number of elements in yank buffer. */
extern guint group_buffer_size();

/** \brief Get maximum capacity of the yank buffer. */
extern guint group_buffer_max_size();

/** \brief Insert image into the yank buffer. */
extern RCode group_buffer_insert(Image* image);



// Internal variables, exported only for use in unit tests

/** \brief Pointer to yank buffer for cutting and pasting from image group. */
extern Image** group_yank_buffer;

/** \brief Number of images in yank buffer.  */
extern guint group_yank_size;

/** \brief Maximal size of the yank buffer */
extern guint group_yank_max_size;

#endif
