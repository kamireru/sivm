/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "src/common/common_def.h"
#include "src/backend/image_group.h"
#include "src/backend/image_group_buffer.h"

/**
 * This function creates new image list instace and set its name and tag. It
 * also creates emty array of ImageGroupRecord elements which will be used as
 * storage.
 *
 * \param groupName string with image list long identification
 * \param groupTag character with image list short identification
 * \return ImageGroup pointer on success, NULL otherwise
 */
ImageGroup* group_create(gchar* groupName, gchar groupTag) {
	ImageGroup* tmp_group = NULL;

	if ( NULL == groupName ) {
		groupName = "unknown";
	}

	TRACE_MESSAGE(IGRP, TL_MINOR, "Creating group (name='%s',tag='%c')", groupName, groupTag);

	if ( NULL == ( tmp_group = (ImageGroup*) g_try_malloc(sizeof(ImageGroup))) ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return NULL;
	}

	if ( NULL == (tmp_group->name = g_strdup(groupName)) ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		g_free((gpointer)tmp_group);
		return NULL;
	}

	tmp_group->list = g_array_new(FALSE, FALSE, sizeof(ImageGroupRecord));
	tmp_group->tag = groupTag;
	return tmp_group;
}


/**
 * This function deallocates all memory allocated for image list.
 *
 * \param group pointer to image list to deallocate
 * \return OK on success
 */
RCode group_destroy(ImageGroup* group) {
	RCode result;
	guint i;

	ASSERT( NULL != group);
	ASSERT( NULL != group->list);

	TRACE_MESSAGE(IGRP, TL_MINOR, "Destroying group (name='%s',tag='%c')", group->name, group->tag);

	// unreference all images that are stored in the buffer
	for (i = 0; i < group->list->len; i++ ) {
		ImageGroupRecord* record = &g_array_index(group->list, ImageGroupRecord, i);

		result = taglist_remove(image_get_groups(record->image), group->tag);
		if ( FAIL == result ) {
			ERROR_LOG();
		}

		result = image_unref(record->image);
		if ( FAIL == result ) {
			ERROR_LOG();
		}
	}

	g_array_free(group->list, TRUE);
	g_free( (gpointer)group->name);
	g_free( (gpointer)group);
	return OK;
}


/**
 * This function adds images into image list. It is kinda special as it does not
 * use yank buffer as an input. It should be used at special occasions only,
 * like for loading image_group from image_pile.
 *
 * \param group image list into which images will be added
 * \param index position to which images will be added
 * \param data array containing list of image pointers
 * \param size number of pointers in array
 * \return OK on success
 */
extern RCode group_insert(ImageGroup* group, gint index, Image* data[], guint size) {
	unsigned int source_index = 0;
	unsigned int target_index = 0;
	ImageGroupRecord* buffer = NULL;

	ASSERT(NULL != group);
	ASSERT(NULL != group->list);

	/* check position into which images are being inserted
	 * we do not fail if index = list length ( which is one record after the
	 * last one to be able to add at the end of array */
	if ( index < 0 || group->list->len < index ) {
		ERROR_SET(ERROR.OUT_OF_RANGE);
		ERROR_NOTE("Index %d out of range [0, %d]", index, group->list->len);
		return FAIL;
	}

	// allocate internal buffer for insertions
	TRACE_MESSAGE(IGRP, TL_DEBUG, "Allocate insert buffer (size=%d)", size);
	buffer = (ImageGroupRecord*) g_try_malloc0(size*sizeof(ImageGroupRecord));
	if ( NULL == buffer ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return FAIL;
	}

	// prepare data in buffer for insertion
	TRACE_MESSAGE(IGRP, TL_DEBUG, "Preparing values into insert buffer (count=%d)",size);
	for ( source_index = 0, target_index = 0; source_index < size; source_index++ ) {
		TagList *tags = image_get_groups(data[source_index]);

		if ( FALSE == taglist_contains(tags, group->tag) ) {
			buffer[target_index].image = data[source_index];
			buffer[target_index].select = FALSE;

			image_ref(buffer[target_index].image);
			taglist_insert(tags, group->tag);
			target_index++;
		}
	}

	// compute position into which to insert and insert the data
	TRACE_MESSAGE(IGRP, TL_INFO, "Inserting records into group (name='%s', index=%d, count=%d, filtered=%d)",
		group->name, index, target_index, (size - target_index)
	);
	group->list = g_array_insert_vals(group->list, index, (gpointer)buffer, target_index);

	// deallocate insert buffer
	g_free( (gpointer)buffer );

	return OK;
}


/**
 * This function yanks specified number of entries from group into yank buffer.
 * If the buffer is too small it will be reallocated
 *
 * \todo Yanking to non-empty yank buffer should result in buffer overwrite (ie
 * clear and insert)
 *
 * \param group image group from which images will be yanked
 * \param range index range which to yank
 * \return OK on success
 */
RCode group_yank_range(ImageGroup* group, Range range) {
	guint size = 0;
	guint i = 0;
	ASSERT(NULL != group);
	ASSERT(NULL != group->list);

	// check if yank buffer is clear and clear it if not
	if ( 0 != group_buffer_size() ) {
		if ( FAIL == group_buffer_clear() ) {
			ERROR_NOTE("Unable to clear internal yank buffer.");
			return FAIL;
		}
	}

	// check if index is in range
	if ( FAIL == range_check_bounds(range, 0, group->list->len - 1) ) {
		ERROR_SET(ERROR.OUT_OF_RANGE);
		ERROR_NOTE("Supplied range is out of bounds (group='%s',range=<%d,%d>,min=0,max=%d)",
			group->name, range.start,range.end,group->list->len-1);
		return FAIL;
	}
	size = range.end - range.start + 1;

	// realloc yank buffer if needed
	if ( group_buffer_max_size() < size ) {
		if ( OK != group_buffer_resize(size) ) {
			ERROR_NOTE("Image group yank buffer reallocation failed");
			return FAIL;
		}
	}

	/* iterate over list until {size} records are yanked or list bounds are
	 * reached */
	TRACE_MESSAGE(IGRP, TL_INFO,
			"Yanking records from group (name='%s',range=<%d,%d>)",
			group->name, range.start, range.end
		);
	for ( i = range.start; i <= range.end; i++) {
		ImageGroupRecord record  = g_array_index(group->list, ImageGroupRecord, i );

		// yank image from record into the buffer
		group_buffer_insert( record.image );
	}

	return OK;
}

/**
 * The function deletes one image from the image group array using supplied
 * index.
 *
 * \param group image group from which image will be removed
 * \param index index of the image to remove
 * \return OK on success
 */
RCode group_delete_index(ImageGroup* group, guint index) {
	ImageGroupRecord record;

	ASSERT(NULL != group);
	ASSERT(NULL != group->list);

	TRACE_MESSAGE(IGRP, TL_INFO, "Removing index (group='%s',index=%d)", group->name, index);

	if ( 0 > index || group->list->len - 1 < index ) {
		ERROR_SET(ERROR.OUT_OF_RANGE);
		ERROR_NOTE("Supplied index is out of bounds (group='%s',index=%d,min=0,max=%d)",
				group->name, index, group->list->len - 1
			);
		return FAIL;
	}

	// unreference images that will be removed
	record = g_array_index(group->list, ImageGroupRecord, index);
	/** \todo Check what to do with the call return code */
	image_unref(record.image);

	group->list = g_array_remove_range(group->list, index, 1);
	return OK;
}

/**
 * This function deletes number of entries from list specified by range
 *
 * \param group image group from which we will remove records
 * \param range range specifying entries
 * \return OK on success
 */
RCode group_delete_range(ImageGroup* group, Range range) {
	guint	i = 0;

	ASSERT(NULL != group);
	ASSERT(NULL != group->list);

	TRACE_MESSAGE(IGRP, TL_INFO,
			"Removing range (group='%s',range=<%d,%d>)", group->name, range.start, range.end
		);

	// check if index is in range
	if ( FAIL == range_check_bounds(range, 0, group->list->len - 1) ) {
		ERROR_SET(ERROR.OUT_OF_RANGE);
		ERROR_NOTE("Supplied range is out of bounds (group='%s',range=<%d,%d>,min=0,max=%d)",
				group->name, range.start,range.end,group->list->len-1
			);
		return FAIL;
	}

	// unreference images that will be removed
	for (i = range.start; i <= range.end; i++) {
		ImageGroupRecord record = g_array_index(group->list, ImageGroupRecord, i);

		/** \todo Check what to do with the call return code */
		image_unref(record.image);
	}

	group->list = g_array_remove_range(group->list, range.start, range.end - range.start + 1);

	return OK;
}

/**
 * This function is wrapper to group_insert and pastes into group list from yank
 * buffer. If yank buffer is empty, nothing is paste and function reports
 * success
 *
 * \param group image group to which yank buffer will be pasted
 * \param index index to which images will be inserted
 * \return OK on success and empty yank buffer
 */
RCode group_yank_paste(ImageGroup* group, gint index) {
	RCode result = OK;

	// if there is nothing to paste, report success
	if ( group_buffer_size() == 0 ) {
		return result;
	}

	// otherwise paste from yank buffer and report possible errors
	result = group_insert(group, index, group_buffer_get_pointer(), group_buffer_size());
	if ( FAIL == result ) {
		ERROR_NOTE("Pasting from yank failed (group=%d,index=%d,size=%d)", group->name, index, group_buffer_size());
	}

	return result;
}

/**
 * This function executes action function on one record in image group specified
 * by its index.
 *
 * \param group pointer to image list
 * \param index index if image to be modified
 * \param action pointer to action function which will be used for modification
 * \return OK on success
 */
RCode group_execute_index(ImageGroup* group, gint index, ActionFn* action) {
	ImageGroupRecord*	record = NULL;

	ASSERT(NULL != group);
	ASSERT(NULL != group->list);
	ASSERT(NULL != action );

	TRACE_MESSAGE(IGRP, TL_INFO,
			"Executing action (name='%s',index=%d,action=0x%x)", group->name, index, action
		);

	// check if index is in bounds
	if ( index < 0 || group->list->len <= index) {
		ERROR_SET(ERROR.OUT_OF_RANGE);
		return FAIL;
	}

	// get image record from the list
	record = &g_array_index(group->list, ImageGroupRecord, index);

	return action(record);
}


/**
 * This function executes action function on the supplied range.
 *
 * \param group pointer to image list
 * \param range index range on which to execute action
 * \param action pointer to action function which will be used for modification
 * \return OK if modification was successfull
 */
RCode group_execute_range(ImageGroup* group, Range range, ActionFn* action) {
	guint	i = 0;
	RCode	result = OK;
	ASSERT(NULL != group);
	ASSERT(NULL != group->list);
	TRACE_MESSAGE(IGRP, TL_INFO, "Executing action on range (name='%s',range=<%d,%d>)",
		group->name, range.start, range.end);

	// check if first index is in range
	if ( FAIL == range_check_bounds( range, 0, group->list->len - 1) ) {
		ERROR_SET(ERROR.OUT_OF_RANGE);
		ERROR_NOTE("Supplied range is out of bounds (group='%s',range=<%d,%d>,min=0,max=%d)",
			group->name, range.start,range.end,group->list->len-1);
		return FAIL;
	}

	for ( i = range.start; i <= range.end; i++) {
		ImageGroupRecord* record = &g_array_index(group->list, ImageGroupRecord, i);

		// execute action on record
		TRACE_MESSAGE(IGRP, TL_INFO, "Executing action (name='%s',index=%d,action=0x%x)",
			group->name, i, action);
		if ( FAIL == action(record) ) {
			ERROR_SET(ERROR.ACTION_FN_FAILED);
			ERROR_NOTE("Action failed on (group='%s',index=%d),action=0x%x", group->name, i, action);
			result = FAIL;
		}
	}
	return result;

}

/**
 * This function executes action function on all record which have select flag
 * set to TRUE.
 *
 * \param group pointer to image list
 * \param action pointer to action function which will be used for modification
 * \return OK on success
 */
RCode group_execute_select(ImageGroup* group, ActionFn* action) {
	RCode result = OK;
	guint	i = 0;
	ASSERT(NULL != group);
	ASSERT(NULL != group->list);
	TRACE_MESSAGE(IGRP, TL_INFO, "Executing action on selected (name='%s')", group->name);

	while ( i < group->list->len  ) {
		ImageGroupRecord* record = &g_array_index(group->list, ImageGroupRecord, i);

		// check if record is selected
		if ( FALSE == record->select ) {
			i++;
			continue;
		}

		// execute action on record
		TRACE_MESSAGE(IGRP, TL_INFO, "Executing action on (name='%s',index=%d)", group->name,i);
		if ( FAIL == action(record) ) {
			ERROR_SET(ERROR.ACTION_FN_FAILED);
			ERROR_NOTE("Action failed on (group='%s',index=%d)", group->name, i);
			result = FAIL;
		}

		i++;
	}
	return result;
}

/**
 * Sort images in group according to filename.
 *
 * \note Currently, we sort only using filename. Later on, we may decide to add
 * more types of sorts (size, timestamp, dimensions).
 *
 * \param group pointer to image list
 * \return OK on success
 */
RCode group_sort(ImageGroup* group) {
	ASSERT( NULL != group );

	g_array_sort(group->list, cmp_filename);
	return OK;
}


/**
 * This function returns number of images in image list
 *
 * \param group pointer to image list
 * \return number of images in list
 */
guint group_get_size(ImageGroup* group) {
	ASSERT(NULL != group);
	ASSERT(NULL != group->list );
	return group->list->len;
}

/**
 * This function returns pointer to the image on {index} position in image list
 *
 * \param group pointer to image list
 * \param index position of image in list
 * \return Image pointer on success, NULL otherwise
 */
Image* group_get_image(ImageGroup* group, gint index) {
	ASSERT(NULL != group);
	ASSERT(NULL != group->list);

	if ( 0 <= index && index <= group->list->len ) {
		ImageGroupRecord record = g_array_index(group->list, ImageGroupRecord, index);
		return record.image;
	} else {
		ERROR_SET(ERROR.OUT_OF_RANGE);
		ERROR_NOTE("Index %d is out of range <%d,%d>", index, 0, group->list->len - 1 );
		return NULL;
	}
}

/**
 * This function executes user specified condition function on the specified
 * index.
 *
 * Since the function may fail from different reasons, we do not return boolean
 * value to indicate condition function result but store it to the destination
 * pointer by supplied pointer.
 *
 * \todo Find out better way for returning error
 *
 * \param group pointer to image list
 * \param index position of image in list
 * \param result pointer to storage for condition function result
 * \param test pointer to condition function
 * \param data pointer to data to be supplied into condition function
 * \return OK if condition function executed without fail
 */
RCode group_get_condition(ImageGroup* group, gint index, gboolean* result, ConditionFn* test, gpointer data) {
	ASSERT(NULL != group);
	ASSERT(NULL != group->list);
	TRACE_MESSAGE(IGRP, TL_INFO, "Checking condition (name='%s',index=%d,condition=0x%x)",
		group->name, index, test);

	if ( index < 0 || group->list->len <= index ) {
		ERROR_SET(ERROR.OUT_OF_RANGE);
		return FAIL;
	}

	ImageGroupRecord record = g_array_index(group->list, ImageGroupRecord, index);
	return test(&record,result, data);
}
