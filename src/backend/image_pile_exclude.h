/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file image_pile_exclude.h
 * \brief Image pile helper functions for pattern matching
 */
#ifndef _CONFIG_IMAGE_PILE_EXCLUDE_H_
#define _CONFIG_IMAGE_PILE_EXCLUDE_H_

#include <glib.h>

/** \brief Create GLIB pattern spec structures from string list. */
extern GPatternSpec** pile_exclude_create(gchar **excludeList);

/** \brief Deallocate GLIB pattern spec structures. */
extern void pile_exclude_destroy(GPatternSpec **patternList);

/** \brief Check if string match to some pattern in list. */
extern gboolean pile_exclude_match(GPatternSpec **patternList, const gchar *string);

#endif

