/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>
#include "src/backend/image_group_record.h"

/**
 * This function checks if range is within preset bounds. Invalid range is range
 * with {range.end} < {range.start}. Testing with invalid range will fail every
 * time.
 *
 * The range check does not return an error, since the caller can handle the
 * negative result differently each time (sometimes fail, sometimes update the
 * range bounds).
 *
 * \param range structure containing range setting
 * \param min minimal bound of range
 * \param max maximal bound of range
 * \return TRUE if the range is inside of the bounds
 */
gboolean range_check_bounds(Range range, gint min, gint max) {
	// check if range is invalid
	if ( range.end < range.start ) {
		TRACE_MESSAGE(GREC, TL_MAJOR, "Invalid range (range=<%d,%d>)", range.start, range.end);
		return FAIL;
	}

	// check if range is within bounds
	if ( range.start < min || max < range.end  ) {
		return FAIL;
	}

	return OK;
}


/**
 * Toggle select flag to TRUE
 *
 * \param record record which will be modified
 * \return OK
 */
RCode action_select_set(ImageGroupRecord *record) {
	record->select = TRUE;
	return OK;
}

/**
 * Toggle select flag to FALSE
 *
 * \param record record which will be modified
 * \return OK
 */
RCode action_select_unset(ImageGroupRecord *record) {
	record->select = FALSE;
	return OK;
}

/**
 * Toggle select flag from TRUE to FALSE and from FALSE to TRUE
 *
 * \param record record which will be modified
 * \return OK
 */
RCode action_select_toggle(ImageGroupRecord *record) {
	if ( TRUE == record->select ) {
		record->select = FALSE;
	} else {
		record->select = TRUE;
	}
	return OK;
}


/**
 * This function checks if record is marked as select
 *
 * \param record record which will be checked
 * \param result pointer to place where result will be stored
 * \param data data which may be need for check
 * \return TRUE if record has 'select' flag on true
 */
RCode condition_select(ImageGroupRecord *record, gboolean* result, gpointer data) {
	*result = record->select;
	return OK;
}

/** \brief Comparator function for sorting by filename. */
gint cmp_filename(gconstpointer a, gconstpointer b) {
	ImageGroupRecord* rec1 = (ImageGroupRecord*) a;
	ImageGroupRecord* rec2 = (ImageGroupRecord*) b;
	ASSERT( NULL != rec1 );
	ASSERT( NULL != rec2 );

	return strcmp(rec1->image->name, rec2->image->name);
}

