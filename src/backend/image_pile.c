/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <glib/gstdio.h>

#include "src/config/config.h"
#include "src/backend/image_pile.h"
#include "src/backend/image_pile_exclude.h"

/**
 * Module-wise global variable - head of double linked list in which all Image
 * records are stored
 */
GList*	pile_list_head = NULL;


/**
 * Function adds image into image pile and stores some basic information
 * about it.
 *
 * \param filename path to the file
 * \param size size of the file in bytes
 * \param time time of the last modification in epoch
 * \return OK
 */
RCode pile_load_file(const gchar* filename, guint size, time_t time) {
	Image*	tmp_image = NULL;

	ASSERT(NULL != filename);

	if ( 0 != g_access(filename, R_OK) ) {
		TRACE_MESSAGE(PILE, TL_MAJOR, "File '%s' is not readable", filename);
		return FAIL;
	}

	if ( NULL == ( tmp_image = image_create(filename)) ) {
		/** \todo error logging */
		return FAIL;
	}

	tmp_image->size = size;
	tmp_image->timestamp = time;

	pile_list_head = g_list_prepend(pile_list_head, (gpointer)tmp_image);

	return OK;
}


/**
 * Function checks depth of directory and depending on max depth it will go
 * through its content and will try to add the files into image pile and recurse
 * into next directories.
 *
 * The {pathBuffer} variable contains pointer to memory where the path to the
 * directory is and which can be used for computing the path of entries found in
 * the directory.
 *
 * \param pathBuffer pointer to temporaty buffer for path joining
 * \param exclude null terminated list of excluded patterns
 * \param maxDepth maximum depth to recurse in
 * \param depth how deep we are from first entry
 * \return OK on success
 */
RCode pile_load_directory(gchar* pathBuffer, GPatternSpec** exclude, gint maxDepth, guint depth) {
	GError*			error = NULL;
	GDir*			tmp_dir = NULL;
	const gchar*	entry_name = NULL;
	struct stat 	stat_buffer;
	guint			path_length = 0;

	ASSERT(NULL != pathBuffer);
	path_length = strlen(pathBuffer);

	// Do not process the directory if recurse depth is greated that configured
	// or if directory is not accessible
	if ( depth > maxDepth ) {
		TRACE_MESSAGE(PILE, TL_INFO, "Recursion depth exceeded maxium in '%s'", pathBuffer);
		return OK;
	}

	if ( 0 != g_access(pathBuffer, R_OK | X_OK) ) {
		TRACE_MESSAGE(PILE, TL_MAJOR, "Directory '%s' is not accessible", pathBuffer);
		return FAIL;
	}

	if ( NULL == ( tmp_dir = g_dir_open(pathBuffer, 0, &error)) ) {
		TRACE_MESSAGE(PILE, TL_MAJOR, "Opening directory '%s' failed", pathBuffer);
		return FAIL;
	}

	while ( NULL != (entry_name = g_dir_read_name(tmp_dir)) ) {
		//  First check if we have enough space in path buffer to complete the
		// path and if we can actually stat the entry.
		if ( path_length + 1 + strlen(entry_name) + 1 > PILE_BUFFER_SIZE) {
			TRACE_MESSAGE(PILE, TL_MAJOR, "Path '%s/%s' exceeds path buffer size %d)",
						pathBuffer, entry_name, PILE_BUFFER_SIZE);
			continue;
		} else {
			pathBuffer[path_length] = '/';
			strcpy(pathBuffer + path_length + 1, entry_name);
		}

		// check if entry match exclude pattern, the function can check for
		// empty pattern list
		if ( TRUE == pile_exclude_match(exclude, entry_name) ) {
			TRACE_MESSAGE(PILE, TL_MAJOR, "Entry '%s' matched exclude pattern", entry_name);
			continue;
		}

		if ( 0 != g_stat(pathBuffer, &stat_buffer) ) {
			TRACE_MESSAGE(PILE, TL_MAJOR, "Cannot stat entry '%s'", pathBuffer);
			continue;
		}

		/*
		 * Depending on entry type either descend into directory or add file.
		 */
		if ( 0 != S_ISDIR(stat_buffer.st_mode) ) {
			TRACE_MESSAGE(PILE, TL_DEBUG, "Descending to directory '%s'", pathBuffer);
			pile_load_directory(pathBuffer, exclude, maxDepth, depth + 1);

		} else if ( 0 != S_ISREG(stat_buffer.st_mode) || 0 != S_ISLNK(stat_buffer.st_mode) ) {
			TRACE_MESSAGE(PILE, TL_INFO, "Adding file '%s'", pathBuffer);
			pile_load_file(pathBuffer, stat_buffer.st_size, stat_buffer.st_mtime);

		} else {
			TRACE_MESSAGE(PILE, TL_MAJOR, "Entry '%s' is not file, link or directory", pathBuffer);
		}
	}

	g_dir_close(tmp_dir);
	pathBuffer[path_length] = '\0';
	return OK;
}


/**
 * Function recieves list of file and directory names in argv like fashion and
 * goes through them according to the setting in global configuration and loads
 * files into image pile
 *
 * \param argc number of arguments in {argv} array
 * \param argv array of directory and file names
 * \return OK on success
 */
RCode pile_load_images(gint argc, gchar** argv) {
	const ConfigStorage *conf;
	GPatternSpec**	exclude_patterns = NULL;
	RCode result;
	struct stat stat_buffer;
	int i = 0, j = 0;

	// get configuration and check it does exist
	conf = config_get();
	if ( NULL == conf ) {
		return FAIL;
	}

	// buffer used for joining directory and file names into path when doing
	// recursive file search
	gchar path_buffer[PILE_BUFFER_SIZE];
	memset(path_buffer, 0, sizeof(path_buffer));

	// check if there are some exclude patterns and precompile them
	if ( NULL != conf->exclude ) {
		TRACE_MESSAGE(PILE, TL_DEBUG, "Compiling exclude patterns");
		exclude_patterns = pile_exclude_create(conf->exclude);

		if ( error_is_error() ) {
			ERROR_NOTE("Memory allocation for exclude patters failed");
			return FAIL;
		}
	}

	// iterate through input file list
	TRACE_MESSAGE(PILE, TL_DEBUG, "Processing %d arguments", argc);
	for ( i = 0; i < argc; i++ ) {

		// check if path + entry_name exceeds path buffer size or if it is not
		// possible to that the entry
		if ( strlen(argv[i]) + 1 > PILE_BUFFER_SIZE ) {
			TRACE_MESSAGE(PILE, TL_MAJOR, "Path '%s' exceeds path buffer size %d)",
						argv[i], PILE_BUFFER_SIZE);
			continue;

		} else {
			strcpy(path_buffer, argv[i]);
		}

		// check if entry match exclude pattern, the function can check for
		// empty pattern list
		if ( TRUE == pile_exclude_match(exclude_patterns, argv[i]) ) {
			TRACE_MESSAGE(PILE, TL_MAJOR, "Entry '%s' matched exclude pattern", argv[i]);
			continue;
		}

		// check if it is possible to stat the file
		if ( 0 != g_stat(path_buffer, &stat_buffer) ) {
			TRACE_MESSAGE(PILE, TL_MAJOR, "Cannot stat entry '%s'", path_buffer);
			continue;
		}

		// depending on entry type descend to directory or add a file
		if ( 0 != S_ISDIR(stat_buffer.st_mode) ) {
			TRACE_MESSAGE(PILE, TL_DEBUG, "Descending to directory '%s'", path_buffer);
			result = pile_load_directory(path_buffer, exclude_patterns, conf->recurse, 1);

		} else if ( 0 != S_ISREG(stat_buffer.st_mode) || 0 != S_ISLNK(stat_buffer.st_mode) ) {
			TRACE_MESSAGE(PILE, TL_INFO, "Adding file '%s' to image pile", path_buffer);
			result = pile_load_file(path_buffer, stat_buffer.st_size, stat_buffer.st_mtime);

		} else {
			TRACE_MESSAGE(PILE, TL_INFO, "Entry '%s' is not file, link or directory", path_buffer);
			result = FAIL;
		}

		if ( FAIL == result ) {
			ERROR_LOG();
		}
	}

	pile_list_head = g_list_reverse(pile_list_head);

	// deallocate exclude patterns
	pile_exclude_destroy(exclude_patterns);

	return OK;
}


/**
 * Compare function for Image lookup in pile. This one compares whole path to
 * the file with supplied argument and returns 0 when they are equal.
 *
 * \param image gpointer to Image
 * \param name gpointer to gchar array containing filename
 * \return same as strcmp() function results
 */
gint pile_cmp_by_path(gpointer image, gpointer name) {
	ASSERT(NULL != image);
	ASSERT(NULL != name);

	return g_strcmp0( ((Image*)image)->name, (gchar*)name);
}

/**
 * The function will deallocate all Image structures which are referenced only
 * from pile (ie having ref == 1 ) in pile and remove them from list.
 *
 * In case some Image structure is referenced not only from pile, function will
 * terminate prematurely and return error. Invocation can be repeated when all
 * references to the structure are removed.
 *
 * \return OK on success
 */
RCode pile_destroy() {
	GList*	iterator = NULL;

	if ( NULL == pile_list_head ) {
		// no need to destroy empty pile
		return OK;
	}

	iterator = g_list_first(pile_list_head);
	while ( NULL !=  iterator  ) {
		TRACE_MESSAGE(PILE, TL_DEBUG, "Deallocating %s", ((Image*)iterator->data)->name);
		if ( FAIL == image_unref((Image*)iterator->data) ) {
			ERROR_NOTE("Unable to deallocate image structure");

			// attempt to leave image list in consistent state after failing to
			// free Image structure. Remove all elements of list which do not
			// point to Image structure anymore (ie previously freed)
			pile_list_head = g_list_remove_all(pile_list_head, NULL);
			return FAIL;
		}

		iterator->data = NULL;
		iterator = g_list_next(iterator);
	}

	TRACE_MESSAGE(PILE, TL_DEBUG, "Freeing image pile list");
	g_list_free(pile_list_head);

	// This needs to be here since freed list elements are returned to glib
	// allocator but remain valid! So it will look as if we have one more
	// element in list.
	pile_list_head = NULL;
	return OK;
}

/**
 * Function search through image pile and returns pointer to Image structure
 * containing information about file indicated by {filename} variable.
 *
 * \param filename file to look up
 * \return point to Image on success, NULL on error
 */
Image* pile_get_image(gchar* filename) {
	GList* found = NULL;

	ASSERT( NULL != pile_list_head);

	found = g_list_find_custom(pile_list_head, (gpointer)filename, (GCompareFunc)pile_cmp_by_path);
	if ( NULL == found ) {
		/** \todo set error not found */
		return NULL;

	} else {
		return (Image*)found->data;
	}
}

/**
 * Function returns number of elements in image pile
 *
 * \return number of elements in image pile
 */
guint pile_get_size() {
	return g_list_length(pile_list_head);
}

/**
 * Return pointer to the list with all Image* pointers that are in the image
 * pile. The function is used for setup of the first image group.
 *
 * The list is dynamically allocated in the function and caller is responsible
 * for its deallocation. The images returned in the list are not referenced by
 * the list. So the caller has to reference them manually if it wants to keep
 * those pointers.
 *
 * The size of the list must be retrieved using different function
 *
 * \return pointer  to the Image* list, NULL on error or empty pile
 */
Image** pile_get_image_list() {
	guint size = 0;
	guint i = 0;
	Image** result = NULL;
	GList* iterator = NULL;

	size = pile_get_size();
	if ( 0 == size ) {
		return NULL;
	}

	// allocate memory for the list
	if ( NULL == ( result = (Image**) g_try_malloc0(size * sizeof(Image*))) ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return result;
	}

	// iterate over the pile list and copy the pointers to result list
	iterator = g_list_first(pile_list_head);
	while ( NULL !=  iterator  ) {
		result[i++] = iterator->data;
		iterator = g_list_next(iterator);
	}

	return result;
}
