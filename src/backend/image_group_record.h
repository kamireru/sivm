/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file image_group_record.h
 * \brief Declare ImageGroupRecord and function which works with it
 *
 * This file contains declaration of one record in image group.
 *
 * This file also contains  number of functions which can do some action on the
 * record or check validity of some condition in record.
 *
 * (This is reason for separate file for simple struct. The action and checking
 * functions are not directly related to the image group and would clutter source
 * files there)
 */
#ifndef BACKEND_IMAGE_LIST_REC_H
#define BACKEND_IMAGE_LIST_REC_H

#include <glib.h>
#include "src/common/common_def.h"
#include "src/backend/image.h"


/**
 * This structure holds information about range in which some operation will be
 * done. It is used in ImageGroup for yanking and filtering.
 *
 * Due to specifics of record list in image group, range is rather <index,count>
 * than <index,index> to account processing or not processing hidden images.
 *
 * \todo Refactor to use <index,index> notation consistently. Rename the member
 * variables to be more readable.
 */
typedef struct {
		/** \brief Lower range bound */
		gint start;

		/** \brief Higher range bound */
		gint end;

	} Range;

/** \brief Check that range record is withing bounds. */
extern gboolean range_check_bounds(Range range, gint min, gint max);

/**
 * \brief Structure for storing information about image in list.
 *
 * The structure holds pointer to the image it stores and few additional, list
 * specific properties (for example flags).
 */
typedef struct {
		/** \brief Stores selection status of image. */
		gboolean	select;

		/** \brief Stores pointer to image. */
		Image*		image;

	} ImageGroupRecord;

/**
 * The typedef specifying common declaration for all action function.
 *
 * \param ImageGroupRecord record which will be altered by function
 * \return OK if alternation is succesfull, FAIL otherwise
 */
typedef   RCode (ActionFn)(ImageGroupRecord*);

/**
 * The typedef specifying common declaration for all condition functions
 *
 * \param ImageGroupRecord record which will be used for evaluating condition
 * \param gboolean sets if condition results should be inverted
 * \param gpointer optional data needed for evaluation
 * \return TRUE if condition is fullfilled
 */
typedef RCode (ConditionFn)(ImageGroupRecord*, gboolean*, gpointer);



/** \brief Action fuction to set record as selected. */
extern ActionFn action_select_set;

/** \brief Action fuction to set record as unselected. */
extern ActionFn action_select_unset;

/** \brief Action fuction to toggle selected setting of the record. */
extern ActionFn action_select_toggle;


/** \brief Condition function returning true if record is selected. */
extern ConditionFn condition_select;

/** \brief Comparator function for sorting by filename. */
extern gint cmp_filename(gconstpointer a, gconstpointer b);

#endif
