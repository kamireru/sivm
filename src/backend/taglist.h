/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file taglist.h
 * \brief Header file with API for TagList structure.
 *
 * The TagList structure provides storage for list of tags associated with some
 * object. It allows tag addition, removal and querying.
 *
 * A tag in this context is a character from range [a-zA-Z0-9].
 */
#ifndef _BACKEND_TAGLIST_H_
#define _BACKEND_TAGLIST_H_

#include <glib.h>
#include "src/common/common_def.h"


/**
 * Macro defininig minimum increment in the taglist size.
 */
#define TAGLIST_MIN_INCREMENT 4

/**
 * A structure for holding taglist information.
 */
typedef struct {
		/** \brief Storage array for tags. */
		gchar* list;

		/** \brief Maximum number of tags in the list. */
		guint max_size;

		/** \brief Number of tags in the list. */
		guint size;

	} TagList;


/** \brief Create new taglist structure. */
extern TagList* taglist_create();

/** \brief Destroy taglist structure. */
extern RCode taglist_destroy(TagList *taglist);

/** \brief Insert tag into the taglist. */
extern RCode taglist_insert(TagList *taglist, gchar tag);

/** \brief Remove tag from the taglist. */
extern RCode taglist_remove(TagList *taglist, gchar tag);

/** \brief Check if tag is in the taglist. */
extern gboolean taglist_contains(TagList *taglist, gchar tag);

/** \brief Get number of tags in the taglist. */
extern guint taglist_get_count(TagList* taglist);

// Utility functions

/** \brief Check if character can be used as tag. */
extern gboolean taglist_is_tag(gchar tag);

#endif
