/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file group_list.h
 * \brief Declare functions for managing list of image groups
 *
 * The group list is something similar to image pile. There can be only one
 * group list in program and thus we do not expose underlaying structures to the
 * user (ie other modules).
 *
 * The module provides only simple methods for managing, for example it won't
 * check if you are trying to add two groups with same name/tag or one group
 * several times. But it provides methods for checking for such situations.
 *
 * The group list manages addition, deletion and modification of image groups.
 */
#ifndef BACKEND_GROUP_LIST_H
#define BACKEND_GROUP_LIST_H

#include <glib.h>
#include "src/common/common_def.h"
#include "src/backend/image_group.h"
#include "src/backend/image_group_record.h"

/** \brief Initialize instance of group list. */
extern RCode glist_create();

/** \brief Destroy instance of group list. */
extern RCode glist_destroy();

/** \brief Return number of image groups in list. */
extern gint glist_get_size();


/** \brief Insert image group into the group list. */
extern RCode glist_group_insert(ImageGroup* group);


/** \brief Return image group index by group tag. */
extern gint glist_index_by_tag(gchar tag);

/** \brief Return image group index by group name. */
extern gint glist_index_by_name(gchar* name);


/** \brief Remove image group from the group list by index. */
extern RCode glist_group_remove_by_index(guint index);

/** \brief Remove image group from the group list by tag. */
extern RCode glist_group_remove_by_tag(gchar tag);

/** \brief Remove image group from the group list by group name. */
extern RCode glist_group_remove_by_name(gchar* name);


/** \brief Return image group by index in the group list. */
extern ImageGroup* glist_get_by_index(guint index);

/** \brief Return image group by group tag. */
extern ImageGroup* glist_get_by_tag(gchar tag);

/** \brief Return image group by group name. */
extern ImageGroup* glist_get_by_name(gchar* name);

#endif
