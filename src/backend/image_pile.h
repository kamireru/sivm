/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file image_pile.h
 * \brief API for loading and storing image information
 *
 * The image pile functions as an image storage. It stores record about all
 * images which were loaded into program and gives pointer to that record to
 * anybody who asks.
 *
 * But it does not change image order or anything like that no matter what is
 * done with images. It only removes image when  when nobody references it.
 */
#ifndef CONFIG_PILE_H
#define CONFIG_PILE_H

#include <glib.h>
#include "src/common/common_def.h"
#include "src/backend/image.h"

#ifndef PILE_BUFFER_SIZE
	// Macro defines size of the buffer for joining directory and file names
	// into path when recursively traversing the filesystem structure
	#define PILE_BUFFER_SIZE 4096
#endif

/** \brief Load several images from files or directories indicated in argv. */
extern RCode pile_load_images(gint argc, gchar** argv);

/** \brief Deallocate all content from pile */
extern RCode pile_destroy();


/** \brief Return pointer to image structure containing file. */
extern Image* pile_get_image(gchar* filename);

/** \brief Return number of elements in pile. */
extern guint pile_get_size();

/** \brief Return Image* list with pile elements. */
extern Image** pile_get_image_list();

#endif
