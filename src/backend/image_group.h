/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file image_group.h
 *
 * The image group is structure grouping information about order, selection and
 * hidden status of selected images from image pile.  The SIVM does not display
 * images from image pile directly, but displays images from one image group.
 *
 * There can be more image group defined in SIVM, see group list.
 */
#ifndef BACKEND_IMAGE_GROUP_H
#define BACKEND_IMAGE_GROUP_H

#include <glib.h>
#include "src/common/common_def.h"
#include "src/backend/image.h"
#include "src/backend/image_group_record.h"


/**
 * The structure contains information about image group. The image group name is used
 * for group identification and group tag is used for fast reference of image
 * group.
 */
typedef struct {
		/** \brief Long identification of ImageGroup instance*/
		gchar*	name;

		/** \brief Short identification of ImageGroup instance in commands */
		gchar	tag;

		/** \brief Data storage */
		GArray*	list;
	} ImageGroup;


/** \brief Create image group structure. */
extern ImageGroup* group_create(gchar* groupName, gchar groupTag);

/** \brief Destroy image group structure. */
extern RCode group_destroy(ImageGroup* group);


// image group content management functions (cut, paste, yank, ...)

/** \brief Insert several images into the group at position. */
extern RCode group_insert(ImageGroup* group, gint index, Image* data[], guint size);

/** \brief Paste images from buffer to the image group at position. */
extern RCode group_yank_paste(ImageGroup* group, gint index);

/** \brief Copy images from image group to the buffer. */
extern RCode group_yank_range(ImageGroup* group, Range range);

/** \brief Delete image from image group on index. */
extern RCode group_delete_index(ImageGroup* group, guint index);

/** \brief Delete image range from image group. */
extern RCode group_delete_range(ImageGroup* group, Range range);


// image group content filtering and action execution functions

/** \brief Execute action function over the image on index. */
extern RCode group_execute_index(ImageGroup* group, gint index, ActionFn* action);

/** \brief Execute action function over the images in specified range. */
extern RCode group_execute_range(ImageGroup* group, Range range, ActionFn* action);

/**
 * \brief Execute action function over the images that are selected.
 * \todo Refactor function to use ConditionFntion for deciding on which record
 * to perform action.
 */
extern RCode group_execute_select(ImageGroup* group, ActionFn* action);

/** \brief Sort images in group according to filename. */
extern RCode group_sort(ImageGroup* group);


// image group query and setup functions

/** \brief Get size of the image group. */
extern guint group_get_size(ImageGroup* group);

/** \brief Get pointer to Image pointer at position. */
extern Image* group_get_image(ImageGroup* group, gint index);

/** \brief Execute condition function over index and store result in variable. */
extern RCode group_get_condition(ImageGroup* group, gint index, gboolean* result, ConditionFn* test, gpointer data);

#endif
