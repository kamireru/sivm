/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "src/backend/group_list.h"

/**
 * Pointer to the list of image groups. This is module-wise global variable, it
 * should not be accessed from module outside.
 */
GArray * group_list;


/**
 * Prepare array for storing pointers to created ImageGroups. This function can
 * be called only once - if called twice in succession, it will end with
 * failure.
 *
 * The only situation when it can be called twice is when the calls are
 * interleaved with calls to glist_destroy().
 *
 * \return OK on success
 */
RCode glist_create() {

	if ( NULL != group_list) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Group list is already created");
		return FAIL;
	}

	group_list = g_array_new(FALSE, FALSE, sizeof(ImageGroup*) );
	if ( NULL == group_list ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		ERROR_NOTE("GroupList memory allocation failed");
		return FAIL;
	} else {
		return OK;
	}
}

/**
 * Function deallocates all image groups in list and then deallocates the list
 * itself. Stored pointers won't be freed it need to be done manually.
 *
 * \return OK on success
 */
RCode glist_destroy() {
	if ( NULL == group_list ) {
		return OK;
	}

	g_array_free(group_list, TRUE);
	group_list = NULL;
	return OK;
}

/**
 * Function returns number of image groups in group list.
 *
 * \return number of elements in list or -1
 */
gint glist_get_size() {
	if ( NULL == group_list ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("The GroupList is not allocated");
		return -1;
	} else {
		return group_list->len;
	}
}


/**
 * Function adds ImageGroup pointer to the GroupList. The pointer is added to
 * the end of the array, so if there is a need to have it sorted, it must be
 * done via other function.
 *
 * \param imageGroup pointer to store in GroupList
 * \return OK on success
 */
RCode glist_group_insert(ImageGroup * imageGroup) {
	ASSERT( NULL != imageGroup);

	if ( NULL == group_list ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("The GroupList is not allocated");
		return FAIL;
	}

	group_list = g_array_append_val(group_list, imageGroup);
	return OK;
}


/**
 * The function returns index of the image group described with tag character or
 * invalid index if the group with tag is not present in the list.
 *
 * \param tag tag of searched ImageGroup
 * \return valid index or -1 if image group is not found
 */
gint glist_index_by_tag(gchar tag) {
	ImageGroup* tmp;
	guint i = 0;

	if ( NULL == group_list ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("The GroupList is not allocated");
		return -1;
	}

	for ( i = 0; i < group_list->len; i++) {
		tmp = g_array_index(group_list, ImageGroup*, i);

		if ( NULL != tmp && tag == tmp->tag ) {
			return i;
		}
	}

	return -1;
}

/**
 * The function returns index of the image group described with name character or
 * invalid index if the group with name is not present in the list.
 *
 * \param name name of searched ImageGroup
 * \return valid index or -1 if image group is not found
 */
gint glist_index_by_name(gchar* name) {
	ImageGroup* tmp;
	guint i = 0;

	ASSERT( NULL != name );

	if ( NULL == group_list ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("The GroupList is not allocated");
		return -1;
	}

	for ( i = 0; i < group_list->len; i++) {
		tmp = g_array_index(group_list, ImageGroup*, i);

		if ( NULL != tmp && 0 == g_strcmp0(name, tmp->name) ) {
			return i;
		}
	}

	return -1;
}


/**
 * Function removes element on given index from list
 *
 * \param index index of element to remove
 * \return OK on success
 */
RCode glist_group_remove_by_index(guint index) {
	if ( NULL == group_list ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("The GroupList is not allocated");
		return FAIL;
	}

	if ( index < 0 || group_list->len <= index ) {
		ERROR_SET(ERROR.OUT_OF_RANGE);
		ERROR_NOTE("index=%d, range=[%d,%d]", index, 0, group_list->len-1);
		return FAIL;
	}

	group_list = g_array_remove_index_fast(group_list, index);
	return OK;
}

/**
 * Locate the ImageGroup index using its tag and the lookup function and then
 * try to remove it.
 *
 * \param tag tag of searched ImageGroup
 * \return OK on success
 */
RCode glist_group_remove_by_tag(gchar tag) {
	gint index;

	index = glist_index_by_tag(tag);
	if ( error_is_error() ) {
		ERROR_NOTE("Image group lookup by tag failed");
		return FAIL;
	}

	if ( -1 == index ) {
		// removing non-existing group alwayssucceeds
		return OK;
	}

	return glist_group_remove_by_index(index);
}

/**
 * Locate the ImageGroup index using name tag and the lookup function and then
 * try to remove it.
 *
 * \param name name of searched ImageGroup
 * \return OK on success
 */
RCode glist_group_remove_by_name(gchar* name) {
	gint index;

	index = glist_index_by_name(name);
	if ( error_is_error() ) {
		ERROR_NOTE("Image group lookup by name failed");
		return FAIL;
	}

	if ( -1 == index ) {
		// removing non-existing group alwayssucceeds
		return OK;
	}

	return glist_group_remove_by_index(index);
}

/**
 * Function checks bounds of GroupList array and if index is in bounds, it will
 * return pointer stored on index-th position.
 *
 * \param index index of ImageGroup pointer to retrieve
 * \return pointer to ImageGroup or NULL on error
 */
ImageGroup* glist_get_by_index(guint index) {
	if ( NULL == group_list ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("The GroupList is not allocated");
		return NULL;
	}

	if ( index < 0 || group_list->len <= index ) {
		ERROR_SET(ERROR.OUT_OF_RANGE);
		ERROR_NOTE("index=%d, range=[%d,%d]", index, 0, group_list->len-1);
		return NULL;
	}

	return g_array_index(group_list, ImageGroup*, index);
}

/**
 * The function does not iterate over the image group list directly, it uses
 * index lookup function and function returning ImageGroup by its index.
 *
 * \param tag tag of searched ImageGroup
 * \return pointer to ImageGroup or NULL on error or not found
 */
ImageGroup* glist_get_by_tag(gchar tag) {
	gint index;

	index = glist_index_by_tag(tag);
	if ( error_is_error() ) {
		ERROR_NOTE("Image group lookup by tag failed");
		return NULL;
	}

	if ( -1 == index ) {
		return NULL;
	}

	return glist_get_by_index(index);
}

/**
 * Function search GroupList for ImageGroup identified by name and returns
 * pointer to it.
 *
 * \param name name of searched ImageGroup
 * \return pointer to ImageGroup or NULL on error or not found
 */
ImageGroup* glist_get_by_name(gchar *name) {
	gint index;

	index = glist_index_by_name(name);
	if ( error_is_error() ) {
		ERROR_NOTE("Image group lookup by name failed");
		return NULL;
	}

	if ( -1 == index ) {
		return NULL;
	}

	return glist_get_by_index(index);
}

