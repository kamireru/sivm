/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>

#include "src/backend.h"
#include "src/backend/image_pile.h"
#include "src/backend/group_list.h"
#include "src/backend/image_group.h"

/**
 * Initialize image pile with files from command line and create group list
 * along with first image group.
 *
 * The image group contains all images from command line.
 *
 * \param argc number of arguments in image path list
 * \param argv list of image paths
 * \return OK on success
 */
RCode backend_init(gint argc, gchar** argv) {
	Image** list = NULL;
	ImageGroup* image_group = NULL;
	RCode result;

	// allocate image pile and load images into it
	result = pile_load_images(argc, argv);
	if ( FAIL == result ) {
		ERROR_NOTE("Image loading into the image pile failed");
		return result;
	}

	// allocate image group list
	result = glist_create();
	if ( FAIL == result ) {
		ERROR_NOTE("Creation of image group list failed");
		pile_destroy();
		return result;
	}

	// create one initial group and add it into the group list
	image_group = group_create("default", '*');
	if ( error_is_error() ) {
		ERROR_NOTE("Creation of default image group failed");
		pile_destroy();
		glist_destroy();
		return FAIL;
	} else {
		result = glist_group_insert(image_group);
		if ( FAIL == result ) {
			ERROR_NOTE("Addition of image group to group list failed");
		}
	}

	// insert images from pile into the image group
	list = pile_get_image_list();
	if ( error_is_error() ) {
		ERROR_NOTE("Error creating temporary image list");
		return FAIL;

	} else if ( NULL == list ) {
		TRACE_MESSAGE(STRG, TL_INFO, "Image pile is empty");
		return OK;

	} else {
		group_insert(image_group, 0, list, pile_get_size());
		group_sort(image_group);
	}

	return result;
}

/**
 * Deallocate all backend structures starting with group list and all associated
 * groups and ending with image pile.
 *
 * Here we do not try for the cleanest error handling. If there is an error,
 * just quit and don't try to deallocate the memory properly. We are interested
 * in the first error that occured.
 *
 * \return OK on success
 */
RCode backend_finish() {
	ImageGroup* image_group = NULL;
	RCode result;

	image_group = glist_get_by_name("default");
	if ( NULL != image_group ) {
		// remove the image group from list
		result = glist_group_remove_by_name("default");
		if ( FAIL == result ) {
			ERROR_NOTE("Removal of image group from list failed");
			return result;
		}

		// destroy the image group
		group_destroy(image_group);
		if ( FAIL == result ) {
			ERROR_NOTE("Deallocation of image group failed");
			return result;
		}
	}

	// destroy the image group list
	result = glist_destroy();
	if ( FAIL == result ) {
		ERROR_NOTE("Unable to destroy image group list");
		return result;
	}

	// destroy the image pile
	result = pile_destroy();
	if ( FAIL == result ) {
		ERROR_NOTE("Unable to destroy image pile");
		return result;
	}

	return result;
}
