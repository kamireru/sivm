/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <malloc.h>
#include <common/trace.h>

/**
 * Storage variable for global trace mask.
 */
TraceMask trace_global_mask = 0;

/**
 * Storage variable with severity names.
 */
static char *trace_severity_name[5] = {
		"DEBUG", "INFO", "MINOR", "MAJOR", "CRITICAL"
	};

/**
 * Array listing string representation of statuses to be used durint setup
 * string parsing.
 */
static char *trace_action_name[2] = {
		"ENABLE", "DISABLE"
	};

/**
 * Storage variable for list of initialized trace area pointers.
 */
static TraceArea* trace_area_list[CHAR_BIT*sizeof(TraceMask)];

/**
 * Storage variable for holding number of trace area pointers already put into
 * the array of trace area pointers.
 *
 * It is also index of first empty field in the trace area list.
 */
static int trace_list_size = 0;

/**
 * Function for displaying trace message on standard output. It should not be
 * used directly, it is used in conveniece TRACE_MESSAGE macro together with
 * neccessary config verifications.
 *
 * Depending on the compilation setup it uses different style of the output.
 * Severity levels can be either numerical or textual, location in file and
 * fuction name can be displayed.
 *
 * It tries to display only file name - ie it looks for '/' characters and
 * starts printing the name only after that character.
 *
 * \todo This can become messy on Windows where '/' character can be in path
 * (need to investigate). No problem on Linux though.
 *
 * \param area trace area which is loggin the message
 * \param sev severity of the message
 * \param file file from which message is logged
 * \param line line in file from which message is logged
 * \param function name of the function from which message is logged
 * \param fmt textual format of message
 *
 * \return None
 */
void trace_log_message(TraceArea *area, TraceSeverity sev, char *file, unsigned int line, const char *function, char *fmt, ...) {
	char*	p_char = NULL;
	va_list args;

	#define TRACE_SEVERITY_NAME_ENABLE 0
	if ( TRACE_SEVERITY_NAME_ENABLE ) {
		printf("[%4s#%s]", area->name, trace_severity_name[sev]);
	} else {
		printf("[%4s#%d]", area->name, sev);
	}

	if ( TRACE_LOCATION_ENABLE ) {
		p_char = strrchr(file, '/');
		if ( NULL != p_char ) {
			printf("[%-20s:%-4d]", p_char+1, line);
		} else {
			printf("[%-20s:%-4d]", file, line);
		}
	}

	if ( TRACE_FN_NAME_ENABLE ) {
		printf(" %25s()", function);
	}

	printf(": ");


	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);

	printf("\n");

	return ;
}

/**
 * Change status of the supplied trace areas between enabled and disabled in the
 * global trace mask. The function can enable, disable or toggle status of trace
 * areas.
 *
 * This is utility function for easy setting of the global trace mask without
 * need to know of trace area bit masks and stuff.
 *
 * The list of trace area pointers must be NULL terminated for function to work
 * correctly.
 *
 * \param status enumeration with toggle status
 * \param ... NULL terminated list of trace are pointers
 */
void trace_toggle(TraceStatus status, ...) {
	TraceArea *area_ptr;
	va_list ap;

	va_start(ap, status);
	area_ptr = va_arg(ap, TraceArea*);
	while ( NULL != area_ptr ) {
		switch (status) {
			case TL_ENABLE:
				trace_global_mask |= area_ptr->mask;
				break;

			case TL_DISABLE:
				trace_global_mask &= ~ area_ptr->mask;
				break;

			case TL_TOGGLE:
				trace_global_mask ^= area_ptr->mask;
				break;

			default:
				// no change to trace area
				break;
		}
		area_ptr = va_arg(ap, TraceArea*);
	}
	va_end(ap);
}

/**
 * Change threshold severity for supplied trace areas.
 *
 * The list of trace area pointers must be NULL terminated for function to work
 * correctly.
 *
 *
 * \param severity new severity to use in trace areas
 * \param ... NULL terminated list of trace are pointers
 */
void trace_set_threshold(TraceSeverity severity, ...) {
	TraceArea *area_ptr;
	va_list ap;

	va_start(ap, severity);
	area_ptr = va_arg(ap, TraceArea*);
	while ( NULL != area_ptr ) {
		area_ptr->severity = severity;
		area_ptr = va_arg(ap, TraceArea*);
	}
	va_end(ap);
}


/**
 * The function adds trace area pointer to the internal list and then verifies
 * that it is a first time the pointer is added to the list.
 *
 * \param area trace area to pointer to initinitialize
 * \return 0 if there is not enough space in the internal list
 */
int trace_init_area(TraceArea *area) {
	int i;

	// check if there is a space in the list
	if ( trace_list_size >= CHAR_BIT*sizeof(TraceMask) ) {
		return 0;
	}

	// add trace area to the end of list
	trace_area_list[trace_list_size] = area;

	// try to find the same pointer in the list
	for (i=0; area != trace_area_list[i]; i++);

	if ( i == trace_list_size ) {
		trace_list_size += 1;
	} else {
		trace_area_list[trace_list_size] = NULL;
	}

	return 1;
}

/**
 * Add supplied trace area pointers to the internal list of used trace areas.
 * All trace areas in the internal list will be handled by trace_setup()
 * function.
 *
 * The list of trace area pointers must be NULL terminated for function to work
 * correctly. At least one trace area must be present in the function call.
 *
 * If function returns with error, it means there is no space left in the list
 * of trace areas. There are only few situations when that can occur:
 *
 * - there are duplicates in the trace area list
 * - there are trace areas which has more than one bit set to 1
 *
 * \param area first trace area to initialize
 * \param ... NULL terminated list of trace are pointers
 * \return 0 if there is not enough space in the internal list
 */
int trace_init(TraceArea *area, ...) {
	TraceArea *area_ptr;
	va_list ap;

	// add first trace area into internal list
	if ( NULL != area ) {
		if ( ! trace_init_area(area) ) {
			return 0;
		}
	}

	// add rest of the parameters until NULL is encountered
	va_start(ap, area);
	area_ptr = va_arg(ap, TraceArea*);
	while ( NULL != area_ptr ) {
		if ( ! trace_init_area(area_ptr) ) {
			return 0;
		}
		area_ptr = va_arg(ap, TraceArea*);
	}
	va_end(ap);

	// initialize rest of the memory if this is first time the function is
	// called
	if ( 0 == trace_list_size ) {
		int i;
		for (i = trace_list_size; i < CHAR_BIT*sizeof(TraceMask); i++) {
			trace_area_list[i] = NULL;
		}
	}
	return 1;
}

/**
 * Use strings with information about initial trace area setup for modification
 * of trace area values.
 *
 * Only trace areas initialized through trace_init() function can be setup using
 * the setup strings.
 *
 * The setup string contains comma separate list of setup options. Each setup
 * option consist of trace area name, equality sign and value. The value can be
 * either trace severity or strings ENABLE/DISABLE.
 *
 * The function then either enable or disable the trace area or set the severity
 * to the trace area.
 *
 * Example: "MAIN=ENABLE,MAIN=DEBUG,LIB=DISABLE,UTIL=ENABLE,UTIL=MAJOR"
 *
 * \param setupString string for changing threshold level of trace areas
 */
void trace_setup(char *setupString) {
	char *save_ptr1, *save_ptr2;
	char *string1, *string2;
	char *token, *subtoken, *input_string;
	char *area_name, *option_name;
	int i;

	input_string = strdup(setupString);

	// iterate over all options in the setup string
	for (string1 = input_string; ; string1 = NULL) {
		token = strtok_r(string1, ",", &save_ptr1);

		if ( NULL == token ) {
			break;
		}

		// reset pointers to key/value strings
		area_name = NULL;
		option_name = NULL;
		for (i = 0, string2 = token; ; i++, string2 = NULL) {
			subtoken = strtok_r(string2, "=", &save_ptr2);

			if ( NULL == subtoken ) {
				break;
			}

			if ( 0 == i ) {
				// first subtoken is area name
				area_name = subtoken;
			} else if ( 1 == i ) {
				// second subtoken is option name
				option_name = subtoken;
			} else {
				// there should not be third subtoken
				break;
			}
		}

		if  ( NULL != area_name && NULL != option_name ) {
			trace_setup_area(area_name, option_name);
		}
	}

	free( (void*)input_string);
}

/**
 * Internal function for setup of one trace area with supplied option string.
 *
 * The opion name can either enable/disable trace area or set new trace level to
 * the tracing area. Unknown options and area names are silently ignored.
 *
 * \param areaName name of the trace area to setup
 * \param optionName name of the option
 */
void trace_setup_area(char *areaName, char *optionName) {
	int list_index, i;

	// find index of the trace area to setup
	for ( list_index = 0; list_index < trace_list_size; list_index++) {
		if ( 0 == strcasecmp(areaName, trace_area_list[list_index]->name) ) {
			break;
		}
	}

	// if no trace area with supplied name is found, exit
	if ( list_index >= trace_list_size ) {
		return;
	}

	// check if option is enable/disable
	for (i = 0; i <= TL_DISABLE; i++ ) {
		if ( 0 == strcasecmp(optionName, trace_action_name[i]) ) {
			trace_toggle(i, trace_area_list[list_index], NULL);
			return;
		}
	}

	// check if option severity level
	for (i = 0; i <= TL_CRITICAL; i++ ) {
		if ( 0 == strcasecmp(optionName, trace_severity_name[i]) ) {
			trace_set_threshold(i, trace_area_list[list_index], NULL);
			return;
		}
	}
}
