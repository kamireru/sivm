/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file assert.h
 * \brief Defines macros for assertion checking
 *
 * This file defines macros which can be used for assertion checking. When
 * assertion fails it displays message to standard error output with like and
 * condition that failed
 */

#ifndef ASSERT_H
#define ASSERT_H

#include <stdio.h>
#include <stdlib.h>

/**
 * The ASSERT_FATAL macro controls whenever assertion failure will terminate
 * program execution. When set to 0, assertion fauilures won't be fatal.
 *
 * This can be set during compilation be specifying ASSERT_FATAL_DISABLE macro.
 */
#ifndef ASSERT_FATAL_DISABLE
	#define ASSERT_FATAL 1
#else
	#define ASSERT_FATAL 0
#endif


/**
 * The ASSERT_ENABLE macro controls whenever assertions will be checked or not.
 * When set to 0, no asserttion checking will be done.
 *
 * This can be set during compilation by specifying ASSERT_DISABLE macro.
 */
#ifndef ASSERT_DISABLE
	#define ASSERT_ENABLE 1
#else
	#define ASSERT_ENABLE 0
#endif


/**
 * Macro checks supplied conditional statement and if it is not true than it
 * displays warning message to the standard error ouput.
 *
 * If ASSERT_FATAL_DISABLE was not specified during compilation, then it also
 * terminates program execution.
 */
#define ASSERT(CONDITION) \
do { \
	if ( (1 == ASSERT_ENABLE ) ) { \
		if ( !(CONDITION) ) { \
			fprintf(stderr, "[%s:%d] %s(): Assert (%s) failed\n", __FILE__, __LINE__, __FUNCTION__, #CONDITION); \
			if ( 1 == ASSERT_FATAL ) { \
				exit(1); \
			} \
		} \
	} \
} while (0)


/**
 * Macro checks supplied conditional statement and if it is not true than it
 * displays warning message to the standard error ouput.
 * User can provide additional infomation using format string and variables like
 * in printf function
 *
 * If ASSERT_FATAL_DISABLE was not specified during compilation, then it also
 * terminates program execution.
 */
#define ASSERT_MESSAGE(CONDITION, FMT, ...) \
do { \
	if ( (1 == ASSERT_ENABLE ) ) { \
		if ( !(CONDITION) ) { \
			fprintf(stderr, "[%s:%d] Assert (%s) failed with message:\n * " FMT "\n", \
				__FILE__, __LINE__, #CONDITION, ##__VA_ARGS__); \
			if ( 1 == ASSERT_FATAL ) { \
				exit(1); \
			} \
		} \
	} \
} while (0)


#endif
