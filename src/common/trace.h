/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TRACE_TRACE_H
#define TRACE_TRACE_H 1

#include <limits.h>
#include "src/config.h"

typedef unsigned long long TraceMask;

/**
 * \brief Enumeration with trace levels that can be used for each trace area.
 */
typedef enum {
		TL_DEBUG	= 0,
		TL_INFO		= 1,
		TL_MINOR	= 2,
		TL_MAJOR	= 3,
		TL_CRITICAL	= 4
	} TraceSeverity;

/**
 * Structure holding information about one trace area which is then checked
 * against global trace setting to determine whenever current trace message
 * should be written to output or not.
 */
typedef struct {
		/** \brief Human readable name of trace area. */
		char		name[5];

		/** \brief Unique identifier of trace area which is used for
		 * determining whenever emit trace message or not. */
		TraceMask	mask;

		/** \brief Setting determinig how severe messages should be
		 * logged from trace are. */
		TraceSeverity	severity;

	} TraceArea;

/**
 * Export of variable for storing global trace mask.
 */
extern TraceMask trace_global_mask;

/**
 * \brief The enumeration of actions which can be done over trace area setting.
 */
typedef enum {
		TL_ENABLE = 0,
		TL_DISABLE = 1,
		TL_TOGGLE = 2
	} TraceStatus;

/**
 * \brief Generic tracing function. It can be used directly or through TRACE_MESSAGE
 * macro which simplifies the usage
 */
extern void trace_log_message(TraceArea *area, TraceSeverity sev, char *file, unsigned int line, const char* function, char *fmt, ...);

/** \brief Change enabled/disabled status for trace areas. */
extern void trace_toggle(TraceStatus status, ...);

/** \brief Change threshold level for supplied trace areas. */
extern void trace_set_threshold(TraceSeverity severity, ...);

/** \brief Prepare trace areas so that they can be set up programaticaly. */
extern int trace_init(TraceArea *area, ...);

/** \brief Setup trace areas using string with settings. */
extern void trace_setup(char *setupString);

/** \brief Setup single area using textual representation. */
extern void trace_setup_area(char *areaName, char *optionName);


/**
 * This macro declares pointer to the structure with area information.
 *
 * It is not necessary to use this if we define and use trace area in one file
 * only. This macro must be used in each source file which will be using trace
 * area with exception of source file where trace area is defined.
 */
#define TRACE_DECLARE(NAME) \
extern TraceArea * const NAME


/**
 * This macro allocates space for area pointer and structure which will
 * contain information about area. The structure is immediately initialized.
 *
 * This macro must be called only once in whole project. Use TRACE_DECLARE macro
 * in all other file where you want to use same trace area.
 *
 * The MASK_OFFSET must be in the range given by sizeof type used for storing
 * masks, so the macro contains logic to enforce the range.
 *
 * \param NAME name of the trace area (1-4 letters only)
 * \param MASK_OFFSET offset of the bit in the mask (0-CHAR_BIT*sizeof(TraceMask))
 * \param SEVERITY default severity
 */
#define TRACE_DEFINE(NAME,MASK_OFFSET,SEVERITY) \
TraceArea trace##NAME = { #NAME, \
	0x01 << ( (0 > (MASK_OFFSET)) ? 0 : \
		((CHAR_BIT*sizeof(TraceMask) <= (MASK_OFFSET)) ? CHAR_BIT*sizeof(TraceMask) - 1 : (MASK_OFFSET)) \
	), SEVERITY }; \
TraceArea * const NAME = & trace##NAME

/**
 * Convenience macro for checking whenever message falls into allowed trace area
 * and whenever it has appropriate trace level before logging it to stantard
 * output
 */
#define TRACE_MESSAGE(NAME,SEVERITY,FORMAT,...)\
do { \
	if ( TRACE_ENABLE ) { \
		if ((trace_global_mask & (NAME)->mask) && ((SEVERITY) >= (NAME)->severity)) { \
			trace_log_message((NAME), (SEVERITY), __FILE__, __LINE__, __FUNCTION__, (FORMAT), ##__VA_ARGS__); \
		} \
	} \
} while (0)

#endif
