/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "src/common/common_def.h"

// trace area for main function and initialization
TRACE_DEFINE(MFUN, 0, TL_DEBUG);
TRACE_DEFINE(LAY,  1, TL_DEBUG);

// trace areas for configuration
TRACE_DEFINE(CONF, 3, TL_DEBUG);

// trace areas for backend
TRACE_DEFINE(STRG, 5, TL_DEBUG); // storage for backend
TRACE_DEFINE(PILE, 6, TL_DEBUG);
TRACE_DEFINE(IGRP, 7, TL_DEBUG);
TRACE_DEFINE(GREC, 8, TL_DEBUG);

// trace areas for front end
TRACE_DEFINE(GUI,   9, TL_DEBUG);
TRACE_DEFINE(VIEW, 10, TL_DEBUG);
TRACE_DEFINE(CNTL, 11, TL_DEBUG);
TRACE_DEFINE(MODL, 12, TL_DEBUG);
TRACE_DEFINE(CONT, 13, TL_DEBUG);

ERROR_DEFINE_START(ERROR)
	ERROR_DEFINE(GENERAL_ERROR, "General error occured"),
	ERROR_DEFINE(PARSE_FILE, "Error parsing file content"),
	ERROR_DEFINE(PARSE_PARAM, "Error parsing command line arguments"),
	ERROR_DEFINE(MEMORY_FAILURE, "Memory allocation failure"),
	ERROR_DEFINE(INVALID_OPERATION, "Invalid operation on object in current state"),
	ERROR_DEFINE(INVALID_PARAM, "Invalid parameter supplied"),
	ERROR_DEFINE(OUT_OF_RANGE, "Out of range"),
	ERROR_DEFINE(ACTION_FN_FAILED, "Action function failed"),
	ERROR_DEFINE(CREATION_FAILURE, "Undefined error when creating structure"),
	ERROR_DEFINE(NOT_IMPLEMENTED, "Functionality not implemented yet")
ERROR_DEFINE_END

/**
 * The function initializes all trace areas defined in this file. It also resets
 * global tracing mask to zero.
 */
void init_trace_areas() {
	trace_global_mask = 0;
	trace_init(
			// main function and initializations
			MFUN, LAY,

			// configuration trace areas
			CONF,

			// backend trace areas
			STRG, PILE, IGRP, GREC,

			// frontend trace areas
			GUI, VIEW, CNTL, MODL, CONT,

			NULL
		);
}

/**
 * Tries to setup all initialized tracing areas using environment variable name
 * and supplied configuration string
 *
 * \param variable name of the environment variable
 * \param string configuration string
 */
void init_trace_setup(char *variable, char *string) {
	char *variable_content = NULL;

	// try to load environment variable with setup
	if ( NULL != variable ) {
		variable_content = getenv(variable);
	}

	if ( NULL != variable_content ) {
		trace_setup(variable_content);
	}

	if ( NULL != string ) {
		trace_setup(string);
	}

	return;
}
