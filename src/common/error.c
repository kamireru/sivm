/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <common/error.h>

/**
 * Storage for information about error encountered. Currently only one error can
 * be handled at one time and all other error will be discarded.
 */
ErrorInfo error_info = {
		NULL,	/* error */
		NULL,	/* file */
		0,		/* line */
		"",		/* note_buffer */
		0,		/* note_size */
		0		/* truncated */
	};


/**
 * Number of discarded errors.
 */
unsigned int count_discarded = 0;


/**
 * Method for checking if there is an error message waiting to be logged.
 *
 * \return 0 is there is no error, 1 if there is an error
 */
int error_is_error() {
	if ( NULL == error_info.error ) {
		return 0;
	} else {
		return 1;
	}
}


/**
 * This function sets error and prepares message buffer for adding notes to the
 * error_info.
 *
 * This function should not be used directly but through ERROR_SET macro which
 * automaticaly sets file and line input variables from FILE and LINE compiler
 * macros.
 *
 * \param file string describing where the error originated
 * \param line number describing line in the file where error originated
 * \param err predefined error type with name and title
 */
void error_set(char *file, int line, const Error *err) {
	if ( NULL != error_info.error ) {
		count_discarded++;
	}

	error_info.error = err;
	error_info.note_size = 0;
	error_info.file = file;
	error_info.line = line;
	memset((void*)(error_info.note_buffer), 0, (ERROR_BUFFER_SIZE +1) * sizeof(char));
}

/**
 * Function clears currently stored error and message buffer.
 *
 * This function should not be used directly, it is used in ERROR_LOG and
 * ERROR_LOG_FATAL macros which log error and automaticaly clears it from
 * storage.
 *
 * The only usage for this function is when error is encountered but we do not
 * want to log it and we also do not want logs indicating that the error was
 * discarded.
 */
void error_reset() {
	error_info.error = NULL;
	error_info.truncated = 0;
	count_discarded = 0;
	error_info.note_size = 0;
	error_info.file = NULL;
	error_info.line = 0;
	memset((void*)(error_info.note_buffer), 0, (ERROR_BUFFER_SIZE +1) * sizeof(char));
}

/**
 * Function adds note into message buffer. This note is then displayed when
 * error is logged.
 *
 * This function should not be used directly, it is used in ERROR_NOTE which
 * automaticaly sets file and line variables according to FILE and LINE compiler
 * macros.
 *
 * \param file string describing from where the note was added
 * \param line number describing line in the file from which note was added
 * \param function string describing function from which note was added
 * \param fmt format string for note
 */
void error_note(char *file, int line, char* function, char *fmt, ...) {
	int written;
	va_list args;

	// if no error is set, finish early
	if ( NULL == error_info.error ) {
		return ;
	}

	 // write function name into buffer
	if ( ERROR_BUFFER_SIZE <= error_info.note_size ) {
		error_info.truncated = 1;
		return;
	}
	written = snprintf(error_info.note_buffer + error_info.note_size * sizeof(char),
			ERROR_BUFFER_SIZE - error_info.note_size,
			"* %s(): ", function
		);
	error_info.note_size += written;

	// write user message into buffer
	if ( ERROR_BUFFER_SIZE <= error_info.note_size ) {
		error_info.truncated = 1;
		return;
	}
	va_start(args, fmt);
	written = vsnprintf(error_info.note_buffer + error_info.note_size * sizeof(char),
			ERROR_BUFFER_SIZE - error_info.note_size,
			fmt, args
		);
	va_end(args);
	error_info.note_size += written;

	// write file and line from where note was added into message buffer
	if ( ERROR_BUFFER_SIZE <= error_info.note_size ) {
		error_info.truncated = 1;
		return;
	}
	written = snprintf(error_info.note_buffer + error_info.note_size * sizeof(char),
			ERROR_BUFFER_SIZE - error_info.note_size,
			" [%s:%d]\n", file, line
		);
	error_info.note_size += written;

	if ( ERROR_BUFFER_SIZE <= error_info.note_size ) {
		error_info.truncated = 1;
	}

	return ;
}

/**
 * This function logs error to the standard error output and terminates program
 * in case error is fatal. Fatality of error is determined by non-zero,
 * non-negative value of exitCode input variable.
 *
 * This function should not be used directly, it is used in ERROR_LOG and
 * ERROR_LOG_FATAL macros.
 *
 * \param exitCode value with which program terminates
 */
void error_log(int exitCode) {
	/*
	 * do not log error if there is none
	 */
	if ( NULL == error_info.error ) {
		return;
	}

	/*
	 * display internal errors first - discarded and truncated messages
	 */
	if ( 1 == count_discarded ) {
		fprintf( stderr, "Error: %d error messages were discarded\n", count_discarded);
	}

	if ( 1 == error_info.truncated ) {
		fprintf( stderr, "Error: Error message was truncated\n");
	}

	/*
	 * display what is in error itself and in message buffer
	 */
	fprintf( stderr, "[%s:%d]: Error (%s): '%s'\n",
			error_info.file, error_info.line,
			error_info.error->name, error_info.error->title
		);

	fprintf( stderr, "%s", error_info.note_buffer);

	if ( 1 == error_info.truncated ) {
		/*
		 * This here adds empty line after truncated error so it looks same as
		 * when the message is not truncated
		 */
		fprintf(stderr, "\n");
	}

	/*
	 * depending on exit code either terminate program or continue
	 */
	if ( -1 == exitCode ) {
		return ;
	} else {
		exit(exitCode);
	}
}

/**
 * The function is for use in unit tests only. It exposes internal variables for
 * testing functions.
 *
 * \return constant pointer to ErrorInfo structure
 */
ErrorInfo * const error_get_info() {
	return &error_info;
};

/**
 * The function is for use in unit tests only. It exposes internal variables for
 * testing functions.
 *
 * \return number of discarded errors.
 */
unsigned int error_get_discarded() {
	return count_discarded;
}
