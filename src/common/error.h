/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef COMMON_ERROR_H
#define COMMON_ERROR_H

/**
 * Macro for setting maximal size of error buffer.
 */
#ifndef ERROR_BUFFER_SIZE
	#define ERROR_BUFFER_SIZE 1024
#endif

/**
 * The Error structure holds generic information about error. It is used for
 * describing general error types which can be encountered during program
 * execution.
 *
 * This structure should not be created directly, it is used in ERROR_DEFINE and
 * ERROR_DECLARE macros which create instance and const pointer to that instance
 * so it can be easily used.
 */
typedef struct {
		/** \brief Short error name. */
		char *name;

		/** \brief Error type description. */
		char *title;
	} Error;

/**
 * Type indicating failure in function
 */
typedef enum {
		FAIL = 0,
		OK = 1
	} RCode;

/**
 * Structure describing error in its fullest. What kind of error it is, where it
 * came from and what else we do know about it
 */
typedef struct {
		/** \brief Pointer to error description. */
		const Error*	error;

		/** \brief Error source file name. */
		char*	file;

		/** \brief Error source file line.*/
		int		line;

		/** \brief Storage for error notes. */
		char	note_buffer[ERROR_BUFFER_SIZE+1];

		/** \brief Length of currently stored note(s). */
		int		note_size;

		/** \brief Indication that notes were truncated.*/
		int		truncated;

	} ErrorInfo;

/** \brief Check if there is error record present in the error stack. */
extern int error_is_error();

/** \brief Set error and prepare error buffer. */
extern void error_set(char *file, int line, const Error* err );

/** \brief Reset error and error buffer. */
extern void error_reset();

/** \brief Add note to the error present in buffer. */
extern void error_note(char *file, int line, char* function, char *fmt, ...);

/** \brief Log error and possibly interrupt execution. */
extern void error_log(int exitCode);

/** \brief DEBUG: Return const pointer to internal structure. */
extern ErrorInfo * const error_get_info();

/** \brief DEBUG: Return number of discarded errors. */
extern unsigned int error_get_discarded();


/**
 * Convenience macro that starts declaration of error list structure. It should
 * be present in header file.
 *
 * Note: No semicolon after this macro!
 */
#define ERROR_DECLARE_START typedef struct {

/** Convenience macro for declaration of one member variable in the error
 * structure.
 *
 * Note: No semicolon after this macro!
 */
#define ERROR_DECLARE_END } ErrorList;

/**
 * Convenience macro for finishing declaration of error list structure.
 *
 * Note: No semicolon after this macro!
 */
#define ERROR_DECLARE(NAME) const Error NAME;

/**
 * The macro declares extern for actual error structure. It should be placed in
 * the header file and the NAME parameter must be same as the NAME parameter of
 * ERROR_DEFINE_START in the implementation file.
 *
 * Note: No semicolon after this macro!
 */
#define ERROR_DECLARE_LIST(NAME) extern const ErrorList NAME;


/**
 * Convenience macro that starts code for filling up the error list structure
 * with error description. This macro should be in implementation file.
 *
 * Note: No semicolon after this macro!
 */
#define ERROR_DEFINE_START(NAME) const ErrorList NAME = {

/**
 * Convenience macro for finishing error list structure definition.
 *
 * Note: No semicolon after this macro!
 */
#define ERROR_DEFINE_END };

/**
 * The data definition convenience macro. It creates strings that identify
 * individual errors.
 *
 * The ERROR_DEFINE macros must define an error names in the same order as they
 * were declared by ERROR_DECLARE macros. Otherwise there will be wrong names
 * associated with error.
 *
 * Note: There should be comma after each call to the macro except the last one.
 *
 * \param NAME error name
 * \param TITLE error description
 */
#define ERROR_DEFINE(NAME, TITLE) { # NAME, TITLE }

/**
 * Convenience macro which sets error. This macro should be prefered to direct
 * call to error_set()
 *
 * \param ERROR name of the error
 */
#define ERROR_SET(ERROR) \
do { \
	error_set(__FILE__, __LINE__, &(ERROR)); \
} while (0)


/**
 * Convenience macro for adding note into error message.
 *
 * This macro should be prefered to direct call to error_note() function
 */
#define ERROR_NOTE(FORMAT, ...) \
error_note(__FILE__, __LINE__, (char*)__FUNCTION__, (FORMAT), ##__VA_ARGS__)

/**
 * Convenience macro for logging not-fatal error with arbitrary exit code.
 * This macro should be prefered to direct call to error_log()
 */
#define ERROR_LOG() \
do { \
	ERROR_NOTE("Error caught and logged"); \
	error_log(-1); \
	error_reset(); \
} while (0)


/**
 * Convenience macro for loggin fatal error with arbitrary exit code.
 * This macro should be prefered to direct call to error_log()
 */
#define ERROR_LOG_FATAL(EXIT_CODE) \
do { \
	ERROR_NOTE("Error caught and logged"); \
	error_log((EXIT_CODE)); \
	error_reset(); \
} while (0)

#endif
