/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef COMMON_COMMONDEF_H
#define COMMON_COMMONDEF_H

#include "src/common/trace.h"
#include "src/common/error.h"
#include "src/common/assert.h"

TRACE_DECLARE(MFUN);
TRACE_DECLARE(LAY);

TRACE_DECLARE(CONF);

TRACE_DECLARE(STRG);
TRACE_DECLARE(PILE);
TRACE_DECLARE(IGRP);
TRACE_DECLARE(GREC);

TRACE_DECLARE(GUI);
TRACE_DECLARE(VIEW);
TRACE_DECLARE(CNTL);
TRACE_DECLARE(MODL);
TRACE_DECLARE(CONT);

ERROR_DECLARE_START
	ERROR_DECLARE(GENERAL_ERROR)
	ERROR_DECLARE(PARSE_FILE)
	ERROR_DECLARE(PARSE_PARAM)
	ERROR_DECLARE(MEMORY_FAILURE)
	ERROR_DECLARE(INVALID_OPERATION)
	ERROR_DECLARE(INVALID_PARAM)
	ERROR_DECLARE(OUT_OF_RANGE)
	ERROR_DECLARE(ACTION_FN_FAILED)
	ERROR_DECLARE(CREATION_FAILURE)
	ERROR_DECLARE(NOT_IMPLEMENTED)
ERROR_DECLARE_END

ERROR_DECLARE_LIST(ERROR)

/** \brief Setup traces areas */
extern void init_trace_areas();

/** \brief Configure trace areas from environment environment. */
extern void init_trace_setup(char *variable, char *string);


/**
 * \brief Float value comparison macro.
 *
 * Due to float (and double) precission issues it is not possible to compare
 * then using == operator. This macro returns true when the difference between
 * values is smaller than preset limit.
 *
 * This allows us to compare floats with given precission
 *
 * \param VALUE1 first float value
 * \param VALUE2 second float value
 * \param EPSILON comparison precission
 * \return true if values are equal on given precission
 */
#define FLOAT_EQUAL(VALUE1, VALUE2, EPSILON) ( fabs((VALUE1) - (VALUE2)) < (EPSILON) )

/**
 * \brief Precission used for zoom related computations.
 *
 * The zoom is computed up to 5 decimal places.
 */
#define ZOOM_PRECISSION 0.000001

/**
 * \brief Utility macro to compare zoom values
 *
 * \param VALUE1 first float value
 * \param VALUE2 second float value
 * \return true if values are equal on ZOOM_PRECISSION
 */
#define ZOOM_EQUAL(VALUE1, VALUE2) FLOAT_EQUAL(VALUE1, VALUE2, ZOOM_PRECISSION)

#endif
