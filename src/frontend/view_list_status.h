/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file view_list_status.h
 */
#ifndef __FRONTEND_VIEW_LIST_STATUS_H__
#define __FRONTEND_VIEW_LIST_STATUS_H__

#include <gdk-pixbuf/gdk-pixbuf.h>

#include "src/frontend/view_common.h"

typedef enum {
		VIEW_STATUS_POSITION = 0,
		VIEW_STATUS_FILENAME = 1
	} ViewStatusType;

/**
 * \brief Struture storing information about image list status view.
 */
typedef struct {
		/** \brief Flag indicating type of information to display in view. */
		ViewStatusType status_type;

		/** \brief Size of the font used for text rendering. */
		GdkPoint text_size;

		/** \brief Size of the borders between widget edges and text. */
		GdkPoint border_size;

		/** \brief Structure for grouping together color storage. */
		struct {

			/** \brief Storage for background color definition. */
			GdkColor bg;

			/** \brief Storage for foreground color definition. */
			GdkColor fg;
		} color;

		GdkFont* font;

		/** \brief Pointer to currently displayed text. */
		gchar* buffer;

	} ViewListStatus;


// specific implementation of common view functions

/** \brief Allocate memory for view and initialize variables. */
extern ViewListStatus* view_lsv_create(ViewGeneric *parent);

/** \brief Deallocate memory for view. */
extern RCode view_lsv_destroy(ViewListStatus *specificData);

/** \brief Update the image display view from model. */
extern RCode view_lsv_update(ViewGeneric *view, guint seq);

/** \brief Redraw image display view area. */
extern RCode view_lsv_redraw(ViewGeneric *view, GdkRectangle *region);

/** \brief Resize the image display view display. */
extern RCode view_lsv_resize(ViewGeneric *view, GdkRectangle newRegion);


// view specific functions

/** \brief Set new status type to the view. */
extern RCode view_lsv_set_type(ViewGeneric *view, ViewStatusType statusType);


// internal functions, exported for unit tests only

/** \brief Cast specific data pointer to correct type. */
extern ViewListStatus* view_lsv_get_data_pointer(ViewGeneric* view);

#endif
