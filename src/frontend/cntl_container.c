/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stddef.h>
#include <gdk/gdkkeysyms.h>

#include "src/fa/fa.h"

#include "src/frontend/cntl_common.h"
#include "src/frontend/cntl_keymap.h"

#include "src/frontend/cntl.h"
#include "src/frontend/view.h"
#include "src/frontend/model.h"

/**
 * The local enumeration naming nodes for easier understanding of the automaton
 * definition.
 */
enum CntApplicationNode {
		CNTL_CV_INIT = 0,
		CNTL_CV_GRAB = 1
	};

static char* cntl_cv_node_name[] = {
		"INIT",
		"GRAB",
	};


/**
 * The function resets container controller specific data before each new
 * automaton run.
 *
 * \param data pointer to CntlGeneric structure of the whole controller
 */
static void cntl_cv_data_reset(void *data) {
	return;
}

/**
 * The function invoked when node that is associated with some action is
 * reached. It checks in which node the automaton is and then invokes
 * appropriate action on the associated model.
 *
 * \param node index of node into which the automaton transitioned
 * \param value that triggered the transition to node
 * \param data pointer to data associated with automaton run
 */
static void cntl_cv_action(unsigned int node, void *value, void* data) {
	CntlGeneric* controller = (CntlGeneric*)data;
	RCode result = OK;

	ASSERT( NULL != controller );

	switch (node) {
	default:
		TRACE_MESSAGE(CNTL, TL_INFO, "Placeholder code (cntl=%p)", controller);
	}

	// check for error in some of the model specific function calls
	if ( FAIL == result ) {
		ERROR_LOG();
	}

	return;
}


/**
 * The function creates automaton implementing logic of container controller.
 *
 * \return pointer to automaton structure
 */
FaAutomaton* cntl_cv_create_automaton() {
	FaAutomaton* result = fa_create(cntl_cv_data_reset, cntl_key_compare, 2,
		fa_node_create(CNTL_CV_INIT, FA_NODE_START, NULL, 1,
			fa_transition_create(CNTL_CV_GRAB, NULL, 1,
				fa_set_create(FA_SET_LIST, 1, &cntl_keymap[0].container_mode )
			)
		),
		fa_node_create(CNTL_CV_GRAB, FA_NODE_FINAL, cntl_cv_action, 0)
	);

	if ( NULL == result ) {
		ERROR_SET(ERROR.CREATION_FAILURE);
		ERROR_NOTE("Creation of container controller automaton has failed");
	}

	return result;
}

/**
 * Currently, there is no specific data for container controller. Thus the
 * function returns NULL.
 *
 * Note: Once there are some data, it is necessary to impleemnt correct reset
 * function for automaton. Currenlty, it does nothing.
 *
 * \return void pointer to created automaton data
 */
void* cntl_cv_create_data() {
	return NULL;
}

/**
 * The container controller handles key processing using following algorithm:
 *
 * 1) If automaton is not blocked, try to process input
 *
 *   a) if automaton partially match, return from function
 *   b) if automaton match and end in final node, reset automaton and return
 *   b) if automaton fails match, forward the input to child controller
 *
 * 2) If automaton associated with controller is blocked (no valid input), send
 * input to the child controller.
 *
 *   a) if child controller succeeds or fails match, reset automaton
 *   b) if child controller match partially, do not reset automaton so that all
 *      further input is redirected to child controller until it matches
 *
 * \param cntl pointer to generic controller structure
 * \param input key code of use input
 * \return see CntlProcessResult for return code explanation
 */
CntlProcessResult cntl_cv_process(CntlGeneric *cntl, GdkEventKey *input) {
	CntlGeneric* child_controller = NULL;
	CntlProcessResult result = CNTL_MATCH_FAIL;

	ASSERT( NULL != cntl );
	ASSERT( NULL != input );

	TRACE_MESSAGE(CNTL, TL_INFO, "Process key (cntl=%p, key=%s)",
			cntl, gdk_keyval_name(input->keyval)
		);

	// if we are not blocked, feed input to automaton (point 1 of algorithm)
	if ( FA_FALSE == fa_runner_is_blocked(cntl->automaton) ) {
		char * node_name = "";

		// we do not care about return value since the state will be checked in
		// next if statement
		if ( FA_FALSE == fa_runner_step(cntl->automaton, (void*)input) ) {
			TRACE_MESSAGE(CNTL, TL_DEBUG, "Key processing failed (cntl=%p, key=%s)",
					cntl, gdk_keyval_name(input->keyval)
				);
		}

		if ( NULL != cntl->automaton->state ) {
			node_name = cntl_cv_node_name[cntl->automaton->state->index];
		}

		// check result of the container automaton processing
		if ( FA_TRUE == fa_runner_is_accept(cntl->automaton) ) {
			TRACE_MESSAGE(CNTL, TL_DEBUG, "Key sequence matched (cntl=%p, key=%s, node='%s')",
					cntl, gdk_keyval_name(input->keyval), node_name
				);
			fa_runner_reset(cntl->automaton);
			return CNTL_MATCH_SUCCESS;
		}

		if ( FA_FALSE == fa_runner_is_blocked(cntl->automaton) ) {
			TRACE_MESSAGE(CNTL, TL_DEBUG, "Key sequence partial match (cntl=%p, key=%s, node='%s')",
					cntl, gdk_keyval_name(input->keyval), node_name
				);
			return CNTL_MATCH_PARTIAL;
		}
	}

	// if automaton blocked on the input, feed child (step 2 of controller logic)
	TRACE_MESSAGE(CNTL, TL_INFO, "Forward processing to child (cntl=%p, key=%s)",
			cntl, gdk_keyval_name(input->keyval)
		);

	// \todo some error checking should be here
	if ( NULL != view_cv_get_view(cntl->view) ) {
		child_controller = view_get_controller( view_cv_get_view( cntl->view ) );

		if ( NULL != child_controller ) {
			result = cntl_process(child_controller, input);
			if ( CNTL_MATCH_FAIL == result || CNTL_MATCH_SUCCESS == result ) {
				fa_runner_reset(cntl->automaton);
			}
		}

	} else {
		TRACE_MESSAGE(CNTL, TL_MAJOR,
				"Container view does not have child in focused node (view=%p)",
				cntl->view
			);
	}

	if ( error_is_error() ) {
		ERROR_LOG();
	}

	return result;
}
