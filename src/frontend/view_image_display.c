/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>

#include "src/config/config.h"
#include "src/frontend/view_generic.h"
#include "src/frontend/view_image_display.h"
#include "src/frontend/model_image_list.h"

/**
 * The constant tells percentage of view region width/height that is used for
 * pannig.
 *
 * Ie, if view region height is 200 pixels and constant is 0.5 then each time
 * user requests image panning up or down, the image will move by 0.5 * 200
 * pixel.
 */
static const double VIEW_PAN_PERCENTAGE = 0.60f;

/**
 * Constant used for changing zoom factor when zooming in.
 */
static const double VIEW_ZOOM_IN = 1.25f;

/**
 * Constant used for changing zoom factor when zooming out.
 */
static const double VIEW_ZOOM_OUT = 0.8f;

/**
 * Constant used for limiting minimum zoom to reasonable amount.
 */
static const double VIEW_ZOOM_MIN = 0.01f;

/**
 * Constant used for limiting maximum zoom to reasonable amount.
 */
static const double VIEW_ZOOM_MAX = 200.0f;


static const int VIEW_BITS_PER_COLOR = 8;

/**
 * Structure holding input for animation frame update function.
 *
 * The structure contains view that need to be updated and sequence number which
 * 'identifies' animation which has to be updated. (to ensure that animation has
 * not changed in meantime).
 */
typedef struct {
		/** \brief The sequence number of animation to display. */
		guint sequence;

		/** \brief Pointer to view with animation. */
		ViewGeneric* view;
	} FrameUpdate;

/**
 * Allocate memory for image display view structure and initialize it with
 * correct values.
 *
 * \return pointer to image display specific data
 */
ViewImageDisplay* view_idv_create(ViewGeneric *parent) {
	const ConfigStorage* conf;
	ViewImageDisplay *result = NULL;

	ASSERT( NULL != parent );

	if ( NULL == ( result = (ViewImageDisplay*) g_try_malloc0(sizeof(ViewImageDisplay))) ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return result;
	}

	result->dimension.x = 0;
	result->dimension.y = 0;
	result->view_buffer = NULL;

	// either get parameters for zooming from configuration or get default if it
	// is not present
	conf = config_get();
	if ( NULL == conf ) {
		ERROR_LOG();
		result->zoom_factor = 1.0;
		result->zoom_type = ZOOM_FIT_CUSTOM;
		result->pan_percentage = VIEW_PAN_PERCENTAGE;
	} else {
		result->zoom_factor = conf->zoom;
		result->zoom_type = conf->auto_zoom;
		result->pan_percentage = conf->pan_percentage;
	}

	return result;
}

/**
 * Perform deinicialization and deallocation of image display view structure.
 *
 * \param specificData pointer to image display view structure
 * \return OK on success
 */
RCode view_idv_destroy(ViewImageDisplay *specificData) {

	ASSERT( NULL != specificData );

	if ( NULL != specificData->view_buffer ) {
		g_object_unref( (gpointer)specificData->view_buffer );
	}

	g_free( (gpointer)specificData );
	return OK;
}

/**
 * Get current image from image list model and transform it fit into view
 * buffer. Then invalidate the region to invoke redraw of the image.
 *
 * Note: The update function can be invoked through model update even before
 * window pointer assignment to view and view buffer allocation. Thus it is very
 * important to check for those conditions.
 *
 * \param view pointer to generic view structure
 * \param seq sequence number identitying the update
 * \return OK on success
 */
RCode view_idv_update(ViewGeneric *view, guint seq) {
	ViewImageDisplay* idv_data = NULL;
	RCode result = OK;
	gint delay = -1;

	ASSERT( NULL != view );

	idv_data = view_idv_get_data_pointer(view);
	if ( NULL == idv_data ) {
		return FAIL;
	}

	// reset offset to top left corner upon image change
	idv_data->offset.x = 0;
	idv_data->offset.y = 0;

	result = view_idv_generate_buffer(view, idv_data, TRUE);

	// install animation frame update timeout if necessary
	delay = model_iml_get_delay(view->model);
	if ( -1 != delay ) {
		FrameUpdate* update = (FrameUpdate*) g_try_malloc0( sizeof(FrameUpdate));
		if ( NULL == update ) {
			ERROR_SET(ERROR.MEMORY_FAILURE);
			ERROR_NOTE("Animation frame update structure allocation failed");
			return FAIL;

		} else {
			update->view = view;
			update->sequence = view->last_sequence;
		}
		g_timeout_add(delay, view_idv_update_frame, (gpointer)update);
	}

	return result;
}

/**
 * Check if view buffer is initialized and if so, redraw portion of it defined
 * in input parameter to the window.
 *
 * If view buffer is not defined, clear the region with black color.
 *
 * \param view pointer to generic view structure
 * \param region region of screen that need to be updated
 * \return OK on success
 */
RCode view_idv_redraw(ViewGeneric *view, GdkRectangle *region) {
	ViewImageDisplay* idv_data = NULL;

	idv_data = view_idv_get_data_pointer(view);
	if ( NULL == idv_data ) {
		return FAIL;
	}

	if ( NULL == idv_data->view_buffer ) {
		gdk_window_clear_area(view->window,
				idv_data->viewport.x, idv_data->viewport.y,
				idv_data->viewport.width, idv_data->viewport.height
			);

	} else {
		gdk_draw_pixbuf(view->window, /* GdkGC */ NULL, idv_data->view_buffer,
				region->x - view->region.x, region->y - view->region.y,
				region->x, region->y,
				region->width, region->height,
				GDK_RGB_DITHER_NONE, 0, 0
			);
	}
	return OK;
}

/**
 * The method resize updates the view internal variables to the changed region
 * size.
 *
 * Resize the image view buffer to the new dimensions.
 *
 * \param view view to resize
 * \param newRegion the size of the new region
 * \return OK on success
 */
RCode view_idv_resize(ViewGeneric *view, GdkRectangle newRegion) {
	ViewImageDisplay* idv_data = NULL;

	ASSERT( NULL != view );

	idv_data = view_idv_get_data_pointer(view);
	if ( NULL == idv_data ) {
		return FAIL;
	}

	// check if buffer need to be resized
	if ( newRegion.width != view->region.width ||
		newRegion.height != view->region.height ) {

		if ( NULL != idv_data->view_buffer ) {
			g_object_unref( (gpointer)idv_data->view_buffer );
		}

		// allocate pixbuf for view region
		idv_data->view_buffer = gdk_pixbuf_new( GDK_COLORSPACE_RGB,
				FALSE, VIEW_BITS_PER_COLOR, newRegion.width, newRegion.height
			);
		if ( NULL == idv_data->view_buffer ) {
			ERROR_SET(ERROR.MEMORY_FAILURE);
			ERROR_NOTE("View image buffer allocation failed");
			return FAIL;
		}

		// this is done in generic view, but we duplicate the code here since we
		// need to have view region updated prior buffer content regeneration
		view->region = newRegion;

		// re-generate content of the view buffer
		return view_idv_generate_buffer(view, idv_data, FALSE);
	}

	return OK;
}

/**
 * Set new zoom type to the image display view structure and force internal
 * buffer content regeneration if new zoom type is different from old zoom type.
 *
 * \param view pointer to generic view structure
 * \param type new zoom type to set for image display view
 * \return OK on success
 */
RCode view_idv_set_zoom(ViewGeneric *view, ZoomType type) {
	ViewImageDisplay* idv_data = NULL;

	ASSERT( NULL != view );

	idv_data = view_idv_get_data_pointer(view);
	if ( NULL == idv_data ) {
		return FAIL;
	}

	if ( type != idv_data->zoom_type ) {
		idv_data->zoom_type = type;

		// reset to original resultion if changing to custom zoom
		if ( ZOOM_FIT_CUSTOM == idv_data->zoom_type ) {
			idv_data->zoom_factor = 1.0f;
		}

		// reset offset to top left corner upon auto zoom type change
		idv_data->offset.x = 0;
		idv_data->offset.y = 0;

		TRACE_MESSAGE(CNTL, TL_DEBUG,
				"Zoom setup has changed to (view=%p, factor=%f, type=%d)",
				view, idv_data->zoom_factor, idv_data->zoom_type
			);

		return view_idv_generate_buffer(view, idv_data, TRUE);
	} else {
		return OK;
	}
}

/**
 * Change zoom by one level in or out. Internally it is done by multiplying or
 * dividing zoom factor by one constant. This way it is possible to smootly
 * increase or descrease the zoom.
 *
 * If zoom type is different from ZOOM_FIT_CUSTOM, then set it to that value to
 * make sure new zoom factor won't be recomputed and overwritten.
 *
 * \param view pointer to generic view structure
 * \param direction direction in which to zoom
 * \return OK on success
 */
RCode view_idv_zoom(ViewGeneric *view, ZoomDirection direction) {
	ViewImageDisplay* idv_data = NULL;
	gboolean is_changed;

	ASSERT( NULL != view );

	idv_data = view_idv_get_data_pointer(view);
	if ( NULL == idv_data ) {
		return FAIL;
	}

	is_changed = view_idv_compute_zoom(view, idv_data, direction);
	return view_idv_generate_buffer(view, idv_data, is_changed);
}

/**
 * Move location of top left corner of rectangle that is being displayed from
 * whole image.
 *
 * \param view pointer to generic view structure
 * \param direction direction of the panning
 * \return OK on success
 */
RCode view_idv_pan(ViewGeneric *view, PanDirection direction) {
	ViewImageDisplay* idv_data = NULL;
	gboolean is_change;

	ASSERT( NULL != view );

	idv_data = view_idv_get_data_pointer(view);
	if ( NULL == idv_data ) {
		return FAIL;
	}

	is_change = view_idv_compute_offset(view, idv_data, direction);

	return view_idv_generate_buffer(view, idv_data, is_change);
}

/**
 * The function checks the type of the view and either cast the pointer to
 * correct type or return NULL to indicate error.
 *
 * This function can use NULL for error indication because image display view
 * should always have specific data.
 *
 * \param view pointer to image display view structure
 * \return ViewApplication pointer or NULL on error
 */
ViewImageDisplay* view_idv_get_data_pointer(ViewGeneric* view) {
	ASSERT( NULL != view );

	if ( VIEW_IMAGE_DISPLAY != view->type ) {
		return NULL;
	}

	if ( NULL == view->specific_data ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Specific data pointer is NULL");
		return NULL;
	}

	return (ViewImageDisplay*) view->specific_data;
}

/**
 * Re-generate content of internal view buffer with changed data. The changed
 * data can be size of the view region, changed image in underlaying model or
 * anything else.
 *
 * Note: The model associated with view should not be NULL (who sent us update
 * notification in that case), but handle the situation gracefully.
 *
 * Note: The force argument servers in occasions when there is no change in
 * viewport or zoom but buffer must be regenerated (for example when panning).
 *
 * \param view pointer to generic view structure
 * \param data pointer to image display view data
 * \param force flag indicating whenever the regeneration should be forced
 * \return OK on success
 */
RCode view_idv_generate_buffer(ViewGeneric* view, ViewImageDisplay *data, gboolean force) {
	GdkRectangle out_view, out_image, reset_area;
	gboolean is_change = FALSE;
	gdouble out_zoom;
	GdkPixbuf* image = NULL;

	ASSERT( NULL != view );
	ASSERT( NULL != data );

	TRACE_MESSAGE(VIEW, TL_DEBUG,
			"Generate view buffer content (view=%p)", view
		);

	// check for call to update before proper view initialization
	if ( NULL == view->window || NULL == data->view_buffer ) {
		// this should not happen but for now we will tolerate it
		TRACE_MESSAGE(VIEW, TL_MAJOR,
				"Updating without initialization (view=%p)", view
			);
		return OK;
	}

	// try to load image from model
	if ( NULL != view->model ) {
		image = model_iml_get_image(view->model);

		if ( NULL == image ) {
			TRACE_MESSAGE(VIEW, TL_MAJOR,
					"Updating view without image (view=%p, model=%p, image=%p)",
					view, view->model, image
				);
			gdk_pixbuf_fill( data->view_buffer, 0x000000ff);
			gdk_window_invalidate_rect(view->window, &(view->region), FALSE);
			return OK;
		}
	}

	data->dimension.x = gdk_pixbuf_get_width(image);
	data->dimension.y = gdk_pixbuf_get_height(image);

	is_change |= view_idv_compute_factor(view, data);
	is_change |= view_idv_compute_viewport(view, data);

	if ( is_change || force) {
		// composite image to the internal view buffer
		gdk_pixbuf_fill( data->view_buffer, 0x000000ff);
		gdk_pixbuf_composite(image, data->view_buffer,
				// destination position
				data->viewport.x, data->viewport.y,
				// destination dimension
				data->viewport.width, data->viewport.height,
				// offsets
				data->viewport.x - data->offset.x,
				data->viewport.y - data->offset.y,
				// zoom in both axes
				data->zoom_factor, data->zoom_factor,
				GDK_INTERP_BILINEAR, 255
			);

		// invalidate the image view region
		gdk_window_invalidate_rect(view->window, &(view->region), FALSE);
	}

	return OK;
}

/**
 * Compute new zoom from image dimensions and zoom type stored in image display
 * structure and return whenever the zoom has changed from original value.
 *
 * \param view pointer to image display view structure
 * \param data pointer to image display view data
 * \return TRUE if zoom has changed
 */
gboolean view_idv_compute_factor(ViewGeneric* view, ViewImageDisplay *data) {
	gdouble tmp_zoom1, tmp_zoom2;
	gboolean result;

	ASSERT( NULL != view );
	ASSERT( NULL != data );

	// compute zoom factor to be used in recomputation
	switch ( data->zoom_type ) {
	case ZOOM_FIT_CUSTOM:
		tmp_zoom1 = data->zoom_factor;
		break;
	case ZOOM_FIT_WIDTH:
		tmp_zoom1 = (gdouble)view->region.width / (gdouble)data->dimension.x;
		break;
	case ZOOM_FIT_HEIGHT:
		tmp_zoom1 = (gdouble)view->region.height / (gdouble)data->dimension.y;
		break;
	case ZOOM_FIT_IMAGE:
		tmp_zoom1 = (gdouble)view->region.width / (gdouble)data->dimension.x;
		tmp_zoom2 = (gdouble)view->region.height / (gdouble)data->dimension.y;

		// get lower value to achieve greater scale down or lesser scale up
		tmp_zoom1 = ( tmp_zoom1 < tmp_zoom2 ) ? tmp_zoom1 : tmp_zoom2;
		break;

	default:
		tmp_zoom1 = data->zoom_factor;
	}

	tmp_zoom1 = ( tmp_zoom1 < VIEW_ZOOM_MIN ) ? VIEW_ZOOM_MIN : tmp_zoom1;
	tmp_zoom1 = ( tmp_zoom1 > VIEW_ZOOM_MAX ) ? VIEW_ZOOM_MAX : tmp_zoom1;

	result = ! ZOOM_EQUAL(tmp_zoom1, data->zoom_factor);

	data->zoom_factor = tmp_zoom1;

	TRACE_MESSAGE(VIEW, TL_DEBUG,
			"Zoom factor has changed to (view=%p, zoom=%f)",
			view, data->zoom_factor
		);

	return result;
}
/**
 * The function computes new zoom and new dimensions of viewport region from
 * zoom type, zoom factor and image dimensions. Sets the new values into the
 * display view structure and returns true if new values are different from old
 * ones.
 *
 * Note: The position of viewport is relative to application screen (not view
 * region).
 *
 * \param view pointer to image display view structure
 * \param data pointer to image display view data
 * \return TRUE if vieport or zoom has changed
 */
gboolean view_idv_compute_viewport(ViewGeneric* view, ViewImageDisplay *data) {
	GdkRectangle tmp_viewport;
	gboolean result = FALSE;

	ASSERT( NULL != view );
	ASSERT( NULL != data );

	// compute width for output view rectangle
	if ( view->region.width <= data->zoom_factor * data->dimension.x ) {
		tmp_viewport.width = view->region.width;
		tmp_viewport.x = view->region.x;
	} else {
		tmp_viewport.width = (gint)(data->zoom_factor * data->dimension.x);
		tmp_viewport.x = view->region.x + (gint)( (view->region.width - tmp_viewport.width) / 2 );
	}

	// compute height of output view rectangle
	if ( view->region.height <= data->zoom_factor * data->dimension.y ) {
		tmp_viewport.height = view->region.height;
		tmp_viewport.y = view->region.y;
	} else {
		tmp_viewport.height = (gint)(data->zoom_factor * data->dimension.y);
		tmp_viewport.y = view->region.y + (gint)( (view->region.height - tmp_viewport.height) / 2 );
	}

	// check if viewport has changed
	result = tmp_viewport.x != data->viewport.x
		|| tmp_viewport.y != data->viewport.y
		|| tmp_viewport.width != data->viewport.width
		|| tmp_viewport.height != data->viewport.height;

	data->viewport = tmp_viewport;

	return result;
}

/**
 * Compute new offset from supplied direction and gc constant
 * and set it into image view structure. Return whenever the offset changed or
 * not.
 *
 * \param view pointer to image display view structure
 * \param data pointer to image display view data
 * \param direction dirction in which to pan image
 * \return TRUE if offset has changed
 */
gboolean view_idv_compute_offset(ViewGeneric* view, ViewImageDisplay *data, PanDirection direction) {
	gint limit, offset, delta;
	gint polarity = 1;

	ASSERT( NULL != view );
	ASSERT( NULL != data );
	switch (direction) {
		case PAN_UP:
			// only change polarity of following position shift
			polarity = -1;

		case PAN_DOWN:
			// compute maximum offset in y axis
			limit = data->dimension.y * data->zoom_factor - view->region.height;

			// compute actual shift in y value
			offset = data->offset.y
				+ polarity * (gint)round(data->pan_percentage * view->region.height);

			// limit shift in x value to <0, limit.x> limits
			offset = ( offset > limit ) ? limit : offset;
			offset = ( offset < 0 ) ? 0 : offset;

			delta = data->offset.y - offset;
			data->offset.y = offset;
			break;

		case PAN_LEFT:
			// only change polarity of following position shift
			polarity = -1;

		case PAN_RIGHT:
			// compute maximum offset in x axis
			limit = data->dimension.x * data->zoom_factor - view->region.width;

			// compute actual shift in x value
			offset = data->offset.x + polarity *
					(gint)round(data->pan_percentage * view->region.width);

			// limit shift in x value to <0, limit.x> limits
			offset = ( offset > limit ) ? limit : offset;
			offset = ( offset < 0 ) ? 0 : offset;

			delta = data->offset.x - offset;
			data->offset.x = offset;
			break;

		default:
			delta = 0;
	}

	TRACE_MESSAGE(VIEW, TL_DEBUG,
			"Panning the image (view=%p, direction=%d, delta=%d)",
			view, direction, delta
		);
	return 0 != delta;
}

/**
 * Compute new zoom factor and offset from supplied zoom direction. Return true
 * when offset or zoom has changed.
 *
 * When computing offset, multiply original value by zoom constant to keep top
 * left corner on the same position of image.
 *
 * \todo: Shift computation formula is known (see below), but not works
 * properly. This must be finished before comitting.
 *
 * Then, compute difference between zoom factors to shift the position so that
 * the region of image that is being displayed has same center as original
 * region.
 *
 * The formula for shift is : dimension / 2 * ( new_factor - old_factor)
 *
 * \param view pointer to image display view structure
 * \param data pointer to image display view data
 * \param direction direction in which to zoom an image
 * \return TRUE if offset has changed
 */
gboolean view_idv_compute_zoom(ViewGeneric* view, ViewImageDisplay *data, ZoomDirection direction) {
	GdkPoint offset;
	gboolean result = FALSE;
	gint limit;
	gdouble factor;

	// change zoom type to custom on any attempt to zoom
	if ( ZOOM_FIT_CUSTOM != data->zoom_type ) {
		data->zoom_type = ZOOM_FIT_CUSTOM;
		result = TRUE;
	}

	if ( ZOOM_DIRECTION_RESET == direction ) {
		factor = 1.0f;
		offset.x = 0;
		offset.y = 0;

	} else {
		gint shift = 0;
		gdouble multi;

		// compute multiplier used for new zoom factor computations
		multi = (ZOOM_DIRECTION_IN == direction) ? VIEW_ZOOM_IN : VIEW_ZOOM_OUT;
		factor = data->zoom_factor * multi;
		factor = ( factor < VIEW_ZOOM_MIN ) ? VIEW_ZOOM_MIN : factor;
		factor = ( factor > VIEW_ZOOM_MAX ) ? VIEW_ZOOM_MAX : factor;

		// compute offset shifts
//		shift = (gint)round(data->dimension.x / 2 * (  factor - data->zoom_factor ));
		offset.x = data->offset.x * multi + shift;

//		shift = (gint)round(data->dimension.y / 2 * (  factor - data->zoom_factor ));
		offset.y = data->offset.y * multi + shift;
	}

	// check if offsets are still in limit - the upper limit must be checked
	// first as sometimes it can be computed negative and it is necessary to
	// clip it back to zero
	limit = data->dimension.y * factor - data->viewport.height;
	offset.y = ( offset.y > limit ) ? limit : offset.y;
	offset.y = ( offset.y < 0 ) ? 0 : offset.y;

	limit = data->dimension.x * factor - data->viewport.width;
	offset.x = ( offset.x > limit ) ? limit : offset.x;
	offset.x = ( offset.x < 0 ) ? 0 : offset.x;

	// compute return value
	result = result
		|| ( ! ZOOM_EQUAL(factor, data->zoom_factor))
		|| offset.x != data->offset.x
		|| offset.y != data->offset.y;

	data->zoom_factor = factor;
	data->offset.x = offset.x;
	data->offset.y = offset.y;

	TRACE_MESSAGE(VIEW, TL_DEBUG, "Zoom setup has changed to (view=%p, factor=%f, type=%d)",
			view, data->zoom_factor, data->zoom_type
		);

	return result;
}

/**
 * Update view buffer with new animation frame data.
 *
 * The functino does not update any display related variable (like offset or
 * viewport) since all animation frames have same dimensions and we want
 * animation to play with given zoom.
 *
 * It is called automatically from timeout installed by g_timeout_add()
 *
 * \todo track if somebody changed image by utilizing sequence number
 *
 * \param data pointer to ViewImageDisplay structure cast to gpointer
 * \return FALSE
 */
gboolean view_idv_update_frame(gpointer data) {
	FrameUpdate *update = (FrameUpdate*) data;
	ViewImageDisplay* idv_data = NULL;
	gint delay = -1;
	GdkPixbuf* image = NULL;

	if ( update->sequence != update->view->last_sequence ) {
		g_free( (gpointer)data );
		return FALSE;
	}

	idv_data = view_idv_get_data_pointer(update->view);
	if ( NULL == idv_data ) {
		g_free( (gpointer)data );
		return FALSE;
	}

	// composite image to the internal view buffer
	image = model_iml_get_image(update->view->model);
	if ( NULL  == image ) {
		ERROR_LOG();
		g_free( (gpointer)data );
		return FALSE;
	}

	gdk_pixbuf_fill( idv_data->view_buffer, 0x000000ff);
	gdk_pixbuf_composite(image, idv_data->view_buffer,
			// destination position
			idv_data->viewport.x, idv_data->viewport.y,
			// destination dimension
			idv_data->viewport.width, idv_data->viewport.height,
			// offsets
			idv_data->viewport.x - idv_data->offset.x,
			idv_data->viewport.y - idv_data->offset.y,
			// zoom in both axes
			idv_data->zoom_factor, idv_data->zoom_factor,
			GDK_INTERP_BILINEAR, 255
		);

	// invalidate the image view region
	gdk_window_invalidate_rect(update->view->window, &(update->view->region), FALSE);

	// check and install new frame update timeout
	delay = model_iml_get_delay(update->view->model);
	if ( -1 != delay ) {
		g_timeout_add(delay, view_idv_update_frame, (gpointer)update);
	} else {
		g_free( (gpointer)data );
	}

	return FALSE;
}
