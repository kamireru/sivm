/*
 * Copyright (C) 2012 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __FRONTEND_CNTL_KEYMAP_H__
#define __FRONTEND_CNTL_KEYMAP_H__

#include <gdk/gdkkeysyms.h>

typedef struct {
	// generic events for forming repeat count parsing range
	GdkEventKey number_0;
	GdkEventKey number_9;

	// generic events to ignore when parsing repeat count
	GdkEventKey number_ignore;

	// generic events for going into four major directions
	// cntl_image_display.c - panning the image on screen
	// cntl_container.c     - changing node that receives keyboard events
	GdkEventKey up;
	GdkEventKey down;
	GdkEventKey left;
	GdkEventKey right;

	// generic events forming event range for group identification
	GdkEventKey group_id_special;
	GdkEventKey group_id_a;
	GdkEventKey group_id_z;

	// application controller events
	GdkEventKey quit;
	GdkEventKey fullscreen;

	// image list controller events
	GdkEventKey next;
	GdkEventKey prev;
	GdkEventKey first;
	GdkEventKey last;
	GdkEventKey offset;

	GdkEventKey zoom_in;
	GdkEventKey zoom_out;
	GdkEventKey zoom_reset;

	GdkEventKey zoom_mode;
	GdkEventKey zoom_type_custom;
	GdkEventKey zoom_type_image;
	GdkEventKey zoom_type_width;
	GdkEventKey zoom_type_height;

	GdkEventKey group_mode;
	GdkEventKey group_copy;
	GdkEventKey group_move;
	GdkEventKey group_select;

	// container controller evens
	GdkEventKey container_mode;

} CntlKeymap;

extern CntlKeymap cntl_keymap[2];

#endif
