/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stddef.h>
#include <gdk/gdkkeysyms.h>

#include "src/fa/fa.h"

#include "src/frontend/cntl_common.h"
#include "src/frontend/cntl_keymap.h"

#include "src/frontend/cntl.h"
#include "src/frontend/view.h"
#include "src/frontend/model.h"

/**
 * The local enumeration naming nodes for easier understanding of the automaton
 * definition.
 */
enum CntlImageDisplayNode {
		CNTL_IDV_INIT = 0,
		CNTL_IDV_COUNT = 1,

		CNTL_IDV_MOVE = 2,

		CNTL_IDV_ZOOM_PREFIX = 3,
		CNTL_IDV_ZOOM_SELECT = 4,

		CNTL_IDV_ZOOM = 5,

		CNTL_IDV_PAN = 6,

		CNTL_IDV_GROUP_PREFIX  = 7,
		CNTL_IDV_ACTION_SELECT = 8,
		CNTL_IDV_GROUP_SELECT  = 9
	};

static char* cntl_idv_node_name[] = {
		"INIT",
		"COUNT",
		"MOVE",
		"ZOOM MODE",
		"ZOOM SELECTED",
		"ZOOM",
		"PAN",
		"GROUP MODE",
		"ACTION SELECTED",
		"GROUP SELECTED"
	};

/**
 * The function resets image display controller specific data before each new
 * automaton run.
 *
 * \param data pointer to CntlGeneric structure of the whole controller
 */
void cntl_idv_data_reset(void *data) {
	CntlGeneric* controller = (CntlGeneric*)data;
	CntlImageDisplayData *spec_data = (CntlImageDisplayData*) controller->specific_data;

	spec_data->count = 0;
	spec_data->movement_type = MOVE_INDEX_FIRST;
	spec_data->auto_zoom = ZOOM_FIT_CUSTOM;
	spec_data->zoom_type = ZOOM_DIRECTION_RESET;
	spec_data->pan_direction = PAN_UP;
	spec_data->group_id = 0;
	spec_data->group_action = GROUP_SELECT;
	return;
}

/**
 * The function adds new decimal place to the count value stored in controller
 * specific data.
 *
 * \param from index of node from which the transition went
 * \param to index of node to which the transition went
 * \param value pointer to key structure that triggered transition
 * \param data pointer to CntlImageDisplayData
 */
void cntl_idv_parse_count(unsigned int from, unsigned int to, void* value, void* data) {
	CntlGeneric* controller = (CntlGeneric*)data;
	CntlImageDisplayData *spec_data = (CntlImageDisplayData*) controller->specific_data;
	GdkEventKey *key = (GdkEventKey*)value;

	if ( GDK_0 <= key->keyval && key->keyval <= GDK_9 ) {
		spec_data->count = spec_data->count * 10 + (key->keyval - GDK_0);
	} else {
		spec_data->count = spec_data->count * 10 + (key->keyval - GDK_KP_0);
	}
	TRACE_MESSAGE(CNTL, TL_MINOR, "Parse prefix count value (value=%d)", spec_data->count);

	return;
}

/**
 * The function decides which direction to move using key value that triggered
 * transition and stores result into specific data structure.
 *
 * It also makes sure that repetition counter is set to meaningfull value in
 * case user did not privide user count in key stroke sequence.
 *
 * \param from index of node from which the transition went
 * \param to index of node to which the transition went
 * \param value pointer to key structure that triggered transition
 * \param data pointer to CntlImageDisplayData
 */
void cntl_idv_parse_movement(unsigned int from, unsigned int to, void* value, void* data) {
	CntlGeneric* controller = (CntlGeneric*)data;
	CntlImageDisplayData *spec_data = (CntlImageDisplayData*) controller->specific_data;
	GdkEventKey *key = (GdkEventKey*)value;

	switch (key->keyval) {
		case GDK_n:
		case GDK_Page_Down:
			spec_data->movement_type = MOVE_INDEX_NEXT;
			break;

		case GDK_p:
		case GDK_Page_Up:
			spec_data->movement_type = MOVE_INDEX_PREV;
			break;

		case GDK_P:
		case GDK_Home:
			spec_data->movement_type = MOVE_INDEX_FIRST;
			break;

		case GDK_N:
		case GDK_End:
			spec_data->movement_type = MOVE_INDEX_LAST;
			break;

		case GDK_M:
			spec_data->movement_type = MOVE_INDEX_INDEX;
			break;
	}

	if ( 0 == spec_data->count ) {
		if ( MOVE_INDEX_INDEX == spec_data->movement_type ) {
			spec_data->count = 0;
		} else {
			spec_data->count = 1;
		}
	}

	TRACE_MESSAGE(CNTL, TL_MINOR, "Parse move type value (value=%d)", spec_data->movement_type);
	return;
}

/**
 * The function decides which zoom fit type to set by using key value that triggered
 * transition. It stores result into specific data structure.
 *
 * \param from index of node from which the transition went
 * \param to index of node to which the transition went
 * \param value pointer to key structure that triggered transition
 * \param data pointer to CntlImageDisplayData
 */
void cntl_idv_parse_auto_zoom(unsigned int from, unsigned int to, void* value, void* data) {
	CntlGeneric* controller = (CntlGeneric*)data;
	CntlImageDisplayData *spec_data = (CntlImageDisplayData*) controller->specific_data;
	GdkEventKey *key = (GdkEventKey*)value;

	switch (key->keyval) {
		case GDK_i:
			spec_data->auto_zoom = ZOOM_FIT_IMAGE;
			break;

		case GDK_w:
			spec_data->auto_zoom = ZOOM_FIT_WIDTH;
			break;

		case GDK_h:
			spec_data->auto_zoom = ZOOM_FIT_HEIGHT;
			break;

		case GDK_c:
			spec_data->auto_zoom = ZOOM_FIT_CUSTOM;
			break;

	}

	TRACE_MESSAGE(CNTL, TL_MINOR, "Parse zoom fit type value (value=%d)", spec_data->auto_zoom);
	return;
}

/**
 * The function decides which zoom direction to use by checking key value that triggered
 * transition. It stores result into specific data structure.
 *
 * \param from index of node from which the transition went
 * \param to index of node to which the transition went
 * \param value pointer to key structure that triggered transition
 * \param data pointer to CntlImageDisplayData
 */
void cntl_idv_parse_zoom(unsigned int from, unsigned int to, void* value, void* data) {
	CntlGeneric* controller = (CntlGeneric*)data;
	CntlImageDisplayData *spec_data = (CntlImageDisplayData*) controller->specific_data;
	GdkEventKey *key = (GdkEventKey*)value;

	switch (key->keyval) {
		case GDK_i:
			spec_data->zoom_type = ZOOM_DIRECTION_IN;
			break;

		case GDK_o:
			spec_data->zoom_type = ZOOM_DIRECTION_OUT;
			break;

		case GDK_r:
			spec_data->zoom_type = ZOOM_DIRECTION_RESET;
			break;
	}

	TRACE_MESSAGE(CNTL, TL_MINOR, "Parse zoom fit type value (value=%d)", spec_data->zoom_type);
	return;
}

/**
 * The function decides which direction to pan the image by checking key value
 * that triggered transition. It stores result into specific data structure.
 *
 * \param from index of node from which the transition went
 * \param to index of node to which the transition went
 * \param value pointer to key structure that triggered transition
 * \param data pointer to CntlImageDisplayData
 */
void cntl_idv_parse_pan(unsigned int from, unsigned int to, void* value, void* data) {
	CntlGeneric* controller = (CntlGeneric*)data;
	CntlImageDisplayData *spec_data = (CntlImageDisplayData*) controller->specific_data;
	GdkEventKey *key = (GdkEventKey*)value;

	switch (key->keyval) {
		case GDK_Up:
		case GDK_k:
			spec_data->pan_direction = PAN_UP;
			break;

		case GDK_Down:
		case GDK_j:
			spec_data->pan_direction = PAN_DOWN;
			break;

		case GDK_Left:
		case GDK_h:
			spec_data->pan_direction = PAN_LEFT;
			break;

		case GDK_Right:
		case GDK_l:
			spec_data->pan_direction = PAN_RIGHT;
			break;
	}

	TRACE_MESSAGE(CNTL, TL_MINOR, "Parse pan direction value (value=%d)", spec_data->pan_direction);
	return;
}

/**
 * The function decides which group action to proceeed - either move, copy
 * images or change group associated with image display view.
 *
 * \param from index of node from which the transition went
 * \param to index of node to which the transition went
 * \param value pointer to key structure that triggered transition
 * \param data pointer to CntlImageDisplayData
 */
void cntl_idv_parse_action(unsigned int from, unsigned int to, void* value, void* data) {
	CntlGeneric* controller = (CntlGeneric*)data;
	CntlImageDisplayData *spec_data = (CntlImageDisplayData*) controller->specific_data;
	GdkEventKey *key = (GdkEventKey*)value;

	switch (key->keyval) {
		case GDK_s:
			spec_data->group_action = GROUP_SELECT;
			break;

		case GDK_c:
			spec_data->group_action = GROUP_COPY;
			break;

		case GDK_m:
			spec_data->group_action = GROUP_MOVE;
			break;
	}

	TRACE_MESSAGE(CNTL, TL_MINOR, "Parse group action value (value=%d)", spec_data->group_action);
	return;
}

/**
 * The function parses out character identification of the group that we shall
 * act upon (valid values are [a-zA-Z*]).
 *
 * \param from index of node from which the transition went
 * \param to index of node to which the transition went
 * \param value pointer to key structure that triggered transition
 * \param data pointer to CntlImageDisplayData
 */
void cntl_idv_parse_group(unsigned int from, unsigned int to, void* value, void* data) {
	CntlGeneric* controller = (CntlGeneric*)data;
	CntlImageDisplayData *spec_data = (CntlImageDisplayData*) controller->specific_data;
	GdkEventKey *key = (GdkEventKey*)value;

	if ( GDK_asterisk == key->keyval ) {
		spec_data->group_id = '*';

	} else if ( GDK_a <= key->keyval && key->keyval <= GDK_z ) {
		spec_data->group_id = 'a' + (key->keyval - GDK_a);

	} else if ( GDK_A <= key->keyval && key->keyval <= GDK_Z ) {
		spec_data->group_id = 'A' + (key->keyval - GDK_A);
	}

	TRACE_MESSAGE(CNTL, TL_MINOR, "Parse group character id (value='%c')", spec_data->group_id);
	return;
}

/**
 * The function invoked when node that is associated with some action is
 * reached. It checks in which node the automaton is and then invokes
 * appropriate action on the associated model.
 *
 * \param node index of node into which the automaton transitioned
 * \param value that triggered the transition to node
 * \param data pointer to data associated with automaton run
 */
static void cntl_idv_action(unsigned int node, void *value, void* data) {
	CntlGeneric* controller = (CntlGeneric*)data;
	CntlImageDisplayData *spec_data = (CntlImageDisplayData*) controller->specific_data;
	ZoomType type = ZOOM_FIT_CUSTOM;
	ZoomDirection direction = ZOOM_DIRECTION_RESET;
	RCode result = FAIL;

	ASSERT( NULL != controller );

	switch (node) {
		case CNTL_IDV_MOVE:
			TRACE_MESSAGE(CNTL, TL_INFO,
					"Request for image index change in list (cntl=%p, type=%d, value=%d)",
					controller, spec_data->movement_type, spec_data->count
				);

			// convert index from user to internal representation
			// (user index starts from 1, internal index starts from 0)
			if ( MOVE_INDEX_INDEX == spec_data->movement_type ) {
					spec_data->count = ( spec_data->count == 0 ) ? 0 : spec_data->count - 1;
			}
			result = model_iml_move_index(
					view_get_model(controller->view),
					spec_data->movement_type, spec_data->count
				);
			break;

		case CNTL_IDV_ZOOM_SELECT:
			TRACE_MESSAGE(CNTL, TL_INFO,
					"Request to change zoom fit type (cntl=%p, type=%d)",
					controller, spec_data->auto_zoom
				);
			result = view_idv_set_zoom(controller->view, spec_data->auto_zoom);
			break;

		case CNTL_IDV_ZOOM:
			TRACE_MESSAGE(CNTL, TL_INFO,
					"Request to change zoom factor (cntl=%p, direction=%d)",
					controller, spec_data->zoom_type
				);
			result = view_idv_zoom(controller->view, spec_data->zoom_type);
			break;

		case CNTL_IDV_PAN:
			TRACE_MESSAGE(CNTL, TL_DEBUG,
					"Request to pan image (cntl=%p, direction=%d)",
					controller, spec_data->pan_direction
				);
			result = view_idv_pan(controller->view, spec_data->pan_direction);
			break;
	}

	// check for error in some of the model specific function calls
	if ( FAIL == result ) {
		ERROR_LOG();
	}

	return;
}

/**
 * The function is invoked when group action related final node is reached. It
 * handles group change and copy, move images to different group.
 *
 * \param node index of node into which the automaton transitioned
 * \param value that triggered the transition to node
 * \param data pointer to data associated with automaton run
 */
static void cntl_idv_group_action(unsigned int node, void *value, void* data) {
	CntlGeneric* controller = (CntlGeneric*)data;
	CntlImageDisplayData *spec_data = (CntlImageDisplayData*) controller->specific_data;
	ZoomType type = ZOOM_FIT_CUSTOM;
	ZoomDirection direction = ZOOM_DIRECTION_RESET;
	RCode result = FAIL;
	ImageGroup* group = NULL;

	ASSERT( NULL != controller );
	ASSERT( node == CNTL_IDV_GROUP_SELECT );

	group = model_iml_load_group(spec_data->group_id);
	if ( error_is_error() ) {
		ERROR_LOG();
		return;
	}

	switch ( spec_data->group_action ) {
		case GROUP_SELECT:
			TRACE_MESSAGE(CNTL, TL_INFO,
					"Request to change displayed image group (cntl=%p, group='%c')",
					controller, spec_data->group_id
				);
			result = model_iml_set_image_group( view_get_model(controller->view), group );
			break;

		case GROUP_COPY:
			spec_data->count = ( 0 == spec_data->count ) ? 1 : spec_data->count;
			TRACE_MESSAGE(CNTL, TL_INFO,
					"Request to copy images into image group (cntl=%p, group='%c', count='%d')",
					controller, spec_data->group_id, spec_data->count
				);
			result = model_iml_transfer_image( view_get_model(controller->view), group, spec_data->count, FALSE);
			break;

		case GROUP_MOVE:
			spec_data->count = ( 0 == spec_data->count ) ? 1 : spec_data->count;
			TRACE_MESSAGE(CNTL, TL_INFO,
					"Request to move images into image group (cntl=%p, group='%c', count='%d')",
					controller, spec_data->group_id, spec_data->count
				);
			result = model_iml_transfer_image( view_get_model(controller->view), group, spec_data->count, TRUE);
			break;
	}

	// check for error in some of the model specific function calls
	if ( FAIL == result ) {
		ERROR_LOG();
	}

	return;
}


/**
 * The function creates automaton implementing logic of application controller.
 *
 * \return pointer to automaton structure
 */
FaAutomaton* cntl_idv_create_automaton() {
	FaAutomaton* result = fa_create(cntl_idv_data_reset, cntl_key_compare, 10,
		fa_node_create(CNTL_IDV_INIT, FA_NODE_START, NULL, 6,
			fa_transition_create(CNTL_IDV_COUNT, cntl_idv_parse_count, 2,
				fa_set_create(FA_SET_RANGE, &cntl_keymap[0].number_0, &cntl_keymap[0].number_9),
				fa_set_create(FA_SET_RANGE, &cntl_keymap[1].number_0, &cntl_keymap[1].number_9)
			),
			fa_transition_create(CNTL_IDV_MOVE, cntl_idv_parse_movement, 1,
				fa_set_create(FA_SET_LIST, 9,
					&cntl_keymap[0].next, &cntl_keymap[1].next,
					&cntl_keymap[0].prev, &cntl_keymap[1].prev,
					&cntl_keymap[0].first, &cntl_keymap[1].first,
					&cntl_keymap[0].last, &cntl_keymap[1].last,
					&cntl_keymap[0].offset
				)
			),
			fa_transition_create(CNTL_IDV_ZOOM_PREFIX, NULL, 1,
				fa_set_create(FA_SET_LIST, 1, &cntl_keymap[0].zoom_mode)
			),
			fa_transition_create(CNTL_IDV_ZOOM, cntl_idv_parse_zoom, 1,
				fa_set_create(FA_SET_LIST, 3,
					&cntl_keymap[0].zoom_in, &cntl_keymap[0].zoom_out, &cntl_keymap[0].zoom_reset
				)
			),
			fa_transition_create(CNTL_IDV_PAN, cntl_idv_parse_pan, 1,
				fa_set_create(FA_SET_LIST, 8,
					&cntl_keymap[0].up, &cntl_keymap[1].up,
					&cntl_keymap[0].down, &cntl_keymap[1].down,
					&cntl_keymap[0].left, &cntl_keymap[1].left,
					&cntl_keymap[0].right, &cntl_keymap[1].right
				)
			),
			fa_transition_create(CNTL_IDV_GROUP_PREFIX, NULL, 1,
				fa_set_create(FA_SET_LIST, 2, &cntl_keymap[0].group_mode, &cntl_keymap[1].group_mode)
			)
		),

		// node for parsing count prefix
		fa_node_create(CNTL_IDV_COUNT, FA_NODE_INNER, NULL, 4,
			fa_transition_create(CNTL_IDV_COUNT, cntl_idv_parse_count, 2,
				fa_set_create(FA_SET_RANGE, &cntl_keymap[0].number_0, &cntl_keymap[0].number_9),
				fa_set_create(FA_SET_RANGE, &cntl_keymap[1].number_0, &cntl_keymap[1].number_9)
			),
			fa_transition_create(CNTL_IDV_COUNT, NULL, 1,
				fa_set_create(FA_SET_LIST, 2,
					&cntl_keymap[0].number_ignore, &cntl_keymap[1].number_ignore
				)
			),
			fa_transition_create(CNTL_IDV_MOVE, cntl_idv_parse_movement, 1,
				fa_set_create(FA_SET_LIST, 9,
					&cntl_keymap[0].next, &cntl_keymap[1].next,
					&cntl_keymap[0].prev, &cntl_keymap[1].prev,
					&cntl_keymap[0].first, &cntl_keymap[1].first,
					&cntl_keymap[0].last, &cntl_keymap[1].last,
					&cntl_keymap[0].offset
				)
			),
			fa_transition_create(CNTL_IDV_GROUP_PREFIX, NULL, 1,
				fa_set_create(FA_SET_LIST, 2, &cntl_keymap[0].group_mode, &cntl_keymap[1].group_mode)
			)
		),

		// final node that handles movement in the image list
		fa_node_create(CNTL_IDV_MOVE, FA_NODE_FINAL, cntl_idv_action, 0),

		// nodes for changing zoom type
		fa_node_create(CNTL_IDV_ZOOM_PREFIX, FA_NODE_INNER, NULL, 1,
			fa_transition_create(CNTL_IDV_ZOOM_SELECT, cntl_idv_parse_auto_zoom, 1,
				fa_set_create(FA_SET_LIST, 4,
					&cntl_keymap[0].zoom_type_custom, &cntl_keymap[0].zoom_type_image,
					&cntl_keymap[0].zoom_type_width, &cntl_keymap[0].zoom_type_height
				)
			)
		),
		fa_node_create(CNTL_IDV_ZOOM_SELECT, FA_NODE_FINAL, cntl_idv_action, 0),

		// node for changing zoom factor
		fa_node_create(CNTL_IDV_ZOOM, FA_NODE_FINAL, cntl_idv_action, 0),

		// node for panning the image
		fa_node_create(CNTL_IDV_PAN, FA_NODE_FINAL, cntl_idv_action, 0),

		// node(s) for group action and id parsing
		fa_node_create(CNTL_IDV_GROUP_PREFIX, FA_NODE_INNER, NULL, 1,
			fa_transition_create(CNTL_IDV_ACTION_SELECT, cntl_idv_parse_action, 1,
				fa_set_create(FA_SET_LIST, 3,
					&cntl_keymap[0].group_copy, &cntl_keymap[0].group_move,
					&cntl_keymap[0].group_select
				)
			)
		),
		fa_node_create(CNTL_IDV_ACTION_SELECT, FA_NODE_INNER, NULL, 2,
			fa_transition_create(CNTL_IDV_GROUP_SELECT, cntl_idv_parse_group, 3,
				fa_set_create(FA_SET_RANGE, &cntl_keymap[0].group_id_a, &cntl_keymap[0].group_id_z),
				fa_set_create(FA_SET_LIST, 2, &cntl_keymap[0].group_id_special, &cntl_keymap[1].group_id_special),
				fa_set_create(FA_SET_RANGE, &cntl_keymap[1].group_id_a, &cntl_keymap[1].group_id_z)
			),
			fa_transition_create(CNTL_IDV_ACTION_SELECT, NULL, 1,
				fa_set_create(FA_SET_LIST, 2,
					&cntl_keymap[0].number_ignore, &cntl_keymap[1].number_ignore
				)
			)
		),
		fa_node_create(CNTL_IDV_GROUP_SELECT, FA_NODE_FINAL, cntl_idv_group_action, 0)
	);

	if ( NULL == result ) {
		ERROR_SET(ERROR.CREATION_FAILURE);
		ERROR_NOTE("Creation of application controller automaton has failed");
	}

	return result;
}

/**
 * Currently, there is no specific data for application controller. Thus the
 * function returns NULL.
 *
 * Note: Once there are some data, it is necessary to impleemnt correct reset
 * function for automaton. Currenlty, it does nothing.
 *
 * \return void pointer to created automaton data
 */
void* cntl_idv_create_data() {
	CntlImageDisplayData* data = NULL;

	data = (CntlImageDisplayData*) g_try_malloc0(sizeof(CntlImageDisplayData));
	if ( NULL == data ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return NULL;
	} else {
		return data;
	}
}

/**
 * The image display controller does not forward any input. It either maches
 * itself or does not match at all.
 *
 * \param cntl pointer to generic controller structure
 * \param input key code of use input
 * \return see CntlProcessResult for return code explanation
 */
CntlProcessResult cntl_idv_process(CntlGeneric *cntl, GdkEventKey *input) {
	char * node_name = "";

	ASSERT( NULL != cntl );
	ASSERT( NULL != input );

	TRACE_MESSAGE(CNTL, TL_INFO, "Process key (cntl=%p, key=%s)",
			cntl, gdk_keyval_name(input->keyval)
		);

	// feed input to the automaton
	// we do not check return value here since we there is much more complex
	// checking later
	fa_runner_step(cntl->automaton, (void*)input);


	if ( NULL != cntl->automaton->state ) {
		node_name = cntl_idv_node_name[cntl->automaton->state->index];
	}

	// check result of the application automaton processing
	if ( FA_TRUE == fa_runner_is_accept(cntl->automaton) ) {
		TRACE_MESSAGE(CNTL, TL_DEBUG, "Key sequence matched (cntl=%p, key=%s, node='%s')",
				cntl, gdk_keyval_name(input->keyval), node_name
			);
		fa_runner_reset(cntl->automaton);
		return CNTL_MATCH_SUCCESS;
	}

	if ( FA_TRUE != fa_runner_is_blocked(cntl->automaton) ) {
		TRACE_MESSAGE(CNTL, TL_DEBUG, "Key sequence partial match (cntl=%p, key=%s, node='%s')",
				cntl, gdk_keyval_name(input->keyval), node_name
			);
		return CNTL_MATCH_PARTIAL;

	} else {
		TRACE_MESSAGE(CNTL, TL_DEBUG, "Key sequence match failed (cntl=%p, key=%s)",
				cntl, gdk_keyval_name(input->keyval)
			);
		fa_runner_reset(cntl->automaton);
		return CNTL_MATCH_FAIL;

	}
}
