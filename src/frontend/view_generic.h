/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file view_generic.h
 * \brief Header file with generic gui view API.
 *
 * The generic view implements something like polymorphism in C language - the
 * gui is composed from generic view building blocks, but generic view itself
 * does nothing. It just checks whats stored in it and calls appropriate
 * specific view.
 *
 * Note: The VIEW_NONE type of the view serves as dummy view for testing generic
 * functionality. It does not call any specific function, but should be able to
 * execute generic functionality without an error.
 *
 * Note: The sequence number should always be non-zero positive. The zero is
 * reserved for state where no notification was received yet.
 */
#ifndef FRONTEND_VIEW_GENERIC_H
#define FRONTEND_VIEW_GENERIC_H

#include "src/frontend/view_common.h"

/** \brief Allocate memory for view and call specific view create. */
extern ViewGeneric* view_create(ViewType type);

/** \brief Deallocate memory for view and call specific view destroy. */
extern RCode view_destroy(ViewGeneric *view);

/** \brief Increase reference counter for view. */
extern RCode view_ref(ViewGeneric *view);

/** \brief Decrease reference counter for view and destroy. */
extern RCode view_unref(ViewGeneric *view);


/** \brief Set pointer to the window. */
extern void view_set_window(ViewGeneric *view, GdkWindow *window);

/** \brief Get pointer to the window associated with view. */
extern GdkWindow* view_get_window(ViewGeneric* view);

/** \brief Associate model pointer with view. */
extern void view_set_model(ViewGeneric *view, ModelGeneric *model);

/** \brief Get pointer to model associatedw with view. */
extern ModelGeneric* view_get_model(ViewGeneric *view);

/** \brief Associate controller pointer with view. */
extern void view_set_controller(ViewGeneric *view, CntlGeneric *controller);

/** \brief Get pointer to controller associatedw with view. */
extern CntlGeneric* view_get_controller(ViewGeneric *view);

/** \brief Forward update() call to the specific view. */
extern RCode view_update(ViewGeneric *view, gint seq);

/** \brief Forward redraw() call to the specific view. */
extern RCode view_redraw(ViewGeneric *view, GdkRectangle *region);

/** \brief Update dimensions and forward call to resize() to specific view. */
extern RCode view_resize(ViewGeneric *view, GdkRectangle newRegion);

/** \brief Get region hint information. */
extern RCode view_get_hint(ViewGeneric* view, GdkPoint *hint);


// Internal functions, exported only for use in unit tests

/** \brief Create specific data structure for view type. */
extern RCode view_create_specific_data(ViewGeneric *view);

/** \brief Deallocate specific data structure for view type. */
extern RCode view_destroy_specific_data(ViewGeneric* view);

#endif
