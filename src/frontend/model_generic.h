/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file model_generic.h
 * \brief Header file with generic gui model API.
 *
 * The generic model works similarilly as generic view - it implements something
 * like polymorphism in C language.
 */
#ifndef __FRONTEND_MODEL_GENERIC_H__
#define __FRONTEND_MODEL_GENERIC_H__

#include "src/frontend/model_common.h"
#include "src/frontend/observer_list.h"

/** \brief Allocate memory for model. */
extern ModelGeneric* model_create(ModelType type);

/** \brief Deallocate memory for model. */
extern RCode model_destroy(ModelGeneric *model);

/** \brief Increase reference counter for model. */
extern RCode model_ref(ModelGeneric *model);

/** \brief Decrease reference counter for model and destroy. */
extern RCode model_unref(ModelGeneric *model);


/** \brief Add view to the list of model observers. */
extern RCode model_insert_view(ModelGeneric *model, ViewGeneric *view);

/** \brief Remove view from the list of model observers. */
extern RCode model_remove_view(ModelGeneric *model, ViewGeneric *view);

/** \brief Send notification to all assigned views. */
extern void model_update(ModelGeneric *model);


// Internal functions, exported only to be used in unit tests

/** \brief Create specific data in the model. */
extern RCode model_create_specific_data(ModelGeneric *model);

/** \brief Deallocate specific data in the model. */
extern RCode model_destroy_specific_data(ModelGeneric *model);

/** \brief Update sequence number in model. */
extern RCode model_generate_sequence(ModelGeneric *model);

#endif
