/*
 * Copyright (C) 2012 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not see <http://www.gnu.org/licenses/>.
 */
#include "src/frontend/cntl_common.h"
#include "src/frontend/cntl_keymap.h"


CntlKeymap cntl_keymap[2] = {
	// primary event map
	{
		// generic events for forming repeat count parsing range
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_0, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_9, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Shift_L, 0 ),

		// generic events for going into four major directions
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Up, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Down, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Left, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Right, 0 ),

		// generic events forming event range for group identification
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_asterisk, GDK_SHIFT_MASK ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_a, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_z, 0 ),

		// application controller events
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_q, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_f, 0 ),

		// image list controller events
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_n, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_p, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_P, GDK_SHIFT_MASK ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_N, GDK_SHIFT_MASK ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_M, GDK_SHIFT_MASK ),

		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_i, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_o, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_r, 0 ),

		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_z, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_c, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_i, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_w, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_h, 0 ),

		// group change, copy and move keywords
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_g, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_c, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_m, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_s, 0 ),

		// container controller evens
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_c, 0 )
	},

	// alternative event map
	{
		// generic events for forming repeat count parsing range
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_KP_0, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_KP_9, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Shift_R, 0 ),

		// generic events for going into four major directions
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_k, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_j, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_h, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_l, 0 ),

		// generic events forming event range for group identification
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_KP_Multiply, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_A, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Z, 0 ),

		// application controller events
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_q, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_f, 0 ),

		// image list controller events
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Page_Up, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Page_Down, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Home, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_End, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_M, GDK_SHIFT_MASK ),

		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_i, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_o, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_r, 0 ),

		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_z, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_c, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_i, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_w, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_h, 0 ),

		// group change, copy and move keywords
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_g, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_c, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_m, 0 ),
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_s, 0 ),

		// container controller evens
		CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_c, 0 )
	}
};
