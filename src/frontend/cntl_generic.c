/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "src/fa/fa.h"
#include "src/frontend/cntl.h"


/**
 * Allocate memory for generic controller structure and setup the type
 * correctly. The use of g_try_malloc0() will correctly initialize whole
 * structure to correct values.
 *
 * The create function creates controller specific data, but uses pointer to
 * whole controller as data pointer for automaton. This is because automaton may
 * need to retrieve view or model associated with controller and access the
 * specific data at one time.
 *
 * The reference count in the new structure is set to 1 to indicate that there
 * is only one place which references it - the variable storing pointer returned
 * from function.
 *
 * \param type which specific controller to create
 * \return pointer to generic controller structure
 */
CntlGeneric* cntl_create(CntlType type) {
	CntlGeneric *result = NULL;
	FaAutomaton* automaton_ptr = NULL;
	FaRunner* runner_ptr = NULL;

	// try to get automaton for specific controller type
	automaton_ptr = cntl_automaton_create(type);
	if ( NULL == automaton_ptr ) {
		ERROR_NOTE("Automaton structure retrieval failed (cntlType=%d)", type);
		return result;
	}

	// allocate memory for controller (necesary before creating runner)
	result = g_try_malloc0(sizeof(CntlGeneric));
	if ( NULL == result ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return NULL;
	}

	// try to allocate controller specific data
	result->type = type;
	if ( FAIL == cntl_create_specific_data(result) ) {
		ERROR_NOTE("Unable to create controller specific data");
		cntl_automaton_destroy(result->type);
		g_free( (gpointer)result );
		return NULL;
	}

	// try to create automaton runner
	runner_ptr = fa_runner_create(automaton_ptr, (void*)result);
	if ( NULL == runner_ptr ) {
		ERROR_SET(ERROR.CREATION_FAILURE);
		ERROR_NOTE("Automaton runner creation failed (cntlType=%d)", type);
		cntl_automaton_destroy(result->type);
		cntl_destroy_specific_data(result);
		g_free( (gpointer)result );
		return NULL;
	} else {
		result->automaton = runner_ptr;
		fa_runner_reset(runner_ptr);
	}

	// setup created structure (type must be set for specific data creation)
	result->reference = 1;
	result->view = NULL;

	return result;
}

/**
 * Deallocate memory for controller
 *
 * Both FaRunner and specific data for controller are deallocated in this
 * function. The automaton itself is not deallocated, only unreferenced
 *
 * \param cntl pointer to generic controller structure
 * \return OK on success
 */
RCode cntl_destroy(CntlGeneric *cntl) {
	ASSERT( NULL != cntl );

	// check reference count
	if ( 0 != cntl->reference ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Reference count is non-zero");
		return FAIL;
	}

	if ( NULL != cntl->automaton ) {
		// deallocate the fa runner
		fa_runner_destroy(cntl->automaton);

		// unreference automaton
		cntl_automaton_destroy(cntl->type);
	}

	// no need to check for null as deallocation function does it for us
	cntl_destroy_specific_data(cntl);

	g_free( (gpointer)cntl );
	return OK;
}

/**
 * Increase reference count of the supplied cntl.
 *
 * \param cntl pointer to generic cntl structure
 * \return OK on success
 */
RCode cntl_ref(CntlGeneric *cntl) {
	ASSERT( NULL != cntl );

	cntl->reference += 1;
	return OK;
}

/** Decrease reference counter for cntl and if reference count reaches zero,
 * then call destroy function on the cntl.
 *
 * \param cntl pointer to generic cntl structure
 * \return OK on success
 */
RCode cntl_unref(CntlGeneric *cntl) {
	ASSERT( NULL != cntl );

	if ( 0 != (--cntl->reference) ) {
		return OK;
	}

	return cntl_destroy(cntl);
}


/**
 * \param cntl pointer to generic controller structure
 * \param view pointer to view to assign to controller
 */
void cntl_set_view(CntlGeneric *cntl, ViewGeneric* view) {
	ASSERT( NULL != cntl );

	cntl->view = view;
}

/**
 * \param cntl pointer to generic controller structure
 * \return view pointer assigned to the controller
 */
extern ViewGeneric* cntl_get_view(CntlGeneric *cntl) {
	ASSERT( NULL != cntl );

	return cntl->view;
}

/**
 * Get one key code from user and feed it to the automaton implementing
 * controller logic. Depending on the result of automaton processing, proceed
 * with additional actions.
 *
 * This function is not common for all controllers, since each controller
 * handles failure/success differently. For some controllers failure mean end of
 * processing, others will try to feed same input to another controller.
 *
 * \param cntl pointer to generic controller structure
 * \param input key code of use input
 * \return see CntlProcessResult for return code explanation
 */
CntlProcessResult cntl_process(CntlGeneric *cntl, GdkEventKey *input) {
	CntlProcessResult result = CNTL_MATCH_FAIL;

	ASSERT( NULL != cntl );
	ASSERT( NULL != input );

	switch (cntl->type) {
		case CNTL_NONE:
			TRACE_MESSAGE(CNTL, TL_INFO, "Processed key (cntl=%p, key=%s)",
					cntl, gdk_keyval_name(input->keyval)
				);
			result = CNTL_MATCH_SUCCESS;
			break;

		case CNTL_APPLICATION:
			result = cntl_app_process(cntl, input);
			break;

		case CNTL_IMAGE_DISPLAY:
			result = cntl_idv_process(cntl, input);
			break;

		case CNTL_CONTAINER:
			result = cntl_cv_process(cntl, input);
			break;

		default:
			TRACE_MESSAGE(CNTL, TL_INFO, "Unsupported controller type (cntl=%p, type=%d)",
					cntl, cntl->type
				);
			result = CNTL_MATCH_FAIL;
			break;
	}
	return result;
}


/**
 * The function uses controller type to determine which data structure is
 * necessary for automaton that implements logic of controller type and creates
 * it.
 *
 * \param cntl pointer to generic controller structure
 * \return OK on success
 */
RCode cntl_create_specific_data(CntlGeneric *cntl) {
	RCode result = OK;

	ASSERT( NULL != cntl );

	switch (cntl->type) {
		case CNTL_NONE:
		case CNTL_APPLICATION:
		case CNTL_CONTAINER:
			// this type of controller does not have specific data
			cntl->specific_data = NULL;
			break;

		case CNTL_IMAGE_DISPLAY:
			cntl->specific_data = cntl_idv_create_data();
			break;

		default:
			// no reson to fail, nobody can call specific function on controller
			// of unkown type
			TRACE_MESSAGE(CNTL, TL_INFO, "Unsupported controller type (cntl=%p, type=%d)",
					cntl, cntl->type
				);
			break;
	}

	if ( error_is_error() ) {
		ERROR_NOTE("Allocation of specific data failed (cntl=%p)", cntl);
		result = FAIL;
	}
	return result;
}

/**
 * Deallocate the controller specific data using appropriate deallocation
 * function.
 *
 * \param cntl pointer to generic controller structure
 * \return OK on success
 */
extern RCode cntl_destroy_specific_data(CntlGeneric *cntl) {
	RCode result = OK;

	ASSERT( NULL != cntl );

	switch (cntl->type) {
		case CNTL_NONE:
		case CNTL_APPLICATION:
		case CNTL_CONTAINER:
			// no data to deallocate
			break;

		case CNTL_IMAGE_DISPLAY:
			g_free( (gpointer)cntl->specific_data);
			break;

		default:
			// no need to fail, since no specific data was allocated
			TRACE_MESSAGE(CNTL, TL_INFO, "Unsupported controller type (cntl=%p, type=%d)",
					cntl, cntl->type
				);
			break;
	}
	return result;
}


/**
 * The structure holds pointer to the automaton implementing controller logic
 * and number of references to that automaton.
 *
 * It is used to ensure that one automaton is created only once and destroyed
 * properly when nobody is using it.
 */
typedef struct {
		FaAutomaton* automaton;
		unsigned int ref_count;
	} CntlAutomatonInfo;

/**
 * Static list of automaton pointers - kind of implementation of list of
 * singletons.
 */
static CntlAutomatonInfo automaton_list[CNTL_LAST] = {
		{NULL, 0}, {NULL, 0}, {NULL, 0}, {NULL, 0}
	};

/**
 * The function uses type to decide which automaton creation function to use.
 * The automaton is created only when there is no automaton yet (reference count
 * is 0).
 *
 * The function relies on the functions defined for specific controllers for
 * creation of the automaton. The only automaton created in the function
 * directly is an automaton for CNTL_NONE type (dummy) accept only empty input.
 *
 * \param type controller type
 * \return pointer to automaton implementing controller type logic
 */
FaAutomaton* cntl_automaton_create(CntlType type) {

	if ( CNTL_LAST <= type ) {
		ERROR_SET(ERROR.INVALID_PARAM);
		ERROR_NOTE("No cntl automaton with type (type=%d)", type);
		return NULL;
	}

	if ( 0 != automaton_list[type].ref_count ) {
		automaton_list[type].ref_count++;
		return automaton_list[type].automaton;
	}

	FaAutomaton *result = NULL;
	switch (type) {
		case CNTL_NONE:
			// create dummy automaton for dummy controller type
			// for testing purposes only
			result = fa_create(NULL, NULL, 1, fa_node_create(1, FA_NODE_START, NULL, 0));
			break;

		case CNTL_APPLICATION:
			result = cntl_app_create_automaton();
			break;

		case CNTL_IMAGE_DISPLAY:
			result = cntl_idv_create_automaton();
			break;

		case CNTL_CONTAINER:
			result = cntl_cv_create_automaton();
			break;

		default:
			TRACE_MESSAGE(CNTL, TL_INFO, "Unsupported controller type (type=%d)", type);
			result = NULL;
	}

	if ( NULL != result ) {
		automaton_list[type].ref_count = 1;
		automaton_list[type].automaton = result;
	} else {
		TRACE_MESSAGE(CNTL, TL_INFO, "Unable to create controller automaton (type=%d)", type);
	}

	return result;
}

/**
 * The function returns number of controller instances that use automaton of the
 * specified type.
 *
 * The function is intended mostly for testing, it should not be needed in the
 * application.
 *
 * \param type controller type
 * \return number of references to the automaton of given type
 */
unsigned int cntl_automaton_count(CntlType type) {
	return automaton_list[type].ref_count;
}

/**
 * The function decreases number of references to the automaton. This call is
 * used when controller instance using automaton of supplied type is being
 * destroyed and won't be needing it anymore.
 *
 * When the number of regerences drops to zero, automaton structure is
 * deallocated.
 *
 * \param type controller type
 * \return OK on success
 */
RCode cntl_automaton_destroy(CntlType type) {
	if ( 0 == automaton_list[type].ref_count ) {
		return FAIL;
	}

	automaton_list[type].ref_count -= 1;

	if ( 0 == automaton_list[type].ref_count ) {
		fa_destroy(automaton_list[type].automaton);
		automaton_list[type].automaton = NULL;
	}
	return OK;
}
