/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "src/frontend/view_generic.h"
#include "src/frontend/view_application.h"

/**
 * Allocate memory for application view structure and initialize it with
 * correct values.
 *
 * \return pointer to application view structure
 */
ViewApplication* view_app_create(ViewGeneric *parent) {
	ViewApplication *result = NULL;

	ASSERT( NULL != parent );

	if ( NULL == ( result = (ViewApplication*) g_try_malloc0(sizeof(ViewApplication))) ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return result;
	}

	result->child = NULL;

	return result;
}


/**
 * Perform deinicialization and deallocation of application view structure.
 *
 * \param specificData pointer to application view structure
 * \return OK on success
 */
RCode view_app_destroy(ViewApplication *specificData) {

	ASSERT( NULL != specificData );

	// TODO: Consider cascading deallocation here (deallocate the child).
	// In case child deallocation will be done here, the tests need to be
	// updated!

	g_free( (gpointer)specificData );
	return OK;
}

/**
 * Forward the call to the child if some exists. This is done because the
 * application view is only wrapper for another view.
 *
 * \param view pointer to application view structure
 * \param seq sequence number describing the update
 * \return result of view_update() on child
 */
extern RCode view_app_update(ViewGeneric *view, guint seq) {
	ViewApplication* app_data = NULL;

	ASSERT( NULL != view );

	app_data = view_app_get_data_pointer(view);
	if ( NULL == app_data ) {
		return FAIL;
	}

	if ( NULL != app_data->child ) {
		return view_update(app_data->child, seq);
	} else {
		TRACE_MESSAGE(VIEW, TL_INFO,
				"Can't forward call to the child (view=%p, child=NULL)", view
			);
		return OK;
	}
}

/**
 * Forward the call to the child if some exists. This is done because the
 * application view is only wrapper for another view.
 *
 * \param view pointer to application view structure
 * \param region region of screen that need to be updated
 * \return result of view_redraw() on child
 */
RCode view_app_redraw(ViewGeneric *view, GdkRectangle *region) {
	ViewApplication* app_data = NULL;

	ASSERT( NULL != view );

	app_data = view_app_get_data_pointer(view);
	if ( NULL == app_data ) {
		return FAIL;
	}

	if ( NULL != app_data->child ) {
		return view_redraw(app_data->child, region);
	} else {
		TRACE_MESSAGE(VIEW, TL_INFO,
				"Can't forward call to the child (view=%p, child=NULL)", view
			);
		return OK;
	}
}

/**
 * The method resize updates the view internal variables to the changed region
 * size.
 *
 * Nothing fancy is done here. Application view is more of wrapper for another
 * view and is not rendered at all. Only call resize function of its child.
 *
 * \param view view to resize
 * \param newRegion dimensions of the new region
 * \return OK on success
 */
RCode view_app_resize(ViewGeneric *view, GdkRectangle newRegion) {
	ViewApplication* app_data = NULL;

	ASSERT( NULL != view );

	app_data = view_app_get_data_pointer(view);
	if ( NULL == app_data ) {
		return FAIL;
	}

	if ( NULL != app_data->child ) {
		return view_resize(app_data->child, newRegion);
	} else {
		TRACE_MESSAGE(VIEW, TL_INFO,
				"Can't forward call to the child (view=%p, child=NULL)", view
			);
		return OK;
	}
}


/**
 * Set the supplied generic view as an application view child.
 *
 * The child view pointer can be NULL since the default child pointer is NULL.
 *
 * \param view pointer to application view structure
 * \param child pointer to generic view structure
 * \return OK on success
 */
RCode view_app_set_child(ViewGeneric *view, ViewGeneric *child) {
	ViewApplication* app_data = NULL;

	ASSERT( NULL != view );

	app_data = view_app_get_data_pointer(view);
	if ( NULL == app_data ) {
		return FAIL;
	}

	// check if there is a need to remove reference
	if ( NULL != app_data->child && app_data->child != child ) {
		view_unref(app_data->child);
	}

	// check if there is a need to reference new child
	if ( NULL != child && child != app_data->child ) {
		view_ref(child);
	}

	app_data->child = child;
	return OK;
}


/**
 * Get child view pointer stored in the specific data of application view.
 *
 * Note: Caller of this function must call error_is_error() to explicitely check
 * for errors. The child view pointer can be NULL and thus it is valid value and
 * not error indication.
 *
 * \param view pointer to application view structure
 * \return pointer to child view
 */
ViewGeneric* view_app_get_child(ViewGeneric *view) {
	ViewApplication* app_data = NULL;

	ASSERT( NULL != view );

	app_data = view_app_get_data_pointer(view);
	if ( NULL == app_data ) {
		return FAIL;
	}

	return app_data->child;
}

/**
 * The function checks the type of the view and either cast the pointer to
 * correct type or return NULL to indicate error.
 *
 * This function can use NULL for error indication because application view
 * should always have specific data.
 *
 * \param view pointer to application view structure
 * \return ViewApplication pointer or NULL on error
 */
ViewApplication* view_app_get_data_pointer(ViewGeneric* view) {
	ASSERT( NULL != view );

	if ( VIEW_APPLICATION != view->type ) {
		return NULL;
	}

	if ( NULL == view->specific_data ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Specific data pointer is NULL");
		return NULL;
	}

	return (ViewApplication*) view->specific_data;
}
