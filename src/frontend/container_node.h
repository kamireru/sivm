/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file container_node.h
 *
 * The container node structure is used for organization of child views on the
 * available screen space.
 *
 * The container node tree must be acyclic, but it is up to calling code to not
 * create cycles in it. Functions for node manipulations do not ensure that no
 * node is added multiple times.
 *
 * Similar condition is that each view pointer must be present only once. And
 * again, there is no check for that and calling code must ensure it by itself.
 */
#ifndef __FRONTEND_VIEW_CONTAINER_NODE_H__
#define __FRONTEND_VIEW_CONTAINER_NODE_H__

#include "src/frontend/view_common.h"

/**
 * Enumeration for indicating container node directionality.
 */
typedef enum {
		CONTAINER_NODE = 0,
		CONTAINER_LEAF = 1
	} ContainerNodeType;

/**
 * Enumeration for indication type of the container node.
 */
typedef enum {
		NODE_HORIZONTAL = 0,
		NODE_VERTICAL = 1
	} ContainerNodeDirection ;

/**
 * \brief Structure holding information about container node.
 *
 * Holds data for each container node be it actual node or leaf. The leaf
 * contains only pointer to generic view that should be displayed in the node
 * region. The node holds list of pointers to child nodes with directional
 * marker.
 */
typedef struct container_node {

		/** \brief Pointer to the parent node. */
		struct container_node *parent;

		/** \brief Screen region reserved for node element. */
		GdkRectangle region;

		/** \brief Flag indicating if structure is leaf or node. */
		ContainerNodeType type;

		/** \brief Union with node or leaf content data. */
		union {

			struct {
				/** \brief Pointer to view contained in the leaf node. */
				ViewGeneric* view;

				/** \brief Flag indicating that there was a change to the region dimensions. */
				gboolean dirty;
			} leaf;

			struct {
				/** \brief Node directionality indication. */
				ContainerNodeDirection direction;

				/** \brief Size of the node list. */
				guint size;

				/** \brief List of pointers to child nodes. */
				struct container_node **list;
			} node;
		} data;

	} ContainerNode;


/** \brief Function creating new container node of given type. */
extern ContainerNode* container_node_create(ContainerNodeType type, ContainerNodeDirection direction);

/** \brief Function deallocating container node and all its childs. */
extern RCode container_node_destroy(ContainerNode* node);

/** \brief Function to return directionality of node. */
extern ContainerNodeDirection container_node_get_direction(ContainerNode *node);


// functions for working with nodes

/** \brief Function adding node into another node. */
extern RCode container_node_insert(ContainerNode *node, ContainerNode *child, guint index);

/** \brief Function removing node from another node. */
extern RCode container_node_remove(ContainerNode *node, guint index);

/** \brief Function to return number of childs in node. */
extern guint container_node_get_size(ContainerNode *node);

/** \brief Function to return node on the given position. */
extern ContainerNode* container_node_get_child(ContainerNode *node, guint index);

/** \brief Function to replace child node on given position. */
extern RCode container_node_set_child(ContainerNode *node, ContainerNode* child, guint index);

/** \brief Function to return position of specific child node. */
extern gint container_node_get_position(ContainerNode *node, ContainerNode *child);

/** \brief Reset dimension changed flag. */
extern RCode container_node_reset_flag(ContainerNode *node);

/** \brief Check if container node or its child has changed dimension. */
extern gboolean container_node_get_flag(ContainerNode *node);

/** \brief Recursively resize container node region and its children. */
extern RCode container_node_resize(ContainerNode *node, GdkRectangle region);

/** \brief Fill supplied variable with region hints for given node. */
extern RCode container_node_get_hint(ContainerNode* node, GdkPoint* hint);


// functions for working with leafs

/** \brief Function setting view pointer to the leaf node. */
extern RCode container_node_set_view(ContainerNode *node, ViewGeneric *view);

/** \brief Function retrieving pointer to the view stored in leaf node. */
extern ViewGeneric* container_node_get_view(ContainerNode *node);

#endif
