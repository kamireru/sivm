/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file view_container.h
 *
 * The container view allows organization of views on the screen available to
 * the application.
 *
 * The container view data contains two pointers - pointer to the top node of
 * the node structure and pionter to the leaf node with focus.
 */
#ifndef __FRONTEND_VIEW_CONTAINER_H__
#define __FRONTEND_VIEW_CONTAINER_H__

#include "src/frontend/view_common.h"
#include "src/frontend/container_node.h"

/**
 * \brief Structure storing information about container view.
 *
 * The structure holds pointer to the top node of the container structure and
 * pointer to the node that is focused right now.
 */
typedef struct {

		/** \brief Pointer to top container node. */
		ContainerNode *top_node;

		/** \brief Pointer to container node that has focus. */
		ContainerNode *focus_node;

		/** \brief Structure for grouping together color storage. */
		struct {

			/** \brief Storage for background color definition. */
			GdkColor bg;

			/** \brief Storage for inactive node color definition. */
			GdkColor inactive;

			/** \brief Storage for inactive color definition. */
			GdkColor active;
		} color;

	} ViewContainer;


// specific implementation of common view functions

/** \brief Allocate memory for view and initialize variables. */
extern ViewContainer* view_cv_create(ViewGeneric *parent);

/** \brief Deallocate memory for view. */
extern RCode view_cv_destroy(ViewContainer *specificData);

/** \brief Update the container view from model. */
extern RCode view_cv_update(ViewGeneric *view, guint seq);

/** \brief Redraw container view area. */
extern RCode view_cv_redraw(ViewGeneric *view, GdkRectangle *region);

/** \brief Resize the container view display. */
extern RCode view_cv_resize(ViewGeneric *view, GdkRectangle newRegion);


// view specific functions

/** \brief Set view pointer to the container node with focus. */
extern RCode view_cv_set_view(ViewGeneric *view, ViewGeneric *child);

/** \brief Get view pointer from container node with focus. */
extern ViewGeneric* view_cv_get_view(ViewGeneric *view);

/** \brief Split currently focused node. */
extern RCode view_cv_node_split(ViewGeneric *view, ContainerNodeDirection dir);

/** \brief Remove currenly focused node. */
extern RCode view_cv_node_remove(ViewGeneric *view);

/** \brief Get pointer to the top node of container. */
extern ContainerNode* view_cv_get_top_node(ViewGeneric *view);

/** \brief Set container node as focus node. */
extern RCode view_cv_node_set_focus(ViewGeneric *view, ContainerNode *node);


// internal functions, exported for unit tests only


/** \brief Cast specific data pointer to correct type. */
extern ViewContainer* view_cv_get_data_pointer(ViewGeneric* view);

/** \brief Redraw recursively node(s) of the container view. */
extern RCode view_cv_redraw_node(ViewContainer *specificData, ContainerNode *node, GdkRectangle region, GdkWindow *window);

/** \brief Resize recursively node(s) of the container view. */
extern RCode view_cv_resize_update(ContainerNode *node, GdkWindow *window);

#endif
