/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file mvc_types.h
 * \brief Header file with types used in the sivm implementation of MVC
 * pattern.
 *
 */
#ifndef FRONTEND_MVC_TYPES_H
#define FRONTEND_MVC_TYPES_H

#include <glib.h>
#include <gdk/gdk.h>
#include "src/fa/fa.h"


/** \brief Enumeration describing all controller types.  */
typedef enum {
		CNTL_NONE = 0,
		CNTL_APPLICATION = 1,
		CNTL_IMAGE_DISPLAY = 2,
		CNTL_CONTAINER = 3,
		CNTL_LAST = 4
	} CntlType;

/** \brief Enumeration describing all model types.  */
typedef enum {
		MODEL_NONE = 0,
		MODEL_APPLICATION = 1,
		MODEL_IMAGE_LIST = 2,
		MODEL_LAST = 3
	} ModelType;

/**
 * \brief Enumeration describing all view types.
 */
typedef enum {
		VIEW_NONE = 0,
		VIEW_APPLICATION = 1,
		VIEW_IMAGE_DISPLAY	= 2,
		VIEW_CONTAINER = 3,
		VIEW_LIST_STATUS = 4,
		VIEW_LAST = 5
	} ViewType;


// forward declarations of the types used in the MVC pattern implementation
struct _CntlGeneric;
struct _ModelGeneric;
struct _ViewGeneric;
struct _ObserverList;

// typdef the forward declarations so that they can be used
typedef struct _CntlGeneric CntlGeneric;
typedef struct _ModelGeneric ModelGeneric;
typedef struct _ViewGeneric ViewGeneric;
typedef struct _ObserverList ObserverList;


/**
 * \brief Holds information about specific controller.
 *
 * The structure holds information about command repetition count (ie how many
 * times will the controller invoke command once it is identified) and buffer
 * for command sequence recognition.
 */
struct _CntlGeneric {

		/** \brief specific controller discriminator */
		CntlType type;

		/** \brief Variable storing number of references. */
		guint reference;

		/** \brief Pointer controller specific data. */
		gpointer specific_data;

		/** \brief View to which the controller is assigned. */
		ViewGeneric* view;

		/** \brief Automaton responsible for parsing input. */
		FaRunner* automaton;

	};

/**
 * \brief Holds information about specific model.
 *
 * The structure holds information about all views that pull data from model so
 * that model knows who should be updated. It also holds pointer to model
 * specific data.
 */
struct _ModelGeneric {

		/** \brief Specific model discriminator. */
		ModelType type;

		/** \brief Variable storing number of references. */
		guint reference;

		/** \brief Pointer to specific model data. */
		gpointer specific_data;

		/** \brief List of views reading data from model. */
		ObserverList* view_list;

		/** \brief Storage for sequence number for marking changes to model. */
		gint sequence;

	};

/**
 * \brief Holds information about specific view.
 *
 * The structure holds information for identifying correct specific view and
 * forwarding the function call to that view.
 */
struct _ViewGeneric {

		/** \brief specific view discriminator */
		ViewType type;

		/** \brief Variable storing number of references. */
		guint reference;

		/** \brief storage for specific view pointer */
		gpointer specific_data;

		/** \brief Pointer to model associated with view. */
		ModelGeneric* model;

		/** \brief Pointer to controller associated with view. */
		CntlGeneric* controller;

		/** \brief Storage for sequence number of last processed notification. */
		gint last_sequence;

		/** \brief Rectangle delimiting area of application window used by view. */
		GdkRectangle region;

		/** \brief Hints for automatic region resizing.
		 *
		 * Region hints are applied only to the views that can be and are childs
		 * of container view.
		 *
		 * The hint have different meaning depending on its value:
		 *   == 0 : get whatever screen size is available
		 *   <  0 : get percentage of screen
		 *   >  0 : get absolute amount of screen
		 */
		GdkPoint region_hint;

		/** \brief Pointer to window in which the view is rendered. */
		GdkWindow* window;

	};

/**
 * \brief The structure holds list of views.
 *
 * The structure is assigned to any model that can be used by views. The it
 * holds all views that registered themselves to receive updates about data
 * changes of given model.
 */
struct _ObserverList {

		/** \brief Array of all registered observers. */
		ViewGeneric **array;

		/** \brief Maximum observer list size. */
		guint maxSize;

		/** \brief Current observer list size. */
		guint currentSize;

	};

#endif
