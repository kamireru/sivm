/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>

#include "src/config/config.h"
#include "src/frontend/view_generic.h"
#include "src/frontend/view_list_status.h"
#include "src/frontend/model_image_list.h"


#define DEFAULT_FONT_NAME "-misc-fixed-medium-r-semicondensed-*-0-0-75-75-c-0-iso8859-*"

/**
 * Allocate memory for list status view structure and initialize it with
 * correct values.
 *
 * \todo: All of the values are hardcoded now, but it should be possible to make
 * them used settable through configuration storage. It would mean to refactor
 * the configuration storage though (flag option list won't be sufficient
 * anymore, especially for colors).
 *
 * \return pointer to image display specific data
 */
ViewListStatus* view_lsv_create(ViewGeneric *parent) {
	const ConfigStorage* conf;
	ViewListStatus *result = NULL;
	GdkFont* font = NULL;

	ASSERT( NULL != parent );

	font = gdk_font_load(DEFAULT_FONT_NAME);
	if ( NULL == font ) {
		ERROR_SET(ERROR.INVALID_PARAM);
		ERROR_NOTE("Font %s does not load", DEFAULT_FONT_NAME);
		return  result;
	}

	if ( NULL == ( result = (ViewListStatus*) g_try_malloc0(sizeof(ViewListStatus))) ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return result;
	}

	result->border_size.x = 5;
	result->border_size.y = 2;

	/// \todo for now, we assume that the font is monospace and does not contain
	// wide characters
	result->text_size.x = gdk_char_width(font, 'A');
	result->text_size.y = gdk_char_height(font, 'A');

	result->color.fg.red = 65535;
	result->color.fg.green = 65535;
	result->color.fg.blue = 65535;

	result->color.bg.red = 0;
	result->color.bg.green = 0;
	result->color.bg.blue = 0;

	result->font = font;

	parent->region_hint.y = result->text_size.y + 2 * result->border_size.y;

	return result;
}

/**
 * Perform deinicialization and deallocation of list status view structure.
 *
 * \param specificData pointer to list status view structure
 * \return OK on success
 */
RCode view_lsv_destroy(ViewListStatus *specificData) {

	ASSERT( NULL != specificData );

	if ( NULL != specificData->font ) {
		gdk_font_unref(specificData->font);
		specificData->font = NULL;
	}

	if ( NULL != specificData->buffer ) {
		g_free( (gpointer) specificData->buffer );
	}

	g_free( (gpointer)specificData );

	return OK;
}

/**
 * Retrieve new information string from underlaying model and convert it to wide
 * characters.
 *
 * \param view pointer to generic view structure
 * \param seq sequence number identitying the update
 * \return OK on success
 */
RCode view_lsv_update(ViewGeneric *view, guint seq) {
	ViewListStatus* lsv_data = NULL;
	RCode result = OK;

	ASSERT( NULL != view );

	lsv_data = view_lsv_get_data_pointer(view);
	if ( NULL == lsv_data ) {
		return FAIL;
	}

	// deallocate buffered information
	if ( NULL != lsv_data->buffer ) {
		g_free( (gpointer)lsv_data->buffer );
		lsv_data->buffer = NULL;
	}

	if ( NULL != view->model ) {
		gchar* info = NULL;

		switch ( lsv_data->status_type ) {
			case VIEW_STATUS_FILENAME:
				lsv_data->buffer = model_iml_query_image_name(view->model);
				break;
			case VIEW_STATUS_POSITION:
			default:
				lsv_data->buffer = model_iml_query_group_info(view->model);
		}
		if ( NULL == lsv_data->buffer ) {
			result = FAIL;
		}
	}

	gdk_window_invalidate_rect(view->window, &(view->region), FALSE);

	return result;
}

/**
 * Redraw portion of view defined by supplied region parameter.
 *
 * \param view pointer to generic view structure
 * \param region region of screen that need to be updated
 * \return OK on success
 */
RCode view_lsv_redraw(ViewGeneric *view, GdkRectangle *region) {
	ViewListStatus* lsv_data = NULL;
	GdkGC *gc = NULL;

	lsv_data = view_lsv_get_data_pointer(view);
	if ( NULL == lsv_data ) {
		return FAIL;
	}

	/// \todo move graphic context creation somewhere else
	gc = gdk_gc_new(view->window);

	gdk_gc_set_rgb_fg_color(gc, &(lsv_data->color.bg));
	gdk_draw_rectangle(view->window, gc, TRUE, region->x, region->y, region->width, region->height);

	if ( NULL != lsv_data->buffer ) {

		gdk_gc_set_rgb_fg_color(gc, &(lsv_data->color.fg));
		gdk_draw_string(view->window, lsv_data->font, gc,
				view->region.x + lsv_data->border_size.x,
				view->region.y + lsv_data->border_size.y + lsv_data->text_size.y,
				lsv_data->buffer
			);
	}

	g_object_unref( (gpointer)gc );

	return OK;
}

/**
 * Change type of the status info displayed by the view instance.
 *
 * \param view pointer to generic view structure
 * \param statusType type of the info to display
 */
RCode view_lsv_set_type(ViewGeneric *view, ViewStatusType statusType) {
	ViewListStatus* lsv_data = NULL;

	lsv_data = view_lsv_get_data_pointer(view);
	if ( NULL == lsv_data ) {
		return FAIL;
	}

	lsv_data->status_type = statusType;

	// \todo temporarily set fixed width hint to position status to make use of
	// full application window width when using both of them
	// The hint will be dropped once container node hint change will trigger
	// node resize event.
	if ( VIEW_STATUS_POSITION == statusType ) {
		// use enough to accomodate group with 10000 images
		view->region_hint.x = lsv_data->text_size.x * ( 2*5 + 3);
	}
}

/**
 * The method resize updates the view internal variables to the changed region
 * size.
 *
 * \param view view to resize
 * \param newRegion the size of the new region
 * \return OK on success
 */
RCode view_lsv_resize(ViewGeneric *view, GdkRectangle newRegion) {
	ViewListStatus* lsv_data = NULL;

	ASSERT( NULL != view );

	lsv_data = view_lsv_get_data_pointer(view);
	if ( NULL == lsv_data ) {
		return FAIL;
	}

	// check if buffer need to be resized
	if ( newRegion.x == view->region.x && newRegion.width == view->region.width
		&& newRegion.y == view->region.y && newRegion.height == view->region.height ) {
		return OK;
	}

	TRACE_MESSAGE(VIEW, TL_DEBUG,
			"Image list status resize (view=%p, [x=%d,y=%d,width=%d,height=%d]",
			view, newRegion.x, newRegion.y, newRegion.width, newRegion.height
		);

	// force redraw of view
	if ( NULL != view->window ) {
		gdk_window_invalidate_rect(view->window, &(newRegion), FALSE);
	}

	return OK;
}

/**
 * The function checks the type of the view and either cast the pointer to
 * correct type or return NULL to indicate error.
 *
 * This function can use NULL for error indication because list status view
 * should always have specific data.
 *
 * \param view pointer to list status view structure
 * \return ViewApplication pointer or NULL on error
 */
ViewListStatus* view_lsv_get_data_pointer(ViewGeneric* view) {
	ASSERT( NULL != view );

	if ( VIEW_LIST_STATUS != view->type ) {
		return NULL;
	}

	if ( NULL == view->specific_data ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Specific data pointer is NULL");
		return NULL;
	}

	return (ViewListStatus*) view->specific_data;
}
