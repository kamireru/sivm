/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file view.h
 * \brief Header file with includes for view component
 *
 * The file groups together all specific view components. When model or
 * controller need to access view functionality, it just need to add this one
 * file to have access to any specific view.
 */
#ifndef __FRONTEND_VIEW_H__
#define __FRONTEND_VIEW_H__

#include "src/frontend/view_generic.h"
#include "src/frontend/view_image_display.h"
#include "src/frontend/view_application.h"
#include "src/frontend/view_container.h"
#include "src/frontend/view_list_status.h"

#endif
