/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file model_application.h
 * \brief Header file describing API of application model
 *
 * The application model is a wrapper connecting implementation of MVC pattern
 * to GDK function calls.
 */
#ifndef __FRONTEND_MODEL_APPLICATION_H__
#define __FRONTEND_MODEL_APPLICATION_H__

#include "src/frontend/model_common.h"

/**
 * \brief Structure holding data specific to the application model.
 *
 * It contains pointers to the main event loop and application window since the
 * model has to handle quit action and window fullscreen change action.
 */
typedef struct {
		/** \brief Storage for GLIB loop pointer. */
		GMainLoop* loop;

	} ModelApplication;


/** \brief Allocate application specific model data. */
extern ModelApplication* model_app_create();

/** \brief Deallocate application specific model data. */
extern RCode model_app_destroy(ModelApplication *specificData);


/** \brief Set pointer to glib main loop to the model. */
extern RCode model_app_set_loop(ModelGeneric *model, GMainLoop *loop);


/** \brief Model specific function for closing application. */
extern RCode model_app_quit(ModelGeneric *model);

/** \brief Model specific function for toggling fullscreen state. */
extern RCode model_app_toggle_fullscreen(ModelGeneric *model, GdkWindow* window);


// Internal functions, exported for use inunit tests only
extern ModelApplication* model_app_get_data_pointer(ModelGeneric* model);

#endif
