/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "src/frontend/model_generic.h"
#include "src/frontend/model_application.h"

/**
 * Allocate memory for application model structure and initialize it with
 * correct values.
 *
 * \return pointer to application model structure
 */
ModelApplication* model_app_create() {
	ModelApplication *result = NULL;

	if ( NULL == ( result = (ModelApplication*) g_try_malloc0(sizeof(ModelApplication))) ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return result;
	}

	result->loop = NULL;
	return result;
}

/**
 * Perform deinicialization and deallocation of application model structure.
 *
 * \param specificData pointer to application model structure
 * \return OK on success
 */
RCode model_app_destroy(ModelApplication *specificData) {
	ASSERT( NULL != specificData );

	g_free( (gpointer)specificData );
	return OK;
}

/**
 * The function setup GLIB event loop pointer to the application model storage
 * area. The pointer is needed in case the application model want to stop
 * event loop and terminate application.
 *
 * \param model pointer to generic model structure
 * \param loop pointer to event loop
 * \return OK on success
 */
RCode model_app_set_loop(ModelGeneric *model, GMainLoop *loop) {
	ModelApplication* app_data = NULL;

	ASSERT( NULL != model );

	app_data = model_app_get_data_pointer(model);
	if ( NULL == app_data ) {
		return FAIL;
	}

	app_data->loop = loop;
	return OK;
}

/**
 * The function requests application termination.
 *
 * \param model pointer to generic model structure
 * \return OK on success
 */
RCode model_app_quit(ModelGeneric *model) {
	ModelApplication* app_data = NULL;

	ASSERT( NULL != model );

	app_data = model_app_get_data_pointer(model);
	if ( NULL == app_data ) {
		return FAIL;
	}

	if ( NULL != app_data->loop ) {
		TRACE_MESSAGE(MODL, TL_INFO,
				"Terminating GLIB event loop (model=%p)", model
			);
		g_main_loop_quit(app_data->loop);
		return OK;
	} else {
		return FAIL;
	}
}


/**
 * The function requests toggle of the fullscreen setting of the application
 * window.
 *
 * \param model pointer to generic model structure
 * \param window pointer to the window to toggle
 * \return OK on success
 */
RCode model_app_toggle_fullscreen(ModelGeneric *model, GdkWindow* window) {
	GdkWindowState window_state;

	ASSERT( NULL != model );

	TRACE_MESSAGE(MODL, TL_INFO, "Toggle fullscreen mode (model=%p)", model);

	if ( NULL == window ) {
		ERROR_SET(ERROR.INVALID_PARAM);
		ERROR_NOTE("Pointer to window is NULL");
		return FAIL;
	}

	window_state = gdk_window_get_state(window);
	if ( 0 == (GDK_WINDOW_STATE_FULLSCREEN & window_state) ) {
		gdk_window_fullscreen(window);
	} else {
		gdk_window_unfullscreen(window);
	}

	return OK;
}

/**
 * The function checks the type of the model and either cast the pointer to
 * correct type or return NULL to indicate error.
 *
 * This function can use NULL for error indication because application model
 * should always have specific data.
 *
 * \param model pointer to generic model structure
 * \return ModelApplication pointer or NULL on error
 */
ModelApplication* model_app_get_data_pointer(ModelGeneric* model) {
	ASSERT( NULL != model );

	if ( MODEL_APPLICATION != model->type ) {
		return NULL;
	}

	if ( NULL == model->specific_data ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Specific data pointer is NULL");
		return NULL;
	}

	return (ModelApplication*) model->specific_data;
}
