/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __FRONTEND_CNTL_COMMON_H__
#define __FRONTEND_CNTL_COMMON_H__

#include "src/common/common_def.h"
#include "src/frontend/mvc_types.h"

/**
 * The enumeration provides return codes for input processing function. The
 * function is using three state logic in the return codes and each state must
 * be handled differently:
 *
 *  - match success
 *  input was recognized and matched to the command
 *
 *  - match fail
 *  input was not recognized
 *
 *  - match partial
 *  input was recognized, but still unsure which command should be invoked
 */
typedef enum {
		CNTL_MATCH_SUCCESS = 0,
		CNTL_MATCH_FAIL = 1,
		CNTL_MATCH_PARTIAL = 2
	} CntlProcessResult;

/**
 * Macro for creating values for initialization of GdkEventKey structure for use
 * in automaton definition.
 *
 * The macro initializes only values that are checked by cntl_key_compare()
 * function.
 *
 * \param TYPE type of the event
 * \param KEYVAL value of the key pressed
 * \param STATE state of the modifier flags (only some are used)
 */
#define CNTL_KEY_STRUCTURE(TYPE,KEYVAL,STATE) { (TYPE), NULL, FALSE, 0, (STATE), (KEYVAL), 0, NULL, 0, 0, 1 }

/**
 * Macro for making pointers from name of the array holding key structures and
 * index of the key to use.
 *
 * \param LIST name of the GdkEventKey array
 * \param INDEX index to the array
 */
#define CNTL_KEY(LIST,INDEX) &(LIST[INDEX])

/** \brief Comparator function for GdkEventKey structure pointers. */
extern FaRelate cntl_key_compare(void* arg1, void* arg2);

#endif
