/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file view_common.h
 * \brief Header file with common includes for view component.
 *
 * The file contains includes that are common to all view components be it
 * generic or specific. Each new component need to include just this file to
 * provide core view functionality.
 */
#ifndef __FRONTEND_VIEW_COMMON_H__
#define __FRONTEND_VIEW_COMMON_H__

#include "src/common/common_def.h"
#include "src/frontend/mvc_types.h"

#endif
