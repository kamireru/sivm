/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>
#include "src/backend/group_list.h"
#include "src/backend/image_group_record.h"
#include "src/frontend/model_generic.h"
#include "src/frontend/model_image_list.h"

/**
 * Allocate memory for image list model structure and initialize it with
 * correct values.
 *
 * \return pointer to image list model structure
 */
ModelImageList* model_iml_create() {
	ModelImageList *result = NULL;

	if ( NULL == ( result = (ModelImageList*) g_try_malloc0(sizeof(ModelImageList))) ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return result;
	}

	result->image_index = -1;
	result->image_group = NULL;
	result->image_data = NULL;
	result->frame_iter = NULL;

	return result;
}

/**
 * Perform deinicialization and deallocation of image list model structure.
 *
 * \param specificData pointer to image list model structure
 * \return OK on success
 */
RCode model_iml_destroy(ModelImageList *specificData) {
	ASSERT( NULL != specificData );

	if ( NULL != specificData->image_data ) {
		g_object_unref( (gpointer)specificData->image_data );
		specificData->image_data = NULL;
	}

	g_free( (gpointer)specificData );
	return OK;
}

/**
 * Set new image group to the model and load data for first image in the group.
 *
 * \param model pointer to generic model structure
 * \param group pointer to image group to use in model
 * \return OK on success
 */
RCode model_iml_set_image_group(ModelGeneric* model, ImageGroup *group) {
	ModelImageList* iml_data = NULL;

	ASSERT( NULL != model );

	TRACE_MESSAGE(MODL, TL_INFO,
			"Setup new image group to model (model=%p, group=%p)", model, group
		);

	iml_data = model_iml_get_data_pointer(model);
	if ( NULL == iml_data ) {
		return FAIL;
	}

	// remove old group (basically, reset the variables to initial state)
	iml_data->image_group = NULL;
	iml_data->image_index = -1;
	if ( NULL != iml_data->image_data ) {
		g_object_unref( (gpointer)iml_data->image_data );
		iml_data->image_data = NULL;
	}

	// check if new group is non NULL and set it into the model
	if ( NULL != group && 0 < group_get_size(group) ) {
		RCode result = FAIL;
		iml_data->image_index = 0;
		iml_data->image_group = group;

		do {
			// get image name
			Image* image = group_get_image(group, iml_data->image_index);
			if ( error_is_error() ) {
				ERROR_NOTE("Image structure retrieval failed");
				return FAIL;
			}

			// load the image data from filesystem
			iml_data->image_data = model_iml_load_image(image->name);

			// check if image loading was processed correctly
			if ( NULL == iml_data->image_data ) {
				if ( FAIL == group_delete_index(group, iml_data->image_index) ) {
					ERROR_NOTE("Image deletion from group failed");
					return FAIL;
				}

				// new image is shifted to the first position in array, no need
				// to update the index
			} else {
				// get animation iterator if image is animation
				if ( NULL != iml_data->frame_iter ) {
					g_object_unref( (gpointer)iml_data->frame_iter );
					iml_data->frame_iter = NULL;
				}
				if ( ! gdk_pixbuf_animation_is_static_image(iml_data->image_data) ) {
					iml_data->frame_iter = gdk_pixbuf_animation_get_iter(
							iml_data->image_data, NULL
						);
				}
				model_generate_sequence(model);
				result = OK;
			}
		} while ( result != OK && 0 < group_get_size(group) );

		// check if image was loaded
		if ( OK != result ) {
			iml_data->image_index = -1;
		}
	}

	// if supplied group is empty, behave as if it were emptied by removing non
	// images
	if ( NULL != group && 0 == group_get_size(group) ) {
		iml_data->image_index = -1;
		iml_data->image_group = group;
		model_generate_sequence(model);
	}

	model_update(model);

	// return true even if the group did become empty during setup
	return OK;
}

/**
 * The function updates index in the image group to the index supplied in
 * function parameter.
 *
 * The new index is limited by minimal and maximal index in the group. If the
 * limited new index is different from the original index, then the function
 * tries to load the new file.
 *
 * If the file can't be loaded because it is not present in filesystem anymore
 * or because it is not image, then remove the offending image from group and
 * try to load next image.
 *
 * This conditin loops until one image can be loaded or until there is no image
 * left in the underlaying image group.
 *
 * The next image index depends on the direction flag supplied to function. If
 * the isForward flag is set to true, then try to load images with higher index
 * than the original one. Otherwise, try to load images with lower index.
 *
 * \param model pointer to generic model structure
 * \param index index that shall be used as a next image
 * \param isForward flag indicating direction of the move
 * \return OK on success
 */
RCode model_iml_change_image(ModelGeneric* model, gint index, gboolean isForward) {
	RCode result = FAIL;
	gint new_index, min_index, max_index;
	ModelImageList* iml_data = NULL;

	ASSERT( NULL != model );

	TRACE_MESSAGE(MODL, TL_INFO,
			"Change image index (model=%p, index=%d, isForward=%d)",
			model, index, isForward
		);

	iml_data = model_iml_get_data_pointer(model);
	if ( NULL == iml_data ) {
		return FAIL;
	}

	// if there is no image group, exit
	if ( NULL == iml_data->image_group ) {
		TRACE_MESSAGE(MODL, TL_DEBUG,
				"No image group is associated with model (model=%p)", model
			);
		return OK;
	}

	// initialize new index base and index bounds
	max_index = group_get_size(iml_data->image_group) - 1;
	min_index = 0;

	new_index = ( index <= max_index ) ? index : max_index;
	new_index = ( new_index >= min_index ) ? new_index : min_index;

	// if there is no change to the index, or if there is no image in the group
	if ( -1 == max_index || new_index == iml_data->image_index ) {
		return OK;
	}

	// stop referencing old image data (if there is some)
	if ( NULL != iml_data->image_data ) {
		g_object_unref( (gpointer)iml_data->image_data );
		iml_data->image_data = NULL;
	}

	do {
		// get image name
		Image* image = group_get_image(iml_data->image_group, new_index);
		if ( error_is_error() ) {
			ERROR_NOTE("Retrieval of Image structure pointer failed");
			return FAIL;
		}

		// load the image data from filesystem
		iml_data->image_data = model_iml_load_image(image->name);
		if ( NULL == iml_data->image_data ) {
			TRACE_MESSAGE(MODL, TL_DEBUG, "Image on index is not valid (index=%d)", new_index);
			if ( FAIL == group_delete_index(iml_data->image_group, new_index) ) {
				ERROR_NOTE("Image deletion from group failed");
				return FAIL;
			}

			max_index = group_get_size(iml_data->image_group) -1 ;

			// compute new index after the image is removed
			if ( isForward ) {
				// if original direction was 'next', then the new image will be
				// moved to the computed index (unless it is last index in group
				new_index = ( new_index >= max_index ) ? max_index : new_index;
			} else {
				// if original direction was 'prev', try to load image with
				// index lower than computed one, unless it is first index in
				// group
				new_index = ( new_index > min_index ) ? new_index - 1 : min_index;
			}
		} else {
			TRACE_MESSAGE(MODL, TL_MINOR,
					"New image loaded (model=%p,file='%s')", model, image->name
				);

			if ( NULL != iml_data->frame_iter ) {
				g_object_unref( (gpointer)iml_data->frame_iter );
				iml_data->frame_iter = NULL;
			}
			if ( ! gdk_pixbuf_animation_is_static_image(iml_data->image_data) ) {
				iml_data->frame_iter = gdk_pixbuf_animation_get_iter(
						iml_data->image_data, NULL
					);
			}

			// generete new sequence only if new image is loaded
			model_generate_sequence(model);
			result = OK;
		}

	} while ( result != OK && 0 < max_index);

	if ( OK == result ) {
		iml_data->image_index = new_index;
	} else {
		iml_data->image_index = -1;
	}

	/// \todo here we should add a check to ensure that we won't be loading
	/// original image that just shuffled to a new index
	model_update(model);

	return OK;
}

/**
 * Move image index in direction specified by type parameter. The value parameter
 * can be ignored or interpreted as either relative or absolute position
 * depending on the type:
 *
 * - move to first and last index ignores value parameter entirely
 * - move to next or prev index uses value paremeter number of moves
 * - move to index uses value parameter as actual index
 *
 * \param model pointer to generic model structure
 * \param type type of the index movement
 * \param value optional interger value used to specify movement size
 * \return OK on succes
 */
RCode model_iml_move_index(ModelGeneric* model, MoveIndexType type, guint value) {
	ModelImageList* iml_data = NULL;
	RCode result = OK;
	gint new_index;
	gboolean is_forward;

	ASSERT( NULL != model );

	TRACE_MESSAGE(MODL, TL_INFO,
			"Move image list index (model=%p, type=%d, step=%d)",
			model, type, value
		);

	iml_data = model_iml_get_data_pointer(model);
	if ( NULL == iml_data ) {
		return FAIL;
	}

	// if there is no image group, exit
	if ( NULL == iml_data->image_group ) {
		TRACE_MESSAGE(MODL, TL_DEBUG,
				"No image group is associated with model (model=%p)", model
			);
		return OK;
	}

	// compute change_image function parameters depending on type
	is_forward = TRUE;
	switch (type) {
		case MOVE_INDEX_FIRST:
			new_index = 0;
			break;

		case MOVE_INDEX_LAST:
			new_index = group_get_size(iml_data->image_group) - 1;
			break;

		case MOVE_INDEX_NEXT:
			new_index = iml_data->image_index + value;
			break;

		case MOVE_INDEX_PREV:
			is_forward = FALSE;
			new_index = iml_data->image_index - value;
			break;

		case MOVE_INDEX_INDEX:
			new_index = value;
			break;

		default:
			TRACE_MESSAGE(MODL, TL_MINOR, "Unknown movement type (type=%d)", type);
			new_index = iml_data->image_index;
	}

	// check if index realy changed
	if ( new_index == iml_data->image_index ) {
		return OK;
	} else {
		return model_iml_change_image(model, new_index, is_forward);
	}
}

/**
 * Return pointer to the currently loaded image data.
 *
 * \param model pointer to generic model structure
 * \return GdkPixbuf* or NULL if no image is loaded
 */
GdkPixbuf* model_iml_get_image(ModelGeneric *model) {
	ModelImageList* iml_data = NULL;

	ASSERT( NULL != model );

	iml_data = model_iml_get_data_pointer(model);
	if ( NULL == iml_data || NULL == iml_data->image_data ) {
		return NULL;
	}

	if ( gdk_pixbuf_animation_is_static_image(iml_data->image_data) ) {
		return gdk_pixbuf_animation_get_static_image(iml_data->image_data);

	} else {
		if ( NULL == iml_data->frame_iter ) {
			return NULL;
		}

		// advance iterator if necessary and return current frame
		gdk_pixbuf_animation_iter_advance(
				iml_data->frame_iter, NULL
			);
		return gdk_pixbuf_animation_iter_get_pixbuf(iml_data->frame_iter);
	}
}

/**
 * Return delay before next frame is to be displayed. Return -1 if animation is
 * in fact static image or if there won't be frame change anymore (for example
 * last frame of non-looping animation).
 *
 * \param model pointer to generic model structure
 * \return delay before next frame
 */
gint model_iml_get_delay(ModelGeneric *model) {
	ModelImageList* iml_data = NULL;

	ASSERT( NULL != model );

	iml_data = model_iml_get_data_pointer(model);
	if ( NULL == iml_data || NULL == iml_data->image_data ) {
		return -1;
	}

	if ( gdk_pixbuf_animation_is_static_image(iml_data->image_data) ) {
		return -1;

	} else {
		if ( NULL == iml_data->frame_iter ) {
			return -1;
		}

		return gdk_pixbuf_animation_iter_get_delay_time(iml_data->frame_iter);
	}
}


/**
 * The function formats information about group, its content and position to
 * which the model currently points. Then it returns pointer to string
 * containing formatted data.
 *
 * The string must be freed by whoever called the method.
 *
 * \param model pointer to generic model structure
 * \return pointer to newly allocated string or NULL on error
 */
gchar* model_iml_query_group_info(ModelGeneric *model) {
	ModelImageList* iml_data = NULL;
	gchar* result = NULL;
	gint length, size;

	ASSERT( NULL != model );

	iml_data = model_iml_get_data_pointer(model);
	if ( NULL == iml_data ) {
		return result;
	}

	if ( NULL == iml_data->image_group ) {
		result = strdup("n/a");
		if ( NULL == result ) {
			ERROR_SET(ERROR.MEMORY_FAILURE);
			ERROR_NOTE("Memory allocation for group info failed");
		}
		return result;
	}

	// compute number of digits the image group size has and add increase the
	// size to accomodate position, size, group tag and delimiters
	for (
			size = group_get_size(iml_data->image_group), length = 1;
			size > 0;
			length += ( size / 10 > 0 ) ? 1 : 0, size /= 10
		);
	length = 2 * length + 5;

	result = (gchar*) g_try_malloc0(length*sizeof(gchar) );
	if ( NULL == result ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		ERROR_NOTE("Memory allocation for group info failed");
		return result;
	}

	snprintf(result, length, "%c %d/%d",
			iml_data->image_group->tag,
			iml_data->image_index + 1,
			group_get_size(iml_data->image_group)
		);

	return result;
}

/**
 * The function returns newly allocated string with filename of image that is
 * currenlty pointed by models index.
 *
 * The string must be freed by whoever called the method.
 *
 * \param model pointer to generic model structure
 * \return pointer to newly allocated string or NULL on error
 */
gchar* model_iml_query_image_name(ModelGeneric *model) {
	ModelImageList* iml_data = NULL;
	Image* image = NULL;
	gchar* result = NULL;

	ASSERT( NULL != model );

	iml_data = model_iml_get_data_pointer(model);
	if ( NULL == iml_data ) {
		return result;
	}

	if ( NULL == iml_data->image_group || -1 == iml_data->image_index) {
		result = strdup("n/a");
		if ( NULL == result ) {
			ERROR_SET(ERROR.MEMORY_FAILURE);
			ERROR_NOTE("Memory allocation for group info failed");
		}
		return result;
	}

	// here we can return error since group can be associated with model only if
	// it is non empty
	image = group_get_image(iml_data->image_group, iml_data->image_index);
	if ( error_is_error() ) {
		ERROR_NOTE("Image structure retrieval failed");
		return result;
	}

	result = strdup(image->name);
	if ( NULL == result ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		ERROR_NOTE("Memory allocation for group info failed");
	}

	return result;
}

/**
 * The function checks the type of the model and either cast the pointer to
 * correct type or return NULL to indicate error.
 *
 * This function can use NULL for error indication because image list model
 * should always have specific data.
 *
 * \param model pointer to generic model structure
 * \return ModelImageList pointer or NULL on error
 */
ModelImageList* model_iml_get_data_pointer(ModelGeneric* model) {
	ASSERT( NULL != model );

	if ( MODEL_IMAGE_LIST != model->type ) {
		return NULL;
	}

	if ( NULL == model->specific_data ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Specific data pointer is NULL");
		return NULL;
	}

	return (ModelImageList*) model->specific_data;
}

/* Return image data from the filesystem. If the file is not image or does not
 * exist in the filesystem anymore, return an error and let calling function
 * sort that out.
 *
 * \param filename name of the file to load
 * \return OK on success
 */
GdkPixbufAnimation* model_iml_load_image(gchar* filename) {
	GdkPixbufAnimation* result = NULL;
	GError* error_ptr = NULL;

	ASSERT( NULL != filename );

	result = gdk_pixbuf_animation_new_from_file(filename, &error_ptr);
	if ( NULL != error_ptr ) {
		// do not set error since this is not an programming error
		TRACE_MESSAGE(MODL, TL_MAJOR, "%s", error_ptr->message);
		g_clear_error(&error_ptr);
	}

	return result;
}

/* Return image data from the filesystem. If the file is not image or does not
 * exist in the filesystem anymore, return an error and let calling function
 * sort that out.
 *
 * \param filename name of the file to load
 * \return OK on success
 */
ImageGroup* model_iml_load_group(char tag) {
	ImageGroup* result = NULL;
	int index;

	index = glist_index_by_tag(tag);
	if ( error_is_error() ) {
		ERROR_NOTE("Unable to locate group '%c'", tag);
		return NULL;
	}

	// no error but group not found
	if ( -1 == index ) {
		result = group_create(NULL, tag);
		if ( error_is_error() ) {
			ERROR_NOTE("Unable to create group '%c'", tag);
			return NULL;
		}

		glist_group_insert(result);
		if ( error_is_error() ) {
			ERROR_NOTE("Unable insert group into group list");
			return NULL;
		}
	} else {
		result = glist_get_by_index((unsigned int)index);
	}

	return result;
}

/**
 * \param model pointer to generic model structure
 * \param target pointer to image group to transfer images to
 * \param count number of images to transfer
 * \param isMove boolean value indicating that images should be moved
 * \return OK on success
 */
RCode model_iml_transfer_image(ModelGeneric* model, ImageGroup* target, int count, gboolean isMove) {
	ModelImageList* iml_data = NULL;
	RCode result = OK;
	Range range;
	int size, index;
	TRACE_MESSAGE(MODL, TL_DEBUG,
			"Copy image to image group (model=%p, dst='%c', count=%d)",
			model, target->tag, count
		);

	ASSERT( NULL != model );

	iml_data = model_iml_get_data_pointer(model);
	if ( NULL == iml_data ) {
		return result;
	}

	// if there is nothing to copy, succeed automatically
	if ( NULL == iml_data->image_group || NULL == target
		|| 0 == group_get_size(iml_data->image_group)
		|| 0 == count
		|| target == iml_data->image_group ) {
		return OK;
	}

	index = iml_data->image_index;
	size = group_get_size(iml_data->image_group);

	range.start = index;
	range.end = ( index + count - 1 < size ) ? index + count - 1 : size - 1;

	TRACE_MESSAGE(MODL, TL_DEBUG,
			"Copy image to image group (model=%p, range=[%d,%d], src='%c', dst='%c')",
			model, range.start, range.end, iml_data->image_group->tag, target->tag
		);

	result = group_yank_range(iml_data->image_group, range);
	if ( error_is_error() ) {
		ERROR_NOTE("Cannot yank images from group '%c' to yank buffer", iml_data->image_group->tag);
		return result;
	}

	size = group_get_size(target);
	result = group_yank_paste(target, size);
	if ( error_is_error() ) {
		ERROR_NOTE("Cannot paste images from yank buffer to group '%c' ", target->tag);
		return result;
	}

	if ( TRUE == isMove ) {
		result = group_delete_range(iml_data->image_group, range);
		if ( error_is_error() ) {
			ERROR_NOTE("Cannot paste images from yank buffer to group '%c' ", target->tag);
			return result;
		}

		// compute new index
		size = group_get_size(iml_data->image_group);
		index = ( index < size ) ? index : size - 1;

		// reset index to -1 to force image change (we know there is an image
		// change, as we have just removed current image from group)
		iml_data->image_index = -1;

		result = model_iml_change_image(model, index, TRUE);
		if ( error_is_error() ) {
			ERROR_NOTE("Cannot update image index after image deletion (index=%d", index);
		}
	}

	return result;
}

