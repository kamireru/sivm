/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "src/frontend/view.h"
#include "src/frontend/model.h"

/**
 * Allocate memory for generic model structure and setup the type
 * correctly.
 *
 * The reference count in the new structure is set to 1 to indicate that there
 * is only one place which references it - the variable storing pointer returned
 * from function.
 *
 * \param type which specific model to create
 * \return pointer to generic model structure or NULL on error
 */
ModelGeneric* model_create(ModelType type) {
	ModelGeneric *result = NULL;
	ObserverList* observers_ptr = NULL;

	if ( NULL == ( result = g_try_malloc0(sizeof(ModelGeneric))) ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return result;
	}

	// type must be set before specific data is allocated
	result->type = type;
	result->specific_data = NULL;
	result->reference = 1;

	// try to allocate list for observers
	result->view_list = observer_create_list();
	if ( NULL == result->view_list ) {
		ERROR_NOTE("Unable to create observer list for model");
		g_free( (gpointer)result );
		return NULL;
	}

	// try to allocate model specific data
	if ( FAIL == model_create_specific_data(result) ) {
		ERROR_NOTE("Unable to create model specific data");
		observer_destroy(result->view_list);
		g_free( (gpointer)result );
		return NULL;
	}

	result->sequence = 1;

	return result;
}

/**
 * Deallocate memory for model. Use model type specific function for
 * deallocation of the specific data.
 *
 * \param model pointer to generic model structure
 * \return OK on success
 */
RCode model_destroy(ModelGeneric *model) {
	ASSERT( NULL != model );

	// check reference count
	if ( 0 != model->reference ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Reference count is non-zero");
		return FAIL;
	}

	// we call it every time, the function can handle NULL in data pointer
	model_destroy_specific_data(model);

	if ( NULL != model->view_list ) {
		observer_destroy(model->view_list);
	}

	g_free( (gpointer)model );
	return OK;
}

/**
 * Increase reference count of the supplied model.
 *
 * \param model pointer to generic model structure
 * \return OK on success
 */
RCode model_ref(ModelGeneric *model) {
	ASSERT( NULL != model );

	model->reference += 1;
	return OK;
}

/** Decrease reference counter for model and if reference count reaches zero,
 * then call destroy function on the model.
 *
 * \param model pointer to generic model structure
 * \return OK on success
 */
RCode model_unref(ModelGeneric *model) {
	ASSERT( NULL != model );

	if ( 0 != (--model->reference) ) {
		return OK;
	}

	return model_destroy(model);
}


/**
 * Add view to the list of model observers. The model observers will be notified
 * each time model changes.
 *
 * The first notification will be sent immediately, since from observer point of
 * view, the model has changed (from none to some).
 *
 * \param model pointer to generic model structure
 * \param view pointer to application view structure
 * \return OK on success
 */
RCode model_insert_view(ModelGeneric *model, ViewGeneric *view) {
	RCode result;
	ASSERT( NULL != model );
	ASSERT( NULL != view );

	result = observer_insert(model->view_list, view);
	model_update(model);

	return result;
}

/**
 * Remove view from the list of model observers.
 *
 * \param model pointer to generic model structure
 * \param view pointer to application view structure
 * \return OK on success
 */
RCode model_remove_view(ModelGeneric *model, ViewGeneric *view) {
	ASSERT( NULL != model );
	ASSERT( NULL != view );

	return observer_remove(model->view_list, view);
}


/**
 * The function uses sequence number stored in the internal variable and sends
 * notification to all assigned views using the sequence number.
 *
 * The function does not update the sequence number - it is up to the function
 * that calls model_update() to provide sequence number update.
 *
 * \param model pointer to generic model structure
 */
void model_update(ModelGeneric *model) {
	ASSERT( NULL != model );

	observer_update(model->view_list, model->sequence);
}

/**
 * The function uses model type to determine which data structure is necessary
 * for specific model of that type and initializes the specific data.
 *
 * \param model pointer to generic model structure
 * \return OK on success
 */
RCode model_create_specific_data(ModelGeneric *model) {
	RCode result = OK;

	ASSERT( NULL != model );

	switch (model->type) {
		case MODEL_NONE:
			// this specific model does not have special data
			break;

		case MODEL_APPLICATION:
			model->specific_data = (gpointer) model_app_create();
			result = ( NULL == model->specific_data ) ? FAIL : OK;
			break;

		case MODEL_IMAGE_LIST:
			model->specific_data = (gpointer) model_iml_create();
			result = ( NULL == model->specific_data ) ? FAIL : OK;
			break;

		default:
			// no need to fail, since no function can operate over specific data
			// of unknown model type
			TRACE_MESSAGE(MODL, TL_INFO, "Unsupported model type (type=%d)", model->type);
			break;
	}
	return result;
}

/**
 * The function uses model type to determine how to deallocate model type
 * specific data and deallocates it.
 *
 * \param model pointer to generic model structure
 * \return OK on success
 */
RCode model_destroy_specific_data(ModelGeneric *model) {
	RCode result = OK;

	ASSERT( NULL != model );

	switch (model->type) {
		case MODEL_NONE:
			// no data to deallocate
			break;

		case MODEL_APPLICATION:
			result = model_app_destroy( (ModelApplication*) model->specific_data);
			break;

		case MODEL_IMAGE_LIST:
			result = model_iml_destroy( (ModelImageList*) model->specific_data);
			break;

		default:
			// no need to fail, since no specific data was allocated
			TRACE_MESSAGE(MODL, TL_INFO, "Unsupported model type (type=%d)", model->type);
			break;
	}

	return result;
}

/**
 * The function updates sequence number in model while preserving contraints on
 * the sequence number. The number must be non-zero, positive integer.
 *
 * Note: Non-zero condition is there since zero is initial sequence number.
 *
 * \param model pointer to generic model structure
 * \return OK on success
 */
RCode model_generate_sequence(ModelGeneric *model) {
	ASSERT( NULL != model );

	model->sequence += 1;
	if ( 0 >= model->sequence ) {
		model->sequence = 1;
	}

	return OK;
}
