/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file cntl_generic.h
 * \brief Header file with generic gui controller API.
 *
 * The generic controller works similarilly as generic view - it implements
 * something like polymorphism in C language.
 *
 * The difference from view is in the implementation. The controller does not
 * need to have specific structures storing different data and providing
 * specific functions for each controller type.
 *
 * The controller logic is described in the finite automaton. And the automaton
 * implementation is content with just void pointer for data storage. Thanks to
 * it, the only thing that is different for each controller type is the
 * automaton structure.
 *
 * Thus each controller type only need to provide two functions:
 *  - function initializing void pointer to point to correct structure
 *  - function returning appropriate automaton structure
 *
 * This allows us to encapsulate controller logic in separate file and minimize
 * and keep most of the functionality on the 'generic' level.
 */
#ifndef __FRONTEND_CNTL_GENERIC_H__
#define __FRONTEND_CNTL_GENERIC_H__

#include "src/frontend/cntl_common.h"

/** \brief Allocate memory for controller. */
extern CntlGeneric* cntl_create(CntlType type);

/** \brief Deallocate memory for controller. */
extern RCode cntl_destroy(CntlGeneric *cntl);

/** \brief Increase reference counter for cntl. */
extern RCode cntl_ref(CntlGeneric *cntl);

/** \brief Decrease reference counter for cntl and destroy. */
extern RCode cntl_unref(CntlGeneric *cntl);


/** \brief Assign view to the controller. */
extern void cntl_set_view(CntlGeneric *cntl, ViewGeneric* view);

/** \brief Return view pointer assigned to the controller. */
extern ViewGeneric* cntl_get_view(CntlGeneric *cntl);

/** \brief Input key to automaton and process it. */
extern CntlProcessResult cntl_process(CntlGeneric *cntl, GdkEventKey *input);


// Internal functions, exported only to be used in unit tests

/** \brief Allocate controller specific data. */
extern RCode cntl_create_specific_data(CntlGeneric *cntl);

/** \brief Deallocate controller specific data. */
extern RCode cntl_destroy_specific_data(CntlGeneric *cntl);


// Internal functions managing automatons, exported for tests only

/** \brief Get pointer to automaton for specific controller type. */
extern FaAutomaton* cntl_automaton_create(CntlType type);

/** \brief Decrease reference count for automaton type. */
extern RCode cntl_automaton_destroy(CntlType type);

/** \brief Get number of references to the automaton type. */
extern unsigned int cntl_automaton_count(CntlType type);


#endif
