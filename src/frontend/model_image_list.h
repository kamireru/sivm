/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file model_image_list.h
 * \brief Header file describing API of image list model
 *
 * The image list model is a wrapper connecting implementation of MVC pattern
 * to GDK function calls.
 */
#ifndef __FRONTEND_MODEL_IMAGE_LIST_H__
#define __FRONTEND_MODEL_IMAGE_LIST_H__

#include <gdk-pixbuf/gdk-pixbuf.h>

#include "src/frontend/model_common.h"
#include "src/backend/image_group.h"

/**
 * Enumeration describing various types of index movement available to the
 * model.
 */
typedef enum {
		MOVE_INDEX_FIRST = 0,
		MOVE_INDEX_LAST = 1,
		MOVE_INDEX_NEXT = 2,
		MOVE_INDEX_PREV = 3,
		MOVE_INDEX_INDEX = 4
	} MoveIndexType;

typedef struct {

		/** \brief Pointer to the image group storing images for view. */
		ImageGroup* image_group;

		/** \brief Index to current image in image group. */
		gint image_index;

		/** \brief Pointer to data of currenlty loaded image. */
		GdkPixbufAnimation* image_data;

		GdkPixbufAnimationIter* frame_iter;

	} ModelImageList;


/** \brief Allocate image list specific model data. */
extern ModelImageList* model_iml_create();

/** \brief Deallocate image list specific model data. */
extern RCode model_iml_destroy(ModelImageList *specificData);


/** \brief Set image group pointer to model. */
extern RCode model_iml_set_image_group(ModelGeneric* model, ImageGroup *group);


/** \brief Move image index using supplied step number. */
extern RCode model_iml_change_image(ModelGeneric* model, gint index, gboolean isForward);

/** \brief Move image index in specified direction by value. */
extern RCode model_iml_move_index(ModelGeneric* model, MoveIndexType type, guint value);

/** \brief Return pointer to buffer with image data. */
extern GdkPixbuf* model_iml_get_image(ModelGeneric *model);

/** \brief Return delay required before next animation frame display. */
extern gint model_iml_get_delay(ModelGeneric *model);


/** \brief Get string describing associated image group. */
extern gchar* model_iml_query_group_info(ModelGeneric *model);

/** \brief Get string with current image filename. */
extern gchar* model_iml_query_image_name(ModelGeneric *model);


// Internal functions, exported for use in unit tests only

/** \brief Return pointer to model specific data. */
extern ModelImageList* model_iml_get_data_pointer(ModelGeneric* model);

/** \brief Load image data into the model. */
extern GdkPixbufAnimation* model_iml_load_image(gchar* filename);

/** \brief Load image group pointer. */
extern ImageGroup* model_iml_load_group(char tag);

/** \brief Move or copy images from currently displayed group to different one. */
extern RCode model_iml_transfer_image(ModelGeneric* model, ImageGroup* target, int count, gboolean isMove);

#endif
