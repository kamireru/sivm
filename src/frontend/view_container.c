/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "src/frontend/view_generic.h"
#include "src/frontend/view_container.h"


/**
 * Allocate memory for container view structure and initialize it with
 * correct values.
 *
 * \return pointer to container view structure
 */
ViewContainer* view_cv_create(ViewGeneric *parent) {

	ViewContainer *result = NULL;
	ContainerNode* node = NULL;

	ASSERT( NULL != parent );

	node = container_node_create(CONTAINER_LEAF, NODE_VERTICAL);
	if ( NULL == node ) {
		ERROR_NOTE("Can't allocate root node for container view.");
		return NULL;
	}

	result = (ViewContainer*) g_try_malloc0(sizeof(ViewContainer));
	if ( NULL == result ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		container_node_destroy(node);
		return result;
	}

	result->top_node = node;
	result->focus_node = node;

	result->color.active.red = 0;
	result->color.active.green = 65535;
	result->color.active.blue = 65535;

	result->color.inactive.red = 0;
	result->color.inactive.green = 0;
	result->color.inactive.blue = 65535;

	result->color.bg.red = 0;
	result->color.bg.green = 0;
	result->color.bg.blue = 0;


	return result;
}


/**
 * Perform deinicialization and deallocation of container view structure.
 *
 * \param specificData pointer to container view structure
 * \return OK on success
 */
RCode view_cv_destroy(ViewContainer *specificData) {
	RCode result = OK;

	ASSERT( NULL != specificData );

	if ( NULL != specificData->top_node ) {
		result = container_node_destroy(specificData->top_node);
	}

	g_free( (gpointer)specificData );
	return result;
}

/**
 * Nothing is done here - the container view does not use model so it does not
 * have a source of update calls.
 *
 * \param view pointer to container view structure
 * \param seq sequence number describing the update
 * \return result of view_update() on child
 */
extern RCode view_cv_update(ViewGeneric *view, guint seq) {

	/// \todo check if this event could be received from view itself???
	return OK;
}

/**
 * Function recursively redraws a node of the container view. Only nodes whose
 * region intersects supplied redraw region are updated.
 *
 * The leave node that has assigned view uses the view itself for redrawing.
 * Leaves that do not have view assigned only outlines leaf region.
 *
 * \param specificData pointer to container specific view structure
 * \param node pointer to currenlty processed node
 * \param region region of screen that need to be redrawn
 * \param window pointer to the window associated with view
 * \return OK on success
 */
RCode view_cv_redraw_node(ViewContainer *specificData, ContainerNode *node, GdkRectangle region, GdkWindow *window) {
	GdkRectangle area;

	ASSERT( NULL != node );
	ASSERT( NULL != specificData );

	TRACE_MESSAGE(VIEW, TL_DEBUG, "Container node redraw (node=%p, type=%d, "
			"[x=%d,y=%d,width=%d,height=%d])",
			node, node->type, region.x, region.y, region.width, region.height
		);

	// redraw only if rectangles do intersect
	if ( ! gdk_rectangle_intersect(&(node->region), &region, &area) ) {
		return OK;
	}

	if ( CONTAINER_LEAF == node->type ) {
		ViewGeneric *view_ptr = container_node_get_view(node);

		if ( NULL != view_ptr ) {
			return view_redraw(view_ptr, &(node->region));

		} else if ( NULL != window ) {
			GdkGC *gc = gdk_gc_new(window);

			// clear inner part of the container node
			gdk_gc_set_rgb_fg_color(gc, &(specificData->color.bg));
			gdk_draw_rectangle(
					window, gc, TRUE,
					node->region.x, node->region.y,
					node->region.width, node->region.height
				);

			// draw rectangle around the node
			if ( node == specificData->focus_node ) {
				gdk_gc_set_rgb_fg_color(gc, &(specificData->color.active));

			} else {
				gdk_gc_set_rgb_fg_color(gc, &(specificData->color.inactive));
			}
			gdk_draw_rectangle(
					window, gc, FALSE,
					node->region.x, node->region.y,
					node->region.width - 1, node->region.height - 1
				);

			g_object_unref( (gpointer)gc );
		}

	} else {
		gint i;

		for (i = 0; i < node->data.node.size; i++ ) {
			ContainerNode *child_node = container_node_get_child(node, i);

			// log underlaying errors, do not quit whole function
			if ( FAIL == view_cv_redraw_node(specificData, child_node, region, window) ) {
				ERROR_LOG();
			}
		}
	}

	return OK;
}

/**
 * Forward the call to the child if some exists. This is done because the
 * container view is only wrapper for another view.
 *
 * \param view pointer to container view structure
 * \param region region of screen that need to be updated
 * \return result of view_redraw() on child
 */
RCode view_cv_redraw(ViewGeneric *view, GdkRectangle *region) {
	ViewContainer* cv_data = NULL;

	ASSERT( NULL != view );

	cv_data = view_cv_get_data_pointer(view);
	if ( NULL == cv_data ) {
		return FAIL;
	}

	return view_cv_redraw_node(cv_data, cv_data->top_node, *region, view->window);
}

/**
 * Iterate over all child nodes of the top container node and resize views of
 * nodes that has changed dimensions.
 *
 * The window parameter contains pointer to GdkWindow where the whole view is
 * located and it is used for forcing redraw of leaves that do not have view in
 * them.
 *
 * \param node pointer to container node
 * \param window pointer to the window associated with view
 * \return OK on success
 */
RCode view_cv_resize_update(ContainerNode *node, GdkWindow *window) {
	ASSERT( NULL != node );

	TRACE_MESSAGE(VIEW, TL_DEBUG, "Container node resize update (node=%p, type=%d, "
			"[x=%d,y=%d,width=%d,height=%d])",
			node, node->type,
			node->region.x, node->region.y, node->region.width, node->region.height
		);

	// return early, there is no node that would need redrawing
	if ( FALSE == container_node_get_flag(node) ) {
		return OK;
	}

	if ( CONTAINER_LEAF == node->type ) {
		ViewGeneric *view_ptr = container_node_get_view(node);

		if ( NULL != view_ptr ) {
			container_node_reset_flag(node);
			return view_resize(view_ptr, node->region );

		} else if ( NULL != window ) {
			// this allows redraw by container itself when there is no view
			gdk_window_invalidate_rect(window, &(node->region), FALSE);
		}

	} else {
		guint i;

		for (i = 0; i < node->data.node.size; i++ ) {
			RCode result = view_cv_resize_update( container_node_get_child(node, i), window );

			if ( FAIL == result ) {
				ERROR_LOG();
			}
		}
	}

	return OK;
}

/**
 * The method resizes (recursively) all nodes and views associated in container
 * view.
 *
 * \param view view to resize
 * \param newRegion dimensions of the new region
 * \return OK on success
 */
RCode view_cv_resize(ViewGeneric *view, GdkRectangle newRegion) {
	ViewContainer* cv_data = NULL;

	ASSERT( NULL != view );

	cv_data = view_cv_get_data_pointer(view);
	if ( NULL == cv_data ) {
		return FAIL;
	}

	container_node_resize(cv_data->top_node, newRegion);

	return view_cv_resize_update(cv_data->top_node, view->window);
}

/**
 * Set a pointer to the child view into node with focus.
 *
 * \param view pointer to generic view structure
 * \param child pointer to view node to be set in focused node
 * \return OK on success
 */
RCode view_cv_set_view(ViewGeneric *view, ViewGeneric *child) {
	RCode result;
	ViewContainer* cv_data = NULL;

	ASSERT( NULL != view );

	cv_data = view_cv_get_data_pointer(view);
	if ( NULL == cv_data ) {
		return FAIL;
	}

	result = container_node_set_view(cv_data->focus_node, child);
	if ( FAIL == result ) {
		ERROR_NOTE("Can't change view pointer associated with focus node");
	}
	return result;
}

/**
 * \param view pointer to generic view structure
 * \return pointer to view or NULL if there is no view assigned
 */
ViewGeneric* view_cv_get_view(ViewGeneric *view) {
	ViewGeneric* result;
	ViewContainer* cv_data = NULL;

	ASSERT( NULL != view );

	cv_data = view_cv_get_data_pointer(view);
	if ( NULL == cv_data ) {
		return FAIL;
	}

	result = container_node_get_view(cv_data->focus_node);
	if ( error_is_error ) {
		ERROR_NOTE("Can't retrieve view pointer from focused node.");
	}
	return result;
}

/**
 * Split the current leaf node horizontally or vertically. The function checks
 * node parent and if the parent direction is same as requested split direction,
 * then new node is added to parent to prevent creation of too deep structure.
 *
 * The focus is kept on the original node.
 *
 * \param view pointer to container view structure
 * \param dir direction of the new split
 * \return OK on success
 */
RCode view_cv_node_split(ViewGeneric *view, ContainerNodeDirection dir) {
	ViewContainer* cv_data = NULL;
	ContainerNode* parent = NULL;
	ContainerNode* new_leaf = NULL;

	ASSERT( NULL != view );

	cv_data = view_cv_get_data_pointer(view);
	if ( NULL == cv_data ) {
		return FAIL;
	}

	// create new leaf node that will be set as newly created child
	new_leaf = container_node_create(CONTAINER_LEAF, dir);
	if ( error_is_error() ) {
		ERROR_NOTE("Can't create new leaf node for view structure.");
		return FAIL;
	}

	// depending on the parent of the current node
	parent = cv_data->focus_node->parent;

	// if parent is node and has direction in which we are adding
	if ( NULL != parent && dir == container_node_get_direction(parent) ) {
		gint location = container_node_get_position(parent, cv_data->focus_node);

		container_node_insert(parent, new_leaf, location + 1);
		if ( error_is_error() ) {
			ERROR_NOTE("Can't insert new leaf into parent node.");
			container_node_destroy(new_leaf);
			return FAIL;
		}

	} else {
		ContainerNode *node = NULL;

		// create new container that will become parent of original leaf node
		node = container_node_create(CONTAINER_NODE, dir);
		if ( error_is_error() ) {
			ERROR_NOTE("Can't create new container node for structure.");
			container_node_destroy(new_leaf);
			return FAIL;
		}

		if ( NULL != parent ) {
			gint location = container_node_get_position(parent, cv_data->focus_node);

			// replace original leaf with new node
			container_node_set_child(parent, node, location);
			if ( error_is_error() ) {
				ERROR_NOTE("Can't replace focused leaf with node.");
				container_node_destroy(node);
				return FAIL;
			}

		} else {
			// set new node as a top node
			cv_data->top_node = node;
		}

		// fill the new node with focused and new leaf
		container_node_insert(node, cv_data->focus_node, 0);
		if ( error_is_error() ) {
			ERROR_NOTE("Can't insert leaf with focus into new node.");
			container_node_destroy(node);
			container_node_destroy(new_leaf);
			return FAIL;
		} else {
			if ( cv_data->top_node == cv_data->focus_node ) {
				cv_data->top_node = node;
			}
		}

		container_node_insert(node, new_leaf, 1);
		if ( error_is_error() ) {
			ERROR_NOTE("Can't insert leaf without focus into new node.");
			container_node_destroy(node);
			container_node_destroy(new_leaf);
			return FAIL;
		}

	}
	return OK;
}

/**
 * Remove the leaf node that is currenly focused and remove the pointer to it
 * from parent node. If parent node has only one node after the update, then its
 * child is merged to the parent and intermediate node is removed too.
 *
 * This way we ensure that each node in the structure has at least two childs
 * which simplifies things a lot.
 *
 * The focus is moved to the node preceding orignal one in the list.
 *
 * \param view pointer to container view structure
 * \return OK on success
 */
RCode view_cv_node_remove(ViewGeneric *view) {
	ViewContainer* cv_data = NULL;
	ContainerNode* parent = NULL;
	gint location;

	ASSERT( NULL != view );

	cv_data = view_cv_get_data_pointer(view);
	if ( NULL == cv_data ) {
		return FAIL;
	}

	// focus leaf can't be removed if it is top node
	if ( cv_data->top_node == cv_data->focus_node ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Last leaf node can't be removed from view");
		return FAIL;
	}

	// parent of leaf node that is not top must be non-NULL node
	parent = cv_data->focus_node->parent;

	// remove focused leaf from parent node and update focus to one of remaining
	// leafs in parent node
	location = container_node_get_position(parent, cv_data->focus_node);
	if ( FAIL == container_node_remove(parent, location) ) {
		ERROR_NOTE(
				"Can't remove leaf from parent node (parent=%p, location=%d)",
				parent, location
			);
		return FAIL;

	} else {
		// deallocate removed node, but do not fail on error
		if ( FAIL == container_node_destroy( cv_data->focus_node ) ) {
			ERROR_LOG();
		}

		location = ( 0 == location ) ? 0 : location - 1;
		cv_data->focus_node = container_node_get_child(parent, location);
	}

	// if parent size was reduced to 1, then remove it since the node is
	// redundant
	if ( 1 == container_node_get_size(parent) ) {
		ContainerNode* grand_parent = parent->parent;

		// remove focused leaf from node so that it is not deallocated along
		// with the node
		if ( FAIL == container_node_remove(parent, 0) ) {
			ERROR_NOTE(
					"Can't remove leaf from parent node (parent=%p, location=0)",
					parent
				);
			return FAIL;
		}

		// set new parent for the focused leaf, as top or in grand parent
		if ( NULL == grand_parent ) {
			cv_data->top_node = cv_data->focus_node;
			cv_data->focus_node->parent = NULL;

		} else {
			location = container_node_get_position(grand_parent, parent);

			container_node_set_child(grand_parent, cv_data->focus_node, location);
			if ( error_is_error() ) {
				ERROR_NOTE(
						"Can't replace node in parent (parent=%p, location=%d)",
						grand_parent, location
					);
				return FAIL;
			}
		}

		// since the structure is OK, do not fail on deallocation, only report it
		container_node_destroy(parent);
		if ( error_is_error() ) {
			ERROR_LOG();
		}

	}

	return OK;
}

/**
 * Return top node of the container structure.
 *
 * \param view pointer to container view structure
 * \return pointer to container node
 */
ContainerNode* view_cv_get_top_node(ViewGeneric *view) {
	ViewContainer* cv_data = NULL;

	ASSERT( NULL != view );

	cv_data = view_cv_get_data_pointer(view);
	if ( NULL == cv_data ) {
		return NULL;
	}

	return cv_data->top_node;
}

/**
 * Setup one of the child nodes as a new focus node
 *
 * \param view pointer to container view structure
 * \param node new child node to set as focus
 * \return FAIL if current focus and new focus have different top
 */
RCode view_cv_node_set_focus(ViewGeneric *view, ContainerNode *node) {
	ViewContainer* cv_data = NULL;
	ContainerNode *new_top = NULL;

	ASSERT( NULL != view );
	ASSERT( NULL != node );

	if ( CONTAINER_LEAF != node->type ) {
		ERROR_SET(ERROR.INVALID_PARAM);
		ERROR_NOTE("Non-leaf node can't be set as focus node");
		return FAIL;
	}

	cv_data = view_cv_get_data_pointer(view);
	if ( NULL == cv_data ) {
		return FAIL;
	}

	// find top node to which the top node is associated
	for ( new_top = node; new_top->parent != NULL; new_top = new_top->parent);
	if ( cv_data->top_node != new_top ) {
		ERROR_SET(ERROR.INVALID_PARAM);
		ERROR_NOTE("Topmost parent of new focus is not current top node");
		return FAIL;
	}

	cv_data->focus_node = node;

	return OK;
}

/**
 * The function checks the type of the view and either cast the pointer to
 * correct type or return NULL to indicate error.
 *
 * This function can use NULL for error indication because container view
 * should always have specific data.
 *
 * \param view pointer to container view structure
 * \return ViewContainer pointer or NULL on error
 */
ViewContainer* view_cv_get_data_pointer(ViewGeneric* view) {
	ASSERT( NULL != view );

	if ( VIEW_CONTAINER != view->type ) {
		return NULL;
	}

	if ( NULL == view->specific_data ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Specific data pointer is NULL");
		return NULL;
	}

	return (ViewContainer*) view->specific_data;
}
