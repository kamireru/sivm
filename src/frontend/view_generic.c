/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <glib.h>

#include "src/frontend/view_generic.h"
#include "src/frontend/view_image_display.h"
#include "src/frontend/view_application.h"
#include "src/frontend/view_container.h"
#include "src/frontend/view_list_status.h"

#include "src/frontend/model_generic.h"
#include "src/frontend/cntl_generic.h"

/**
 * Allocate memory for generic view structure and setup the type correctly.
 * Depending on the type, call appropriate create() method for specific view and
 * let it handle specific view stuff.
 *
 * The reference count in the new structure is set to 1 to indicate that there
 * is only one place which references it - the variable storing pointer returned
 * from function.
 *
 * \param type which specific view to create
 * \return pointer to generic view structure
 */
ViewGeneric* view_create(ViewType type) {

	ViewGeneric *result = NULL;

	if ( NULL == ( result = (ViewGeneric*) g_try_malloc0(sizeof(ViewGeneric))) ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return result;
	}

	result->type = type;
	result->reference = 1;

	// all generic data must be set before specific data creation since the
	// function that creates specific data may need to update it.
	result->window = NULL;
	result->region.x = 0;
	result->region.y = 0;
	result->region.width = 0;
	result->region.height = 0;
	result->last_sequence = 0;

	result->specific_data = NULL;
	if ( FAIL == view_create_specific_data(result) ) {
		ERROR_NOTE("Unable to create view specific data");
		g_free( (gpointer)result );
		return NULL;
	}

	return result;
}


/**
 * Call specific view destroy() function and then deallocate the generic view
 * and do a cleanup.
 *
 * \param view pointer to generic view structure
 * \return OK on success
 */
RCode view_destroy(ViewGeneric *view) {
	ASSERT( NULL != view );

	// check reference count
	if ( 0 != view->reference ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Reference count is non-zero");
		return FAIL;
	}

	// remove links to the model and controller
	view_set_model(view, NULL);
	view_set_controller(view, NULL);

	// deallocate specific view data if some are present
	if ( NULL != view->specific_data ) {
		view_destroy_specific_data(view);
	}

	// destroy generic view structure
	g_free( (gpointer)view );
	return OK;
}

/**
 * Increase reference count of the supplied view.
 *
 * \param view pointer to generic view structure
 * \return OK on success
 */
RCode view_ref(ViewGeneric *view) {
	ASSERT( NULL != view );

	view->reference += 1;
	return OK;
}

/** Decrease reference counter for view and if reference count reaches zero,
 * then call destroy function on the view.
 *
 * \param view pointer to generic view structure
 * \return OK on success
 */
RCode view_unref(ViewGeneric *view) {
	ASSERT( NULL != view );

	if ( 0 != (--view->reference) ) {
		return OK;
	}

	return view_destroy(view);
}

/**
 * Set link to the window into which the view is to be rendered.
 *
 * \param view pointer to generic view structure
 * \param window pointer to the window
 */
void view_set_window(ViewGeneric *view, GdkWindow *window) {
	ASSERT( NULL != view );

	view->window = window;
}

/**
 * Get pointer to the window associated with view.
 *
 * \param view pointer to generic view structure
 * \return pointer to window
 */
GdkWindow* view_get_window(ViewGeneric* view) {
	ASSERT( NULL != view );

	return view->window;
}

/**
 * Set link from view to model and also establish back link from model
 * to view. Make sure that if there already is link between view and some
 * model that it is removed first.
 *
 * Also make sure that the last sequence is set to zero when model is completely
 * unset.
 *
 * \param view pointer to generic view structure
 * \param model pointer to the model structure to assign
 */
void view_set_model(ViewGeneric *view, ModelGeneric *model) {
	ASSERT( NULL != view );

	// check if there is need for unlinking old model first
	if ( NULL != view->model && view->model != model ) {
		model_remove_view(view->model, view);
		model_unref(view->model);
	}

	if ( NULL == model || view->model != model ) {
		view->last_sequence = 0;
	}

	// the assignment of model must be before view is inserted into observer
	// list and simultaneously we can't set model before if statement. Thus we
	// have to set model in both branches of if statement
	if ( NULL != model && view->model != model ) {
		view->model = model;
		model_insert_view(model, view);
		model_ref(model);
	} else {
		view->model = model;
	}
}

/**
 * \param view pointer to generic view structure
 * \return pointer to model associated with view
 */
ModelGeneric* view_get_model(ViewGeneric *view) {
	ASSERT( NULL != view );

	return view->model;
}

/**
 * Set link from view to controller and also establish back link from controller
 * to view. Make sure that if there already is link between view and some
 * controller that it is removed first.
 *
 * \param view pointer to generic view structure
 * \param controller pointer to the controller structure to assign
 */
void view_set_controller(ViewGeneric *view, CntlGeneric *controller) {
	ASSERT( NULL != view );

	// check if there is need for unlinking old controller first
	if ( NULL != view->controller && view->controller != controller ) {
		cntl_set_view(view->controller, NULL);
		cntl_unref(view->controller);
	}

	// check if there is a need to link to new controller
	if ( NULL != controller && view->controller != controller ) {
		cntl_set_view(controller, view);
		cntl_ref(controller);
	}

	// assign new controller to the view
	view->controller = controller;
}

/**
 * \param view pointer to generic view structure
 * \return pointer to controller associated with view
 */
CntlGeneric* view_get_controller(ViewGeneric *view) {
	ASSERT( NULL != view );

	return view->controller;
}

/**
 * The function check what kind of specific view is linked from the generic
 * one and call appropriate update() function over appropriate object.
 *
 * The function does not return status since caller usually does not know how to
 * resolve the error (the function should be called from model notification
 * function).
 *
 * The function intercepts all errors comming from specific functions and logs
 * them for investigation.
 *
 * The sequence number should uniquely identify the change that triggered the
 * update. It can be used for checking if same update request was being sent
 * twice (and also for unit testing to make sure notification was received).
 *
 * \param view pointer to generic view structure
 * \param seq sequence number of the update
 */
RCode view_update(ViewGeneric *view, gint seq) {
	RCode result = OK;

	ASSERT( NULL != view );

	// check if invalid or already known sequence number is being sent
	// in that case do not process the notification
	if ( view->last_sequence == seq || 0 == seq ) {
		return OK;
	} else {
		view->last_sequence = seq;
	}

	TRACE_MESSAGE(VIEW, TL_INFO,
			"Update view event received (view=%p, type=%d, seq=%d)",
			view, view->type, seq
		);

	switch ( view->type ) {
		case VIEW_NONE:
			TRACE_MESSAGE(VIEW, TL_INFO, "No specific method for update() call (view=%p, type=%d)",
					view, view->type
				);
			result = OK;
			break;

		case VIEW_APPLICATION:
			result = view_app_update(view, seq);
			break;

		case VIEW_IMAGE_DISPLAY:
			result = view_idv_update(view, seq);
			break;

		case VIEW_CONTAINER:
			result = view_cv_update(view, seq);
			break;

		case VIEW_LIST_STATUS:
			result = view_lsv_update(view, seq);
			break;

		default:
			TRACE_MESSAGE(VIEW, TL_INFO, "Unsupported view type (cntl=%p, type=%d)",
					view, view->type
				);
	}

	if ( FAIL == result ) {
		ERROR_LOG();
	}

	return result;
}

/**
 * The function check what kind of specific view is linked from the generic
 * one and call appropriate redraw() function over appropriate object.
 *
 * \param view pointer to generic view structure
 * \param region region of screen that need to be updated
 * \return OK on success
 */
RCode view_redraw(ViewGeneric *view, GdkRectangle *region) {
	RCode result = OK;
	GdkRectangle subregion;

	ASSERT( NULL != view );
	ASSERT( NULL != region );

	// check if updated region is in view region
	if ( ! gdk_rectangle_intersect(&(view->region), region, &subregion) ) {
		return result;
	} else {
		TRACE_MESSAGE(VIEW, TL_DEBUG,
				"Redraw view event received (view=%p, type=%d, region=[%d,%d,%d,%d])",
				view, view->type,
				subregion.x, subregion.y, subregion.width, subregion.height
			);
	}

	switch ( view->type ) {
		case VIEW_NONE:
			TRACE_MESSAGE(VIEW, TL_INFO, "No specific method for update() call (view=%p, type=%d)",
					view, view->type
				);
			result = OK;
			break;

		case VIEW_APPLICATION:
			result = view_app_redraw(view, &subregion);
			break;

		case VIEW_IMAGE_DISPLAY:
			result = view_idv_redraw(view, &subregion);
			break;

		case VIEW_CONTAINER:
			result = view_cv_redraw(view, &subregion);
			break;

		case VIEW_LIST_STATUS:
			result = view_lsv_redraw(view, &subregion);
			break;

		default:
			TRACE_MESSAGE(VIEW, TL_INFO, "Unsupported view type (cntl=%p, type=%d)",
					view, view->type
				);
			result = FAIL;
	}

	return result;
}

/**
 * The function update position and dimension of the view in the application
 * window and then it will call appropriate view_*_resize() function for
 * specific view.
 *
 * The specific funtion is called with new region as a parameter, old region can
 * be retrievable from generic view. The view region is update after the call to
 * specific function finish.
 *
 * The new region dimensions are set only if there is no error in the specific
 * resize function invocation.
 *
 * \param view pointer to generic view structure
 * \param newRegion new area assigned to the view
 * \return OK on success
 */
RCode view_resize(ViewGeneric *view, GdkRectangle newRegion) {
	RCode result = OK;

	ASSERT( NULL != view );

	// call update only if there is a difference between old and new region
	if ( view->region.x != newRegion.x || view->region.width != newRegion.width
		|| view->region.y != newRegion.y || view->region.height != newRegion.height ) {

		TRACE_MESSAGE(VIEW, TL_DEBUG,
				"Resize view event received(view=%p, type=%d, [x=%d,y=%d,width=%d,height=%d])",
				view, view->type, newRegion.x, newRegion.y, newRegion.width, newRegion.height
			);

		switch ( view->type ) {
			case VIEW_NONE:
				TRACE_MESSAGE(VIEW, TL_INFO, "No specific method for update() call (view=%p, type=%d)",
						view, view->type
					);
				result = OK;
				break;

			case VIEW_APPLICATION:
				result = view_app_resize(view, newRegion);
				break;

			case VIEW_IMAGE_DISPLAY:
				result = view_idv_resize(view, newRegion);
				break;

			case VIEW_CONTAINER:
				result = view_cv_resize(view, newRegion);
				break;

			case VIEW_LIST_STATUS:
				result = view_lsv_resize(view, newRegion);
				break;

			default:
				TRACE_MESSAGE(VIEW, TL_INFO, "Unsupported view type (cntl=%p, type=%d)",
						view, view->type
					);
				result = FAIL;
		}
	}

	// update view region
	if ( OK == result ) {
		view->region = newRegion;
	}

	return result;
}

/**
 * Store hints to the desired screen estate into provided variable.
 *
 * \param view pointer to generic view structure
 * \param hint pointer to storage space
 * \return OK
 */
RCode view_get_hint(ViewGeneric* view, GdkPoint *hint) {
	ASSERT( NULL != view );
	ASSERT( NULL != hint );

	hint->x = view->region_hint.x;
	hint->y = view->region_hint.y;
	return OK;
}

/**
 * The function uses view type to determine which data structure is
 * necessary for specific view of that type and allocates and initializes the
 * structure for view.
 *
 * \param view pointer to generic view structure
 * \return OK on success
 */
RCode view_create_specific_data(ViewGeneric *view) {
	RCode result = OK;

	ASSERT( NULL != view );

	switch (view->type) {
		case VIEW_NONE:
			// this specific view does not have special data
			view->specific_data = NULL;
			break;

		case VIEW_APPLICATION:
			view->specific_data = (gpointer) view_app_create(view);
			result = ( NULL == view->specific_data ) ? FAIL : OK;
			break;

		case VIEW_IMAGE_DISPLAY:
			view->specific_data = (gpointer) view_idv_create(view);
			result = ( NULL == view->specific_data ) ? FAIL : OK;
			break;

		case VIEW_CONTAINER:
			view->specific_data = (gpointer) view_cv_create(view);
			result = ( NULL == view->specific_data ) ? FAIL : OK;
			break;

		case VIEW_LIST_STATUS:
			view->specific_data = (gpointer) view_lsv_create(view);
			result = ( NULL == view->specific_data ) ? FAIL : OK;
			break;

		default:
			// no need to fail, since no function can operate over specific data
			// of unknown view type
			TRACE_MESSAGE(VIEW, TL_INFO, "Unsupported view type (cntl=%p, type=%d)",
					view, view->type
				);
			break;
	}

	return result;
}

/**
 * The function uses view type to determine how to deallocate view type
 * specific data and deallocates it.
 *
 * \param view pointer to generic view structure
 * \return OK on success
 */
RCode view_destroy_specific_data(ViewGeneric* view) {
	RCode result = OK;

	ASSERT( NULL != view );

	switch (view->type) {
		case VIEW_NONE:
			// this type does not have specific data to deallocate
			break;

		case VIEW_APPLICATION:
			result = view_app_destroy((ViewApplication*)view->specific_data);
			break;

		case VIEW_IMAGE_DISPLAY:
			result = view_idv_destroy((ViewImageDisplay*)view->specific_data);
			break;

		case VIEW_CONTAINER:
			result = view_cv_destroy((ViewContainer*)view->specific_data);
			break;

		case VIEW_LIST_STATUS:
			result = view_lsv_destroy((ViewListStatus*)view->specific_data);
			break;

		default:
			TRACE_MESSAGE(VIEW, TL_INFO, "Unsupported view type (cntl=%p, type=%d)",
					view, view->type
				);
			break;
	}

	view->specific_data = NULL;
	return result;
}
