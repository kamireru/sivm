/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/frontend/view.h"
#include "src/frontend/container_node.h"

/**
 * Allocate memory for container node of given type or container leaf. The
 * contructor function does not allocate any space for child pointers.
 *
 * \param type type of the node
 * \param direction direction of the node
 * \return pointer to created node
 */
ContainerNode* container_node_create(ContainerNodeType type, ContainerNodeDirection direction) {
	ContainerNode *result = NULL;

	if ( NULL == ( result = (ContainerNode*) g_try_malloc0(sizeof(ContainerNode))) ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return result;
	}

	result->type = type;
	if ( CONTAINER_NODE == type ) {
		result->data.node.direction = direction;
		result->data.node.size = 0;
		result->data.node.list = NULL;
	} else {
		result->data.leaf.view = NULL;
	}

	return result;
}

/**
 * Deallocate container node and all its childs. Do not deallocate views, only
 * unreference them.
 *
 * \param node pointer to container node
 * \return OK on success
 */
RCode container_node_destroy(ContainerNode* node) {

	// deallocation of NULL pointer is always successful
	if ( NULL == node ) {
		return OK;
	}

	if ( CONTAINER_NODE == node->type ) {
		if ( 0 < node->data.node.size ) {
			int i;

			// deallocate child nodes
			for (i = 0; i < node->data.node.size; i++) {
				container_node_destroy(node->data.node.list[i]);
			}
			g_free( (gpointer) node->data.node.list );
		}
	} else {
		if ( NULL != node->data.leaf.view ) {
			view_unref(node->data.leaf.view);
		}
	}

	g_free( (gpointer)node );
	return OK;
}

/**
 * If the supplied node is leaf, then always return vertical directionality.
 *
 * \param node pointer to container node
 * \return node directionality
 */
ContainerNodeDirection container_node_get_direction(ContainerNode *node) {
	ASSERT( NULL != node );
	if ( CONTAINER_NODE == node->type ) {
		return node->data.node.direction;
	} else {
		return NODE_VERTICAL;
	}
}

/**
 * \param node pointer to container node
 * \param child pointer to container node to be added
 * \param index position in list where node will be added
 * \return OK on success
 */
RCode container_node_insert(ContainerNode *node, ContainerNode *child, guint index) {
	ContainerNode **tmp_list = NULL;
	guint tmp_size;
	guint i;

	ASSERT( NULL != node );
	ASSERT( NULL != child );

	// check node type
	if ( CONTAINER_LEAF == node->type ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Can't insert container node into leaf");
		return FAIL;
	}

	// try to reallocate memory for a child node list
	tmp_size = node->data.node.size + 1;
	tmp_list = node->data.node.list;
	node->data.node.list = (ContainerNode**) g_try_realloc(
			node->data.node.list, tmp_size * sizeof(ContainerNode*)
		);

	// check if allocation was successfull
	if ( NULL == node->data.node.list ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		ERROR_NOTE("Can't reallocate node list to % elements", tmp_size);
		node->data.node.list = tmp_list;
		return FAIL;

	} else {
		node->data.node.size = tmp_size;
	}

	// insert child pointer to the desired place
	index = ( node->data.node.size <= index ) ? node->data.node.size-1 : index;
	index = ( index < 0 ) ? 0 : index;

	for (i = node->data.node.size - 1; i != index; i--) {
		node->data.node.list[i] = node->data.node.list[i-1];
	}
	node->data.node.list[i] = child;
	child->parent = node;

	return OK;
}

/**
 * Remove child node from specified position in the list.
 *
 * The child node pointer is not deallocated, only remove from list. Calling
 * code must handle node deallocation separately.
 *
 * \param node pointer to container node
 * \param index position in list from which node will be removed
 * \return OK on success
 */
RCode container_node_remove(ContainerNode *node, guint index) {
	ContainerNode **tmp_list = NULL;
	guint tmp_size;
	guint i;

	ASSERT( NULL != node );

	// check node type
	if ( CONTAINER_LEAF == node->type ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Can't insert container node into leaf");
		return FAIL;
	}

	// check index bounds and return OK if index is not in list
	if ( index < 0 || node->data.node.size <= index ) {
		return OK;
	}

	// reset the parent pointer of container node
	node->data.node.list[index]->parent = NULL;

	// shift list content to overwrite removed node
	for (i = index; i < node->data.node.size - 1; i++ ) {
		node->data.node.list[i] = node->data.node.list[i+1];
	}

	// reallocate memory for list
	tmp_list = node->data.node.list;
	tmp_size = node->data.node.size - 1;

	if ( 0 == tmp_size ) {
		g_free( (gpointer)tmp_list );
		tmp_list = NULL;

	} else {
		tmp_list = (ContainerNode**) g_try_realloc(
				tmp_list, tmp_size * sizeof(ContainerNode*)
			);
		if ( NULL == tmp_list ) {
			ERROR_SET(ERROR.MEMORY_FAILURE);
			ERROR_NOTE("Can't reallocate node list to % elements", tmp_size);
			return FAIL;
		}
	}

	node->data.node.size = tmp_size;
	node->data.node.list = tmp_list;

	return OK;
}

/**
 * Return number of child nodes assigned to the current node. If current node is
 * leaf then zero is returned
 *
 * \param node pointer to container node
 * \return number of child nodes
 */
guint container_node_get_size(ContainerNode *node) {
	ASSERT( NULL != node );

	if ( CONTAINER_LEAF == node->type ) {
		return 0;

	} else {
		return node->data.node.size;
	}
}

/**
 * Return child node on the given position in the node list. If position is out
 * of range or if function is called over leaf then return NULL.
 *
 * \param node pointer to container node
 * \param index position in list from which node will be returned
 * \return NULL on errror, pointer otherwise
 */
ContainerNode* container_node_get_child(ContainerNode *node, guint index) {
	ASSERT( NULL != node );

	if ( CONTAINER_LEAF == node->type ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Can't get child node from leaf.");
		return NULL;

	} else {
		if ( index < 0 || node->data.node.size <= index ) {
			ERROR_SET(ERROR.OUT_OF_RANGE);
			ERROR_NOTE("Container do not have child with given index (child=%d, size=%d)",
					index, node->data.node.size
				);
			return NULL;

		} else {
			return node->data.node.list[index];
		}
	}
}

/**
 * Replace node on given location with a new node. Original node is
 * disconnected, but not deallocated.
 *
 * Note: There is a possibility for memory leak if replaced node is not managed
 * properly (retrieved beforehand and reassigned or deallocated).
 *
 * \param node pointer to container node
 * \param child pointer to container node to be added
 * \param index position in list where node will be added
 * \return OK on success
 */
RCode container_node_set_child(ContainerNode *node, ContainerNode* child, guint index) {
	ASSERT( NULL != node );

	if ( CONTAINER_LEAF == node->type ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Can't get child node from leaf.");
		return FAIL;

	} else {
		if ( index < 0 || node->data.node.size <= index ) {
			ERROR_SET(ERROR.OUT_OF_RANGE);
			ERROR_NOTE("Container do not have child with given index (child=%d, size=%d)",
					index, node->data.node.size
				);
			return FAIL;

		} else {
			node->data.node.list[index]->parent = NULL;
			node->data.node.list[index] = child;
			node->data.node.list[index]->parent = node;
			return OK;
		}
	}
}

/**
 * Return index of the child node pointer in list or indicate that the child is
 * not in the node.
 *
 * \param node pointer to container node
 * \param child pointer to container node to be located
 * \return index of child or -1 if it is not found
 */
gint container_node_get_position(ContainerNode *node, ContainerNode *child) {
	ASSERT( NULL != node );

	if ( CONTAINER_NODE == node->type ) {
		int i;

		for (i = 0; i < node->data.node.size; i++) {
			if ( node->data.node.list[i] == child ) {
				return i;
			}
		}
	}

	return -1;
}


/**
 * Reset dirty flag on the container leaf node.
 *
 * \param node pointer to container node
 * \return OK
 */
RCode container_node_reset_flag(ContainerNode *node) {
	ASSERT( NULL != node );

	if ( CONTAINER_LEAF == node->type ) {
		node->data.leaf.dirty = FALSE;
	}

	return OK;
}

/**
 * Check if either node has dirty flag set to true
 *
 * \param node pointer to container node
 * \return TRUE if node has dirty flag set
 */
gboolean container_node_get_flag(ContainerNode *node) {
	gboolean result = TRUE;

	ASSERT( NULL != node );

	if ( CONTAINER_LEAF == node->type ) {
		result = node->data.leaf.dirty;

	} else {
		guint i;

		for (i = 0; i < node->data.node.size; i++) {
			result = result || container_node_get_flag(node->data.node.list[i]);
		}
	}
	return result;
}

/**
 * Recursively resize container nodes according to new region dimension and view
 * hints.
 *
 * Hint has two values - 0 which means grab whatever space is available and
 * positive value with gives fixed amount of space requested.
 *
 * If the new region is not big enough to accomodate all childs, then region
 * will be split proportionaly to requests (where nodes with hint==0 are assumed
 * to reqest half space of new region).
 *
 * \param node pointer to container node
 * \param region new node region dimension
 * \return OK on success
 */
RCode container_node_resize(ContainerNode *node, GdkRectangle region) {
	ASSERT( NULL != node );

	// if there is no difference between new region and old region, do nothing
	TRACE_MESSAGE(CONT, TL_DEBUG, "Container node resize (node=%p, type=%d, "
			"[x=%d,y=%d,width=%d,height=%d])",
			node, node->type,
			region.x, region.y, region.width, region.height
		);

	if ( CONTAINER_LEAF == node->type ) {
		if ( node->region.x != region.x || node->region.width != region.width
			|| node->region.y != region.y || node->region.height != region.height ) {

			node->region = region;
			node->data.leaf.dirty = TRUE;
		}

	} else {
		gboolean is_horizontal = (NODE_HORIZONTAL == node->data.node.direction);
		gboolean is_enough = TRUE;
		guint offset = 0;
		guint desired_space = 0;
		guint greedy_count = 0;
		GdkRectangle new_region = region;
		gint i, tmp;

		// first, check how much screen estate do child nodes require
		for (i = 0; i < node->data.node.size; i++ ) {
			ContainerNode* child = node->data.node.list[i];
			GdkPoint hint = {0, 0};

			container_node_get_hint(child, &hint);

			tmp = ( is_horizontal ) ? hint.x : hint.y;
			desired_space += tmp;
			greedy_count += (guint)( 0 == tmp);
		}

		// check if there is enough estate for all requests and if not add
		// requests of greedy nodes (greedy node wants 50% of available space)
		is_enough = desired_space <= ( is_horizontal ? region.width : region.height);
		if ( ! is_enough ) {
			desired_space += (greedy_count * (( is_horizontal ) ? region.width : region.height)) / 2;
		}

		// actually update the child nodes
		for (i = 0; i < node->data.node.size; i++ ) {
			ContainerNode* child = node->data.node.list[i];
			GdkPoint hint = {0, 0};

			container_node_get_hint(child, &hint);

			if ( is_horizontal ) {
				if ( is_enough ) {
					new_region.width = ( 0 != hint.x)
						? hint.x
						:  (region.width - desired_space) / greedy_count;

				} else {
					new_region.width = ( 0 != hint.x)
						? hint.x
						:  region.width / 2;
					new_region.width = (new_region.width * region.width) /  desired_space;
				}

				new_region.x = region.x + offset;
				offset = offset + new_region.width;

			} else {
				if ( is_enough ) {
					new_region.height = ( 0 != hint.y)
						? hint.y
						:  (region.height - desired_space) / greedy_count;

				} else {
					new_region.height = ( 0 != hint.y)
						? hint.y
						:  region.height / 2;
					new_region.height = (new_region.height * region.height) /  desired_space;
				}

				new_region.y = region.y + offset;
				offset = offset + new_region.height;
			}

			container_node_resize(node->data.node.list[i], new_region);
		}

		// change region size for parent node
		node->region = region;
	}

	return OK;
}

/**
 * Retrieve region dimenstion hints for container node. Use hints stored in view
 * if the node is LEAF and compute the hints from children otherwise.
 *
 * The x hint is equal to (y hints are similar):
 *  - max from child x hint if node is vertical
 *  - sum of child x hints if node is horizontal and all hints greater than zero
 *  - min of child x hints if node is horizontal and there is hint equal to zero
 *
 * IE:, when child nodes form row, y hint is maximum child y hint (assuming that
 * greedy y hint childs will be ok with whatever size they will get).
 *
 * And x hint is sum if all childs have concrete needs and greedy otherwise.
 *
 * \note: greedy with minimum would be nice here
 *
 * \param node pointer to container node
 * \param hint pointer to variable where the hint should be stored
 * \return OK on success
 */
RCode container_node_get_hint(ContainerNode* node, GdkPoint* hint) {
	GdkPoint greedy = {0, 0};
	GdkPoint max = {0, 0};
	GdkPoint sum = {0, 0};

	ASSERT( NULL != node );

	if ( CONTAINER_LEAF == node->type ) {
		if ( NULL != node->data.leaf.view ) {
			view_get_hint(node->data.leaf.view, hint);

		} else {
			*hint = sum;
		}

	} else {
		guint i;

		// get range information about child hints
		for (i = 0; i < node->data.node.size; i++ ) {
			ContainerNode* child = node->data.node.list[i];
			GdkPoint tmp = {0, 0};

			if ( CONTAINER_LEAF == child->type && NULL != child->data.leaf.view ) {
				view_get_hint(child->data.leaf.view, &tmp);
			} else {
				container_node_get_hint(child, &tmp);
			}

			greedy.x += ( 0 == tmp.x ) ? 1 : 0;
			greedy.y += ( 0 == tmp.y ) ? 1 : 0;

			max.x = ( max.x > tmp.x ) ? max.x : tmp.x;
			max.y = ( max.y > tmp.y ) ? max.y : tmp.y;

			sum.x += tmp.x;
			sum.y += tmp.y;
		}

		// compute hints according to node directionality
		if (NODE_HORIZONTAL == node->data.node.direction) {
			hint->y = max.y;
			hint->x = ( 0 < greedy.x ) ? 0 : sum.x;
		} else {
			hint->y = ( 0 < greedy.y ) ? 0 : sum.y;
			hint->x = max.x;
		}
	}

	return OK;
}

/**
 * Set pointer to the view into the container node.
 *
 * Setting NULL pointer as a view resets the container to inital state without
 * any view set.
 *
 * \param node pointer to container node
 * \param view pointer to generic view structure
 * \return OK on success
 */
RCode container_node_set_view(ContainerNode *node, ViewGeneric *view) {
	ASSERT( NULL != node );

	// check node type
	if ( CONTAINER_NODE == node->type ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Can't insert view pointer into node");
		return FAIL;
	}

	if ( NULL != node->data.leaf.view ) {
		view_unref(node->data.leaf.view);
	}

	node->data.leaf.view = view;

	if ( NULL != view ) {
		view_ref(view);
	}

	return OK;
}

/**
 * Return view pointer stored in the leaf node.
 *
 * \param node pointer to container node
 * \return pointer to view structure stored in leaf
 */
ViewGeneric* container_node_get_view(ContainerNode *node) {
	ASSERT( NULL != node );

	// check node type
	if ( CONTAINER_NODE == node->type ) {
		ERROR_SET(ERROR.INVALID_OPERATION);
		ERROR_NOTE("Container nodes do not contain view pointer");
		return NULL;
	}

	return node->data.leaf.view;
}
