/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file observer_list.h
 * \brief Header file with observer list API.
 *
 * The observer list is used in the model to store all views that draw data from
 * the model.
 *
 * Every new view that intends to use data from given model registers
 * in the models observer list. All view that depends on model are updated when
 * the model data change.
 *
 * Note: For more information, see Observer/Observable design pattern. In this
 * case, view is an observer and model is an observable.
 */
#ifndef __FRONTEND_OBSERVER_LIST_H__
#define __FRONTEND_OBSERVER_LIST_H__

#include "src/frontend/mvc_types.h"
#include "src/common/common_def.h"


/**
 * \brief The initial size of the list if no size is given during creation.
 */
#define OBSERVER_LIST_INCREMENT 4

/** \brief Create observer list. */
extern ObserverList* observer_create_list();

/** \brief Deallocate list and unregister all observers. */
extern RCode observer_destroy(ObserverList *list);

/** \brief Get number of current observers. */
extern guint observer_get_count(ObserverList *list);

/** \brief Forward update() call to all registered views. */
extern void observer_update(ObserverList *list, guint seq);

/** \brief Add new observer view into the list. */
extern RCode observer_insert(ObserverList *list, ViewGeneric *view);

/** \brief Remove an observer view from the list. */
extern RCode observer_remove(ObserverList * list, ViewGeneric *view);


#endif
