/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FRONTEND_MVC_FUNCTION_H
#define FRONTEND_MVC_FUNCTION_H

#include "src/common/common_def.h"
#include "src/frontend/mvc_types.h"

/** \brief Create MVC triad from components of specified types. */
extern ViewGeneric* mvc_triad_create(ModelType modelType, ViewType viewType, CntlType cntlType);

#endif
