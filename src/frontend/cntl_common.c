/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gdk/gdk.h>
#include "src/fa/fa.h"
#include "src/frontend/cntl_common.h"

/**
 * Storage variable holding mask of state flags that is used for filtering out
 * uninteresting states from the compared keys.
 *
 * Note: Only first MOD mask is clear from the documentation (used for ALT key).
 * The rest is unsure so we exclude them from valid inputs. If we find some use
 * for those masks, they can be safely added.
 */
static guint key_state_mask = GDK_SHIFT_MASK | GDK_LOCK_MASK | GDK_CONTROL_MASK |
	GDK_MOD1_MASK;


/**
 * The compare function for comparing the keys takes pointer to GdkEventKey
 * structure and uses information stored there for comparing two such
 * structures.
 *
 * For comparison it is not using all fields, it is interested only in subset
 * which relates directly to the key input parsing that need to be done by
 * automatons:
 *
 * GdkEventType type :
 *   the type of the event (GDK_KEY_PRESS or GDK_KEY_RELEASE)
 *
 * guint keyval :
 *    the key that was pressed or released. See the <gdk/gdkkeysyms.h> header
 *    file for a complete list of GDK key codes.
 *
 * guint state :
 *   a bit-mask representing the state of the modifier keys and the
 *   pointer buttons. See GdkModifierType for all flags that might be present
 *
 *   The comparison function is using {key_state_mask} variable for filtering
 *   out states that does not interest us
 *
 * \param arg1 first data pointer
 * \param arg2 second data pointer
 * \return FA_LESS if arg1 < arg2, FA_GREATER if arg1 > arg2, FA_EQUAL otherwise
 */
FaRelate cntl_key_compare(void* arg1, void* arg2) {
	GdkEventKey *key1 = (GdkEventKey*)arg1;
	GdkEventKey *key2 = (GdkEventKey*)arg2;

	// test for NULL which is considered smaller than everything else
	if ( NULL == key1 || NULL == key2 ) {
		if ( NULL == key1 && NULL != key2 ) {
			return FA_LESS;
		} else if ( NULL != key1 && NULL == key2 ) {
			return FA_GREATER;
		} else {
			return FA_EQUAL;
		}
	}
	// no key pointer can be null here (due to part above)

	// compare type fields, if they are equal, continue with testing
	if ( key1->type < key2->type ) {
		return FA_LESS;
	} else if ( key1->type > key2->type ) {
		return FA_GREATER;
	}

	// compare keyval fields, if they are equal, continue with testing
	if ( key1->keyval < key2->keyval ) {
		return FA_LESS;
	} else if ( key1->keyval > key2->keyval ) {
		return FA_GREATER;
	}

	// compare masked state fields
	if ( (key1->state & key_state_mask) < (key2->state & key_state_mask) ) {
		return FA_LESS;
	} else if ( (key1->state & key_state_mask) > (key2->state & key_state_mask) ) {
		return FA_GREATER;
	}

	// finaly decide that the keys are equal
	return FA_EQUAL;
}
