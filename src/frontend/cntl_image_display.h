/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file cntl_image_display.h
 * \brief Provide API for image display controller initialization.
 *
 * The image display controller is in charge of handling high level input.
 */
#ifndef __TEST_FRONTEND_CNTL_IMAGE_DISPLAY_H__
#define __TEST_FRONTEND_CNTL_IMAGE_DISPLAY_H__

#include "src/frontend/cntl_common.h"

#include "src/frontend/cntl.h"
#include "src/frontend/view.h"
#include "src/frontend/model.h"

typedef enum {
		GROUP_SELECT = 0,
		GROUP_COPY = 1,
		GROUP_MOVE = 2
	} CntlGroupActionType;

/**
 * Specific data for image display controller.
 *
 * Provides storage for values parsed out from key sequence.
 */
typedef struct {
		/** \brief Storage variable for parsed out count. */
		guint count;

		/** \brief Storage variable for type of movement in list. */
		MoveIndexType movement_type;

		/** \brief Storage variable for auto zoom type. */
		ZoomType auto_zoom;

		/** \brief Storage variable for zoom type. */
		ZoomDirection zoom_type;

		/** \brief Storage variable for direction of panning. */
		PanDirection pan_direction;

		/** \brief Storage for group identificator. */
		unsigned char group_id;

		/** \brief Storage variable for group action selection. */
		CntlGroupActionType group_action;

	} CntlImageDisplayData;


/** \brief Create image display controller automaton. */
extern FaAutomaton* cntl_idv_create_automaton();

/** \brief Allocate memory for image display controller data. */
extern void* cntl_idv_create_data();

/** \brief Key press processing function for generic controller. */
extern CntlProcessResult cntl_idv_process(CntlGeneric *cntl, GdkEventKey *input);


// Internal functions - exposed for unit tests only

/** \brief Reset controller specific data. */
extern void cntl_idv_data_reset(void *data);

/** \brief Parse count from keystroke sequence. */
extern void cntl_idv_parse_count(unsigned int from, unsigned int to, void* value, void* data);

/** \brief Parse type of the movement in the image list. */
extern void cntl_idv_parse_movement(unsigned int from, unsigned int to, void* value, void* data);

/** \brief Parse type of the auto-zoom to use. */
extern void cntl_idv_parse_auto_zoom(unsigned int from, unsigned int to, void* value, void* data);

/** \brief Parse type of the zoom modification. */
extern void cntl_idv_parse_zoom(unsigned int from, unsigned int to, void* value, void* data);

/** \brief Parse direction of the panning. */
extern void cntl_idv_parse_pan(unsigned int from, unsigned int to, void* value, void* data);

/** \brief Parse group related action. */
extern void cntl_idv_parse_action(unsigned int from, unsigned int to, void* value, void* data);

/** \brief Parse character id of the group to act on. */
extern void cntl_idv_parse_group(unsigned int from, unsigned int to, void* value, void* data);


#endif
