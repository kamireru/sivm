/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file view_application.h
 *
 * The ViewApplication structure implements view representing whole application.
 * This view does not render anything, it acts as highest level view, parent to
 * all views in application. There can be only one such view in whole
 * application.
 *
 * The view contains pointer to one child view. This pointer is set to NULL per
 * default and must be filled manually upon application startup. Since the child
 * can be NULL, the whole application view must be able to handle such child
 * gracefully.
 *
 * Note: The functions exported in this header file are used only in
 * view_generic.c and nowhere else. They should not be called directly, only
 * through ViewGeneric functions.
 *
 * Because of that, naming convention for these 'view internal' functions allows
 * for more cryptic names. The function names have three letter acronym after
 * the 'view_' prefix.
 *
 * In case of application view, the acronym is 'app'.
 */
#ifndef FRONTEND_VIEW_APPLICATION_H
#define FRONTEND_VIEW_APPLICATION_H

#include "src/frontend/view_common.h"

/**
 * \brief Structure storing information about application view.
 *
 * The application view does not display anything, it servers as intermediate
 * layer between all views and framework used for displaying everyhing. It
 * handles application exit in manner consistent with rest of the GUI framework.
 */
typedef struct {

		/** \brief Pointer to child generic view. */
		ViewGeneric *child;

	} ViewApplication;


// specific implementation of common view functions

/** \brief Allocate memory for view and initialize variables. */
extern ViewApplication* view_app_create(ViewGeneric *parent);

/** \brief Deallocate memory for view. */
extern RCode view_app_destroy(ViewApplication *specificData);

/** \brief Update the application view from model. */
extern RCode view_app_update(ViewGeneric *view, guint seq);

/** \brief Redraw application view area. */
extern RCode view_app_redraw(ViewGeneric *view, GdkRectangle *region);

/** \brief Resize the application view display. */
extern RCode view_app_resize(ViewGeneric *view, GdkRectangle newRegion);


// view specific functions

/** \brief Set child view to the application view. */
extern RCode view_app_set_child(ViewGeneric *view, ViewGeneric *child);

/** \brief Get pointer to the child view of application view. */
extern ViewGeneric* view_app_get_child(ViewGeneric *view);


// internal functions, exported for unit tests only

/** \brief Cast specific data pointer to correct type. */
extern ViewApplication* view_app_get_data_pointer(ViewGeneric* view);

#endif
