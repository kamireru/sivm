/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file view_image_display.h
 *
 * Note: The functions exported in this header file are used only in
 * view_generic.c and nowhere else. They should not be called directly, only
 * through ViewGeneric functions.
 *
 * Because of that, naming convention for these 'view internal' functions allows
 * for more cryptic names. The function names have three letter acronym after
 * the 'view_' prefix.
 *
 * In case of image display view, the acronym is 'idv'.
 */
#ifndef FRONTEND_VIEW_IMAGE_DISPLAY_H
#define FRONTEND_VIEW_IMAGE_DISPLAY_H

#include <gdk-pixbuf/gdk-pixbuf.h>

#include "src/frontend/view_common.h"

/**
 * The enum defines various zoom types that can be used for initial display of
 * an image.
 */
typedef enum {
		/** Zoom to the original image size (ie 100%). */
		ZOOM_FIT_CUSTOM = 0,

		/** Zoom so that the whole image can be seen. */
		ZOOM_FIT_IMAGE = 1,

		/** Zoom so that the whole image width can be seen. */
		ZOOM_FIT_WIDTH = 2,

		/** Zoom so that the whole image height can be seen. */
		ZOOM_FIT_HEIGHT = 3,
	} ZoomType;

/**
 * The enum defines direction in which the current zoom can change.
 */
typedef enum {
		/** Reset zoom to the preset according to ZoomType. */
		ZOOM_DIRECTION_RESET = 0,

		/** Zoom in and increase magnification of image. */
		ZOOM_DIRECTION_IN = 1,

		/** Zoom out and decrease magnification of image. */
		ZOOM_DIRECTION_OUT = 2
	} ZoomDirection;

/**
 * The enum defines directions into which panning function can pan the image.
 */
typedef enum {
		PAN_UP = 0,
		PAN_RIGHT = 1,
		PAN_DOWN = 2,
		PAN_LEFT = 3
	} PanDirection;

/**
 * \brief Struture storing informatio about image display view.
 *
 * The image display view actually displays image on the allocated portion of
 * the application window (viewport). Often image is smaller or bigger than
 * allocated viewport. This view allows zooming and panning to display some part
 * of the image.
 */
typedef struct {

		/** \brief Area in the view region where image is be displayed. */
		GdkRectangle viewport;

		/** \brief Dimensions of currently displayed image. */
		GdkPoint dimension;

		/** \brief Offset of the image when displayed. */
		GdkPoint offset;

		/** \brief Zoom factor to use for image display. */
		gdouble zoom_factor;

		/** \brief Percentage of the view region used for panning. */
		gdouble pan_percentage;

		/** \brief Zoom type to use when first displaying an image. */
		ZoomType zoom_type;

		/** \brief Buffer for image scaling before it is displayed. */
		GdkPixbuf* view_buffer;

	} ViewImageDisplay;


// specific implementation of common view functions

/** \brief Allocate memory for view and initialize variables. */
extern ViewImageDisplay* view_idv_create(ViewGeneric *parent);

/** \brief Deallocate memory for view. */
extern RCode view_idv_destroy(ViewImageDisplay *specificData);

/** \brief Update the image display view from model. */
extern RCode view_idv_update(ViewGeneric *view, guint seq);

/** \brief Redraw image display view area. */
extern RCode view_idv_redraw(ViewGeneric *view, GdkRectangle *region);

/** \brief Resize the image display view display. */
extern RCode view_idv_resize(ViewGeneric *view, GdkRectangle newRegion);


// view specific functions

/** \brief Set zoom type to the view settings. */
extern RCode view_idv_set_zoom(ViewGeneric *view, ZoomType type);

/** \brief Master image zooming function. */
extern RCode view_idv_zoom(ViewGeneric *view, ZoomDirection direction);

/** \brief Master image panning function. */
extern RCode view_idv_pan(ViewGeneric *view, PanDirection direction);


// internal functions, exported for unit tests only

/** \brief Cast specific data pointer to correct type. */
extern ViewImageDisplay* view_idv_get_data_pointer(ViewGeneric* view);

/** \brief Regenerate view internal buffer. */
extern RCode view_idv_generate_buffer(ViewGeneric* view, ViewImageDisplay *data, gboolean force);

/** \brief Compute new zoom for image dimensions and zoom type */
extern gboolean view_idv_compute_factor(ViewGeneric* view, ViewImageDisplay *data);

/** \brief Compute vieport for image dimensions and set zoom */
extern gboolean view_idv_compute_viewport(ViewGeneric* view, ViewImageDisplay *data);

/** \brief Recompute internal variables on panning in direction. */
extern gboolean view_idv_compute_offset(ViewGeneric* view, ViewImageDisplay *data, PanDirection direction);

/** \brief Recompute internal variables on zooming in direction. */
extern gboolean view_idv_compute_zoom(ViewGeneric* view, ViewImageDisplay *data, ZoomDirection direction);

/** \brief Timeout function for updating animation frame. */
extern gboolean view_idv_update_frame(gpointer data);

#endif
