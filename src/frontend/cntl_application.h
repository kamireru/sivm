/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file cntl_application.h
 * \brief Provide API for application controller initialization.
 *
 * The application controller is in charge of handling high level input.
 * Following key mapping take place in current implementation:
 *
 * 'q' : quit from application
 */
#ifndef __TEST_FRONTEND_CNTL_APPLICATION_H__
#define __TEST_FRONTEND_CNTL_APPLICATION_H__

#include "src/frontend/cntl_common.h"

#include "src/frontend/cntl.h"
#include "src/frontend/view.h"
#include "src/frontend/model.h"

/** \brief Create application controller automaton. */
extern FaAutomaton* cntl_app_create_automaton();

/** \brief Allocate memory for application controller data. */
extern void* cntl_app_create_data();

/** \brief Key press processing function for generic controller. */
extern CntlProcessResult cntl_app_process(CntlGeneric *cntl, GdkEventKey *input);

#endif
