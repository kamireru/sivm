/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "src/frontend/observer_list.h"
#include "src/frontend/view_generic.h"


/**
 * The function allocate memory for observer list. The initial size of the array
 * that is used for storing observers is zero. It is allocated when the first
 * observer is being added.
 *
 * \return pointer to the new ObserverList
 */
ObserverList* observer_create_list() {

	ObserverList* result = NULL;

	if ( NULL == (result = (ObserverList*) g_try_malloc(sizeof(ObserverList))) ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return result;
	}

	result->maxSize = 0;
	result->currentSize = 0;
	result->array = NULL;

	return result;
}


/**
 * The function deallocates both array storing the observer views and the
 * structure holding observer list variables.
 *
 * \param list observer list to deallocate
 * \return OK on success
 */
RCode observer_destroy(ObserverList *list) {

	ASSERT( NULL != list);

	if ( NULL != list->array ) {
		g_free((gpointer)list->array);
		list->array = NULL;
	}

	g_free((gpointer)list);

	return OK;
}

/**
 * \param list observer list to retrieve count from
 * \return number of observers in list
 */
guint observer_get_count(ObserverList *list) {
	ASSERT( NULL != list );

	return list->currentSize;
}

/**
 * Iterate over all observers in the list and invoke an update() function on
 * each of them. Supply received sequence number to the update function call
 * too.
 *
 * \param list observer list to update
 * \param seq sequence number to supply to observers
 */
void observer_update(ObserverList *list, guint seq) {
	int i;

	ASSERT( NULL != list);

	// for each view in the observer list, call notification function
	for ( i = 0; i < list->currentSize; i++ ) {
		view_update(list->array[i], seq);
	}
}


/**
 * Check the observer list and insert the view into it. The view is inserted
 * only when it is not present yet.
 *
 * The function also handles memory allocation for the list. If there is no
 * space allocated yet, it either allocates or reallocates a memory for the
 * list.
 *
 * \param list observer list to update
 * \param view generic view to add into the list
 * \return OK on success
 */
RCode observer_insert(ObserverList *list, ViewGeneric *view) {

	int i;

	ASSERT( NULL != list);
	ASSERT( NULL != view);

	// check if the view is already present in the list
	for ( i = 0; i < list->maxSize; i++ ) {
		if ( view == list->array[i] ) {
			return OK;
		}
	}

	//  reallocate the space - we are using the tmp variable instead of
	//  list->array to preserve original pointer in case realloc fails
	if ( list->currentSize + 1 > list->maxSize ) {

		ViewGeneric** tmp = list->array;

		list->maxSize += OBSERVER_LIST_INCREMENT;
		tmp = (ViewGeneric**) g_try_realloc(tmp, list->maxSize * sizeof(ViewGeneric*));

		if ( NULL == tmp ) {
			list->maxSize -= OBSERVER_LIST_INCREMENT;
			ERROR_SET(ERROR.MEMORY_FAILURE);
			return FAIL;

		} else {
			// initialize newly allocated memory to 0
			for ( i = list->currentSize; i < list->maxSize; i++ ) {
				tmp[i] = NULL;
			}
		}

		list->array = tmp;
	}

	list->array[list->currentSize++] = view;

	return OK;
}


/**
 * Check the list of observer views and if supplied view is present, remove it
 * from the list. If the view is not present, nothing happens.
 *
 * \param list observer list to update
 * \param view generic view to add into the list
 * \return OK on success
 */
RCode observer_remove(ObserverList * list, ViewGeneric *view) {

	int i = 0;

	ASSERT( NULL != list);
	ASSERT( NULL != view);

	// try to find view in the list
	while ( i < list->maxSize && view != list->array[i] ) {
		i++;
	}

	// remove the view by overwriting it with last element and NULLing the last
	// element afterwards
	if ( i != list->maxSize ) {
		list->currentSize -= 1;
		list->array[i] = list->array[list->currentSize];
		list->array[list->currentSize] = NULL;
	}

	return OK;
}
