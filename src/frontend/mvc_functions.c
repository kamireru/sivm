/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/frontend/mvc_functions.h"

#include "src/frontend/model.h"
#include "src/frontend/view.h"
#include "src/frontend/cntl.h"

/**
 * Create model, view and controller of given types and link them together to
 * form MVC triad. No specific setup is done on the triad elements.
 *
 * The function should serve as shortcut for creating associated MVC elements.
 *
 * The returned pointer is to the view part of the triad since the view works as
 * a master element in the implementation of the MVC pattern here.
 *
 * \param modelType type of the model
 * \param viewType type of the view
 * \param cntlType type of the controller
 * \return pointer to the ViewGeneric
 */
ViewGeneric* mvc_triad_create(ModelType modelType, ViewType viewType, CntlType cntlType) {
	ViewGeneric* view = NULL;
	ModelGeneric* model = NULL;
	CntlGeneric* cntl = NULL;

	// create model, view and controller for application element
	view = view_create(viewType);
	if ( NULL == view ) {
		ERROR_NOTE("Creation of view component of MVC triad failed (type=%d)", viewType);
		return NULL;
	}

	model = model_create(modelType);
	if ( NULL == model ) {
		ERROR_NOTE("Creation of model component of MVC triad failed (type=%d)", modelType);
		view_unref(view);
		return NULL;
	}

	cntl = cntl_create(cntlType);
	if ( NULL == cntl ) {
		ERROR_NOTE("Creation of controller component of MVC triad failed (type=%d)", cntlType);
		view_unref(view);
		model_unref(model);
		return NULL;
	}

	// link all there elements together
	view_set_model(view, model);
	model_unref(model);

	view_set_controller(view, cntl);
	cntl_unref(cntl);

	return view;
}

