/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>

#include "src/layout.h"

#include "src/backend/image_group.h"
#include "src/backend/group_list.h"

#include "src/frontend/view.h"
#include "src/frontend/cntl.h"
#include "src/frontend/model.h"

#include "src/frontend/mvc_functions.h"

/**
 * Create layout with only image display view.
 *
 * \param window pointer to window associated with views
 * \return poiter to top view of hierarchy
 */
static ViewGeneric* layout_create_minimal(GdkWindow *window) {
	RCode result;
	ImageGroup* default_group = NULL;
	ViewGeneric *display_view = NULL;

	// locate image group to link to image display view
	default_group = glist_get_by_name("default");
	if ( NULL == default_group ) {
		ERROR_NOTE("Unable to locate 'default' image group");
		return NULL;
	}

	// create application child view, for now of type IMAGE DISPLAY
	display_view = mvc_triad_create(MODEL_IMAGE_LIST, VIEW_IMAGE_DISPLAY, CNTL_IMAGE_DISPLAY);
	if ( NULL == display_view ) {
		ERROR_NOTE("Unable to create image display MVC triad");
		return NULL;

	} else {

		view_set_window(display_view, window);

		// set image group to the model
		result = model_iml_set_image_group(view_get_model(display_view), default_group);
		if ( FAIL == result ) {
			ERROR_NOTE("Unable to set image group to image list model");
			view_unref(display_view);
			return NULL;
		}
	}

	return display_view;
}


/**
 * Create layout with container splitting the screen for image display and
 * image list status views.
 *
 * \param window pointer to window associated with views
 * \return poiter to top view of hierarchy
 */
static ViewGeneric* layout_create_simple(GdkWindow *window) {
	RCode result;
	guint progress = 0, i;
	ViewGeneric *container_view = NULL;
	ViewGeneric *display_view = NULL;
	ViewGeneric *status_view = NULL;
	ContainerNode *node = NULL;
	ContainerNode *leaf = NULL;
	ViewStatusType status_list[2] = { VIEW_STATUS_POSITION, VIEW_STATUS_FILENAME };

	TRACE_MESSAGE(LAY, TL_INFO, "Create minimal gui structure with one display view");
	display_view = layout_create_minimal(window);
	if ( NULL == display_view ) {
		ERROR_NOTE("Can't create display view for container");
		goto ERROR;
	} else {
		progress = 1;
	}

	TRACE_MESSAGE(LAY, TL_INFO, "Create container view");
	container_view = mvc_triad_create(MODEL_NONE, VIEW_CONTAINER, CNTL_CONTAINER);
	if ( NULL == container_view ) {
		goto ERROR;
	} else {
		view_set_window(container_view, window);
		progress = 2;
	}

	TRACE_MESSAGE(LAY, TL_INFO, "Create split and assign display view to it");
	if ( FAIL == view_cv_node_split(container_view, NODE_VERTICAL) ) {
		ERROR_NOTE("Split creation for display view failed");
		goto ERROR;
	} else {
		node = view_cv_get_top_node(container_view);
		if ( NULL == node ) { goto ERROR; }

		leaf =  container_node_get_child(node, 0);
		if ( NULL == leaf ) { goto ERROR; }

		// move node pointer to second child in preparation for next split
		node =  container_node_get_child(node, 1);
		if ( NULL == node ) { goto ERROR; }

		if ( FAIL == container_node_set_view(leaf, display_view) ) {
			goto ERROR;
		} else {
			progress = 3;
		}
	}

	TRACE_MESSAGE(LAY, TL_INFO, "Create splits for status line view(s)");
	if ( FAIL == view_cv_node_set_focus(container_view, node) ) {
		goto ERROR;
	}
	if ( FAIL == view_cv_node_split(container_view, NODE_HORIZONTAL) ) {
		ERROR_NOTE("Split creation for status views failed");
		goto ERROR;
	}

	for (i = 0; i < 2; i++ ) {
		TRACE_MESSAGE(LAY, TL_INFO, "Create status view (index=%d, type=%d)", i, status_list[i]);
		status_view = view_create(VIEW_LIST_STATUS);
		if ( NULL == status_view ) {
			ERROR_NOTE("Unable to create image list status view");
			goto ERROR;
		} else {
			progress = 4;

			if ( FAIL == view_lsv_set_type(status_view, status_list[i]) ) {
				goto ERROR;
			}

			view_set_window(status_view, window);
			view_set_model(status_view, view_get_model(display_view));
		}

		TRACE_MESSAGE(LAY, TL_INFO, "Assign status view to split (index=%d)", i);
		node = view_cv_get_top_node(container_view);
		if ( NULL == node ) { goto ERROR; }

		node =  container_node_get_child(node, 1);
		if ( NULL == node ) { goto ERROR; }

		leaf =  container_node_get_child(node, i);
		if ( NULL == leaf ) { goto ERROR; }

		if ( FAIL == container_node_set_view(leaf, status_view) ) {
			goto ERROR;
		} else {
			// once the status view is assigned, we need to unref only container
			// view
			progress = 3;
		}
	}

	TRACE_MESSAGE(LAY, TL_INFO, "Set focus to display view");
	node = view_cv_get_top_node(container_view);
	if ( NULL == node ) { goto ERROR; }

	leaf = container_node_get_child(node, 0);
	if ( NULL == leaf ) { goto ERROR; }

	if ( FAIL == view_cv_node_set_focus(container_view, leaf) ) {
		goto ERROR;
	}

	return container_view;

	ERROR:
	ERROR_NOTE("Error creating simple layout (progress=%d)", progress);
	switch (progress) {
		case 4:
			view_unref(status_view);

		case 3:
			view_unref(container_view);
			return NULL;

		case 2:
			view_unref(container_view);

		case 1:
			view_unref(display_view);

		case 0:
		default:
			return NULL;
	}
}

/**
 * Create layout with only image display view.
 *
 * \param window pointer to window associated with views
 * \return poiter to top view of hierarchy
 */
static ViewGeneric* layout_create_container(GdkWindow *window) {
	RCode result;
	ViewGeneric *container_view = NULL;

	container_view = mvc_triad_create(MODEL_NONE, VIEW_CONTAINER, CNTL_CONTAINER);
	if ( NULL == container_view ) {
		ERROR_NOTE("Unable to create container view MVC triad");
		return NULL;

	} else {
		view_set_window(container_view, window);

		view_cv_node_split(container_view, NODE_VERTICAL);
		view_cv_node_split(container_view, NODE_VERTICAL);
		view_cv_node_split(container_view, NODE_HORIZONTAL);
		view_cv_node_split(container_view, NODE_HORIZONTAL);
		view_cv_node_split(container_view, NODE_HORIZONTAL);
	}

	return container_view;
}


/**
 * Function creates gui layout according to name and hints and returns pointer
 * to the top view of create structure.
 *
 * The function does not handle top level creation (ie application view), only
 * what is below (ie container view, image display view, ...)
 *
 * \param name name of the layout
 * \param hints NULL teminated list of layout hints
 * \param window window to which the view should be associated
 * \return pointer to top level view
 */
ViewGeneric *layout_create(const gchar *name, const gchar **hints, GdkWindow *window) {
	ViewGeneric* result = NULL;

	if ( NULL == name ) {
		name = "simple";
	}

	// choose layout model according to its name
	TRACE_MESSAGE(LAY, TL_INFO, "Create gui layout (name=%s)", name);
	if ( 0 == strcmp("minimal", name) ) {
		result = layout_create_minimal(window);

	} else if ( 0 == strcmp("simple", name) ) {
		result = layout_create_simple(window);

	} else if ( 0 == strcmp("container", name) ) {
		result = layout_create_container(window);

	} else {
		ERROR_SET(ERROR.INVALID_PARAM);
		ERROR_NOTE("Supplied layout is not valid (layout=%s)", name);
	}

	if ( NULL == result ) {
		ERROR_NOTE("Layout creation failed (layout=%s)", name);
	}

	return result;
}
