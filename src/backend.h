/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SRC_BACKEND_H_
#define _SRC_BACKEND_H_

#include <glib.h>

#include "src/common/common_def.h"

/** \brief Initialize backend structures. */
extern RCode backend_init(gint argc, gchar** argv);

/** \brief Deallocate backend structures. */
extern RCode backend_finish();

#endif
