/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <gdk/gdk.h>
#include <gdk/gdkkeysyms.h>

#include "src/gui.h"
#include "src/backend.h"
#include "src/config/config.h"
#include "src/backend/image_pile.h"
#include "src/common/common_def.h"


/**
 * \brief Main program function.
 *
 * Program initialization, command line parameter parsing and glib, gdk setup is
 * done here. Also deinicialization and cleanup procedures.
 *
 * \param argc number of parameters from command line
 * \param argv array of paramters from command line
 * \return 0 when program finished with success
 */
int main(int argc, char **argv) {
	int file_index;
	GuiData gui_data = { NULL, NULL };
	GdkWindow *window = NULL;

	// glib and gdk initialization code
	g_type_init();
	gdk_init(&argc,&argv);

	// initialize tracing system using environment variable must be done before
	// configuration loading
	init_trace_areas();
	init_trace_setup("SIVM_TRACE", NULL);

	// parse application specific parameter from command line
	if ( FAIL == config_load(argc, argv, &file_index) ) {
		ERROR_LOG_FATAL(EXIT_FAILURE);
	}

	// load images from command line and filter out executable name
	if ( FAIL == backend_init(argc - file_index, argv + file_index) ) {
		ERROR_LOG_FATAL(EXIT_FAILURE);
	}

	// allocate gui structures
	if ( FAIL == gui_init(&gui_data) ) {
		ERROR_LOG_FATAL(EXIT_FAILURE);
	}

	// start main event loop
	if ( FAIL == gui_loop(&gui_data) ) {
		ERROR_LOG_FATAL(EXIT_FAILURE);
	}

	// deallocate gui structures
	if ( FAIL == gui_finish(&gui_data) ) {
		ERROR_LOG_FATAL(EXIT_FAILURE);
	}

	// deallocate  image pile
	if ( FAIL == backend_finish() ) {
		ERROR_LOG_FATAL(EXIT_FAILURE);
	}
	config_remove();
	return (EXIT_SUCCESS);
}
