/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _SRC_LAYOUT_H_
#define _SRC_LAYOUT_H_

#include <gdk/gdk.h>

#include "src/frontend/mvc_types.h"
#include "src/frontend/view_generic.h"

/** \brief Create gui element structure. */
extern ViewGeneric *layout_create(const gchar *name, const gchar **hints, GdkWindow *window);

#endif
