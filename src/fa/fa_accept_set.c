/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup fa
 * \{
 *
 * \file fa_accept_set.c
 */

#include <malloc.h>
#include <stdarg.h>
#include "src/fa/fa_accept_set.h"

/**
 * The function takes accept set type and then depending on it it get number of
 * optional parameters:
 *
 * - FA_SET_RANGE takes two parameters of (void*)
 * - FA_SET_LIST takes list size (int) and then arbitrary number of (void*)
 *
 * If the accept set is a FA_SET_LIST, then it must have at least one element.
 * Creation of list set without elements is not allowed.
 *
 * Note: Supplying NULL as element is valid scenario. User might design
 * automaton which uses NULL as valid input. The only requirement is a
 * comparator() function that can handle it.
 *
 * \param type type of the accept set
 * \return pointer to FaAcceptSet
 */
FaAcceptSet* fa_set_create(FaSetType type, ...) {

	unsigned int i;
	va_list ap;

	// allocate memory for result set
	FaAcceptSet* result = (FaAcceptSet*) malloc(sizeof(FaAcceptSet));
	if ( NULL == result ) {
		return NULL;
	}

	va_start(ap, type);

	// depending on type, set size of the data array
	result->is_normalized = FA_FALSE;
	result->type = type;
	switch (result->type) {

		// range has always data array size 2
		case FA_SET_RANGE:
			result->size = 2;
			break;

		// list contain size as first argument
		case FA_SET_LIST:
			result->size = va_arg(ap, unsigned int);
			if ( 0 == result->size ) {
				free((void*)result);
				return NULL;
			}
			break;

		// fail on unknown type
		default:
			free((void*)result);
			return NULL;
	}

	// allocate memory for data array
	result->data = (void**) malloc( result->size * sizeof(void*) );
	if ( NULL == result->data ) {
		free((void*)result);
		return NULL;
	}

	// get data pointers from function arguments
	for (i = 0; i < result->size; i++) {
		result->data[i] = va_arg(ap, void*);
	}

	va_end(ap);
	return result;
}

/**
 * Destructor function deallocate memory for accept set.
 *
 * Note: The function deallocate accept set structure and data array structure
 * it contains. But it will not deallocate data pointers that were supplied in
 * create function.
 *
 * The reason is that we do not know where the pointers came from, whenver they
 * are dynamically allocated or just addresses or static variables.
 *
 * \param set pointer to the accept set
 */
void fa_set_destroy(FaAcceptSet *set) {

	if ( NULL == set ) {
		return;
	}

	// deallocate data array
	if ( NULL != set->data ) {
		free((void*)set->data);
		set->data = NULL;
	}

	free((void*)set);
	return;
}

/**
 * \brief Utility function for contain check for range accept sets.
 *
 * The function pre-compute upper and lower bounds of the range as they can be
 * swapped for non-normalized accept sets. Only after then it compares input
 * value.
 *
 * \param set pointer to accept set to check with
 * \param compare comparator function
 * \param input pointer to value to check
 * \return FA_TRUE if input value is contained in set
 */
static FaBoolean fa_set_range_contain(FaAcceptSet *set, FaCompareFn compare, void *input) {

	void *lower = set->data[0];
	void *upper = set->data[1];
	if ( FA_FALSE == set->is_normalized ) {
		if ( 1 == compare( set->data[0], set->data[1] ) ) {
			lower = set->data[1];
			upper = set->data[0];
		}
	}

	if ( 1 != compare(lower, input) && -1 != compare(upper, input) ) {
		return FA_TRUE;
	} else {
		return FA_FALSE;
	}
}

/**
 * \brief Utility function for contain check for list accept sets.
 *
 * The function uses 'iterate until found or out of data' or binary search
 * depending on the {is_normalized} flag.
 *
 * \param set pointer to accept set to check with
 * \param compare comparator function
 * \param input pointer to value to check
 * \return FA_TRUE if input value is contained in set
 */
static FaBoolean fa_set_list_contain(FaAcceptSet *set, FaCompareFn compare, void *input) {

	if ( FA_FALSE == set->is_normalized ) {
		int i = 0, result = 1;

		// iterage over array until value is found or out of data

		while ( i < set->size && 0 != result ) {
			result = compare(input, set->data[i++]);
		}

		if ( 0 == result ) {
			return FA_TRUE;
		} else {
			return FA_FALSE;
		}

	} else {
		// preset search bounds
		int lower = -1;
		int upper = set->size;

		// binary search
		int result;
		int mid;
		while ( lower <= upper ) {

			mid = lower + (upper - lower) / 2;

			// check if mid is out of arrray bounds
			if ( mid < 0 || set->size <= mid ) {
				return FA_FALSE;
			}

			// check if we have match
			result = compare( input, set->data[mid] );
			if ( 0 == result ) {
				return FA_TRUE;
			}

			if ( -1 == result ) {
				upper = mid - 1;
			} else {
				lower = mid + 1;
			}
		}

		return FA_FALSE;
	}
}

/**
 * The function need a comparator function supplied from outside as it does not
 * know what kind of data it is storing in list and range.
 *
 * If the accept list is normalized, than it uses either binary search or
 * iteration over whole list.
 *
 * If either set or comparator function is NULL, we return FA_FALSE. The NULL in
 * input value is possible value.
 *
 * \param set pointer to accept set to check with
 * \param compare comparator function
 * \param input pointer to value to check
 * \return FA_TRUE if input value is contained in set
 */
FaBoolean fa_set_contain(FaAcceptSet *set, FaCompareFn compare, void *input) {

	if ( NULL == set || NULL == compare ) {
		return FA_FALSE;
	}

	switch (set->type) {
		case FA_SET_RANGE:
			return fa_set_range_contain(set, compare, input);
		case FA_SET_LIST:
			return fa_set_list_contain(set, compare, input);
		default:
			return FA_FALSE;
	}
}

/** \} */
