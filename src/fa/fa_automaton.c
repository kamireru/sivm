/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup fa
 * \{
 *
 * \file fa_automaton.c
 */

#include <malloc.h>
#include <stdarg.h>
#include "src/fa/fa_automaton.h"

/**
 * Allocate memory for automaton and initialize its member variables.
 *
 * Note: Creation of automaton with no nodes is not allowed.
 *
 * \param reset pointer to data reset function
 * \param compare pointer to comparator function to use
 * \param nodeCount number of nodes to initialize
 * \return pointer to automaton or NULL on failure
 */
FaAutomaton* fa_create(FaResetDataFn reset, FaCompareFn compare, unsigned int nodeCount, ...) {

	int i;
	va_list list;

	if ( 0 == nodeCount ) {
		return NULL;
	}

	FaAutomaton *result = (FaAutomaton*) malloc(sizeof(FaAutomaton));
	if ( NULL == result ) {
		return NULL;
	}

	result->reset = reset;
	result->compare = compare;
	result->node_count = nodeCount;

	result->node_list = (FaNode**) malloc(nodeCount * sizeof(FaNode*));
	if ( NULL == result->node_list ) {
		free((void*)result);
		return NULL;
	}

	va_start(list, nodeCount);
	for (i=0; i < nodeCount; i++) {
		result->node_list[i] = va_arg(list, FaNode*);
	}
	va_end(list);

	return result;
}

/**
 * Deallocate memory associated with automaton and its nodes.
 *
 * \param fa pointer to automaton to deallocate
 */
void fa_destroy(FaAutomaton *fa) {

	if ( NULL == fa ) {
		return;
	}

	if ( NULL != fa->node_list ) {

		// deallocate nodes associated with nodes
		int i;
		for (i = 0; i < fa->node_count; i++) {
			fa_node_destroy( fa->node_list[i] );
		}

		// deallocate list of nodes
		free((void*)fa->node_list);
		fa->node_list = NULL;
	}

	// deallocate fa itself
	free((void*)fa);
}

/**
 * The function iterates looks up which node in automaton is start node.
 *
 * TODO: The implementation differs depending on whenever the automaton is
 * normalized or not.
 *
 * \param fa pointer to automaton to deallocate
 * \return pointer to start node or NULL on error
 */
FaNode* fa_get_start_node(FaAutomaton *fa) {

	if ( NULL != fa && NULL != fa->node_list ) {

		int i;
		for (i = 0; i < fa->node_count; i++) {
			if ( FA_NODE_START == fa_node_get_type( fa->node_list[i] ) ) {
				return fa->node_list[i];
			}
		}
	}

	return NULL;
}
/**
 * The function returns pointer to the node with given index.
 *
 * TODO: Here we can normalize to have node indexes from 0 to node_count and
 * each node fullfill node_list[index] == node_list[index]->index.
 *
 * \param fa pointer to automaton
 * \param index index of the node to return
 * \return pointer to node with index or NULL on error
 */
FaNode* fa_get_node(FaAutomaton *fa, unsigned int index) {

	if ( NULL != fa && NULL != fa->node_list ) {

		int i;
		for (i = 0; i < fa->node_count; i++) {
			if ( index == fa_node_get_index( fa->node_list[i] ) ) {
				return fa->node_list[i];
			}
		}
	}

	return NULL;
}

/**
 * Return reset function that is to be used with data supplied to the given
 * automaton.
 *
 * \param fa pointer to automaton
 * \return pointer to reset function
 */
FaResetDataFn* fa_get_fn_reset(FaAutomaton *fa) {
	if ( NULL == fa ) {
		return NULL;
	} else {
		return fa->reset;
	}
}

/**
 * Return compare function that is to be used with input data type for given
 * automaton.
 *
 * \param fa pointer to automaton
 * \return pointer to compare function
 */
FaCompareFn* fa_get_fn_compare(FaAutomaton *fa) {
	if ( NULL == fa ) {
		return NULL;
	} else {
		return fa->compare;
	}
}

/** \} */
