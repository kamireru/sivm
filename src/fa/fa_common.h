/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file fa_common.h
 * \brief Header file with common types and constants used through FA API.
 */
/*\addtogroup fa */
/*\@{*/
#ifndef __FA_COMMON_SET_H__
#define __FA_COMMON_SET_H__

/**
 * Enum used instead of int for representing boolean variables
 */
typedef enum {
		FA_FALSE = 0,
		FA_TRUE  = 1
	} FaBoolean;

/**
 * Enum used as output of comparator functions.
 */
typedef enum {
		FA_LESS    = -1,
		FA_EQUAL   =  0,
		FA_GREATER =  1
	} FaRelate;

/**
 * The comparator function prototype.
 *
 * The FA works with void pointers to allow operation on various kinds of data.
 * Since it does not know what kind of data it gets from caller, the caller has
 * to specify comparator function that can work with the data.
 *
 * \param arg1 first pointer to compare
 * \param arg2 second pointer to compare
 * \return
 *	- -1 if first is less than second
 *	-  0 if first is equal to second
 *	-  1 if first is greater than second
 */
typedef FaRelate (FaCompareFn)(void *, void *);

#endif
/*\@}*/
