/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup fa
 * \{
 *
 * \file fa_node.h
 * \brief Header file with API for managing nodes used by nodes.
 *
 * The node is a structure containing set of transitions that the finite
 * automaton can take from that node.
 */
#ifndef __FA_NODE_H__
#define __FA_NODE_H__

#include "src/fa/fa_common.h"
#include "src/fa/fa_transition.h"

/**
 * Prototype of function that can be set to be invoked when finite automaton
 * gets into given node.
 *
 * \param node index of node on which the function was invoked
 * \param input pointer to the input that caused transition to node
 * \param data pointer to data associated with automaton run
 */
typedef void (FaNodeFn)(unsigned int, void*, void*);

/**
 * Enum storing node type
 */
typedef enum {
		FA_NODE_NONE = -1,
		FA_NODE_START = 0,
		FA_NODE_INNER = 1,
		FA_NODE_FINAL = 2
	} FaNodeType;

/**
 * Structure for storing information about one automaton node
 */
typedef struct {
		/** \brief Index of the node (node name). */
		unsigned int index;

		/** \brief The type of the node. */
		FaNodeType type;

		/** \brief Function to be invoked when automaton gets into the node. */
		FaNodeFn *function;

		/** \brief Number of transitions from the node. */
		unsigned int transition_count;

		/** \brief Array of transitions associated with node. */
		FaTransition **transition_list;
	} FaNode;


/** \brief Constructor function for node. */
extern FaNode* fa_node_create(unsigned int index, FaNodeType type, FaNodeFn *function, unsigned int transitionCount, ...);

/** \brief Destructor function for node. */
extern void fa_node_destroy(FaNode *node);

/** \brief Lookup transition to take for given input. */
extern FaTransition* fa_node_get_transition(FaNode* node, FaCompareFn compare, void* input);

/** \brief Get node type. */
extern FaNodeType fa_node_get_type(FaNode* node);

/** \brief Get node index. */
extern long long fa_node_get_index(FaNode* node);

/** \brief Get function associated with node. */
extern FaNodeFn* fa_node_get_fn(FaNode* node);

#endif
/** \} */
