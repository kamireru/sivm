/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup fa
 * \{
 *
 * \file fa_runner.c
 */

#include <malloc.h>
#include "src/fa/fa_runner.h"


/**
 * The function for creation of the automaton runner.
 *
 * It allocates memory for FaRunner and initializes the variables of the
 * structure.
 *
 * \param fa pointer to automaton to use with runner
 * \param data pointer to user data to use with automaton
 * \return pointer to new runner on success, NULL on fail
 */
FaRunner* fa_runner_create(FaAutomaton *fa, void *data) {

	if ( NULL == fa ) {
		return NULL;
	}

	FaRunner *result = (FaRunner*) malloc(sizeof(FaRunner));
	if ( NULL == result ) {
		return NULL;
	}

	result->fa = fa;
	result->data = data;
	result->state = NULL;
	result->position = 0;

	return result;
}

/**
 * The function for deallocation of the automaton runner.
 *
 * The function do not deallocate the automaton as it might be used by another
 * runner and that would result in crash.
 *
 * It also do not deallocate memory for user data as the data is managed from
 * outside and we do not know it if even is dynamically allocated memory. It
 * might just be pointer to variable on stack.
 *
 * \param runner pointer to runner to deallocate
 */
void fa_runner_destroy(FaRunner *runner) {
	if ( NULL == runner ) {
		return;
	}

	free((void*)runner);
}

/**
 * Reset runner state so that it can start parsing input from the beginning. It
 * means:
 *
 *  - set state pointer to start node
 *  - reset position counter to 0
 *  - call reset function if one is provided over user data
 *
 * \param runner pointer to runner to reset
 * \return FA_OK on success
 */
FaBoolean fa_runner_reset(FaRunner *runner) {

	if ( NULL == runner || NULL == runner->fa ) {
		return FA_FALSE;
	}

	runner->state = fa_get_start_node(runner->fa);
	runner->position = 0;

	if ( NULL != fa_get_fn_reset(runner->fa) ) {
		fa_get_fn_reset(runner->fa)(runner->data);
	}

	if ( NULL == runner->state ) {
		return FA_FALSE;
	} else {
		return FA_TRUE;
	}
}

/**
 * The function processes one input value in the automaton. If applicable, it
 * applies transition and node functions to the user data stored in FaRunner.
 *
 * The function sets {state} pointer to point to new current node or to NULL if
 * there is no transition to take.
 *
 * The position counter is update only when transition happens (it {state} is
 * not null at the start of the function execution.
 *
 * Note: NULL as input pointer is valid as input checking really depends on how
 * the user designs the automaton. In some cases, NULL might be valid input and
 * we can't omit the possibility.
 *
 * \param runner pointer to runner
 * \param input pointer to the input to process
 * \return FA_TRUE on sucess, FA_FALSE on error
 */
FaBoolean fa_runner_step(FaRunner *runner, void* input) {
	FaTransition* transition = NULL;
	FaTransitionFn* fn_transition = NULL;
	FaNode* node = NULL;
	FaNodeFn* fn_node = NULL;

	if ( NULL == runner || NULL == runner->state ) {
		return FA_FALSE;
	}

	// try to find transition to take from current node
	transition = fa_node_get_transition(runner->state, fa_get_fn_compare(runner->fa), input);
	if ( -1 == fa_transition_get_target(transition) ) {
		runner->state = NULL;
		return FA_FALSE;
	}

	// check if target node exists in automaton, block if not
	node = fa_get_node(runner->fa, fa_transition_get_target(transition) );
	if ( NULL == node ) {
		runner->state = NULL;
		return FA_FALSE;
	}

	// execute functions if possible
	// TODO: Supply function for function retrieval fa_transition_get_fn_execute(transition)
	fn_transition = fa_transition_get_fn(transition);
	if ( NULL !=  fn_transition ) {
		fn_transition( fa_node_get_index(runner->state), fa_node_get_index(node), input, runner->data);
	}

	fn_node = fa_node_get_fn(node);
	if ( NULL != fn_node ) {
		fn_node( fa_node_get_index(node), input, runner->data );
	}

	// advance automaton state
	runner->state = node;
	runner->position += 1;

	return FA_TRUE;
}

/**
 * Depending on the runner state, decide whenever the input was accepted or not.
 *
 * If {state} points to the final node, the input is accepted. If
 * {state} pointer points to NULL or non-final node, the input is rejected.
 *
 * \param runner pointer to runner
 * \return FA_TRUE if input is accepted, FA_FALSE otherwise
 */
FaBoolean fa_runner_is_accept(FaRunner *runner) {
	FaNodeType type = FA_NODE_NONE;

	if ( NULL == runner || NULL == runner->state ) {
		return FA_FALSE;
	}

	// check if automaton state is in final node
	type = fa_node_get_type(runner->state);
	if ( FA_NODE_FINAL != type ) {
		return FA_FALSE;
	}

	return FA_TRUE;
}

/**
 * Depending on the runner state, decide whenever the input has blocked the
 * automaton or not. The automaton is blocked when there was attempt to use
 * input that is not handled in the automaton.
 *
 * \param runner pointer to runner
 * \return FA_TRUE if automaton is blocked, FA_FALSE otherwise
 */
FaBoolean fa_runner_is_blocked(FaRunner *runner) {

	if ( NULL == runner || NULL == runner->state ) {
		return FA_TRUE;
	}

	return FA_FALSE;
}

/** \} */
