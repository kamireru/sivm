/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup fa
 * \{
 *
 * \file fa_runner.h
 * \brief Header file with API for managing automaton runner.
 *
 * The automaton runner is a structure that contains variables for tracking the
 * state of the automaton and function for actually supplying input to automaton
 * and checking the results.
 *
 * It also handles pointer to the user data that is supplied to the given run of
 * the automaton.
 *
 * Having the functionality for running the automaton separate from automaton
 * structure allows us to use one automaton definition, but have several runners
 * attached to it in different states with different inputs.
 */
#ifndef __FA_RUNNER_H__
#define __FA_RUNNER_H__

#include "src/fa/fa_node.h"
#include "src/fa/fa_automaton.h"

/**
 * Structure holding information about which automaton the runner is using and
 * what is its current state. It also tracks user data supplied to the
 * automaton.
 */
typedef struct {

		/** \brief Pointer to user data. */
		void *data;

		/** \brief Pointer to automaton to use. */
		FaAutomaton *fa;

		/** \brief Pointer to automaton node representing current state. */
		FaNode *state;

		/** \brief Counter for tracking amount of input from last reset. */
		unsigned int position;

	} FaRunner;


/** \brief Constructor function for fa runner. */
extern FaRunner* fa_runner_create(FaAutomaton *fa, void *data);

/** \brief Destructor function for fa runner. */
extern void fa_runner_destroy(FaRunner *runner);

/** \brief Reset runner state to the initial one. */
extern FaBoolean fa_runner_reset(FaRunner *runner);

/** \brief Supply one input to the runner and execute transition. */
extern FaBoolean fa_runner_step(FaRunner *runner, void *input);

/** \brief Check if input so far was accepted, */
extern FaBoolean fa_runner_is_accept(FaRunner *runner);

/** \brief Check if input so far has blocked the automaton. */
extern FaBoolean fa_runner_is_blocked(FaRunner *runner);

#endif
/** \} */
