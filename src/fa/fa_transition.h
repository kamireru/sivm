/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup fa
 * \{
 *
 * \file fa_transition.h
 * \brief Header file with API for managing transitions used by nodes.
 *
 * The transition is a structure containing node index and one or more accept
 * sets. It represents possible path that the automaton can take from one node
 * to another.
 *
 * Note: Creation of transition without accept set is not allowed as there is no
 * way to add accept set to the transition yet. It might be allowed in the
 * future when new methods for creating transition are implemented.
 */
#ifndef __FA_TRANSITION_SET_H__
#define __FA_TRANSITION_SET_H__

#include "src/fa/fa_common.h"
#include "src/fa/fa_accept_set.h"

/**
 * Prototype of function that can be assigned to the transition. The function
 * is invoked when the transition to which it is assigned is used for advancing
 * state of the automaton.
 *
 * \param from index of node from which the transition went
 * \param to index of node to which the transition went
 * \param value that triggered the transition
 * \param data pointer to data associated with automaton run
 */
typedef void (FaTransitionFn)(unsigned int, unsigned int, void*, void*);

/**
 * Structure for storing transition data.
 */
typedef struct {
		/** \brief Index of node into which transition. */
		unsigned int target;

		/** \brief Pointer to function to be invoked upon transition. */
		FaTransitionFn* function;

		/** \brief Number of ranges in list. */
		unsigned int set_count;

		/** \brief List of ranges associated with transition. */
		FaAcceptSet **set_list;
	} FaTransition;


/** \brief Constructor function for transition. */
extern FaTransition* fa_transition_create(unsigned int target, FaTransitionFn* function, unsigned int setCount, ...);

/** \brief Destructor function for transition. */
extern void fa_transition_destroy(FaTransition *transition);

/** \brief Test function to check presence of input in transition accept set. */
extern FaBoolean fa_transition_contain(FaTransition *transition, FaCompareFn compare, void* input);

/** \brief Get target node index. */
extern long long fa_transition_get_target(FaTransition *transition);

/** \brief Get function associated with transition. */
extern FaTransitionFn* fa_transition_get_fn(FaTransition* transition);

#endif
/** \} */
