/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup fa
 * \{
 *
 * \file fa_accept_set.h
 * \brief Header file with API for managing accept sets used by nodes.
 *
 * The accept set is structure containing information about inputs that are
 * valid for transaction. Depending on the set of valid inputs, the accept set
 * can use different implementations to speed up checks:
 *
 * - value range
 * - value list
 *
 * TODO: Normalization and validation of the accept set.
 * TODO: Range list type implementation
 */
#ifndef __FA_ACCEPT_SET_H__
#define __FA_ACCEPT_SET_H__

#include "src/fa/fa_common.h"

/**
 * The enum contains values used for marking different implementations of
 * storage of acceptable input values in FaAcceptSet.
 */
typedef enum {
		FA_SET_RANGE = 0,
		FA_SET_LIST = 1
	} FaSetType;

/**
 * Structure for storing character range.
 *
 * These are used in FaTransition to store information about characters which
 * will make automaton use that transition.
 *
 * The {is_normalized} flag is considered internal and should not be set from
 * outside. The only allowed method for setting it is performing normalization
 * on the accept set.
 */
typedef struct {
		/** \brief Storage type of accept set. */
		FaSetType type;

		/** \brief Size of the data list. */
		unsigned int size;

		/** \brief Flag telling if set is normalized. */
		FaBoolean is_normalized;

		/** \brief Pointer to the data pointer array. */
		void **data;
	} FaAcceptSet;

/** \brief Constructor function for accept set. */
extern FaAcceptSet* fa_set_create(FaSetType type, ...);

/** \brief Destructor function for accept set. */
extern void fa_set_destroy(FaAcceptSet *set);

/** \brief Check if given input value is in accept set. */
extern FaBoolean fa_set_contain(FaAcceptSet *set, FaCompareFn compare, void *input);

#endif
/** \} */
