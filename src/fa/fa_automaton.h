/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup fa
 * \{
 *
 * \file fa_automaton.h
 * \brief Header file with API for managing automaton.
 *
 * The node is a structure containing set of transitions that the finite
 * automaton can take from that node.
 *
 * The automaton contains two important function pointers. One is pointer to the
 * comparator fucntion that should be used for the testing the input data of
 * automaton and the second is reset function that resets user data before each
 * automaton run.
 */
#ifndef __FA_AUTOMATON_H__
#define __FA_AUTOMATON_H__

#include "src/fa/fa_common.h"
#include "src/fa/fa_node.h"

/**
 * \brief Function to invoke when the automaton state is reset.
 *
 * The automaton can use transition and node functions to update data structure
 * supplied to the the automaton. But the content of the structure is valid only
 * for one automaton run. It must be reset before each run is begun.
 *
 * This is function prototype for the reset function that can be supplied to the
 * automaton.
 *
 * \param data pointer to user data supplied to automaton
 */
typedef void (FaResetDataFn)(void*);

/**
 * Structure holding information about whole finite automaton.
 */
typedef struct {
		/** \brief Comparator function used for this automaton. */
		FaCompareFn *compare;

		/** \brief Reset data function used for this automaton. */
		FaResetDataFn *reset;

		/** \brief Number of nodes in the automaton. */
		unsigned int node_count;

		/** \brief Node list for an automaton. */
		FaNode **node_list;
	} FaAutomaton;


/** \brief Constructor function for automaton. */
extern FaAutomaton* fa_create(FaResetDataFn reset, FaCompareFn compare, unsigned int nodeCount, ...);

/** \brief Destructor function for automaton. */
extern void fa_destroy(FaAutomaton *fa);

/** \brief Function to get start node. */
extern FaNode* fa_get_start_node(FaAutomaton *fa);

/** \brief Retrieve pointer to the node with index. */
extern FaNode* fa_get_node(FaAutomaton *fa, unsigned int index);

/** \brief Retrieve pointer to the reset function if there is any. */
extern FaResetDataFn* fa_get_fn_reset(FaAutomaton *fa);

/** \brief Retrieve pointer to the reset function if there is any. */
extern FaCompareFn* fa_get_fn_compare(FaAutomaton *fa);

#endif
/** \} */
