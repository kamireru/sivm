/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup fa
 * \{
 *
 * \file fa_node.c
 */

#include <malloc.h>
#include <stdarg.h>
#include "src/fa/fa_node.h"

/**
 * Allocate memory for node structure and initialize values in it.
 *
 * On contrary to FaAcceptSet and FaTransition, creating node without any
 * FaTransition is allowed. Such nodes are valid in the automaton as they can
 * represent final node where the input should end.
 *
 * \param index name of the node
 * \param type type of the node
 * \param function function to invoke on node change
 * \param transitionCount number of transitions the node has
 * \return pointer to new node
 */
FaNode* fa_node_create(unsigned int index, FaNodeType type, FaNodeFn *function, unsigned int transitionCount, ...) {

	int i;
	va_list list;

	FaNode *result = (FaNode*) malloc(sizeof(FaNode));
	if ( NULL == result ) {
		return NULL;
	}

	result->index = index;
	result->type = type;
	result->function = function;
	result->transition_count = transitionCount;

	// check if we are creating node without transitions
	if ( 0 == result->transition_count ) {
		result->transition_list = NULL;
		return result;
	}

	result->transition_list = (FaTransition**) malloc(transitionCount * sizeof(FaTransition*));
	if ( NULL == result->transition_list ) {
		free((void*)result);
		return NULL;
	}

	va_start(list, transitionCount);
	for (i=0; i < transitionCount; i++) {
		result->transition_list[i] = va_arg(list, FaTransition*);
	}
	va_end(list);

	return result;
}

/**
 * Deallocate the structure used for storing information about node.
 *
 * \param node pointer to the node to deallocate
 */
void fa_node_destroy(FaNode *node) {
	if ( NULL == node ) {
		return ;
	}

	if ( NULL != node->transition_list ) {
		// deallocate transitions associated with nodes
		int i;
		for (i = 0; i < node->transition_count; i++) {
			fa_transition_destroy(node->transition_list[i]);
		}

		// deallocate list of transitions
		free((void*)node->transition_list);
		node->transition_list = NULL;
	}

	// deallocate node itself
	free((void*)node);
}

/**
 * Return pointer to transition to take from input node under given input value.
 *
 * \param node pointer to the node
 * \param compare pointer to comparator function
 * \param input pointer to input data
 * \return pointer to transaction to take under the input
 */
FaTransition* fa_node_get_transition(FaNode* node, FaCompareFn compare, void* input) {
	int i;

	if ( NULL == node ) {
		return NULL;
	}

	// go through transition list until one transition is valid for given input
	// value
	for (i = 0; i < node->transition_count; i++) {
		if ( fa_transition_contain(node->transition_list[i], compare, input) ) {
			return node->transition_list[i];
		}
	}
	return NULL;
}

/**
 * Return type of the node. If the pointer is NULL, then the node type returned
 * should be FA_NODE_NONE to indicate error.
 *
 * \param node pointer to the node
 * \return type of the node or FA_NODE_NONE on error
 */
FaNodeType fa_node_get_type(FaNode* node) {
	if ( NULL == node ) {
		return FA_NODE_NONE;
	} else {
		return node->type;
	}
}

/**
 * Return index of the node.
 *
 * The type of the index is unsigned int, but we are using signed long long as a
 * return value to be able to return -1 to indicate error and return whole
 * unsigned int range of node indexes.
 *
 * \param node pointer to the node
 * \return index of the node or -1 on error
 */
long long fa_node_get_index(FaNode* node) {
	if ( NULL == node ) {
		return -1;
	} else {
		return node->index;
	}
}

/**
 * The function is to be executed when automaton state gets to the node.
 *
 * \param node pointer to the node
 * \return pointer to the node function
 */
FaNodeFn* fa_node_get_fn(FaNode* node) {
	if ( NULL == node ) {
		return NULL;
	} else {
		return node->function;
	}
}

/** \} */
