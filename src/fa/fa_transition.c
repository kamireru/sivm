/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup fa
 * \{
 *
 * \file fa_transition.c
 */

#include <malloc.h>
#include <stdarg.h>

#include "src/fa/fa_transition.h"

/**
 * The function allocate memory for transition representation and initialize
 * values according to parameters.
 *
 * TODO: If some FaAcceptSet pointer is null, deallocate whole structure along
 * with the pointers.
 *
 * \param target index of the transition target node
 * \param function pointer to function to invoke in the node
 * \param setCount number of accept sets used by this transition
 * \return pointer to new transition on success, NULL otherwise
 */
FaTransition* fa_transition_create(unsigned int target, FaTransitionFn* function, unsigned int setCount, ...) {
	int i;
	va_list ap;

	// do not allow creation of empty transactions
	if ( 0 == setCount ) {
		return NULL;
	}

	FaTransition* result = (FaTransition*) malloc(sizeof(FaTransition));
	if ( NULL == result ) {
		return NULL;
	}

	result->target = target;
	result->function = function;
	result->set_count = setCount;

	result->set_list = (FaAcceptSet**) malloc(setCount * sizeof(FaAcceptSet*));
	if ( NULL == result->set_list ) {
		free((void*)result);
		return NULL;
	}

	va_start(ap, setCount);
	for( i=0; i < setCount; i++ ) {
		result->set_list[i] = va_arg(ap, FaAcceptSet*);
	}
	va_end(ap);

	return result;
}

/**
 * Deallocate memory for transition.
 *
 * \param transition transition to deallocate
 */
void fa_transition_destroy(FaTransition *transition) {
	if ( NULL == transition ) {
		return ;
	}

	if ( NULL != transition->set_list ) {
		// deallocate accept sets associated with nodes
		int i;
		for (i = 0; i < transition->set_count; i++) {
			fa_set_destroy(transition->set_list[i]);
		}

		free((void*)transition->set_list);
		transition->set_list = NULL;
	}
	free((void*)transition);
}

/**
 * The function check if the input is contained in some accept set associated
 * with the transition. If it does, then the function will return FA_TRUE.
 *
 * Return FA_FALSE if the supplied transition pointer is NULL or if comparator
 * function pointer is NULL.
 *
 * \param transition pointer to the transition to check
 * \param compare the comparator function for given input value type
 * \param input value to check
 * \return FA_TRUE if value is contained in some set, FA_FALSE otherwise
 */
FaBoolean fa_transition_contain(FaTransition *transition, FaCompareFn compare, void* input) {
	int i;

	if ( NULL == transition || NULL == compare ) {
		return FA_FALSE;
	}

	// try to find accept set that contains input
	for (i = 0; i < transition->set_count; i++) {
		if ( FA_TRUE == fa_set_contain( transition->set_list[i], compare, input) ) {
			return FA_TRUE;
		}
	}
	return FA_FALSE;
}

/**
 * Return transition of the node.
 *
 * The type of the index is unsigned int, but we are using signed long long as a
 * return value to be able to return -1 to indicate error and return whole
 * unsigned int range of node indexes.
 *
 * \param transition pointer to the transition
 * \return index of the target node or -1 on error
 */
long long fa_transition_get_target(FaTransition *transition) {
	if ( NULL == transition ) {
		return -1;
	} else {
		return transition->target;
	}
}

/**
 * The function is to be executed when automaton takes the transition to get
 * from node to node.
 *
 * \param transition pointer to the transition
 * \return pointer to the transition function
 */
FaTransitionFn* fa_transition_get_fn(FaTransition* transition) {
	if ( NULL == transition ) {
		return NULL;
	} else {
		return transition->function;
	}
}

/** \} */
