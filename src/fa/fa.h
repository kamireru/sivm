/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * \defgroup fa Finite Automaton
 * \{
 *
 * \file fa.h
 * \brief Header file with finite automaton API.
 *
 * This header file includes (by header include transitivity) declarations of
 * whole automaton API. It is provides for readability sake - it is easier to
 * include fa.h than checking which of several include files is the top one with
 * all declarations.
 */
#ifndef __FA_H__
#define __FA_H__

#include "src/fa/fa_runner.h"

#endif
/** \} */
