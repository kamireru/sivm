/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <gdk/gdkkeysyms.h>

#include "src/gui.h"
#include "src/layout.h"

#include "src/config/config.h"

#include "src/backend/image_group.h"
#include "src/backend/group_list.h"

#include "src/frontend/view.h"
#include "src/frontend/cntl.h"
#include "src/frontend/model.h"

#include "src/frontend/mvc_functions.h"

/**
 * Storage variable for window attributes. The variable is used in the window
 * initialization code
 */
static GdkWindowAttr gui_window_attr;


/**
 * The function creates and initializes window for application. The pointer to
 * the window is then returned to the caller.
 *
 * The deallocation of the window pointer is callers responsibility.
 *
 * \param width initial width of the window
 * \param height initial height of the window
 * \return pointer to the GdkWindow for application
 */
GdkWindow* gui_init_window(guint width, guint height) {

	// use autoconf variable for setting window title
	gui_window_attr.title = PACKAGE;

	// handle only keyboard input for now
	gui_window_attr.event_mask = GDK_STRUCTURE_MASK | GDK_KEY_PRESS_MASK
			| GDK_KEY_RELEASE_MASK | GDK_EXPOSURE_MASK;

	gui_window_attr.window_type = GDK_WINDOW_TOPLEVEL;
	gui_window_attr.wclass = GDK_INPUT_OUTPUT;
	gui_window_attr.width = width;
	gui_window_attr.height = height;

	return gdk_window_new(NULL, &gui_window_attr, GDK_WA_TITLE);
}

/**
 * The function creates structures for handling gui element display and input
 * parsing. These structures are then used in the even handling loop.
 *
 * \param guiData pointer to main application loop
 * \param layout name of the layout to use when creating structure
 * \param layoutHints list of hints for tuning the layout
 * \return pointer to the top level view
 */
ViewGeneric* gui_structure_init(GuiData* guiData, const gchar *layout, const gchar **layoutHints) {
	RCode result;
	ViewGeneric* app_view = NULL;
	ViewGeneric* child_view = NULL;

	// create child for application view from layout name
	child_view = layout_create(layout, layoutHints, guiData->window);
	if ( NULL == child_view ) {
		ERROR_NOTE("Layout creation failed (layout=%s)", layout);
		return NULL;
	}

	// create application view and setup it
	app_view = mvc_triad_create(MODEL_APPLICATION, VIEW_APPLICATION, CNTL_APPLICATION);
	if ( NULL == app_view ) {
		ERROR_NOTE("Unable to create application MVC triad");
		view_unref(child_view);
		return NULL;

	} else {
		model_app_set_loop( view_get_model(app_view), guiData->loop );
		view_set_window( app_view, guiData->window );
	}

	// link application view and top of layout together
	view_app_set_child( app_view, child_view );

	// remove excessive reference to child view
	view_unref(child_view);

	return app_view;
}

/**
 * The function processes events in the even loop of the application.
 *
 * \param ev event to handle
 * \param data pointer to some input data
 */
static void gui_handle_event(GdkEvent *ev, gpointer data) {
	GuiData *gui_data = (GuiData*)data;
	GdkEventKey key;
	GdkRectangle region = { 0, 0, 0, 0 };
	RCode result;

	switch (ev->type) {

		// handle window geometry change
		case GDK_CONFIGURE:
			TRACE_MESSAGE(GUI, TL_MINOR, "Configure event received (x=%d, y=%d, width=%d, height=%d)",
					ev->configure.x, ev->configure.y, ev->configure.width, ev->configure.height
				);

			// feed new application region to the top level view
			region.width = ev->configure.width;
			region.height = ev->configure.height;
			view_resize(gui_data->application, region);

			gdk_window_configure_finished(ev->configure.window);
			break;

		// handle key press
		case GDK_KEY_PRESS:
			TRACE_MESSAGE(GUI, TL_MINOR, "Key press event received (code=%s)",
					gdk_keyval_name(ev->key.keyval)
				);

			// get controller assigned to the top level view and feed it key data
			key = ev->key;
			cntl_process( view_get_controller(gui_data->application), &key );
			break;

		case GDK_EXPOSE:
			TRACE_MESSAGE(GUI, TL_MINOR, "Expose event received (x=%d, y=%d, width=%d, height=%d)",
					ev->expose.area.x, ev->expose.area.y,
					ev->expose.area.width, ev->expose.area.height
				);

			gdk_window_set_debug_updates(TRUE);

			gdk_window_begin_paint_region(ev->expose.window, ev->expose.region);

			region = ev->expose.area;
			result = view_redraw(gui_data->application, &region);

			gdk_window_end_paint(ev->expose.window);
			gdk_window_set_debug_updates(FALSE);

			if ( FAIL == result ) {
				TRACE_MESSAGE(GUI, TL_MAJOR, "View redraw failed");
				if ( error_is_error() ) {
					ERROR_LOG();
				}
			}
			break;

		// handle click on the close button in decorations
		case GDK_DELETE:
			if ( NULL != gui_data ) {
				g_main_quit(gui_data->loop);
			} else {
				TRACE_MESSAGE(GUI, TL_MAJOR, "There is no data supplied to the event handler");
			}
			break;

		// do not process other events
		default:
			break;
	}
}

/**
 * The function takes pointer to pre-allocated buffer for gui data and
 * initializes window, main loop and MVC structures.
 *
 * \param guiData pointer to allocated space for gui data
 * \return OK on success
 */
RCode gui_init(GuiData *guiData) {
	const ConfigStorage* conf;
	RCode result = OK;
	GdkGeometry geometry = { 400, 300 };

	// create main loop and window structure
	TRACE_MESSAGE(MFUN, TL_INFO, "Initialize GLIB main event loop");
	guiData->loop = g_main_loop_new(NULL, FALSE);

	TRACE_MESSAGE(MFUN, TL_INFO, "Initialize application window");
	conf = config_get();
	if ( NULL == conf ) {
		guiData->window = gui_init_window(400, 320);
	} else {
		guiData->window = gui_init_window(conf->geometry_width, conf->geometry_height);
	}

	// create data structures for gui
	TRACE_MESSAGE(MFUN, TL_INFO, "Initialize GUI data structures");
	guiData->application = gui_structure_init(guiData, conf->layout, (const gchar **)conf->layout_hint);

	if ( error_is_error() ) {
		g_object_unref(guiData->window);
		g_main_destroy(guiData->loop);
		return FAIL;
	}

	gdk_window_enable_synchronized_configure(guiData->window);
	gdk_window_set_geometry_hints(guiData->window, &geometry, GDK_HINT_MIN_SIZE);

	// setup event handler function
	gdk_event_handler_set(gui_handle_event, (gpointer)(guiData), NULL);
	return OK;
}

/**
 * The function deallocates and clears up gui structures so that the application
 * can safely teminate.
 *
 * \param guiData pointer to allocated space with gui data
 * \return OK on success
 */
RCode gui_finish(GuiData *guiData) {
	RCode result = OK;

	if ( FAIL == view_unref(guiData->application) ) {
		ERROR_NOTE("Deallocation of application view failed");
		result = FAIL;
	}

	g_object_unref(guiData->window);
	g_main_destroy(guiData->loop);

	return result;
}

/**
 * The function starts main event loop using the supplied data.
 *
 * \param guiData pointer to allocated space with gui data
 * \return OK on success
 */
RCode gui_loop(GuiData *guiData) {
	const ConfigStorage* conf;

	// window must be showed before it is set to fullscreen
	gdk_window_show(guiData->window);

	// display window and set fullscreen if enabled
	conf = config_get();
	if ( NULL != conf ) {
		if ( conf->fullscreen ) {
			gdk_window_fullscreen(guiData->window);
		}
	} else {
		ERROR_LOG();
	}

	gdk_flush();

	// start main event loop
	g_main_loop_run(guiData->loop);

	return OK;
}
