/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup config
 * \{
 * \file config_storage.c
 */

#include <string.h>

#include "src/config/config_storage.h"

/**
 * Allocate memory for configuration storage and reset all values to the
 * defaults.
 *
 * \return pointer to allocated structure or NULL on error
 */
ConfigStorage* config_create() {
	ConfigStorage *result = NULL;

	result = (ConfigStorage*) g_try_malloc0(sizeof(ConfigStorage));
	if ( NULL == result ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return result;
	}

	// set only values which might not be reset correctly by memsetting the
	// structure with zeroes
	result->filename = NULL;
	result->profile = NULL;
	result->exclude = NULL;
	result->layout = NULL;
	result->layout_hint = NULL;

	result->zoom = 1.0f;
	result->pan_percentage = 0.6f;
	result->geometry_width = 400;
	result->geometry_height= 320;
	result->auto_zoom = 1;

	return result;
}

/**
 * Deallocate all memory associated with configuration storage.
 *
 * \param conf pointer to configuration storage
 * \return OK on success
 */
RCode config_destroy(ConfigStorage *conf) {
	ASSERT( NULL != conf );

	if ( NULL != conf->filename ) {
		g_free( (gpointer) conf->filename );
	}

	if ( NULL != conf->profile ) {
		g_free( (gpointer) conf->profile );
	}

	if ( NULL != conf->layout ) {
		g_free( (gpointer) conf->layout );
	}

	// the NULL handling is inside of the function
	config_destroy_list(conf->exclude);
	config_destroy_list(conf->layout_hint);

	g_free( (gpointer)conf );
	return OK;
}

/**
 * The function updates config options int dst with values from src. Only
 * options that are marked in options bit mask are updated.
 *
 * The function does deep copy of the objects, so the pointers differ but
 * content is same.
 *
 * \param dst pointer to structure that will be updated
 * \param src pointer to structure from which values will come
 * \param options bit mask of options to be updated
 * \return OK on success
 */
RCode config_update(ConfigStorage *dst, ConfigStorage *src, ConfigOption options) {
	if ( options & CONF_FULLSCREEN ) {
		dst->fullscreen = src->fullscreen;
	}

	if ( options & CONF_AUTO_ZOOM ) {
		dst->auto_zoom = src->auto_zoom;
	}

	if ( options & CONF_ZOOM ) {
		dst->zoom = src->zoom;
	}

	if ( options & CONF_PAN_PERCENTAGE ) {
		dst->pan_percentage = src->pan_percentage;
	}

	if ( options & CONF_GEOMETRY_WIDTH ) {
		dst->geometry_width = src->geometry_width;
	}

	if ( options & CONF_GEOMETRY_HEIGHT ) {
		dst->geometry_height = src->geometry_height;
	}

	if ( options & CONF_RECURSE ) {
		dst->recurse = src->recurse;
	}

	if ( options & CONF_FILENAME ) {
		if ( NULL != dst->filename ) {
			g_free( (gpointer) dst->filename );
		}

		if ( NULL != src->filename ) {
			dst->filename = strdup(src->filename);
		} else {
			dst->filename = NULL;
		}
	}

	if ( options & CONF_PROFILE ) {
		if ( NULL != dst->profile ) {
			g_free( (gpointer) dst->profile );
		}
		if ( NULL != src->profile ) {
			dst->profile = strdup(src->profile);
		} else {
			dst->profile = NULL;
		}
	}

	if ( options & CONF_EXCLUDE ) {
		if ( NULL != dst->exclude ) {
			config_destroy_list(dst->exclude);
		}
		if ( NULL != src->exclude ) {
			dst->exclude = config_clone_list(src->exclude);

			if ( NULL == dst->exclude && error_is_error() ) {
				ERROR_NOTE("Exclude list cloning failed");
				return FAIL;
			}
		} else {
			dst->exclude = NULL;
		}
	}

	if ( options & CONF_LAYOUT ) {
		if ( NULL != dst->layout ) {
			g_free( (gpointer) dst->layout );
		}
		if ( NULL != src->layout ) {
			dst->layout = strdup(src->layout);
		} else {
			dst->layout = NULL;
		}
	}

	if ( options & CONF_LAYOUT_HINT ) {
		if ( NULL != dst->layout_hint ) {
			config_destroy_list(dst->layout_hint);
		}
		if ( NULL != src->layout_hint ) {
			dst->layout_hint = config_clone_list(src->layout_hint);

			if ( NULL == dst->layout_hint && error_is_error() ) {
				ERROR_NOTE("Exclude list cloning failed");
				return FAIL;
			}
		} else {
			dst->layout_hint = NULL;
		}
	}

	return OK;
}

/**
 * Create deep copy of the NULL terminated string list and all of its content.
 *
 * \param list pointer to the list to duplicate
 * \return pointer to new list
 */
gchar** config_clone_list(gchar** list) {
	int count, i;
	gchar** result = NULL;

	if ( NULL == list ) {
		return NULL;
	}

	// get number of elements in list
	for ( count = 0; NULL != list[count]; count++ );

	// allocate memory
	result = (gchar**) g_try_malloc0(sizeof(gchar*) * (count+1));
	if ( NULL == result ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return result;
	}

	for ( i = 0; i < count; i++ ) {
		result[i] = strdup(list[i]);
	}
	result[count] = NULL;

	return result;
}

/**
 * Deallocate NULL terminated string list and all its content.
 *
 * \param list pointer to the list
 * \return OK on success
 */
RCode config_destroy_list(gchar** list) {
	if ( NULL != list ) {
		int i = 0;
		while ( NULL != (list[i]) ) {
			g_free( (gpointer) (list[i]) );
			i++;
		}
		g_free( (gpointer) list );
	}
	return OK;
}

/** \} */
