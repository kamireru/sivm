/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup config
 * \{
 * \file config_cmdline.c
 */

#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>

#include "src/config.h"
#include "src/config/config_cmdline.h"

/**
 * String holding definition of short flags that program can accept.
 */
static char* short_option = "c:p:fg:a:z:P:r:e:l:L:t:hv";

/**
 * Structure hodling definition of flags that the program can accept
 */
static struct option long_option[] = {
		{ "config",		required_argument,	0, 'c' },
		{ "profile",	required_argument,	0, 'p' },

		{ "fullscreen",	no_argument,		0, 'f' },
		{ "geometry",	required_argument,	0, 'g' },

		{ "auto-zoom",	required_argument,	0, 'a' },
		{ "zoom",		required_argument,	0, 'z' },
		{ "pan",		required_argument,	0, 'P' },

		{ "recurse",	required_argument,	0, 'r' },
		{ "depth",		required_argument,	0, 'd' },
		{ "exclude",	required_argument,	0, 'e' },

		{ "layout",		required_argument,	0, 'l' },
		{ "layout-hint",required_argument,	0, 'L' },

		{ "trace", 		required_argument,	0, 't' },
		{ "version", 	no_argument, 		0, 'v' },
		{ "help", 		no_argument,		0, 'h' },
		{ 0, 0, 0, 0 }
	};

/**
 * Load configuration from command line arguments.
 *
 * \param argc number of arguments in command line
 * \param argv list of argument strings
 * \param foundFlags pointer where the flags for found options is stored
 * \param firstArg pointer where index of first non-option argument is stored
 * \return pointer to new configuration storage or NULL on error
 */
ConfigStorage* config_from_cmdline(int argc, char** argv, int *foundFlags, int *firstArg) {
	ConfigStorage* result = NULL;
	gchar *string = NULL;
	int c, index, tmp_int, alloc_line;
	int exclude_count = 0, hint_count = 0;
	double tmp_double;
	gboolean is_exit = FALSE;

	// create configuration storage structure
	result = config_create();
	if ( NULL == result ) {
		ERROR_NOTE("Can't create configuration storage");
		return result;
	}

	while ( 1 ) {
		c = getopt_long (argc, argv, short_option, long_option, &index);

		if ( -1 == c ) break;

		string = NULL;
		alloc_line = 0;
		switch (c) {
			case 'c' :
				*foundFlags |= CONF_FILENAME;
				result->filename = strdup(optarg);
				string = result->filename;
				alloc_line = __LINE__;
				break;

			case 'p' :
				*foundFlags |= CONF_PROFILE;
				result->profile = strdup(optarg);
				string = result->profile;
				alloc_line = __LINE__;
				break;

			case 'l' :
				*foundFlags |= CONF_LAYOUT;
				result->layout = strdup(optarg);
				string = result->layout;
				alloc_line = __LINE__;
				break;

			case 'f' :
				*foundFlags |= CONF_FULLSCREEN;
				result->fullscreen = TRUE;
				break;

			case 'g' :
				string = strchr(optarg, 'x');
				if ( NULL == string ) {
					TRACE_MESSAGE(CONF, TL_MINOR,
							"The '%s' string format is not valid", optarg
						);
					break;
				}

				errno = 0;
				tmp_int = strtol(optarg, NULL, 10);
				if ( ERANGE == errno ) {
					TRACE_MESSAGE(CONF, TL_MINOR,
							"The '%s' is out of range", optarg
						);
					break;
				} else {
					*foundFlags |= CONF_GEOMETRY_WIDTH;
					result->geometry_width = tmp_int;
				}

				tmp_int = strtol(string+1, NULL, 10);
				if ( ERANGE == errno ) {
					TRACE_MESSAGE(CONF, TL_MINOR,
							"The '%s' is out of range", optarg
						);
				} else {
					*foundFlags |= CONF_GEOMETRY_HEIGHT;
					result->geometry_height = tmp_int;
				}
				break;

			case 'a' :
				errno = 0;
				tmp_int = strtol(optarg, &string, 10);
				if ( ERANGE == errno ) {
					TRACE_MESSAGE(CONF, TL_MINOR,
							"The '%s' is out of range", optarg
						);

				} else if ( optarg[0] != '\0' && string[0] == '\0' ) {
					if ( tmp_int < 0 || 3 < tmp_int ) {
						TRACE_MESSAGE(CONF, TL_MINOR,
								"The '%s' value is invalid for auto-zoom option", optarg
							);
					} else {
						*foundFlags |= CONF_AUTO_ZOOM;
						result->auto_zoom = tmp_int;
					}
				}
				break;

			case 'z' :
				errno = 0;
				tmp_double = strtod(optarg, NULL);

				if ( ERANGE == errno ) {
					TRACE_MESSAGE(CONF, TL_MINOR,
							"The '%s' is out of range", optarg
						);
				} else {
					*foundFlags |= CONF_ZOOM;
					result->zoom = tmp_double;
				}
				break;

			case 'P' :
				errno = 0;
				tmp_double = strtod(optarg, NULL);

				if ( ERANGE == errno ) {
					TRACE_MESSAGE(CONF, TL_MINOR,
							"The '%s' is out of range", optarg
						);
				} else {
					*foundFlags |= CONF_PAN_PERCENTAGE;
					result->pan_percentage = tmp_double;
				}
				break;

			case 'r' :
				*foundFlags |= CONF_RECURSE;
				if ( NULL == optarg ) {
					result->recurse = -1;
					break;
				}

				// parse out optional parameter with depth
				errno = 0;
				tmp_int = strtol(optarg, &string, 10);
				if ( ERANGE == errno ) {
					TRACE_MESSAGE(CONF, TL_MINOR,
							"The '%s' is out of range", optarg
						);
				} else if ( optarg[0] != '\0' && string[0] == '\0' ) {
					if ( tmp_int < -1 ) {
						result->recurse = -1;
					} else {
						result->recurse = tmp_int;
					}
				}
				break;

			case 'L' :
				*foundFlags |= CONF_LAYOUT_HINT;
				if ( NULL == result->layout_hint ) {
					hint_count = 1;
					result->layout_hint = (gchar**) g_try_malloc0(
							sizeof(gchar*) * (hint_count + 1)
						);
					alloc_line = __LINE__;
				} else {
					hint_count += 1;
					result->layout_hint = (gchar**) g_try_realloc(result->layout_hint,
							 sizeof(gchar*) * (hint_count + 1)
						);
					alloc_line = __LINE__;
				}

				// if allocation or reallocation failed, fall through to memory
				// allocation error handler
				if ( NULL == result->layout_hint ) {
					break;
				}

				result->layout_hint[hint_count-1] = strdup(optarg);
				alloc_line = __LINE__;
				string = result->layout_hint[hint_count-1];

				result->layout_hint[hint_count] = NULL;
				break;

			case 'e' :
				*foundFlags |= CONF_EXCLUDE;
				if ( NULL == result->exclude ) {
					exclude_count = 1;
					result->exclude = (gchar**) g_try_malloc0(
							sizeof(gchar*) * (exclude_count + 1)
						);
					alloc_line = __LINE__;
				} else {
					exclude_count += 1;
					result->exclude = (gchar**) g_try_realloc(result->exclude,
							 sizeof(gchar*) * (exclude_count + 1)
						);
					alloc_line = __LINE__;
				}

				// if allocation or reallocation failed, fall through to memory
				// allocation error handler
				if ( NULL == result->exclude ) {
					break;
				}

				result->exclude[exclude_count-1] = strdup(optarg);
				alloc_line = __LINE__;
				string = result->exclude[exclude_count-1];

				result->exclude[exclude_count] = NULL;
				break;

			case 't' :
				// this options is special, it does not set value into config
				// storage, it updates the trace setup immediately
				init_trace_setup(NULL, optarg);
				break;

			case 'h' :
				printf("usage: sivm [OPTIONS] [FILENAME | DIRECTORY]...\n\n");
				printf("See 'man sivm' for more detailed description\n");

				is_exit = TRUE;
				break;

			case 'v' :
				printf("Package    : %s\n", PACKAGE );
				printf("Version    : %s (%s)\n", VERSION, CODENAME);
				printf("Bug report : %s\n", PACKAGE_BUGREPORT);

				is_exit = TRUE;
				break;

			case '?' :
				break;
		}

		// check if memory allocation failed
		if ( 0 < alloc_line && NULL == string ) {
			ERROR_SET(ERROR.MEMORY_FAILURE);
			ERROR_NOTE("Allocation on line '%d' has failed", alloc_line);
			config_destroy(result);
			result = NULL;
			return result;

		} else if ( is_exit ) {
			config_destroy(result);
			exit(EXIT_SUCCESS);
		}
	}

	*firstArg = optind;
	return result;
}

/** \} */
