/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \defgroup config Configuration
 * \{
 *
 * \file src/config/config.h
 * \brief Declare high level interface for configuration loading and querying
 */
#ifndef __CONFIG_CONFIG_H__
#define __CONFIG_CONFIG_H__

#include "src/common/common_def.h"
#include "src/config/config_storage.h"


/** \brief Load configuration from cmdline and/or config file. */
extern RCode config_load(int argc, char** argv, int *index);

/** \brief Get read only pointer to the configuration storage. */
extern const ConfigStorage* config_get();

/** \brief Deallocate global configuration storage. */
extern RCode config_remove();

#endif
/** \} */
