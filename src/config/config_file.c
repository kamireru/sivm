/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup config
 * \{
 * \file config_file.c
 */

#include <string.h>
#include <confuse.h>
#include "src/config/config_file.h"

/**
 * Structure with definition of options that can be configured in the config
 * file.
 */
static cfg_opt_t config_option[] = {
		CFG_BOOL("fullscreen", cfg_false, CFGF_NONE),
		CFG_INT_LIST("geometry", "{400, 320}", CFGF_NONE),

		CFG_INT("auto-zoom", 1, CFGF_NONE),
		CFG_FLOAT("zoom", 1.0f, CFGF_NONE),
		CFG_FLOAT("pan", 0.6f, CFGF_NONE),

		CFG_BOOL("recursive", cfg_false, CFGF_NONE),
		CFG_INT("depth", -1, CFGF_NONE),
		CFG_STR_LIST("exclude", "{}", CFGF_NODEFAULT),

		CFG_STR("layout", "minimal", CFGF_NONE),
		CFG_STR_LIST("layout-hint", "{}", CFGF_NODEFAULT),
		CFG_END()
	};

/**
 * Structure with definition of configuration sections.
 */
static cfg_opt_t config_section[] = {
		CFG_SEC("config", config_option, CFGF_TITLE | CFGF_MULTI | CFGF_NO_TITLE_DUPES ),
		CFG_END()
	};


/**
 * Utility function for loading values of configuration options from library
 * structures into the config storage structure.
 *
 * Note: The function is not deallocating exclude list when string duplication
 * fails. The deallocation is done in caller function where the whole config
 * storage function is deallocated.
 *
 * \param cfg pointer to libconfuse structure
 * \param config pointer to sivm storage structure
 * \return OK on success
 */
static RCode config_load_values(cfg_t *cfg, ConfigStorage* config) {
	gint count, i;
	cfg_bool_t flag;

	flag = cfg_getbool(cfg, "fullscreen");
	config->fullscreen = ( flag == cfg_false ) ? FALSE : TRUE;

	if ( 2 != cfg_size(cfg, "geometry") ) {
		TRACE_MESSAGE(CONF, TL_MAJOR, "There must be 2 values for 'geometry' option");
	} else {
		config->geometry_width = cfg_getnint(cfg, "geometry", 0);
		config->geometry_height = cfg_getnint(cfg, "geometry", 1);
	}

	config->auto_zoom = cfg_getint(cfg, "auto-zoom");
	config->zoom = cfg_getfloat(cfg, "zoom");
	config->pan_percentage = cfg_getfloat(cfg, "pan");

	flag = cfg_getbool(cfg, "recursive");
	if ( cfg_true == flag ) {
		// todo
		config->recurse = cfg_getint(cfg, "depth");
	} else {
		config->recurse = 0;
	}

	count = cfg_size(cfg, "exclude");
	if ( 0 < count ) {
		config->exclude = (gchar**) g_try_malloc0( sizeof(gchar*) * (count+1));
		if ( NULL == config->exclude ) {
			ERROR_SET(ERROR.MEMORY_FAILURE);
			return FAIL;
		}

		for (i = 0; i < count; i++) {
			config->exclude[i] = strdup(cfg_getnstr(cfg, "exclude", i));
			if ( NULL == config->exclude[i] ) {
				ERROR_SET(ERROR.MEMORY_FAILURE);
				return FAIL;
			}
		}
		config->exclude[count] = NULL;
	}

	config->layout = strdup(cfg_getstr(cfg, "layout"));
	if ( NULL == config->layout ) {
		ERROR_SET(ERROR.MEMORY_FAILURE);
		return FAIL;
	}

	count = cfg_size(cfg, "layout-hint");
	if ( 0 < count ) {
		config->layout_hint = (gchar**) g_try_malloc0( sizeof(gchar*) * (count+1));
		if ( NULL == config->layout_hint ) {
			ERROR_SET(ERROR.MEMORY_FAILURE);
			return FAIL;
		}

		for (i = 0; i < count; i++) {
			config->layout_hint[i] = strdup(cfg_getnstr(cfg, "layout-hint", i));
			if ( NULL == config->layout_hint[i] ) {
				ERROR_SET(ERROR.MEMORY_FAILURE);
				return FAIL;
			}
		}
		config->layout_hint[count] = NULL;
	}

	return OK;
}

/**
 * Load configuration for given profile from configuration file.
 *
 * \param filename name of the configuration file
 * \param profile name of the configuration profile
 * \return pointer to new configuration storage or NULL on error
 */
ConfigStorage* config_from_file(gchar *filename, gchar *profile) {
	ConfigStorage* result = NULL;
	cfg_t *cfg, *cfg_config;
	gint i;

	ASSERT( NULL != filename );
	ASSERT( NULL != profile );

	// initialize config file parsing
	cfg = cfg_init(config_section, CFGF_IGNORE_UNKNOWN );

	if ( CFG_PARSE_ERROR == cfg_parse(cfg, filename) ) {
		ERROR_SET(ERROR.PARSE_FILE);
		ERROR_NOTE("Configuration file: '%s'", filename);
		return NULL;
	}

	// create configuration storage structure
	result = config_create();
	if ( NULL == result ) {
		ERROR_NOTE("Can't create configuration storage");
		g_free(cfg);
		return result;
	}

	// search for config section with profile name
	for (i = 0; i < cfg_size(cfg, "config"); i++) {
		cfg_config = cfg_getnsec(cfg, "config", i);

		if ( 0 != strcmp(profile, cfg_title(cfg_config)) ) {
			continue;
		}

		// load values from profile
		if ( FAIL == config_load_values(cfg_config, result) ) {
			ERROR_NOTE("Can't load configuration values from library");
			config_destroy(result);
			result = NULL;
		}

		cfg_free(cfg);
		return result;
	}

	cfg_free(cfg);
	return result;
}


/** \} */
