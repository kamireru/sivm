/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup config
 * \{
 *
 * \file src/config/config_file.h
 * \brief Header file for configuration loading from file
 *
 * The header file declares functions used for retrieving configuration from
 * file.
 */
#ifndef __CONFIG_CONFIG_CMDLINE_H__
#define __CONFIG_CONFIG_CMDLINE_H__

#include "src/config/config_storage.h"


/** \brief Allocate storage and fill it from file. */
ConfigStorage* config_from_cmdline(int argc, char** argv, int *foundFlags, int *firstArg);

#endif
/** \} */
