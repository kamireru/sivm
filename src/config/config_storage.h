/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup config
 * \{
 *
 * \file src/config/config_storage.h
 * \brief Header file for configuration storage and manipulation.
 *
 * The whole configuration for SIVM program is stored in one dynamically
 * allocated structure. It is possible to allocate more of these structures but
 * only one will be used for final configuration.
 *
 * The allocation of more structures is used in configuration loading to load
 * config from several sources and then combine it in predefined order.
 */
#ifndef __CONFIG_CONFIG_STORAGE_H__
#define __CONFIG_CONFIG_STORAGE_H__

#include <glib.h>
#include "src/common/common_def.h"

/**
 * The structure holding all program startup options.
 */
typedef struct _ConfigStorage {
		/** \brief Storage for configuration file name. */
		gchar *filename;

		/** \brief Storage for configuration profile name. */
		gchar *profile;

		/** \brief Storage for fullscreen startup indicator. */
		gboolean fullscreen;

		/** \brief Storage for auto-zoom startup indicator. */
		gint auto_zoom;

		/** \brief Storage for zoom startup value. */
		gdouble zoom;

		/** \brief Storage for startup pan percentage value. */
		gdouble pan_percentage;

		/** \brief Storage for window width startup value. */
		gint geometry_width;

		/** \brief Storage for window height startup value. */
		gint geometry_height;

		/** \brief Storage for recursive loading indicator. */
		gboolean recurse;

		/**
		 * \brief Storage for exclude patterns.
		 *
		 * The exclude patterns are stored as NULL terminated list of char*
		 * pointers.
		 * */
		gchar **exclude;

		/** \brief Storage for layout name. */
		gchar *layout;

		/** \brief Storage for layout hint names.
		 *
		 * The hint names are stored as NULL terminated list of char pointers
		 */
		gchar **layout_hint;

	} ConfigStorage;

/**
 * The enumeration contains bit flags for each configuration options. It is used
 * for controlling configuration merging.
 */
typedef enum  {
		CONF_FILENAME		= 0x1 << 0,
		CONF_PROFILE		= 0x1 << 1,
		CONF_FULLSCREEN		= 0x1 << 2,
		CONF_AUTO_ZOOM		= 0x1 << 3,
		CONF_ZOOM			= 0x1 << 4,
		CONF_GEOMETRY_WIDTH	= 0x1 << 5,
		CONF_GEOMETRY_HEIGHT= 0x1 << 6,
		CONF_RECURSE		= 0x1 << 7,
		CONF_EXCLUDE		= 0x1 << 8,
		CONF_LAYOUT			= 0x1 << 9,
		CONF_LAYOUT_HINT	= 0x1 << 10,
		CONF_PAN_PERCENTAGE	= 0x1 << 11
	} ConfigOption;

/** \brief Allocate new configuration storage. */
extern ConfigStorage* config_create();

/** \brief Deallocate configuration storage. */
extern RCode config_destroy(ConfigStorage *conf);

/** \brief Update configuration from another configuration. */
extern RCode config_update(ConfigStorage *dst, ConfigStorage *src, ConfigOption options);


// internal functions exported only to use in unit tests

/** \brief Clone NULL terminated string list. */
extern gchar** config_clone_list(gchar** list);

/** \brief Deallocate NULL terminated string list. */
extern RCode config_destroy_list(gchar** list);

#endif
/** \} */
