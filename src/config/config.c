/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup config
 * \{
 * \file config.c
 */

#include <string.h>

#include "src/config/config.h"
#include "src/config/config_file.h"
#include "src/config/config_cmdline.h"

#define CONFIG_DEFAULT_FILE ".sivmrc"

/**
 * Static variable for storing pointer to the global configuration storage.
 */
static ConfigStorage* config = NULL;

/**
 * Load options from command line and then decide whenever there should be
 * attempt to load them from confguration file too. Merge those two together and
 * store into global configuration storage.
 *
 * \param argc number of arguments
 * \param argv array of arguments
 * \param index pointer to int where the position of first non-option argument
 * in list is to be stored
 * \return OK on success
 */
RCode config_load(int argc, char** argv, int *index) {
	int flags = 0;
	ConfigStorage *file = NULL;
	ConfigStorage *cmd = NULL;
	char* filename = NULL;
	char* profile = NULL;
	char buffer[4096] = "";

	cmd = config_from_cmdline(argc, argv, &flags, index);
	if ( NULL == cmd ) {
		ERROR_NOTE("Command line parsing failed");
		return FAIL;
	}

	// check if filename was supplied or use default
	if ( NULL != cmd->filename ) {
		filename = cmd->filename;
	} else {
		char* directory = getenv("HOME");
		if ( NULL != directory ) {
			filename = buffer;
			filename = strcat(filename, directory);
			filename = strcat(filename, "/");
			filename = strcat(filename, CONFIG_DEFAULT_FILE);
		}
	}

	// check if profile was supplied or use default
	if ( NULL != cmd->profile ) {
		profile = cmd->profile;
	} else {
		profile = "default";
	}

	// check if we have both filename and profile. if not, use only
	// configuration from command line
	if ( NULL == profile || NULL == filename ) {
		config = cmd;
		return OK;
	}

	// load configuration from file and merge it with command line config
	file = config_from_file(filename, profile);
	if ( NULL != file ) {
		if ( FAIL == config_update( file, cmd, flags ) ) {
			ERROR_NOTE("Can't merge command line and file '%s' configuration",
					filename
				);
			config_destroy(cmd);
			config_destroy(file);
			return FAIL;
		}

		// this structure is no longer needed
		config_destroy( cmd );
		config = file;

	} else {
		config = cmd;
	}

	return OK;
}

/**
 * Get constant pointer to the configuration storage.
 *
 * \return pointer to ConfigStorage structure
 */
const ConfigStorage* config_get() {
	return config;
}

/**
 * Deallocate global configuration storage
 */
RCode config_remove() {
	config_destroy(config);
	config = NULL;
	return OK;
}

/** \} */
