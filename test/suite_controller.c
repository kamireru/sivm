#include <stdlib.h>
#include <check.h>

#include "src/common/common_def.h"

#include "test/frontend/test_cntl_common.h"
#include "test/frontend/test_cntl_generic.h"
#include "test/frontend/test_cntl_application.h"
#include "test/frontend/test_cntl_image_display.h"
#include "test/frontend/test_cntl_container.h"

/**
 * \brief Unit test suite main method.
 *
 * Create test suite runner and add test suites it should run. Use environment
 * variable to control how the suites will be run (forked/no forked mode).
 *
 * This suite is not only for generic controller test suite, it is used for all controller
 * related test suites (something like mini master suite for controller related
 * functionality)
 *
 * \param argc number of parameters to program
 * \param argv array of parameters to program
 * \return number of failed suites
 */
int main(int argc, char** argv)
{
	int number_failed = 0;

	/* enable tracing when suite is invoked separately from master suite */
	trace_global_mask = 0x1 << 11;

	SRunner *suite_runner = srunner_create( create_cntl_common_suite() );
	srunner_add_suite( suite_runner, create_cntl_generic_suite() );
	srunner_add_suite( suite_runner, create_cntl_application_suite() );
	srunner_add_suite( suite_runner, create_cntl_image_display_suite() );
	srunner_add_suite( suite_runner, create_cntl_container_suite() );

	// setup tracing from command line and environment
	init_trace_areas();
	init_trace_setup("SIVM_SUITE_TRACE", (argc >= 2) ? argv[1] : NULL );

	srunner_run_all(suite_runner, CK_ENV);
	number_failed = srunner_ntests_failed(suite_runner);

	srunner_free(suite_runner);

	return ( 0 == number_failed ) ? EXIT_SUCCESS : EXIT_FAILURE;
}
