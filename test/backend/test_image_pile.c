/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>
#include <malloc.h>
#include "src/config/config.h"
#include "src/backend/image_pile.h"

#include "test/backend/test_image_pile.h"
#include "test/common/common.h"

/** \brief Module-wide variable to simulate argc entry from OS. */
static int argc = 0;

/** \brief Module-wide variable to simulate argv entry from OS. */
static char **argv = NULL;

/** \brief Module-wide variable for accessing configuration. */
static ConfigStorage* conf;

/**
 * A structure used for storing data for checking presence of filename in loaded
 * image pile.
 *
 * The test data should contain boolean indicating whenever the image is found
 * in pile (ie lookup returns not NULL pointer) and filename.
 */
struct FileInPileTest {
		gboolean is_present;
		gchar * filename;
	};

/**
 * \brief Wrapper function for 'file in pile' tests
 *
 * The function takes pointer to the null terminated array of test data and
 * checks if file is/is not in the image pile.
 *
 * The NULL used for termination of processing is in the 'filename' member of
 * the data structure.
 */
static void test_presence_in_file(struct FileInPileTest data[]) {
	int i;

	for (i = 0; NULL != (data + i)->filename; i++) {
		Image *image = pile_get_image( (data + i)->filename );

		ck_assert_msg(
				(data+i)->is_present == (NULL != image),

				"pile_get_image('%s') %s found image where it should %s be found",
				(data+i)->filename,
				( NULL != image ) ? "" : "not",
				( (data + i)->is_present ) ? "" : "not"
			);
	}
}

static struct FileInPileTest data_non_recurse[] = {
		{  TRUE, "data/unit/4x4_black.png" },
		{  TRUE, "data/unit/4x4_white.png" },
		{ FALSE, "data/unit/lvl1_dir1" },
		{ FALSE, "sivm/unit/lvl1_dir2" },
		{ FALSE,  NULL }
	};

/**
 * \brief Test image loading without recursion and image pile deallocation
 *
 * Load few files into the image pile and verify they are loaded correctly and
 * that the image references are handled correctly when the pile is destroyed.
 */
START_TEST(pile_load_file) {
	Image* image;
	RCode result;
	char *cmdline = "data/unit/4x4_black.png data/unit/lvl1_dir1 data/unit/4x4_white.png";

	create_program_args(cmdline, &argc, &argv);
	mark_point();

	// setup configuration variable and load images from pile
	conf->recurse = 0;

	result = pile_load_images(argc, argv);
	ck_assert_int_eq( OK, result );

	test_presence_in_file(data_non_recurse);

	// get pointer to one image and reference it so we can use it later
	image = pile_get_image("data/unit/4x4_black.png");
	fail_unless( OK == image_ref(image), NULL );
	fail_unless( 2 == image->ref, NULL );

	fail_if( FAIL == pile_destroy(), NULL);
	fail_if( error_is_error(), NULL );
	fail_if( 0 != pile_get_size(),
			"Failure '0 != pile_get_size()' occured where %d == pile_get_size()",
			pile_get_size()
		);

	// check if image reference count is correct
	fail_unless( 1 == image->ref, NULL );
	fail_unless( OK == image_unref(image), NULL );

	destroy_program_args(argc, &argv);
} END_TEST


static struct FileInPileTest data_recurse_one_level[] = {
		{  TRUE, "data/unit/4x4_black.png" },
		{  TRUE, "data/unit/4x4_white.png" },
		{ FALSE, "data/unit/lvl1_dir1" },
		{ FALSE, "data/unit/lvl1_dir2" },
		{ FALSE, "data/unit/lvl1_dir3" },
		{ FALSE, "data/unit/lvl1_dir1/4x4_cyan.png" },
		{ FALSE, "data/unit/lvl1_dir2/4x4_gray.png" },
		{ FALSE, "data/unit/lvl1_dir3/4x4_brown.png" },
		{ FALSE,  NULL }
	};

/**
 * \brief Test recursive loading up to 1 directory level
 */
START_TEST(pile_load_recurse1) {
	RCode result;
	char *cmdline = "data/unit";

	create_program_args(cmdline, &argc, &argv);
	mark_point();

	// setup configuration variable and load images from pile
	conf->recurse = 1;

	result = pile_load_images(argc, argv);
	ck_assert_int_eq( OK, result );

	test_presence_in_file(data_recurse_one_level);

	// deallocation check
	fail_if( FAIL == pile_destroy(), NULL);
	destroy_program_args(argc, &argv);
} END_TEST


static struct FileInPileTest data_recurse_two_level[] = {
		{ TRUE, "data/unit/4x4_black.png" },
		{ TRUE, "data/unit/4x4_white.png" },
		{ TRUE, "data/unit/lvl1_dir1/4x4_cyan.png" },
		{ TRUE, "data/unit/lvl1_dir2/4x4_gray.png" },
		{ TRUE, "data/unit/not_image.txt" },
		{ TRUE, "data/unit/lvl1_dir3/4x4_brown.png" },
		{ FALSE, "data/unit/lvl1_dir1" },
		{ FALSE, "data/unit/lvl1_dir2" },
		{ FALSE, "data/unit/lvl1_dir3" },
		{ FALSE, "data/unit/lvl1_dir1/lvl2_dir1" },
		{ FALSE, "data/unit/lvl1_dir1/lvl2_dir1/4x4_orange.png" },
		{ FALSE, "data/unit/lvl1_dir1/lvl2_dir2/4x4_red.png" },
		{ FALSE, "data/unit/lvl1_dir1/lvl2_dir2" },
		{ FALSE,  NULL }
	};

/**
 * \brief Test recursive loading up to 2 directory level
 */
START_TEST(pile_load_recurse2) {
	RCode result;
	char *cmdline = "data/unit";

	create_program_args(cmdline, &argc, &argv);
	mark_point();

	// setup configuration variable and load images from pile
	conf->recurse = 2;

	result = pile_load_images(argc, argv);
	ck_assert_int_eq( OK, result );

	test_presence_in_file(data_recurse_two_level);

	// deallocation check
	fail_if( FAIL == pile_destroy(), NULL);
	destroy_program_args(argc, &argv);
} END_TEST


static struct FileInPileTest data_recurse_unlimited[] = {
		{ TRUE, "data/unit/4x4_black.png" },
		{ TRUE, "data/unit/4x4_white.png" },
		{ TRUE, "data/unit/lvl1_dir1/4x4_cyan.png" },
		{ TRUE, "data/unit/lvl1_dir2/4x4_gray.png" },
		{ TRUE, "data/unit/not_image.txt" },
		{ TRUE, "data/unit/lvl1_dir3/4x4_brown.png" },
		{ TRUE, "data/unit/lvl1_dir1/lvl2_dir1/4x4_magenta.png" },
		{ TRUE, "data/unit/lvl1_dir1/lvl2_dir1/4x4_green.png" },
		{ TRUE, "data/unit/lvl1_dir1/lvl2_dir1/4x4_orange.png" },
		{ TRUE, "data/unit/lvl1_dir1/lvl2_dir2/4x4_red.png" },
		{ TRUE, "data/unit/lvl1_dir1/lvl2_dir2/4x4_yellow.png" },
		{ FALSE, "data/unit/lvl1_dir1" },
		{ FALSE, "data/unit/lvl1_dir2" },
		{ FALSE, "data/unit/lvl1_dir3" },
		{ FALSE, "data/unit/lvl1_dir1/lvl2_dir1" },
		{ FALSE, "data/unit/lvl1_dir1/lvl2_dir2" },
		{ FALSE,  NULL }
	};

/**
 * \brief Test recursive loading without depth limit
 */
START_TEST(pile_load_recurse_unlimit) {
	RCode result;
	char *cmdline = "data/unit";

	create_program_args(cmdline, &argc, &argv);
	mark_point();

	// setup configuration variable and load images from pile
	conf->recurse = -1;

	result = pile_load_images(argc, argv);
	ck_assert_int_eq( OK, result );

	test_presence_in_file(data_recurse_unlimited);

	// deallocation check
	fail_if( FAIL == pile_destroy(), NULL);
	destroy_program_args(argc, &argv);
} END_TEST


static struct FileInPileTest data_recurse_one_level_multi[] = {
		{ TRUE, "data/unit/4x4_black.png" },
		{ TRUE, "data/unit/4x4_white.png" },
		{ TRUE, "data/unit/lvl1_dir1/4x4_cyan.png" },
		{ FALSE, "data/unit/lvl1_dir1" },
		{ FALSE, "data/unit/lvl1_dir2" },
		{ FALSE, "data/unit/lvl1_dir3" },
		{ FALSE, "data/unit/lvl1_dir1/lvl2_dir1" },
		{ FALSE, "data/unit/lvl1_dir1/lvl2_dir1/4x4_orange.png" },
		{ FALSE, "data/unit/lvl1_dir1/lvl2_dir2/4x4_red.png" },
		{ FALSE, "sivm" },
		{ FALSE, "data/unit/lvl1_dir1/lvl2_dir2" },
		{ FALSE,  NULL }
	};

/**
 * \brief Test recursive loading up to one directory level using multiple start
 * points.
 */
START_TEST(pile_load_multi_recurse1) {
	RCode result;
	char *cmdline = "data/unit data/unit/lvl1_dir1";

	create_program_args(cmdline, &argc, &argv);
	mark_point();

	// setup configuration variable and load images from pile
	conf->recurse = 1;

	result = pile_load_images(argc, argv);
	ck_assert_int_eq( OK, result );

	test_presence_in_file(data_recurse_one_level_multi);

	// deallocation check
	fail_if( FAIL == pile_destroy(), NULL);
	destroy_program_args(argc, &argv);
} END_TEST


static struct FileInPileTest data_recurse_exclude[] = {
		{ TRUE, "data/unit/4x4_white.png" },
		{ TRUE, "data/unit/lvl1_dir1/4x4_cyan.png" },
		{ TRUE, "data/unit/lvl1_dir1/lvl2_dir1/4x4_green.png" },
		{ TRUE, "data/unit/lvl1_dir1/lvl2_dir1/4x4_magenta.png" },
		{ TRUE, "data/unit/lvl1_dir1/lvl2_dir1/4x4_orange.png" },
		{ TRUE, "data/unit/not_image.txt" },
		{ FALSE, "data/unit/4x4_black.png" },
		{ FALSE, "data/unit/lvl1_dir1" },
		{ FALSE, "data/unit/lvl1_dir1/lvl2_dir1" },
		{ FALSE, "data/unit/lvl1_dir1/lvl2_dir2" },
		{ FALSE, "data/unit/lvl1_dir1/lvl2_dir2/4x4_red.png" },
		{ FALSE, "data/unit/lvl1_dir1/lvl2_dir2/4x4_yellow.png" },
		{ FALSE, "data/unit/lvl1_dir2" },
		{ FALSE, "data/unit/lvl1_dir2/4x4_gray.png" },
		{ FALSE, "data/unit/lvl1_dir3" },
		{ FALSE, "data/unit/lvl1_dir3/4x4_brown.png" },
		{ FALSE,  NULL }
	};

/**
 * \brief Test recursive loading without directory depth limit and with two
 * exclude patterns set.
 */
START_TEST(pile_load_multi_exclude) {
	RCode result;
	char** exclude = NULL;
	char pattern1[10] = "lvl?_dir2";
	char pattern2[10] = "4x4_b*";
	char *cmdline = "data/unit";

	// allocate memory for pattern list
	exclude = malloc(3*sizeof(char*));
	ck_assert_int_ne( NULL, exclude );
	exclude[0] = pattern1;
	exclude[1] = pattern2;
	exclude[2] = NULL;

	create_program_args(cmdline, &argc, &argv);
	mark_point();

	// setup configuration variable and load images from pile
	conf->recurse = -1;
	conf->exclude = exclude;

	result = pile_load_images(argc, argv);
	ck_assert_int_eq( OK, result );

	test_presence_in_file(data_recurse_exclude);
	// deallocation check
	fail_if( FAIL == pile_destroy(), NULL);

	// we must free exclude array here and assign NULL to conf storage this
	// array is artificialy created and is not whole dynamic allocated (as
	// opposed to real situation. Due to that any attempt to deallocate it with
	// config_reset() will fail
	free( (void*) exclude );
	conf->exclude = NULL;

	destroy_program_args(argc, &argv);
} END_TEST

/**
 * \brief Check retrieval of the list of images from pile
 */
START_TEST(test_get_list) {
	Image** array = NULL;
	char *cmdline = "data/unit/4x4_black.png data/unit/4x4_white.png";

	// prepare argv structure for test
	create_program_args(cmdline, &argc, &argv);

	mark_point();

	// load two images into the image pile and test they are there
	ck_assert( OK == pile_load_images(argc, argv) );
	ck_assert( 2 == pile_get_size() );
	ck_assert( NULL != pile_get_image("data/unit/4x4_black.png") );
	ck_assert( NULL != pile_get_image("data/unit/4x4_white.png") );

	// check retrieval of the image list
	array = pile_get_image_list();
	ck_assert( NULL != array );
	ck_assert_str_eq( "data/unit/4x4_black.png", array[0]->name );
	ck_assert_str_eq( "data/unit/4x4_white.png", array[1]->name );

	fail_if( FAIL == pile_destroy(), NULL);

	destroy_program_args(argc, &argv);
	g_free( (gpointer)array);
} END_TEST

/**
 * \brief Setup function for file loading tests
 *
 * The function resets the static variables and configuration singleton to
 * prepare for tests.
 */
static void test_setup(void) {
	int index;
	char* list[] = {
			"no argument",
			NULL
		};

	COLOR_SETUP_START;

	config_load(1, list, &index);
	argc = 0;
	argv = NULL;

	// we are intentionally discarding const modifier here
	conf = (ConfigStorage*) config_get();

	if ( error_is_error() ) {
		ERROR_LOG();
	}
	COLOR_SETUP_END;
	return ;
}

/**
 * \brief Teardown funciton for file loading tests.
 *
 * The function resets any error that occured in the tests and were not expected
 * (and reset in the test).
 */
static void test_teardown(void) {
	COLOR_TEARDOWN_START;

	config_remove();
	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_TEARDOWN_END;
	return ;
}

/**
 * \brief Create test suite for image pile.
 * \return pointer to accept set suite
 */
Suite* create_image_pile_suite (void) {
  Suite *s = suite_create ("ImagePile");

  TCase *tc_load = tcase_create ("Load");
  tcase_add_checked_fixture (tc_load, test_setup, test_teardown);
  tcase_add_test (tc_load, pile_load_file);
  tcase_add_test (tc_load, pile_load_recurse1);
  tcase_add_test (tc_load, pile_load_recurse2);
  tcase_add_test (tc_load, pile_load_recurse_unlimit);
  tcase_add_test (tc_load, pile_load_multi_recurse1);
  tcase_add_test (tc_load, pile_load_multi_exclude);

  TCase *tc_manage = tcase_create("Manage");
  tcase_add_checked_fixture (tc_manage, test_setup, test_teardown);
  tcase_add_test(tc_manage, test_get_list);

  suite_add_tcase(s, tc_load);
  suite_add_tcase(s, tc_manage);

  return s;
}

