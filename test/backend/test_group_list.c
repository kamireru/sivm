/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <glib.h>
#include <string.h>
#include <malloc.h>
#include "src/common/common_def.h"
#include "src/backend/group_list.h"
#include "src/backend/image_group.h"

#include "test/backend/test_group_list.h"
#include "test/common/common.h"

static ImageGroup * group_data[5];

/**
 * \brief Test creation and destruction of group list.
 *
 * This test must be run without setup and teardown function which create group
 * list for other tests.
 */
START_TEST(test_create) {

	fail_if( OK != glist_create(), NULL);
	fail_if( 0 != glist_get_size(), NULL);

	// only one group list can be created
	fail_if( FAIL != glist_create(), NULL);

	fail_if( OK != glist_destroy(), NULL);

} END_TEST


/**
 * \brief Test function invocation on not created group list.
 *
 * This test must be run without setup funtions which are used for other tests
 *
 * Its aim is to test response of function when GroupList is not allocated.
 */
START_TEST(test_noexistent) {

	ImageGroup *tmp = group_create("test", 'a');

	fail_unless( FAIL == glist_group_insert(tmp), NULL);
	fail_unless( FAIL == glist_group_remove_by_index(0), NULL);

	fail_unless( FAIL == glist_get_by_index(0), NULL);
	fail_unless( NULL == glist_get_by_tag('Y'), NULL);
	fail_unless( NULL == glist_get_by_name("some name"), NULL);

	fail_unless( -1 == glist_get_size(), NULL);

	group_destroy(tmp);
	error_reset();
} END_TEST


/**
 * Test addition of pointers into GroupList
 */
START_TEST(test_group_insert) {

	fail_unless( 0 == glist_get_size(), NULL);

	fail_unless( OK == glist_group_insert(group_data[0]), NULL);
	fail_unless( 1 == glist_get_size(), NULL);

	fail_unless( OK == glist_group_insert(group_data[1]), NULL);
	fail_unless( OK == glist_group_insert(group_data[2]), NULL);
	fail_unless( OK == glist_group_insert(group_data[3]), NULL);
	fail_unless( OK == glist_group_insert(group_data[4]), NULL);

	fail_unless( 5 == glist_get_size(), NULL);

	fail_unless( 'a' == glist_get_by_index(0)->tag, NULL);
	fail_unless( 'b' == glist_get_by_index(1)->tag, NULL);
	fail_unless( 'c' == glist_get_by_index(2)->tag, NULL);
	fail_unless( 'd' == glist_get_by_index(3)->tag, NULL);
	fail_unless( 'e' == glist_get_by_index(4)->tag, NULL);

} END_TEST


/**
 * Test removal of pointers from GroupList
 */
START_TEST(test_group_remove) {

	// fill some data into list
	fail_unless( OK == glist_group_insert(group_data[0]), NULL);
	fail_unless( OK == glist_group_insert(group_data[1]), NULL);
	fail_unless( OK == glist_group_insert(group_data[2]), NULL);
	fail_unless( OK == glist_group_insert(group_data[3]), NULL);
	fail_unless( OK == glist_group_insert(group_data[4]), NULL);
	fail_unless( 5 == glist_get_size(), NULL);

	// simple removal of present element
	fail_unless( OK == glist_group_remove_by_index(1), NULL );
	fail_unless( 'a' == glist_get_by_index(0)->tag, NULL);
	fail_unless( 'e' == glist_get_by_index(1)->tag, NULL);
	fail_unless( 'c' == glist_get_by_index(2)->tag, NULL);
	fail_unless( 'd' == glist_get_by_index(3)->tag, NULL);
	fail_unless( 4 == glist_get_size(), NULL);

	// removal of image group by tag
	fail_unless( OK == glist_group_remove_by_tag('e'), NULL );
	fail_unless( 'a' == glist_get_by_index(0)->tag, NULL);
	fail_unless( 'd' == glist_get_by_index(1)->tag, NULL);
	fail_unless( 'c' == glist_get_by_index(2)->tag, NULL);
	fail_unless( 3 == glist_get_size(), NULL);

	// removal of image group by name
	fail_unless( OK == glist_group_remove_by_name("test group 4"), NULL );
	fail_unless( 'a' == glist_get_by_index(0)->tag, NULL);
	fail_unless( 'c' == glist_get_by_index(1)->tag, NULL);
	fail_unless( 2 == glist_get_size(), NULL);

	// removal of non-existent tag or name should not fail
	fail_unless( OK == glist_group_remove_by_name("test group 4"), NULL );
	fail_unless( OK == glist_group_remove_by_tag('e'), NULL );

	// removal of out of bounds index
	fail_unless( FAIL == glist_group_remove_by_index(-1), NULL);
	fail_unless( FAIL == glist_group_remove_by_index(4), NULL);
	fail_unless( 2 == glist_get_size(), NULL);

	error_reset();

} END_TEST


/**
 * Test lookup according to the tag and name
 */
START_TEST(test_group_lookup) {

	// fill some data into list
	fail_unless( OK == glist_group_insert(group_data[0]), NULL);
	fail_unless( OK == glist_group_insert(group_data[1]), NULL);
	fail_unless( OK == glist_group_insert(group_data[2]), NULL);
	fail_unless( OK == glist_group_insert(group_data[3]), NULL);
	fail_unless( OK == glist_group_insert(group_data[4]), NULL);
	fail_unless( 5 == glist_get_size(), NULL);

	// lookups using indices as a result
	fail_unless( 2 == glist_index_by_tag('c'), NULL);
	fail_unless( 3 == glist_index_by_name("test group 4"), NULL);

	fail_unless( -1 == glist_index_by_tag('Y'), NULL);
	fail_unless( -1 == glist_index_by_name("some name"), NULL);

	// lookups using pointer to image group as result
	fail_unless( 'c' == glist_get_by_tag('c')->tag, NULL);
	fail_unless( 'd' == glist_get_by_name("test group 4")->tag, NULL);

	fail_unless( NULL == glist_get_by_tag('Y'), NULL);
	fail_unless( NULL == glist_get_by_name("some name"), NULL);

} END_TEST


/**
 * The setup and teardown function will be used for all test except for creation
 * and destruction of group list.
 */
static void test_setup(void) {
	COLOR_SETUP_START;
	glist_create();

	// prepare image groups we will use for tests
	group_data[0] = group_create("test group 1", 'a');
	group_data[1] = group_create("test group 2", 'b');
	group_data[2] = group_create("test group 3", 'c');
	group_data[3] = group_create("test group 4", 'd');
	group_data[4] = group_create("test group 5", 'e');

	if ( error_is_error() ) {
		ERROR_LOG();
	}
	COLOR_SETUP_END;
	return ;
}

/**
 * Deallocate the group list and destroy the test data created for unit tests.
 */
static void test_teardown(void) {

	COLOR_TEARDOWN_START;
	glist_destroy();

	group_destroy(group_data[0]);
	group_destroy(group_data[1]);
	group_destroy(group_data[2]);
	group_destroy(group_data[3]);
	group_destroy(group_data[4]);

	if ( error_is_error() ) {
		ERROR_LOG();
	}
	COLOR_TEARDOWN_END;
	return ;
}

/**
 * \brief Create test suite for group list.
 * \return pointer to accept set suite
 */
Suite* create_group_list_suite (void) {

  Suite *s = suite_create ("GroupList");

  TCase *tc_create = tcase_create ("Manage");
  tcase_add_checked_fixture (tc_create, NULL, NULL);
  tcase_add_test (tc_create, test_create);
  tcase_add_test (tc_create, test_noexistent);

  TCase *tc_manage = tcase_create ("Use");
  tcase_add_checked_fixture (tc_manage, test_setup, test_teardown);
  tcase_add_test (tc_manage, test_group_insert);
  tcase_add_test (tc_manage, test_group_remove);
  tcase_add_test (tc_manage, test_group_lookup);

  suite_add_tcase (s, tc_create);
  suite_add_tcase (s, tc_manage);

  return s;
}
