/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include "src/common/error.h"
#include "src/backend/image.h"

#include "test/backend/test_image.h"
#include "test/common/common.h"

/**
 * \brief Verify Image instance creation and destructoin
 *
 * Check that the initial values are set correctly and that the destroy() and
 * unref() functions are working.
 */
START_TEST(test_image_create) {
	Image* image = NULL;

	image = image_create("data/unit/4x4_black.png");
	ck_assert( NULL != image );
	ck_assert( NULL != image->group );
	ck_assert( NULL != image->name );
	ck_assert( ! error_is_error() );

	ck_assert_str_eq( "data/unit/4x4_black.png", image->name );
	ck_assert_int_eq( 0, image->size );
	ck_assert_int_eq( 0, image->timestamp );
	ck_assert_int_eq( 1, image->ref );

	// end with error since image with reference can't be destroyed
	ck_assert( FAIL == image_destroy(image) );
	ck_assert( error_is_error() );
	error_reset();

	ck_assert( OK == image_unref(image) );

} END_TEST

/**
 * \brief Verify Image reference handling
 *
 * Check that reference is increased when the image is referenced and that the
 * get reference function returns correct value.
 */
START_TEST(test_image_reference) {
	Image* image = NULL;

	image = image_create("data/unit/4x4_black.png");
	ck_assert( NULL != image );
	ck_assert( 1 == image->ref );
	ck_assert( 1 == image_ref_count(image) );

	ck_assert( OK == image_ref(image) );
	ck_assert( OK == image_ref(image) );
	ck_assert_int_eq( 3, image->ref );
	ck_assert_int_eq( 3, image_ref_count(image) );

	ck_assert( OK == image_unref(image) );
	ck_assert_int_eq( 2, image->ref );
	ck_assert_int_eq( 2, image_ref_count(image) );

	// destroy of the structure is possible only when there are no references
	ck_assert( FAIL == image_destroy(image) );
	error_reset();

	image->ref = 0;
	ck_assert( OK == image_destroy(image) );

} END_TEST


/**
 * Check addition and removal of group tag to the image. This test does not
 * check working of internal storage.
 */
START_TEST(test_image_groups) {
	Image* image = NULL;

	image = image_create("data/unit/4x4_black.png");

	ck_assert( NULL != image);
	ck_assert( NULL != image->group );
	ck_assert( image->group == image_get_groups(image) );

	// new image does not belong to any group
	ck_assert( 0 == taglist_get_count(image->group) );
	ck_assert( FALSE == taglist_contains(image->group, 'a') );

	// add few groups to the image
	ck_assert( OK == taglist_insert(image->group, 'a') );
	ck_assert( OK == taglist_insert(image->group, 'b') );

	// check that the image is in those groups
	ck_assert( TRUE == taglist_contains(image->group, 'a') );
	ck_assert( TRUE == taglist_contains(image->group, 'b') );
	ck_assert_int_eq( 2, taglist_get_count(image->group) );

	// remove group from image and check it is removed
	ck_assert( OK == taglist_remove(image->group, 'a') );
	ck_assert_int_eq( 1, taglist_get_count(image->group) );

	ck_assert( OK == image_unref(image) );
} END_TEST

/**
 * \brief Create test suite for image.
 * \return pointer to accept set suite
 */
Suite* create_image_suite (void)
{
  Suite *s = suite_create ("Image");

  TCase *tc_image_basic = tcase_create ("ImageBasic");
  tcase_add_checked_fixture (tc_image_basic, common_setup, common_teardown);
  tcase_add_test (tc_image_basic, test_image_create);
  tcase_add_test (tc_image_basic, test_image_reference);
  tcase_add_test (tc_image_basic, test_image_groups);

  suite_add_tcase (s, tc_image_basic);

  return s;
}

