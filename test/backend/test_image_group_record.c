/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <glib.h>
#include <string.h>
#include <malloc.h>
#include "src/common/common_def.h"
#include "src/backend/image_group_record.h"

#include "test/backend/test_image_group_record.h"
#include "test/common/common.h"

/**
 * Verify function for checking if range is inside bounds works correctly
 * (See range_check_bounds for description)
 */
START_TEST(test_range) {
	Range	range;

	range.start = 5;
	range.end = 15;

	// return OK if start and end is within bounds
	fail_unless( OK == range_check_bounds(range, 1, 20), NULL);
	fail_unless( OK == range_check_bounds(range, 5, 15), NULL);
	fail_unless( OK == range_check_bounds(range, 5, 20), NULL);
	fail_unless( OK == range_check_bounds(range, 1, 15), NULL);

	// return fail if start or end is outside of bounds
	fail_unless( FAIL == range_check_bounds(range, 1, 10), NULL);
	fail_unless( FAIL == range_check_bounds(range, 10, 20), NULL);
	fail_unless( FAIL == range_check_bounds(range, 6, 14), NULL);
	fail_unless( FAIL == range_check_bounds(range, 6, 20), NULL);
	fail_unless( FAIL == range_check_bounds(range, 1, 14), NULL);

	// test invalid range
	range.start = 15;
	range.end = 5;
	fail_unless( FAIL == range_check_bounds(range, 1, 20), NULL);
	fail_unless( FAIL == range_check_bounds(range, 5, 15), NULL);
	fail_unless( FAIL == range_check_bounds(range, 5, 20), NULL);
	fail_unless( FAIL == range_check_bounds(range, 1, 15), NULL);
} END_TEST


/**
 * Verify action functions for setting select work OK
 */
START_TEST(test_action_select) {
	ImageGroupRecord record = { FALSE, NULL };

	// verify set
	fail_unless( FALSE == record.select, NULL);
	fail_unless( OK == action_select_set(&record), NULL);
	fail_unless( TRUE == record.select, NULL);

	// verify unset
	fail_unless( OK == action_select_unset(&record), NULL);
	fail_unless( FALSE == record.select, NULL);

	// verify toggling
	fail_unless( OK == action_select_toggle(&record), NULL);
	fail_unless( TRUE == record.select, NULL);
	fail_unless( OK == action_select_toggle(&record), NULL);
	fail_unless( FALSE == record.select, NULL);
} END_TEST

/**
 * Check if condition function works correctly on select flag
 */
START_TEST(test_condition_select) {
	gboolean	result = TRUE;
	ImageGroupRecord record = { FALSE, NULL };

	// record.select = FALSE, check normal and inverted response
	fail_unless( OK == condition_select(&record, &result, NULL), NULL);
	fail_unless( FALSE == result, NULL);

	// record.select = TRUE, check normal and inverted response
	result = FALSE;
	record.select = TRUE;
	fail_unless( OK == condition_select(&record, &result, NULL), NULL);
	fail_unless( TRUE == result, NULL);
} END_TEST


/**
 * \brief Create test suite for group record.
 * \return pointer to accept set suite
 */
Suite* create_image_group_record_suite (void) {

  Suite *s = suite_create ("ImageGroupRecord");

  TCase *tc_range = tcase_create ("Range");
  tcase_add_checked_fixture (tc_range, common_setup, common_teardown);
  tcase_add_test (tc_range, test_range);

  TCase *tc_action = tcase_create ("Action");
  tcase_add_checked_fixture (tc_action, common_setup, common_teardown);
  tcase_add_test (tc_action, test_action_select);

  TCase *tc_condition = tcase_create ("Condition");
  tcase_add_checked_fixture (tc_condition, common_setup, common_teardown);
  tcase_add_test (tc_condition, test_condition_select);

  suite_add_tcase (s, tc_range);
  suite_add_tcase (s, tc_action);
  suite_add_tcase (s, tc_condition);

  return s;
}
