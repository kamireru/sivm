/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "src/backend/taglist.h"

#include "test/common/common.h"
#include "test/backend/test_taglist.h"

/**
 * \brief Check taglist creation and destruction
 */
START_TEST(test_create) {

	TagList* tglist = NULL;

	tglist = taglist_create();
	fail_if(  NULL == tglist, NULL );

	fail_if( OK != taglist_destroy(tglist), NULL );
	fail_if( error_is_error(), NULL );

} END_TEST


/**
 * \brief Check function for verifying valid tag characters
 */
START_TEST(test_is_tag) {

	// check valid tags
	fail_if( TRUE != taglist_is_tag('a'), NULL );
	fail_if( TRUE != taglist_is_tag('z'), NULL );
	fail_if( TRUE != taglist_is_tag('A'), NULL );
	fail_if( TRUE != taglist_is_tag('Z'), NULL );
	fail_if( TRUE != taglist_is_tag('0'), NULL );
	fail_if( TRUE != taglist_is_tag('9'), NULL );

	// check invalid tags
	fail_if( FALSE != taglist_is_tag(' '), NULL );
	fail_if( FALSE != taglist_is_tag('"'), NULL );
	fail_if( FALSE != taglist_is_tag('.'), NULL );
	fail_if( FALSE != taglist_is_tag('='), NULL );

} END_TEST

/**
 * Storage variable for holding pointer to taglist instance managed by setup and
 * teardown functions.
 */
static TagList* tag_list = NULL;

/**
 * Setup function allocating new taglist instance for unit tests.
 */
void taglist_setup(void) {

	COLOR_SETUP_START;

	tag_list = taglist_create();

	if ( error_is_error() ) {
		ERROR_LOG();
	}
	COLOR_SETUP_END;
	return;
}

/**
 * Teardown function deallocating taglist instance.
 */
void taglist_teardown(void) {

	COLOR_TEARDOWN_START;

	taglist_destroy(tag_list);
	if ( error_is_error() ) {
		ERROR_LOG();
	}
	COLOR_TEARDOWN_END;
	return;
}

/**
 * \brief Check insertion and removal in and from taglist
 *
 * The test case combines both insertion and removal test using simple use case.
 * Insert and then remove one tag and try to insert tags that are either invalid
 * or already present.
 */
START_TEST(test_content_simple) {

	// test addition
	fail_if( OK != taglist_insert(tag_list, 'a'), NULL );
	fail_if( 1 != tag_list->size, NULL );
	fail_if( 0 != strncmp(tag_list->list, "a", tag_list->size), NULL );
	fail_if( TAGLIST_MIN_INCREMENT != tag_list->max_size );

	// test addition of the same character
	fail_if( OK != taglist_insert(tag_list, 'a'), NULL );
	fail_if( 1 != tag_list->size, NULL );
	fail_if( 0 != strncmp(tag_list->list, "a", tag_list->size), NULL );

	// test addition of invalid tag character
	fail_if( FAIL != taglist_insert(tag_list, '='), NULL );
	fail_if( 1 != tag_list->size, NULL );
	fail_if( 0 != strncmp(tag_list->list, "a", tag_list->size), NULL );
	error_reset();


	// test removal of invalid character
	fail_if( OK != taglist_remove(tag_list, '='), NULL );
	fail_if( 1 != tag_list->size, NULL );
	fail_if( 0 != strncmp(tag_list->list, "a", tag_list->size), NULL );
	error_reset();

	// test removela of character that is not in list
	fail_if( OK != taglist_remove(tag_list, 'b'), NULL );
	fail_if( 1 != tag_list->size, NULL );
	fail_if( 0 != strncmp(tag_list->list, "a", tag_list->size), NULL );

	// test removal of the character
	fail_if( OK != taglist_remove(tag_list, 'a'), NULL );
	fail_if( 0 != tag_list->size, NULL );

} END_TEST

/**
 * \brief Test the way insert/remove reorganizes data in list
 *
 * The insert should always add at the end of the list and remove should remove
 * from the middle of list and fill the space with the element from list end.
 *
 * Note: The * character in list comparison is there to ensure that comparation
 * did not go beyont list limit (the * should never be compared due to list
 * size limit)
 */
START_TEST(test_content_structure) {

	fail_if( OK != taglist_insert(tag_list, 'a'), NULL );
	fail_if( OK != taglist_insert(tag_list, 'b'), NULL );
	fail_if( OK != taglist_insert(tag_list, 'c'), NULL );
	fail_if( OK != taglist_insert(tag_list, 'd'), NULL );
	fail_if( OK != taglist_insert(tag_list, 'e'), NULL );
	fail_if( 0 != strncmp(tag_list->list, "abcde*", tag_list->size), NULL );

	fail_if( OK != taglist_remove(tag_list, 'a'), NULL );
	fail_if( 0 != strncmp(tag_list->list, "ebcd*", tag_list->size), NULL );

	fail_if( OK != taglist_remove(tag_list, 'b'), NULL );
	fail_if( 0 != strncmp(tag_list->list, "edc*", tag_list->size), NULL );
} END_TEST

/**
 * \brief Check allocation and reallocation of list
 *
 * The list allocation should increase when the list is full and should increase
 * about 50 percent or minimum increase size.
 */
START_TEST(test_content_grow) {
	int i;

	// allocate min size after first addition
	fail_if( OK != taglist_insert(tag_list, 'a'), NULL );
	fail_if( TAGLIST_MIN_INCREMENT != tag_list->max_size, NULL );

	// allocate new min increment after space is filler
	for (i = 0; i < TAGLIST_MIN_INCREMENT; i++) {
		fail_if( OK != taglist_insert(tag_list, ('0' + i)), NULL );
	}
	fail_if( 2*TAGLIST_MIN_INCREMENT != tag_list->max_size, NULL );

	// third allocation is again min increment size
	for (i = 0; i < TAGLIST_MIN_INCREMENT; i++) {
		fail_if( OK != taglist_insert(tag_list, ('b' + i)), NULL );
	}
	fail_if( 3*TAGLIST_MIN_INCREMENT != tag_list->max_size, NULL );

	// fourth allocation is 150% of minimum increment
	for (i = 0; i < TAGLIST_MIN_INCREMENT; i++) {
		fail_if( OK != taglist_insert(tag_list, ('A' + i)), NULL );
	}
	fail_if( 4.5*TAGLIST_MIN_INCREMENT != tag_list->max_size, NULL );

} END_TEST

/**
 * \brief Check allocation and reallocation of list
 *
 * The allocation should decrese when half of the list is empty and shall
 * decrease about half of the empty space.
 *
 * The list size should not decrease below minimum increament size.
 */
START_TEST(test_content_shrink) {
	int i;

	// continue to add elements to allocate twice the minumum increment size
	fail_if( OK != taglist_insert(tag_list, 'a'), NULL );
	fail_if( TAGLIST_MIN_INCREMENT != tag_list->max_size );

	for (i = 0; i < TAGLIST_MIN_INCREMENT; i++) {
		fail_if( OK != taglist_insert(tag_list, ('b' + i)), NULL );
	}
	fail_if( 2*TAGLIST_MIN_INCREMENT != tag_list->max_size, NULL );
	fail_if( TAGLIST_MIN_INCREMENT+1 != tag_list->size, NULL );

	// remove element to decrese tag count below threshold
	fail_if( OK != taglist_remove(tag_list, 'a'), NULL );

	fail_if( 1.5*TAGLIST_MIN_INCREMENT != tag_list->max_size, NULL );
	fail_if( TAGLIST_MIN_INCREMENT != tag_list->size, NULL );

	// remove more elements to get below treshold that would decrese list size
	// under the min increment size
	for (i = 0; i < TAGLIST_MIN_INCREMENT - 1 ; i++) {
		fail_if( OK != taglist_remove(tag_list, ('b' + i)), NULL );
	}
	fail_if( 1 != tag_list->size, NULL );
	fail_if( TAGLIST_MIN_INCREMENT != tag_list->max_size, NULL );

} END_TEST

/**
 * \brief Check if contains test works correctly.
 */
START_TEST(test_content_contains) {

	// check empty list ( NULL pointer in taglist->list)
	fail_if( FALSE != taglist_contains(tag_list, 'a'), NULL );

	// fill in some data
	fail_if( OK != taglist_insert(tag_list, 'a'), NULL );
	fail_if( OK != taglist_insert(tag_list, 'b'), NULL );
	fail_if( OK != taglist_insert(tag_list, 'c'), NULL );
	fail_if( 0 != strncmp(tag_list->list, "abc*", tag_list->size), NULL );

	// check for invalid tag
	fail_if( FALSE != taglist_contains(tag_list, '*'), NULL );

	// check for present tag
	fail_if( OK != taglist_contains(tag_list, 'a'), NULL );
	fail_if( OK != taglist_contains(tag_list, 'b'), NULL );
	fail_if( OK != taglist_contains(tag_list, 'c'), NULL );

	// remove all content
	fail_if( OK != taglist_remove(tag_list, 'a'), NULL );
	fail_if( OK != taglist_remove(tag_list, 'b'), NULL );
	fail_if( OK != taglist_remove(tag_list, 'c'), NULL );

	// check empty list (taglist->list is not NULL)
	fail_if( FALSE != taglist_contains(tag_list, 'a'), NULL );

} END_TEST

/**
 * \brief Create test suite for taglist.
 * \return pointer to accept set suite
 */
Suite* create_taglist_suite (void) {
	Suite *s = suite_create ("Taglist");

	TCase *tc_manage = tcase_create ("Manage");
	tcase_add_checked_fixture (tc_manage, common_setup, common_teardown);
	tcase_add_test (tc_manage, test_create);
	tcase_add_test (tc_manage, test_is_tag);

	TCase *tc_use = tcase_create ("Usage");
	tcase_add_checked_fixture (tc_use, taglist_setup, taglist_teardown);
	tcase_add_test (tc_use, test_content_simple);
	tcase_add_test (tc_use, test_content_structure);
	tcase_add_test (tc_use, test_content_grow);
	tcase_add_test (tc_use, test_content_shrink);
	tcase_add_test (tc_use, test_content_contains);

	suite_add_tcase (s, tc_manage);
	suite_add_tcase (s, tc_use);

	return s;
}
