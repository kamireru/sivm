/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <string.h>
#include <malloc.h>
#include "src/common/error.h"
#include "src/backend/image_group.h"
#include "src/backend/image_group_record.h"
#include "src/backend/image_group_buffer.h"

#include "test/backend/test_image_group.h"
#include "test/common/common.h"

/**
 * \brief Array with pointers to images for group creation.
 *
 * Array of image pointers that is used for fast creation of image group
 * content.
 */
static Image* pointer_list[10] = { NULL };

/**
 * \brief The pointer to image group that is used through tests.
 *
 * The variable has to be global in the module to have access to it in all tests
 * and setup() & teardown() functions.
 */
static ImageGroup* image_group = NULL;

/**
 * \brief Verify image group instance creation
 */
START_TEST(test_create) {
	ImageGroup* test_group = group_create("test_name", 't');

	ck_assert( NULL != test_group );
	ck_assert( NULL != test_group->list );
	ck_assert_int_eq( 0, group_get_size(test_group) );

	ck_assert( 't' == test_group->tag );
	ck_assert_str_eq( "test_name", test_group->name );

	ck_assert( OK == group_destroy(test_group) );
	ck_assert( ! error_is_error() );
} END_TEST

/**
 * \brief Verify insertion of images into empty list
 *
 * Insert images into empty list and verify its position in the image group
 * arrays is correct. Verify that the image reference counters are handled
 * correctly.
 *
 * The number of references is 2 at the start of the test (1 reference is held
 * by pointer_list array and another by static image_group).
 */
START_TEST(test_insert_image_simple) {
	Image*		test_data[3] = { pointer_list[0], pointer_list[1], pointer_list[2] };
	ImageGroup *test_group = group_create("test_name", 't');
	int i;

	// check inserting three images into empty image file
	ck_assert( OK == group_insert(test_group, 0, test_data, 3) );
	ck_assert_int_eq( 3, group_get_size(test_group) );

	for (i = 0; i < 3; i++ ) {
		Image* image = group_get_image(test_group, i);

		ck_assert_int_eq( 3, image->ref );
		ck_assert_str_eq( test_data[i]->name, image->name );
		// the image is also in the 'general' group created in setup function
		ck_assert_int_eq( 2, taglist_get_count(image->group));
		ck_assert_int_eq( TRUE, taglist_contains(image->group, 't') );
	}

	// destroy image list
	ck_assert( OK == group_destroy(test_group) );
	ck_assert( ! error_is_error() );

	// check reference count on the images has droped and they are no longer in
	// group
	for (i = 0; i < 3; i++ ) {
		ck_assert_int_eq( 2, test_data[i]->ref );
		ck_assert_int_eq( 1, taglist_get_count(test_data[i]->group));
		ck_assert_int_eq( FALSE, taglist_contains(test_data[i]->group, 't') );
	}
} END_TEST


/**
 * \brief Verify insertion of images into non-empty list
 *
 * Insert from one to three images into various positions in the image group
 * array and verify the insertion was done to correct place. Verify that the
 * image reference counters are handled correctly.
 */
START_TEST(test_insert_image_complex) {
	Image*		test_data_1[1] = { pointer_list[0] };
	Image*		test_data_2[3] = { pointer_list[1], pointer_list[2], pointer_list[3] };
	Image*		test_data_3[2] = { pointer_list[4], pointer_list[5] };
	Image*		test_data_4[1] = { pointer_list[6] };

	ImageGroup* test_group = group_create("test_name", 't');
	ck_assert( NULL != test_group );
	ck_assert( NULL != test_group->list );
	ck_assert_int_eq( 0, group_get_size(test_group) );

	// initial insertion of 3 images into list
	ck_assert( OK == group_insert(test_group, 0, test_data_2, 3) );
	ck_assert_int_eq( 3, group_get_size(test_group) );
	ck_assert_int_eq( 3, test_data_2[0]->ref );
	ck_assert_int_eq( 3, test_data_2[1]->ref );
	ck_assert_int_eq( 3, test_data_2[2]->ref );
	ck_assert_str_eq( test_data_2[0]->name, group_get_image(test_group, 0)->name );
	ck_assert_str_eq( test_data_2[1]->name, group_get_image(test_group, 1)->name );
	ck_assert_str_eq( test_data_2[2]->name, group_get_image(test_group, 2)->name );

	// insert one image at the beginning of image list
	ck_assert( OK == group_insert(test_group, 0, test_data_1, 1) );
	ck_assert_int_eq( 4, group_get_size(test_group) );
	ck_assert_int_eq( 3, test_data_1[0]->ref );
	ck_assert_str_eq( test_data_1[0]->name, group_get_image(test_group, 0)->name );
	ck_assert_str_eq( test_data_2[0]->name, group_get_image(test_group, 1)->name );

	// insert one image at the end of image list
	ck_assert( OK == group_insert(test_group, 4, test_data_4, 1) );
	ck_assert_int_eq( 5, group_get_size(test_group) );
	ck_assert_int_eq( 3, test_data_4[0]->ref );
	ck_assert_str_eq( test_data_4[0]->name, group_get_image(test_group, 4)->name );
	ck_assert_str_eq( test_data_2[2]->name, group_get_image(test_group, 3)->name );

	// insert two images in the middle of image list
	ck_assert( OK == group_insert(test_group,2, test_data_3, 2) );
	ck_assert_int_eq( 7, group_get_size(test_group) );
	ck_assert_int_eq( 3, test_data_3[0]->ref );
	ck_assert_int_eq( 3, test_data_3[1]->ref );
	ck_assert_str_eq( test_data_2[0]->name, group_get_image(test_group, 1)->name );
	ck_assert_str_eq( test_data_3[0]->name, group_get_image(test_group, 2)->name );
	ck_assert_str_eq( test_data_3[1]->name, group_get_image(test_group, 3)->name );
	ck_assert_str_eq( test_data_2[1]->name, group_get_image(test_group, 4)->name );

	// destroy image list
	ck_assert( OK == group_destroy(test_group) );
	ck_assert( ! error_is_error() );

	// check references of selected image pointers
	ck_assert_int_eq( 2, test_data_1[0]->ref );
	ck_assert_int_eq( 2, test_data_2[0]->ref );
	ck_assert_int_eq( 2, test_data_2[1]->ref );
	ck_assert_int_eq( 2, test_data_2[2]->ref );
	ck_assert_int_eq( 2, test_data_3[0]->ref );
	ck_assert_int_eq( 2, test_data_3[1]->ref );
} END_TEST

/**
 * \brief Verify that it is not possible to insert one image twice
 */
START_TEST(test_insert_image_double) {
	Image*		test_data_1[4] = { pointer_list[0], pointer_list[1], pointer_list[2], pointer_list[3] };
	Image*		test_data_2[4] = { pointer_list[1], pointer_list[4], pointer_list[2], pointer_list[5] };

	ImageGroup* test_group = group_create("test_name", 't');
	ck_assert_int_eq( 0, group_get_size(test_group) );

	// initial insertion of 3 images into list
	ck_assert( OK == group_insert(test_group, 0, test_data_1, 4) );
	ck_assert_int_eq( 4, group_get_size(test_group) );
	ck_assert_str_eq( test_data_1[0]->name, group_get_image(test_group, 0)->name );
	ck_assert_str_eq( test_data_1[1]->name, group_get_image(test_group, 1)->name );
	ck_assert_str_eq( test_data_1[2]->name, group_get_image(test_group, 2)->name );
	ck_assert_str_eq( test_data_1[3]->name, group_get_image(test_group, 3)->name );

	// insert two images at the end of image list
	ck_assert( OK == group_insert(test_group, 4, test_data_2, 4) );
	ck_assert_int_eq( 6, group_get_size(test_group) );
	ck_assert_str_eq( test_data_2[1]->name, group_get_image(test_group, 4)->name );
	ck_assert_str_eq( test_data_2[3]->name, group_get_image(test_group, 5)->name );

	ck_assert( OK == group_destroy(test_group) );
	ck_assert( ! error_is_error() );
} END_TEST


/**
 * \brief Verify yanking of the image range
 *
 * Yank range of the images into the yank buffer and check yank buffer
 * clearing.
 *
 * Verify that the image reference counters are handled correctly
 * when image is added to yank buffer and removed from yank buffer.
 *
 * Verify that image group tags are updated correctly during yanking.
 */
START_TEST(test_yank_range) {
	Range rng = { 2, 5 };
	int i;

	// yank some data from the middle of group and do initial yank buffer
	// allocation
	ck_assert( OK == group_yank_range(image_group, rng) );
	ck_assert_int_eq( 4, group_buffer_size() );
	ck_assert_int_eq( 4, group_buffer_max_size() );
	for (i = 0; i < 4; i++) {
		ck_assert_str_eq( pointer_list[rng.start+i]->name, group_yank_buffer[i]->name );
		ck_assert_int_eq( 3, pointer_list[rng.start+i]->ref );

		ck_assert_int_eq( 2, taglist_get_count(group_yank_buffer[i]->group));
		ck_assert_int_eq( TRUE, taglist_contains(group_yank_buffer[i]->group, 'G') );
		ck_assert_int_eq( TRUE, taglist_contains(group_yank_buffer[i]->group, '*') );
	}

	// test explicit yank buffer clear
	ck_assert( OK == group_buffer_clear() );

	for (i = 0; i < 4; i++) {
		ck_assert_int_eq( 2, pointer_list[rng.start+i]->ref );

		ck_assert_int_eq( 1, taglist_get_count(pointer_list[rng.start+i]->group));
		ck_assert_int_eq( TRUE, taglist_contains(pointer_list[rng.start+i]->group, 'G') );
		ck_assert_int_eq( FALSE, taglist_contains(pointer_list[rng.start+i]->group, '*') );
	}
	ck_assert_int_eq( 0, group_buffer_size() );
	ck_assert_int_eq( 4, group_buffer_max_size() );

	// test implicit yank buffer clear (by not clearing it manually

	// yank from 0 index and do not resize yank buffer
	rng.start = 0;
	rng.end = 2;

	ck_assert( OK == group_yank_range(image_group, rng) );
	ck_assert_str_eq( pointer_list[0]->name, group_yank_buffer[0]->name );
	ck_assert_str_eq( pointer_list[1]->name, group_yank_buffer[1]->name );
	ck_assert_str_eq( pointer_list[2]->name, group_yank_buffer[2]->name );
	ck_assert_int_eq( 3, pointer_list[0]->ref );
	ck_assert_int_eq( 3, pointer_list[1]->ref );
	ck_assert_int_eq( 3, pointer_list[2]->ref );
	ck_assert_int_eq( 3, group_buffer_size() );
	ck_assert_int_eq( 4, group_buffer_max_size() );

	// yank more than is current yank buffer size and reallocate it
	rng.start = 5;
	rng.end = 9;

	ck_assert( OK == group_yank_range(image_group, rng) );
	ck_assert_str_eq( pointer_list[5]->name, group_yank_buffer[0]->name );
	ck_assert_str_eq( pointer_list[6]->name, group_yank_buffer[1]->name );
	ck_assert_str_eq( pointer_list[7]->name, group_yank_buffer[2]->name );
	ck_assert_str_eq( pointer_list[8]->name, group_yank_buffer[3]->name );
	ck_assert_str_eq( pointer_list[9]->name, group_yank_buffer[4]->name );
	ck_assert_int_eq( 5, group_buffer_size() );
	ck_assert_int_eq( 5, group_buffer_max_size() );

	ck_assert( OK == group_buffer_clear() );
	ck_assert( NULL == group_yank_buffer[0] );
	ck_assert_int_eq( 0, group_buffer_size() );
	ck_assert_int_eq( 5, group_buffer_max_size() );

} END_TEST

/**
 * \brief Verify range pasting works correctly
 *
 * Yank and paste some image pointers from one group and paste it to another
 * group. Verify the image pointer references are handled correctly.
 */
START_TEST(test_yank_paste) {
	Range rng;

	ImageGroup* test_group = group_create("test_name", 't');
	ck_assert_int_eq( 0, group_get_size(test_group) );

	// yank some data from the middle of group and do initial yank buffer
	// allocation
	rng.start = 2;
	rng.end = 4;

	ck_assert( OK == group_yank_range(image_group, rng) );
	ck_assert_str_eq( pointer_list[2]->name, group_yank_buffer[0]->name );
	ck_assert_str_eq( pointer_list[3]->name, group_yank_buffer[1]->name );
	ck_assert_str_eq( pointer_list[4]->name, group_yank_buffer[2]->name );
	ck_assert_int_eq( 3, pointer_list[2]->ref );
	ck_assert_int_eq( 3, pointer_list[3]->ref );
	ck_assert_int_eq( 3, pointer_list[4]->ref );
	ck_assert_int_eq( 3, group_buffer_size() );
	ck_assert_int_eq( 3, group_buffer_max_size() );

	// paste from yank buffer
	ck_assert( OK == group_yank_paste(test_group, 0) );
	ck_assert_str_eq( pointer_list[2]->name, group_get_image(test_group, 0)->name );
	ck_assert_str_eq( pointer_list[3]->name, group_get_image(test_group, 1)->name );
	ck_assert_str_eq( pointer_list[4]->name, group_get_image(test_group, 2)->name );
	ck_assert_int_eq( 4, pointer_list[2]->ref );
	ck_assert_int_eq( 4, pointer_list[3]->ref );
	ck_assert_int_eq( 4, pointer_list[4]->ref );
	ck_assert_int_eq( 3, group_get_size(test_group) );

	ck_assert( OK == group_destroy(test_group) );
	ck_assert( ! error_is_error() );
} END_TEST


/**
 * \brief Verify deletion of image on given index
 *
 * Remove one image pointer from the middle of the image group array and
 * verify the image references are handled correctly.
 */
START_TEST(test_delete_index) {

	ck_assert( OK == group_delete_index(image_group, 2) );
	ck_assert_str_eq( pointer_list[0]->name, group_get_image(image_group, 0)->name );
	ck_assert_str_eq( pointer_list[1]->name, group_get_image(image_group, 1)->name );
	ck_assert_str_eq( pointer_list[3]->name, group_get_image(image_group, 2)->name );
	ck_assert_int_eq( 2, pointer_list[0]->ref );
	ck_assert_int_eq( 2, pointer_list[1]->ref );
	ck_assert_int_eq( 1, pointer_list[2]->ref );
	ck_assert_int_eq( 2, pointer_list[3]->ref );
	ck_assert_int_eq( 9, group_get_size(image_group) );

} END_TEST
/**
 * \brief Verify range deletion works correctly
 *
 * Remove some image pointers from the middle of the image group array and
 * verify the image references are handled correctly.
 */
START_TEST(test_delete_range) {
	Range rng;
	// delete data from image group
	rng.start = 2;
	rng.end = 6;

	ck_assert( OK == group_delete_range(image_group, rng) );
	ck_assert_str_eq( pointer_list[0]->name, group_get_image(image_group, 0)->name );
	ck_assert_str_eq( pointer_list[1]->name, group_get_image(image_group, 1)->name );
	ck_assert_str_eq( pointer_list[7]->name, group_get_image(image_group, 2)->name );
	ck_assert_str_eq( pointer_list[8]->name, group_get_image(image_group, 3)->name );
	ck_assert_str_eq( pointer_list[9]->name, group_get_image(image_group, 4)->name );
	ck_assert_int_eq( 1, pointer_list[2]->ref );
	ck_assert_int_eq( 1, pointer_list[3]->ref );
	ck_assert_int_eq( 1, pointer_list[4]->ref );
	ck_assert_int_eq( 1, pointer_list[5]->ref );
	ck_assert_int_eq( 1, pointer_list[6]->ref );
	ck_assert_int_eq( 5, group_get_size(image_group) );

} END_TEST

/**
 * \brief Verify internal buffer is destroyed properly
 *
 * The main aim of the test is to verify that the images in the yank buffer are
 * unreferenced. Ie, that the buffer is cleared before it is destroyed.
 *
 * The second test is to check that calling destroy on already destroyed buffer
 * won't break things.
 */
START_TEST(test_destroy_buffer) {
	Range rng;

	// yank some images from the buffer
	rng.start = 0;
	rng.end = 1;

	ck_assert( OK == group_yank_range(image_group, rng) );
	ck_assert_str_eq( pointer_list[0]->name, group_yank_buffer[0]->name );
	ck_assert_str_eq( pointer_list[1]->name, group_yank_buffer[1]->name );
	ck_assert_int_eq( 3, pointer_list[0]->ref );
	ck_assert_int_eq( 3, pointer_list[1]->ref );

	// call buffer destroy and check image reference count
	ck_assert( OK == group_buffer_destroy() );
	ck_assert_int_eq( 2, pointer_list[0]->ref );
	ck_assert_int_eq( 2, pointer_list[1]->ref );

	// call destroy on the buffer once again
	ck_assert( OK == group_buffer_destroy() );

} END_TEST

/**
 * \brief Check function execution over index works
 *
 * The test uses action_select_* function for testing the action function and
 * condition function execution over index.
 */
START_TEST(test_execute_index) {
	gboolean result = FALSE;

	// set two images as select
	fail_unless( OK == group_execute_index(image_group, 1, action_select_set), NULL);
	fail_unless( OK == group_execute_index(image_group, 2, action_select_set), NULL);

	result = FALSE;
	fail_unless( OK == group_get_condition(image_group, 1, &result, condition_select, NULL), NULL);
	fail_unless( TRUE == result, NULL);

	result = FALSE;
	fail_unless( OK == group_get_condition(image_group, 2, &result, condition_select, NULL), NULL);
	fail_unless( TRUE == result, NULL);

	// set one of them as not select and toggle second one
	fail_unless( OK == group_execute_index(image_group, 1, action_select_unset), NULL);
	fail_unless( OK == group_execute_index(image_group, 2, action_select_toggle), NULL);

	result = TRUE;
	fail_unless( OK == group_get_condition(image_group, 1, &result, condition_select, NULL), NULL);
	fail_unless( FALSE == result, NULL);

	result = TRUE;
	fail_unless( OK == group_get_condition(image_group, 2, &result, condition_select, NULL), NULL);
	fail_unless( FALSE == result, NULL);

	result = FALSE;
	fail_unless( OK == group_execute_index(image_group, 3, action_select_toggle), NULL);
	fail_unless( OK == group_get_condition(image_group, 3, &result, condition_select, NULL), NULL);
	fail_unless( TRUE == result, NULL);
} END_TEST


/**
 * \brief Check function execution over index range
 *
 * The test uses action_select_* function for testing the action function and
 * condition function execution over index range.
 */
START_TEST(test_execute_range) {
	Range range;
	gboolean result;

	// set index range to use
	range.start = 0;
	range.end = 4;

	fail_unless( OK == group_execute_range(image_group, range,  action_select_set), NULL);
	fail_unless( OK == group_get_condition(image_group, 0, &result, condition_select, NULL),  NULL);
	fail_unless( TRUE == result, NULL);
	fail_unless( OK == group_get_condition(image_group, 1, &result, condition_select, NULL),  NULL);
	fail_unless( TRUE == result, NULL);
	fail_unless( OK == group_get_condition(image_group, 2, &result, condition_select, NULL),  NULL);
	fail_unless( TRUE == result, NULL);
	fail_unless( OK == group_get_condition(image_group, 3, &result, condition_select, NULL),  NULL);
	fail_unless( TRUE == result, NULL);
	fail_unless( OK == group_get_condition(image_group, 4, &result, condition_select, NULL),  NULL);
	fail_unless( TRUE == result, NULL);

	fail_unless( OK == group_get_condition(image_group, 5, &result, condition_select, NULL),  NULL);
	fail_unless( FALSE == result, NULL);
	fail_unless( OK == group_get_condition(image_group, 6, &result, condition_select, NULL),  NULL);
	fail_unless( FALSE == result, NULL);
	fail_unless( OK == group_get_condition(image_group, 7, &result, condition_select, NULL),  NULL);
	fail_unless( FALSE == result, NULL);
	fail_unless( OK == group_get_condition(image_group, 8, &result, condition_select, NULL),  NULL);
	fail_unless( FALSE == result, NULL);
	fail_unless( OK == group_get_condition(image_group, 9, &result, condition_select, NULL),  NULL);
	fail_unless( FALSE == result, NULL);

} END_TEST


/**
 * \brief Verify action function execution over selected images
 *
 * \todo This test is hard with just one one condition/flag to set and test.
 * Update it so that we can test using different flags once there is more than
 * select flag.
 */
START_TEST(test_execute_select) {
	gboolean result;
	int indices[5] = { 2, 4, 5, 7, 9 };
	int i;

	// prepare some selection (it also tests another action funtion used on index)
	for (i = 0; i < 5; i++ ) {
		fail_unless(
				OK == group_execute_index(image_group, indices[i], action_select_set),
				"Assertion 'OK == group_execute_index(image_group, %d, action_select_set)' failed",
				indices[i]
			);
	}

	// set all selected records as not selected
	fail_unless( OK == group_execute_select(image_group,  action_select_unset), NULL);

	// verify setting was done correctly
	for( i = 0; i < 5; i++ ) {
		result = FALSE;
		fail_unless(
				OK == group_get_condition(image_group, indices[i], &result, condition_select, NULL),
				"Assertion 'OK == group_get_condition(image_group, %d, &result, condition_select, NULL)' failed",
				indices[i]
			);
		fail_unless( FALSE == result, NULL);
	}
} END_TEST


/**
 * \brief Setup structures before test.
 *
 * Create test image group and store its pointer to static variable. Allocate
 * image structures for the test and insert them into test group.
 */
static void group_setup(void) {
	char image_number = '0';
	char image_name[10] = "image 0";
	int i;
	COLOR_SETUP_START;

	// create temporary image structures
	for (i = 0; i < 10; i++) {
		image_name[6] = image_number + i;
		pointer_list[i] = image_create(image_name);
		if ( error_is_error() ) ERROR_LOG();
	}

	// create group with 10 elements for next testing
	image_group = group_create("general", 'G');
	group_insert(image_group, 0, pointer_list, 10);

	if ( error_is_error() ) ERROR_LOG();
	COLOR_SETUP_END;
	return ;
}


/**
 * \brief Tear down test related structures after the test finished.
 *
 * Deallocate test group and all test image structures.
 */
static void group_teardown(void) {
	int i;
	COLOR_TEARDOWN_START;

	// destroy test image group and its content
	group_destroy(image_group);
	group_buffer_destroy();

	if ( error_is_error() ) ERROR_LOG();

	// destroy temporary image structures
	for (i = 0; i < 10; i++) {
		image_unref(pointer_list[i]);
		pointer_list[i] = NULL;
		if ( error_is_error() ) ERROR_LOG();
	}
	COLOR_TEARDOWN_END;
	return ;
}

/**
 * \brief Create test suite for image group.
 * \return pointer to accept set suite
 */
Suite* create_image_group_suite (void) {
	Suite *s = suite_create ("ImageGroup");

	TCase *tc_manage = tcase_create ("Manage");
	tcase_add_checked_fixture (tc_manage, group_setup, group_teardown);
	tcase_add_test (tc_manage, test_create);
	tcase_add_test (tc_manage, test_insert_image_simple);
	tcase_add_test (tc_manage, test_insert_image_complex);
	tcase_add_test (tc_manage, test_insert_image_double);

	TCase *tc_shuffle = tcase_create("Shuffle");
	tcase_add_checked_fixture (tc_shuffle, group_setup, group_teardown);
	tcase_add_test (tc_shuffle, test_yank_range);
	/** \todo tcase_add_test (tc_shuffle, test_yank_select); */
	tcase_add_test (tc_shuffle, test_yank_paste);
	tcase_add_test (tc_shuffle, test_delete_index);
	tcase_add_test (tc_shuffle, test_delete_range);
	/** \todo tcase_add_test (tc_shuffle, test_delete_select); */
	tcase_add_test (tc_shuffle, test_destroy_buffer );

	TCase *tc_filter = tcase_create("Filter");
	tcase_add_checked_fixture (tc_filter, group_setup, group_teardown);
	tcase_add_test (tc_filter, test_execute_index);
	tcase_add_test (tc_filter, test_execute_range);
	tcase_add_test (tc_filter, test_execute_select);

	suite_add_tcase (s, tc_manage);
	suite_add_tcase (s, tc_shuffle);
	suite_add_tcase (s, tc_filter);

	return s;
}

