/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <check.h>

#include "test/fa/test_fa_accept_set.h"
#include "test/fa/test_fa_transition.h"
#include "test/fa/test_fa_node.h"
#include "test/fa/test_fa_automaton.h"
#include "test/fa/test_fa_runner.h"

/**
 * \brief Unit test suite main method.
 *
 * Create test suite runner and add test suites it should run. Use environment
 * variable to control how the suites will be run (forked/no forked mode).
 *
 * This suite is not only for generic view test suite, it is used for all view
 * related test suites (something like mini master suite for view related
 * functionality)
 *
 * \param argc number of parameters to program
 * \param argv array of parameters to program
 * \return number of failed suites
 */
int main(int argc, char** argv)
{
	int number_failed = 0;

	SRunner *suite_runner = srunner_create( create_fa_accept_set_suite() );
	srunner_add_suite( suite_runner, create_fa_transition_suite() );
	srunner_add_suite( suite_runner, create_fa_node_suite() );
	srunner_add_suite( suite_runner, create_fa_automaton_suite() );
	srunner_add_suite( suite_runner, create_fa_runner_suite() );

	srunner_run_all(suite_runner, CK_ENV);
	number_failed = srunner_ntests_failed(suite_runner);

	srunner_free(suite_runner);

	return ( 0 == number_failed ) ? EXIT_SUCCESS : EXIT_FAILURE;
}
