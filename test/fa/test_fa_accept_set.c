/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup fa_test
 * \{
 *
 * \file test_fa_accept_set.c
 */

#include "src/fa/fa_common.h"
#include "src/fa/fa_accept_set.h"

#include "test/fa/test_fa_common.h"
#include "test/fa/test_fa_accept_set.h"

/**
 * \brief Test if constructor and destructor functions of accept set works
 * correctly.
 *
 * This function check creation of FA_SET_RANGE with two data pointers
 */
START_TEST(test_manage_range) {

	FaAcceptSet *set = fa_set_create(FA_SET_RANGE, VALUE_PTR(0), VALUE_PTR(1));

	fail_if( NULL == set, NULL );
	fail_if( FA_SET_RANGE != set->type, NULL );
	fail_if( FA_FALSE != set->is_normalized, NULL );

	fail_if( 2 != set->size, NULL );

	fail_if( VALUE_PTR(0) != set->data[0], NULL );
	fail_if( VALUE_PTR(1) != set->data[1], NULL );

	fail_if( VALUE(0) != *((int*)set->data[0]), NULL );
	fail_if( VALUE(1) != *((int*)set->data[1]), NULL );

	fa_set_destroy( set );
} END_TEST

/**
 * \brief Test if constructor and destructor functions of accept set works
 * correctly.
 *
 * This function check creation of FA_SET_LIST with more than two data pointers.
 */
START_TEST(test_manage_list) {

	FaAcceptSet *set = fa_set_create(FA_SET_LIST, 3,
			VALUE_PTR(0), VALUE_PTR(1), VALUE_PTR(2));

	fail_if( NULL == set, NULL );
	fail_if( FA_SET_LIST != set->type, NULL );
	fail_if( FA_FALSE != set->is_normalized, NULL );

	fail_if( 3 != set->size, NULL );

	fail_if( VALUE_PTR(0) != set->data[0], NULL );
	fail_if( VALUE_PTR(1) != set->data[1], NULL );
	fail_if( VALUE_PTR(2) != set->data[2], NULL );

	fail_if( VALUE(0) != *((int*)set->data[0]), NULL );
	fail_if( VALUE(1) != *((int*)set->data[1]), NULL );
	fail_if( VALUE(2) != *((int*)set->data[2]), NULL );

	fa_set_destroy( set );
} END_TEST

/**
 * \brief Test creation of empty set of the list type.
 *
 * Creation of such set is not allowed, make sure that it is not possible.
 */
START_TEST(test_manage_list_empty) {

	FaAcceptSet *set = fa_set_create(FA_SET_LIST, 0);

	fail_if( NULL != set, NULL );

} END_TEST

/**
 * \brief Test contain check for list type.
 *
 * The test data contain normalized list (it is usable for both cases) and both
 * normalized and non-normalized search alorithm is used.
 *
 * In contrary to range testing where having non-normalized data is necessary
 * for correct testing, here we can use normalized list for both algorithms.
 */
START_TEST(test_contain_list) {

	FaAcceptSet *set = fa_set_create(FA_SET_LIST, 4,
			VALUE_PTR(0), VALUE_PTR(2), VALUE_PTR(4), VALUE_PTR(6));
	set->is_normalized = FA_FALSE;

	fail_if( FA_TRUE  != fa_set_contain(set, comparator, VALUE_PTR(4)), NULL );
	fail_if( FA_FALSE != fa_set_contain(set, comparator, VALUE_PTR(5)), NULL );

	set->is_normalized = FA_TRUE;
	fail_if( FA_TRUE  != fa_set_contain(set, comparator, VALUE_PTR(4)), NULL );
	fail_if( FA_FALSE != fa_set_contain(set, comparator, VALUE_PTR(5)), NULL );
	fail_if( FA_FALSE != fa_set_contain(set, comparator, NULL), NULL );

	fa_set_destroy( set );

} END_TEST

/**
 * \brief Test contain check for list type for single value accept set.
 *
 * Test both normalized and non-normalized algorithms on accept set that
 * contains only one element.
 */
START_TEST(test_contain_list_single) {

	FaAcceptSet *set = fa_set_create(FA_SET_LIST, 1, VALUE_PTR(4));
	set->is_normalized = FA_FALSE;

	fail_if( FA_TRUE  != fa_set_contain(set, comparator, VALUE_PTR(4)), NULL );
	fail_if( FA_FALSE != fa_set_contain(set, comparator, VALUE_PTR(5)), NULL );

	set->is_normalized = FA_TRUE;
	fail_if( FA_TRUE  != fa_set_contain(set, comparator, VALUE_PTR(4)), NULL );
	fail_if( FA_FALSE != fa_set_contain(set, comparator, VALUE_PTR(5)), NULL );
	fail_if( FA_FALSE != fa_set_contain(set, comparator, NULL), NULL );

	fa_set_destroy( set );

} END_TEST

/**
 * \brief Test contain check for range using non-normalized range.
 *
 * Test both lookup of value that is contained in list and that is not contained
 * in range. Test range that is not normalized.
 */
START_TEST(test_contain_range) {

	FaAcceptSet *set = fa_set_create(FA_SET_RANGE, VALUE_PTR(6), VALUE_PTR(2));

	fail_if( FA_TRUE != fa_set_contain(set, comparator, VALUE_PTR(4)), NULL );

	fail_if( FA_FALSE != fa_set_contain(set, comparator, VALUE_PTR(1)), NULL );
	fail_if( FA_FALSE != fa_set_contain(set, comparator, NULL), NULL );

	fa_set_destroy( set );

} END_TEST

/**
 * \brief Test contain check for range using normalized range.
 *
 * Test both lookup of value that is contained in list and that is not contained
 * in range. Test range that is normalized.
 */
START_TEST(test_contain_range_normalized) {

	FaBoolean result;

	FaAcceptSet *set = fa_set_create(FA_SET_RANGE, VALUE_PTR(2), VALUE_PTR(6));
	// manually set normalized to true (preset accept set is in normalized state);
	set->is_normalized = FA_TRUE;

	result = fa_set_contain(set, comparator, VALUE_PTR(4));
	fail_if( FA_TRUE != result, NULL );

	result = fa_set_contain(set, comparator, VALUE_PTR(1));
	fail_if( FA_TRUE == result, NULL );

	fa_set_destroy( set );

} END_TEST

/**
 * \brief Test result of contain check when input is invalid.
 *
 * The invalid input consists of NULL as set or comparator pointer.
 */
START_TEST(test_contain_invalid) {

	FaAcceptSet *set = fa_set_create(FA_SET_RANGE, VALUE_PTR(2), VALUE_PTR(6));

	FaBoolean result = fa_set_contain(NULL, comparator, VALUE_PTR(4));
	fail_if( FA_FALSE != result, NULL );

	result = fa_set_contain(set, NULL, VALUE_PTR(4));
	fail_if( FA_FALSE != result, NULL );

	fa_set_destroy( set );

} END_TEST

/**
 * \brief Create test suite for fa accept set.
 * \return pointer to accept set suite
 */
Suite* create_fa_accept_set_suite (void) {

	Suite *suite = suite_create("FaAcceptSet");

	TCase *tc_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_manage, NULL, NULL);
	tcase_add_test(tc_manage, test_manage_range);
	tcase_add_test(tc_manage, test_manage_list);
	tcase_add_test(tc_manage, test_manage_list_empty);


	TCase *tc_contain = tcase_create("Contain");
	tcase_add_checked_fixture(tc_contain, NULL, NULL);
	tcase_add_test(tc_contain, test_contain_range);
	tcase_add_test(tc_contain, test_contain_range_normalized);
	tcase_add_test(tc_contain, test_contain_list);
	tcase_add_test(tc_contain, test_contain_list_single);
	tcase_add_test(tc_contain, test_contain_invalid);

	suite_add_tcase(suite, tc_manage);
	suite_add_tcase(suite, tc_contain);

	return suite;
}

/** \} */
