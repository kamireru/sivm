/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup fa_test
 * \{
 *
 * \file test_fa_transition.c
 */

#include "src/fa/fa_common.h"
#include "src/fa/fa_accept_set.h"
#include "src/fa/fa_transition.h"

#include "test/fa/test_fa_common.h"
#include "test/fa/test_fa_transition.h"

static FaAcceptSet *set_range = NULL;

static FaAcceptSet *set_list = NULL;

/**
 * \brief Test creation and destruction of transaction with accept sets.
 *
 * The test checks if all internal variables are set in the constructor
 * function.
 */
START_TEST(test_manage) {

	FaTransition* transition = fa_transition_create(1, fn_transition, 2, set_list, set_range );

	fail_if( NULL == transition, NULL );
	fail_if( 2 != transition->set_count, NULL );
	fail_if( fn_transition != transition->function, NULL );
	fail_if( set_list != transition->set_list[0], NULL );
	fail_if( set_range != transition->set_list[1], NULL );

	fa_transition_destroy( transition );

	// explicidely set accept set pointers to NULL as they were deallocated in
	// destructor
	set_range = NULL;
	set_list = NULL;

} END_TEST

/**
 * \brief Test creation and destruction of transaction without accept set.
 *
 * The test checks if transaction can be created without any accept set (it
 * should return NULL in that case).
 */
START_TEST(test_manage_empty) {

	FaTransition* transition = fa_transition_create(1, fn_transition, 0);

	fail_if( NULL != transition, NULL );

	fa_transition_destroy( transition );

} END_TEST

/**
 * \brief Test contain check on the transition.
 *
 * The test checks implementation of the contain check. The transition has two
 * non-overlapping sets and it makes sure to test using values that fall to both
 * of them. And also value that is not cover at all.
 *
 * Test correct result when NULL is input as value to check. Since the
 * comparator function can handle NULL on input, it is valid check.
 */
START_TEST(test_contain) {

	FaTransition* transition = fa_transition_create(1, fn_transition, 2, set_list, set_range );

	fail_if( FA_TRUE != fa_transition_contain(transition, comparator, VALUE_PTR(8)), NULL );
	fail_if( FA_TRUE != fa_transition_contain(transition, comparator, VALUE_PTR(3)), NULL );

	fail_if( FA_FALSE != fa_transition_contain(transition, comparator, VALUE_PTR(7)), NULL );
	fail_if( FA_FALSE != fa_transition_contain(transition, comparator, NULL), NULL );

	fa_transition_destroy( transition );

	// explicidely set accept set pointers to NULL as they were deallocated in
	// destructor
	set_range = NULL;
	set_list = NULL;

} END_TEST

/**
 * \brief Test contain check when input is invalid.
 *
 * The invalid input here consits of NULL for transition or comparator function.
 */
START_TEST(test_contain_invalid) {

	FaTransition* transition = fa_transition_create(1, fn_transition, 2, set_list, set_range );

	fail_if( FA_FALSE != fa_transition_contain(NULL, comparator, VALUE_PTR(8)), NULL );
	fail_if( FA_FALSE != fa_transition_contain(transition, NULL, VALUE_PTR(8)), NULL );

	fa_transition_destroy( transition );

	// explicidely set accept set pointers to NULL as they were deallocated in
	// destructor
	set_range = NULL;
	set_list = NULL;

} END_TEST

/**
 * \brief Test return of the target node from pointer.
 */
START_TEST(test_get_target_node) {

	FaTransition* transition = fa_transition_create(1, fn_transition, 2, set_list, set_range );

	fail_if( 1 != fa_transition_get_target(transition), NULL );
	fail_if( -1 != fa_transition_get_target(NULL), NULL );

	fa_transition_destroy( transition );

	// explicidely set accept set pointers to NULL as they were deallocated in
	// destructor
	set_range = NULL;
	set_list = NULL;

} END_TEST

/**
 * \brief Test return of the function associated with transition.
 */
START_TEST(test_get_function) {

	FaTransition* transition = fa_transition_create(1, fn_transition, 2, set_list, set_range );

	fail_if( transition->function != fa_transition_get_fn(transition), NULL );
	fail_if( fn_transition != fa_transition_get_fn(transition), NULL );
	fail_if( NULL != fa_transition_get_fn(NULL), NULL );

	fa_transition_destroy( transition );

	// explicidely set accept set pointers to NULL as they were deallocated in
	// destructor
	set_range = NULL;
	set_list = NULL;

} END_TEST

/**
 * \brief The function to invoke on test teardown.
 *
 * This implementation deallocates accept sets created during test setup.
 */
static void test_teardown(void) {
	if ( NULL != set_range ) {
		fa_set_destroy(set_range);
		set_range = NULL;
	}
	if ( NULL != set_list ) {
		fa_set_destroy(set_list);
		set_list = NULL;
	}
}

/**
 * \brief The function to invoke on the test setup.
 *
 * This implementation prepares two accept set pointers used for creation of the
 * transaction.
 */
static void test_setup(void) {
	if ( NULL == set_list ) {
		set_list = fa_set_create( FA_SET_LIST, 4,
				VALUE_PTR(0), VALUE_PTR(6), VALUE_PTR(8), VALUE_PTR(9)
			);
	}
	if ( NULL == set_range ) {
		set_range = fa_set_create(FA_SET_RANGE,
				VALUE_PTR(2), VALUE_PTR(4)
			);
	}
}

/**
 * \brief Create test suite for fa transition.
 * \return pointer to transition suite
 */
Suite* create_fa_transition_suite (void) {

	Suite *suite = suite_create("FaTransition");

	TCase *tc_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_manage, test_setup, test_teardown);
	tcase_add_test(tc_manage, test_manage);
	tcase_add_test(tc_manage, test_manage_empty);

	TCase *tc_contain = tcase_create("Contain");
	tcase_add_checked_fixture(tc_contain, test_setup, test_teardown);
	tcase_add_test(tc_contain, test_contain);
	tcase_add_test(tc_contain, test_contain_invalid);

	TCase *tc_util = tcase_create("Util");
	tcase_add_checked_fixture(tc_util, test_setup, test_teardown);
	tcase_add_test(tc_util, test_get_target_node);
	tcase_add_test(tc_util, test_get_function);

	suite_add_tcase(suite, tc_manage);
	suite_add_tcase(suite, tc_contain);
	suite_add_tcase(suite, tc_util);

	return suite;
}

/** \} */
