/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup fa_test
 * \{
 *
 * \file test_fa_common.c
 */

#include <stddef.h>
#include <stdio.h>

#include "src/fa/fa_common.h"

#include "test/fa/test_fa_common.h"

/** \brief Array with values used in tests */
int value_list[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

/**
 * The function compares two void pointers that point to the int variables. The
 * function is used in the unit testing of the finite automaton modules.
 *
 * \param arg1 first data pointer
 * \param arg2 second data pointer
 * \return FA_LESS if arg1 < arg2, FA_GREATER if arg1 > arg2, FA_EQUAL otherwise
 */
FaRelate comparator(void* arg1, void* arg2) {

	// test for NULL which is considered smaller than everything else
	if ( NULL == arg1 || NULL == arg2 ) {
		if ( NULL == arg1 && NULL != arg2 ) {
			return FA_LESS;
		} else if ( NULL != arg1 && NULL == arg2 ) {
			return FA_GREATER;
		} else {
			return FA_EQUAL;
		}
	}


	// get values behind the pointers (no pointer can be null here)
	int int_arg1 = *((int*)arg1);
	int int_arg2 = *((int*)arg2);

	// check for values
	if ( int_arg1 == int_arg2 ) {
		return FA_EQUAL;
	} else if ( int_arg1 < int_arg2 ) {
		return FA_LESS;
	} else {
		return FA_GREATER;
	}
}

/**
 * The function is used for testing setup of transition and testing invocation
 * of the function upon transition.
 *
 * \param fromNode index of node from which the transition went
 * \param toNode index of node to which the transition went
 * \param input that triggered the transition
 * \param data pointer to data associated with automaton run
 */
void fn_transition(unsigned int fromNode, unsigned int toNode, void* input, void* data) {

	int input_value = *((int*)input);
	unsigned int* data_ptr = (unsigned int*)data;

	/* for test debugging purposes only
	printf("transition %d -- (%d) --> %d\tdata: [%d->%d]\n",
			fromNode, input_value, toNode,
			*data_ptr, (*data_ptr * 10) + input_value
		);
	*/
	*data_ptr = (*data_ptr * 10) + input_value;
	return;
}

/**
 * The function is used for testing setup of node and testing the invocation of
 * the function upon automaton state change to the node.
 *
 * \param node index of node on which the function was invoked
 * \param input pointer to the input that caused transition to node
 * \param data pointer to data associated with automaton run
 */
void fn_node(unsigned int node, void* input, void* data) {
	/* for test debugging purposes only
	printf("node %d\n", node);
	*/
	return;
}

/**
 * The function is used for testing setup of the automaton and testing the
 * invocation of the reset function in the automaton runner.
 *
 * The testing data consist of one usigned int variable that holds information
 * about number parsed so far.
 *
 * \param data pointer to user data for given automaton.
 */
void fn_reset(void *data) {
	unsigned int *tmp = (unsigned int*)data;
	*tmp = 0;
	return;
}

/** \} */
