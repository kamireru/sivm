/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \defgroup fa_test Finite Automaton Tests
 * \{
 *
 * \file test_fa_common.h
 *
 * The file contains common definitions used in all finite automaton unit tests.
 * All defines here are using int variables as data that that is being supplied
 * to the automaton functions through void pointers.
 *
 * The int type was chosen as it is bigger than and has less special uses than
 * char. And it does not require implementation of extra functionality that
 * would be neeeded if we wanted to use structs or unions.
 */
#ifndef __TEST_FA_COMMON_H__
#define __TEST_FA_COMMON_H__

#define LIMIT(INDEX) ( (INDEX) < 0 ? 0 : ((INDEX) > 9 ? 9 : (INDEX)) )

/**
 * \brief The macro simplify creation of void pointer from global int variable.
 *
 * It is used as fast and readable way of creating void* data that most of the
 * finite automaton funcitonality requires.
 *
 * It uses predefined list of values and value selection is done by supplying an
 * index of the value.
 */
#define VALUE_PTR(INDEX) ((void*)&(value_list[LIMIT(INDEX)]))

/**
 * \brief The macro returns value of test data.
 *
 * The macro is pair macro to the VALUE_PTR() that returns pointer to value.
 * This macro returns value associated with index.
 */
#define VALUE(INDEX) value_list[LIMIT(INDEX)]

extern int value_list[];

/** \brief Comparator function for testing lookup in set. */
extern FaRelate comparator(void* arg1, void* arg2);

/** \brief Transition function for testing FaTransition setup. */
extern void fn_transition(unsigned int fromNode, unsigned int toNode, void* input, void* data);

/** \brief Node function for testing FaNode setup. */
extern void fn_node(unsigned int node, void* input, void* data);

/** \brief Reset data function for testing in automaton. */
extern void fn_reset(void *data);

#endif
/** \} */
