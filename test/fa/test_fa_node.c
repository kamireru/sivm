/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup fa_test
 * \{
 *
 * \file test_fa_node.c
 */

#include "src/fa/fa_common.h"
#include "src/fa/fa_accept_set.h"
#include "src/fa/fa_transition.h"
#include "src/fa/fa_transition.h"
#include "src/fa/fa_node.h"

#include "test/fa/test_fa_common.h"
#include "test/fa/test_fa_transition.h"
#include "test/fa/test_fa_node.h"

static FaTransition *transition1 = NULL;
static FaTransition *transition2 = NULL;

/**
 * \brief Test creation and destruction of node with some transitions.
 *
 * The test checks if all internal variables are set in the constructor
 * function.
 */
START_TEST(test_manage) {

	FaNode* node = fa_node_create(1, FA_NODE_START, fn_node, 2, transition1, transition2);

	fail_if( NULL == node, NULL );
	fail_if( FA_NODE_START != node->type, NULL );
	fail_if( fn_node != node->function, NULL );
	fail_if( 2 != node->transition_count, NULL );

	fail_if( transition1 != node->transition_list[0], NULL );
	fail_if( transition2 != node->transition_list[1], NULL );

	fa_node_destroy(node);

	// set to NULL as the transitions were destroyed in node destructor
	transition1 = NULL;
	transition2 = NULL;

} END_TEST

/**
 * \brief Test creation and destruction of node without transitions.
 *
 * Creating such node is allowed (opposed to the transitions or accept sets
 * which must have some elements).
 */
START_TEST(test_manage_empty) {
	FaNode* node = fa_node_create(1, FA_NODE_START, fn_node, 0);

	fail_if( NULL == node, NULL );
	fail_if( FA_NODE_START != node->type, NULL );
	fail_if( fn_node != node->function, NULL );
	fail_if( 0 != node->transition_count, NULL );

	fa_node_destroy(node);

} END_TEST

/**
 * \brief Test getting transition for given input on non-empty set.
 *
 * Test the scenarios with suppying value from first, second transition and
 * situation where there is no transition in the node for given input.
 */
START_TEST(test_contain) {

	FaNode* node = fa_node_create(1, FA_NODE_START, fn_node, 2, transition1, transition2);

	FaTransition *found = fa_node_get_transition(node, comparator, VALUE_PTR(1));
	fail_if(  transition1 != found, NULL );

	found = fa_node_get_transition(node, comparator, VALUE_PTR(6));
	fail_if(  transition1 != found, NULL );

	found = fa_node_get_transition(node, comparator, VALUE_PTR(9));
	fail_if(  transition2 != found, NULL );

	found = fa_node_get_transition(node, comparator, VALUE_PTR(3));
	fail_if(  NULL != found, NULL );

	fa_node_destroy(node);

	// set to NULL as the transitions were destroyed in node destructor
	transition1 = NULL;
	transition2 = NULL;

} END_TEST

/**
 * \brief Test getting transition for given input on empty set.
 */
START_TEST(test_contain_empty) {

	FaNode* node = fa_node_create(1, FA_NODE_START, fn_node, 0);

	FaTransition *found = fa_node_get_transition(node, comparator, VALUE_PTR(1));
	fail_if(  NULL != found, NULL );

	found = fa_node_get_transition(node, comparator, VALUE_PTR(6));
	fail_if(  NULL != found, NULL );

	found = fa_node_get_transition(node, comparator, VALUE_PTR(9));
	fail_if(  NULL != found, NULL );

	found = fa_node_get_transition(node, comparator, VALUE_PTR(3));
	fail_if(  NULL != found, NULL );

	fa_node_destroy(node);

} END_TEST

/**
 * \brief Test getting of node type from node pointer.
 */
START_TEST(test_node_type) {

	FaNode* node = fa_node_create(1, FA_NODE_START, fn_node, 0);

	fail_if( node->type != fa_node_get_type(node), NULL);
	fail_if( FA_NODE_NONE != fa_node_get_type(NULL), NULL);

	fa_node_destroy(node);

} END_TEST

/**
 * \brief Test getting of node index from node pointer.
 */
START_TEST(test_node_index) {

	FaNode* node = fa_node_create(1, FA_NODE_START, fn_node, 0);

	fail_if( node->index != fa_node_get_index(node), NULL);
	fail_if( 1 != fa_node_get_index(node), NULL);
	fail_if( -1 != fa_node_get_index(NULL), NULL);

	fa_node_destroy(node);

} END_TEST

/**
 * \brief Test getting the node function from node pointer
 */
START_TEST(test_node_function) {

	FaNode* node = fa_node_create(1, FA_NODE_START, fn_node, 0);

	fail_if( node->function != fa_node_get_fn(node), NULL);
	fail_if( fn_node != fa_node_get_fn(node), NULL);
	fail_if( NULL != fa_node_get_fn(NULL), NULL);

	fa_node_destroy(node);

} END_TEST

/**
 * \brief The function to invoke on the test setup.
 *
 * Deallocate the test transitions that were allocated during test setup.
 */
static void test_teardown(void) {
	if ( NULL != transition1 ) {
		fa_transition_destroy(transition1);
		transition1 = NULL;
	}
	if ( NULL != transition2 ) {
		fa_transition_destroy(transition2);
		transition2 = NULL;
	}
}

/**
 * \brief The function to invoke on test teardown.
 *
 * Setup two transitions, one with two accept sets and one with one accept set.
 */
static void test_setup(void) {
	if ( NULL == transition1 ) {
		transition1 = fa_transition_create(1, fn_transition, 2,
				fa_set_create(FA_SET_RANGE, VALUE_PTR(0), VALUE_PTR(2)),
				fa_set_create(FA_SET_RANGE, VALUE_PTR(5), VALUE_PTR(7))
			);
	}
	if ( NULL == transition2 ) {
		transition2 = fa_transition_create(2, fn_transition, 1,
				fa_set_create(FA_SET_LIST, 1, VALUE_PTR(9))
			);
	}
}

/**
 * \brief Create test suite for fa node.
 * \return pointer to node suite
 */
Suite* create_fa_node_suite (void) {

	Suite *suite = suite_create("FaNode");

	TCase *tc_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_manage, test_setup, test_teardown);
	tcase_add_test(tc_manage, test_manage);
	tcase_add_test(tc_manage, test_manage_empty);

	TCase *tc_contain = tcase_create("Contain");
	tcase_add_checked_fixture(tc_contain, test_setup, test_teardown);
	tcase_add_test(tc_contain, test_contain);
	tcase_add_test(tc_contain, test_contain_empty);

	TCase *tc_util = tcase_create("Util");
	tcase_add_checked_fixture(tc_contain, test_setup, test_teardown);
	tcase_add_test(tc_util, test_node_type);
	tcase_add_test(tc_util, test_node_index);
	tcase_add_test(tc_util, test_node_function);

	suite_add_tcase(suite, tc_manage);
	suite_add_tcase(suite, tc_contain);
	suite_add_tcase(suite, tc_util);

	return suite;
}

/** \} */
