/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup fa_test
 * \{
 *
 * \file test_fa_automaton.c
 */

#include "src/fa/fa_common.h"
#include "src/fa/fa_accept_set.h"
#include "src/fa/fa_transition.h"
#include "src/fa/fa_automaton.h"

#include "test/fa/test_fa_common.h"
#include "test/fa/test_fa_automaton.h"


static FaNode *node1 = NULL;
static FaNode *node2 = NULL;

/**
 * \brief Test creation and destruction of automaton with nodes
 *
 * Test creation of the automaton with two nodes. Check that the structure was
 * initialized properly.
 */
START_TEST(test_manage) {

	FaAutomaton *fa = fa_create(fn_reset, comparator, 2, node1, node2);

	fail_if( NULL == fa, NULL );
	fail_if( fn_reset != fa->reset, NULL );
	fail_if( comparator != fa->compare, NULL );
	fail_if( 2 != fa->node_count, NULL );

	fa_destroy(fa);

	// set to NULL as the nodes were destroyed in fa destructor
	node1 = NULL;
	node2 = NULL;

} END_TEST

/**
 * \brief Test creation and destruction of automaton without nodes.
 *
 * Test that it is not possible to create automaton without any nodes.
 */
START_TEST(test_manage_empty) {

	FaAutomaton *fa = fa_create(fn_reset, comparator, 0);

	fail_if( NULL != fa, NULL );

	fa_destroy(fa);

} END_TEST


/**
 * \brief Test function returning pointer to the start node.
 *
 * TODO: This function shall test both normalized and non-normalized lookups.
 */
START_TEST(test_exec_get_start) {

	FaAutomaton *fa = fa_create(fn_reset, comparator, 2, node1, node2);

	fail_if( node1 != fa_get_start_node(fa), NULL );

	fa_destroy(fa);

	// set to NULL as the nodes were destroyed in fa destructor
	node1 = NULL;
	node2 = NULL;

} END_TEST

/**
 * \brief Test function returning pointer to node with given index.
 *
 * TODO: This function shall test both normalized and non-normalized lookups.
 */
START_TEST(test_exec_get_node) {

	FaAutomaton *fa = fa_create(fn_reset, comparator, 2, node1, node2);

	fail_if( node1 != fa_get_node(fa, 1), NULL );
	fail_if( node2 != fa_get_node(fa, 2), NULL );
	fail_if( NULL != fa_get_node(fa, 3), NULL );

	fa_destroy(fa);

	// set to NULL as the nodes were destroyed in fa destructor
	node1 = NULL;
	node2 = NULL;

} END_TEST

/**
 * \brief Test function returning pointer to reset function
 */
START_TEST(test_exec_get_reset) {

	FaAutomaton *fa = fa_create(fn_reset, comparator, 2, node1, node2);

	fail_if( fn_reset != fa_get_fn_reset(fa), NULL );
	fail_if( NULL != fa_get_fn_reset(NULL), NULL );

	fa_destroy(fa);

	// set to NULL as the nodes were destroyed in fa destructor
	node1 = NULL;
	node2 = NULL;

} END_TEST

/**
 * \brief Test function returning pointer to compare function
 */
START_TEST(test_exec_get_compare) {

	FaAutomaton *fa = fa_create(fn_reset, comparator, 2, node1, node2);

	fail_if( comparator != fa_get_fn_compare(fa), NULL );
	fail_if( NULL != fa_get_fn_compare(NULL), NULL );

	fa_destroy(fa);

	// set to NULL as the nodes were destroyed in fa destructor
	node1 = NULL;
	node2 = NULL;

} END_TEST

/**
 * \brief The function to invoke on test teardown.
 *
 * Deallocate nodes created for testing intialization of the automaton.
 */
static void test_teardown(void) {
	if ( NULL != node1 ) {
		fa_node_destroy(node1);
		node1 = NULL;
	}
	if ( NULL != node2 ) {
		fa_node_destroy(node2);
		node2 = NULL;
	}
}

/**
 * \brief The function to invoke on the test setup.
 *
 * Create two nodes for testing initialization of the automaton.
 */
static void test_setup(void) {
	if ( NULL == node1 ) {
		node1 = fa_node_create(1, FA_NODE_START, fn_node, 2,
				fa_transition_create(1, fn_transition, 1,
					fa_set_create(FA_SET_RANGE, VALUE_PTR(0), VALUE_PTR(2))
				),
				fa_transition_create(2, fn_transition, 1,
					fa_set_create(FA_SET_RANGE, VALUE_PTR(3), VALUE_PTR(5))
				)
			);
	}
	if ( NULL == node2 ) {
		node2 = fa_node_create(2, FA_NODE_FINAL, fn_node, 1,
				fa_transition_create(2, fn_transition, 1,
					fa_set_create(FA_SET_RANGE, VALUE_PTR(0), VALUE_PTR(9))
				)
			);
	}
}

/**
 * \brief Create test suite for fa automaton.
 * \return pointer to automaton suite
 */
Suite* create_fa_automaton_suite (void) {

	Suite *suite = suite_create("FaAutomaton");

	TCase *tc_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_manage, test_setup, test_teardown);
	tcase_add_test(tc_manage, test_manage);
	tcase_add_test(tc_manage, test_manage_empty);

	TCase *tc_execute = tcase_create("Execute");
	tcase_add_checked_fixture(tc_execute, test_setup, test_teardown);
	tcase_add_test(tc_execute, test_exec_get_start);
	tcase_add_test(tc_execute, test_exec_get_node);
	tcase_add_test(tc_execute, test_exec_get_reset);

	suite_add_tcase(suite, tc_manage);
	suite_add_tcase(suite, tc_execute);

	return suite;
}

/** \} */
