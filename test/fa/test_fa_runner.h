/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __TEST_FA_RUNNER_SET_H__
#define __TEST_FA_RUNNER_SET_H__

#include <check.h>

/**
 * \addtogroup fa_test
 * \{
 *
 * \file test_fa_runner.h
 */

/**
 * \brief Create runner test suite and return pointer to it.
 */
extern Suite* create_fa_runner_suite (void);

#endif
/** \} */
