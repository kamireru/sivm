/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup fa_test
 * \{
 *
 * \file test_fa_runner.c
 */

#include "src/fa/fa_common.h"
#include "src/fa/fa_accept_set.h"
#include "src/fa/fa_transition.h"
#include "src/fa/fa_automaton.h"
#include "src/fa/fa_runner.h"

#include "test/fa/test_fa_common.h"
#include "test/fa/test_fa_runner.h"


static FaAutomaton *fa = NULL;
static unsigned int result_value;

/**
 * \brief Test creation and destruction of runner.
 *
 * Create automaton runner with finite automaton and user data and check correct
 * initialization.
 */
START_TEST(test_manage) {

	FaRunner* runner = fa_runner_create(fa, ((void*)&result_value) );

	fail_if( NULL == runner, NULL );
	fail_if( fa != runner->fa, NULL );
	fail_if( NULL != runner->state, NULL );
	fail_if( 0 != runner->position, NULL );
	fail_if( (void*)&result_value != runner->data, NULL );

	fa_runner_destroy(runner);

} END_TEST


/**
 * \brief Test reset to the start state.
 *
 * Test function that resets automaton state to the  initial one. It means that
 * current node pointer points to NULL and content of the user data is cleared
 * using provided reset function.
 */
START_TEST(test_exec_reset) {

	FaRunner* runner = fa_runner_create(fa, ((void*)&result_value) );

	// test unsuccesfull reset first
	fail_if( FA_FALSE != fa_runner_reset(NULL), NULL );

	// set data variable to anything, it should be reset to 0
	result_value = 15;
	fail_if( FA_TRUE != fa_runner_reset(runner), NULL );
	fail_if( runner->state != fa_get_node(fa, 1), NULL);
	fail_if( FA_NODE_START != fa_node_get_type(runner->state), NULL );
	fail_if( 0 != result_value, NULL);
	fail_if( 0 != runner->position, NULL );

	fa_runner_destroy(runner);

} END_TEST

/**
 * \brief Check runner execution failure conditions
 *
 * Check if failure is properly indicated for both step() and execute() calls.
 */
START_TEST(test_exec_fail) {

	FaRunner* runner = fa_runner_create(fa, ((void*)&result_value) );

	// test that position is not increased when state pointer is NULL
	fail_if( FA_FALSE != fa_runner_step(runner, VALUE_PTR(0)), NULL);

	// test that execution on NULL runner returns false
	fail_if( FA_FALSE != fa_runner_step(NULL, VALUE_PTR(0)), NULL );

	fa_runner_destroy(runner);

} END_TEST

/**
 * \brief Test execution of one automaton step and accept check.
 *
 * The test will check error inducing input and then it will reset automaton and
 * provide 0, 1, 2, NULL, 2 input sequence to simulate one run of automaton and
 * check if runner state is being changed correctly.
 *
 * It will also check that function checking for accept returns correct values.
 */
START_TEST(test_exec_step) {

	FaRunner* runner = fa_runner_create(fa, ((void*)&result_value) );

	// reset runner state to start node
	fail_if( FA_TRUE != fa_runner_reset(runner), NULL );

	// supply input 0, remain in first node and do not accept
	fail_if( FA_TRUE != fa_runner_step(runner, VALUE_PTR(0)), NULL);
	fail_if( 1 != runner->position, NULL );
	fail_if( runner->state != fa_get_node(fa, 1), NULL);
	fail_if( FA_FALSE != fa_runner_is_accept(runner), NULL);
	fail_if( FA_FALSE != fa_runner_is_blocked(runner), NULL);
	fail_if( 0 != result_value, NULL);

	// supply input 1, transition to second node and do not accept
	fail_if( FA_TRUE != fa_runner_step(runner, VALUE_PTR(1)), NULL);
	fail_if( 2 != runner->position, NULL );
	fail_if( runner->state != fa_get_node(fa, 2), NULL);
	fail_if( FA_FALSE != fa_runner_is_accept(runner), NULL);
	fail_if( FA_FALSE != fa_runner_is_blocked(runner), NULL);
	fail_if( 1 != result_value, "1 != result_value[%d]", result_value);

	// supply input 2, transition to third node and accept
	fail_if( FA_TRUE != fa_runner_step(runner, VALUE_PTR(2)), NULL);
	fail_if( 3 != runner->position, NULL );
	fail_if( runner->state != fa_get_node(fa, 3), NULL);
	fail_if( FA_TRUE != fa_runner_is_accept(runner), NULL);
	fail_if( FA_FALSE != fa_runner_is_blocked(runner), NULL);
	fail_if( 12 != result_value, NULL);

	// supply input NULL, indicate error and block automaton, do not accept
	fail_if( FA_FALSE != fa_runner_step(runner, NULL), NULL);
	fail_if( 3 != runner->position, NULL );
	fail_if( NULL != runner->state, NULL);
	fail_if( FA_FALSE != fa_runner_is_accept(runner), NULL);
	fail_if( FA_TRUE != fa_runner_is_blocked(runner), NULL);
	fail_if( 12 != result_value, NULL);

	// supply input 2, stay blocked, do not accept
	fail_if( FA_FALSE != fa_runner_step(runner, VALUE_PTR(2)), NULL);
	fail_if( 3 != runner->position, NULL );
	fail_if( NULL != runner->state, NULL);
	fail_if( FA_FALSE != fa_runner_is_accept(runner), NULL);
	fail_if( FA_TRUE != fa_runner_is_blocked(runner), NULL);
	fail_if( 12 != result_value, NULL);

	fa_runner_destroy(runner);

} END_TEST

/**
 * \brief The function to invoke on test teardown.
 *
 * Deallocate automaton created for testing and reset user test data.
 */
static void test_teardown(void) {
	if ( NULL != fa ) {
		fa_destroy(fa);
		fa = NULL;
	}
}

/**
 * \brief The function to invoke on the test setup.
 *
 * Create automaton to use for testing. The automaton is checking if sequence of
 * numbers makes even number.
 */
static void test_setup(void) {
	if ( NULL == fa ) {
		fa = fa_create(fn_reset, comparator, 3,
			fa_node_create(1, FA_NODE_START, fn_node, 3,
				fa_transition_create(1, fn_transition, 1,
					fa_set_create(FA_SET_LIST, 1, VALUE_PTR(0))
				),
				fa_transition_create(2, fn_transition, 1,
					fa_set_create(FA_SET_LIST, 5,
						VALUE_PTR(1), VALUE_PTR(3), VALUE_PTR(5), VALUE_PTR(7), VALUE_PTR(9)
					)
				),
				fa_transition_create(3, fn_transition, 1,
					fa_set_create(FA_SET_LIST, 4,
						VALUE_PTR(2), VALUE_PTR(4), VALUE_PTR(6), VALUE_PTR(8)
					)
				)
			),
			fa_node_create(2, FA_NODE_INNER, fn_node, 2,
				fa_transition_create(2, fn_transition, 1,
					fa_set_create(FA_SET_LIST, 5,
						VALUE_PTR(1), VALUE_PTR(3), VALUE_PTR(5), VALUE_PTR(7), VALUE_PTR(9)
					)
				),
				fa_transition_create(3, fn_transition, 1,
					fa_set_create(FA_SET_LIST, 5,
						VALUE_PTR(0), VALUE_PTR(2), VALUE_PTR(4), VALUE_PTR(6), VALUE_PTR(8)
					)
				)
			),
			fa_node_create(3, FA_NODE_FINAL, fn_node, 2,
				fa_transition_create(2, fn_transition, 1,
					fa_set_create(FA_SET_LIST, 5,
						VALUE_PTR(1), VALUE_PTR(3), VALUE_PTR(5), VALUE_PTR(7), VALUE_PTR(9)
					)
				),
				fa_transition_create(3, fn_transition, 1,
					fa_set_create(FA_SET_LIST, 5,
						VALUE_PTR(0), VALUE_PTR(2), VALUE_PTR(4), VALUE_PTR(6), VALUE_PTR(8)
					)
				)
			)
		);
	}
}

/**
 * \brief Create test suite for fa runner.
 * \return pointer to runner suite
 */
Suite* create_fa_runner_suite (void) {

	Suite *suite = suite_create("FaRunner");

	TCase *tc_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_manage, test_setup, test_teardown);
	tcase_add_test(tc_manage, test_manage);

	TCase *tc_execute = tcase_create("Execute");
	tcase_add_checked_fixture(tc_execute, test_setup, test_teardown);
	tcase_add_test(tc_execute, test_exec_reset);
	tcase_add_test(tc_execute, test_exec_reset);
	tcase_add_test(tc_execute, test_exec_step);
	tcase_add_test(tc_execute, test_exec_fail);

	suite_add_tcase(suite, tc_manage);
	suite_add_tcase(suite, tc_execute);

	return suite;
}

/** \} */
