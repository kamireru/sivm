/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <check.h>
#include <glib.h>

#include "test/common/test_error.h"
#include "test/common/test_trace.h"
#include "test/common/test_common.h"

#include "test/config/test_config.h"
#include "test/config/test_config_storage.h"
#include "test/config/test_config_file.h"
#include "test/config/test_config_cmdline.h"

#include "test/backend/test_image_pile.h"
#include "test/backend/test_image.h"
#include "test/backend/test_taglist.h"
#include "test/backend/test_image_group.h"
#include "test/backend/test_image_group_record.h"
#include "test/backend/test_group_list.h"

#include "test/frontend/test_mvc_functions.h"

#include "test/frontend/test_container_node.h"
#include "test/frontend/test_view_generic.h"
#include "test/frontend/test_view_image_display.h"
#include "test/frontend/test_view_application.h"
#include "test/frontend/test_view_container.h"
#include "test/frontend/test_view_list_status.h"

#include "test/frontend/test_cntl_common.h"
#include "test/frontend/test_cntl_generic.h"
#include "test/frontend/test_cntl_application.h"
#include "test/frontend/test_cntl_image_display.h"
#include "test/frontend/test_cntl_container.h"

#include "test/frontend/test_observer_list.h"
#include "test/frontend/test_model_generic.h"
#include "test/frontend/test_model_application.h"
#include "test/frontend/test_model_image_list.h"

/**
 * \brief Unit test suite main method.
 *
 * Create test suite runner and add test suites it should run. Use environment
 * variable to control how the suites will be run (forked/no forked mode).
 *
 * \param argc number of parameters to program
 * \param argv array of parameters to program
 * \return number of failed suites
 */
int main(int argc, char** argv)
{
	int number_failed = 0;

	g_type_init();
	gdk_init(&argc,&argv);

	SRunner *suite_runner = srunner_create( create_error_suite() );
	srunner_add_suite( suite_runner, create_trace_suite() );
	srunner_add_suite( suite_runner, create_common_suite() );

	// config related tests
	srunner_add_suite( suite_runner, create_config_suite() );
	srunner_add_suite( suite_runner, create_config_storage_suite() );
	srunner_add_suite( suite_runner, create_config_file_suite() );
	srunner_add_suite( suite_runner, create_config_cmdline_suite() );

	// backend related tests
	srunner_add_suite( suite_runner, create_image_pile_suite() );
	srunner_add_suite( suite_runner, create_image_suite() );
	srunner_add_suite( suite_runner, create_taglist_suite() );
	srunner_add_suite( suite_runner, create_image_group_suite() );
	srunner_add_suite( suite_runner, create_image_group_record_suite() );
	srunner_add_suite( suite_runner, create_group_list_suite() );

	// GUI - MVC and generic test suites
	srunner_add_suite( suite_runner, create_mvc_functions_suite() );

	// GUI - view related tests
	srunner_add_suite( suite_runner, create_view_generic_suite() );
	srunner_add_suite( suite_runner, create_view_image_display_suite() );
	srunner_add_suite( suite_runner, create_view_application_suite() );
	srunner_add_suite( suite_runner, create_view_container_suite() );
	srunner_add_suite( suite_runner, create_view_list_status_suite() );

	srunner_add_suite( suite_runner, create_container_node_suite() );

	// GUI - controller related tests
	srunner_add_suite( suite_runner, create_cntl_common_suite() );
	srunner_add_suite( suite_runner, create_cntl_generic_suite() );
	srunner_add_suite( suite_runner, create_cntl_application_suite() );
	srunner_add_suite( suite_runner, create_cntl_image_display_suite() );
	srunner_add_suite( suite_runner, create_cntl_container_suite() );

	// GUI - model related tests
	srunner_add_suite( suite_runner, create_observer_list_suite() );
	srunner_add_suite( suite_runner, create_model_generic_suite() );
	srunner_add_suite( suite_runner, create_model_application_suite() );
	srunner_add_suite( suite_runner, create_model_image_list_suite() );

	srunner_run_all(suite_runner, CK_ENV);
	number_failed = srunner_ntests_failed(suite_runner);

	srunner_free(suite_runner);

	return ( 0 == number_failed ) ? EXIT_SUCCESS : EXIT_FAILURE;
}
