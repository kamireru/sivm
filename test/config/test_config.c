/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup config_test
 * \{
 *
 * \file test_config.c
 */

#include <string.h>
#include <malloc.h>
#include "src/common/error.h"
#include "src/common/trace.h"
#include "src/config/config.h"

#include "test/config/test_config.h"
#include "test/common/common.h"

/** \brief Module-wide variable to simulate argc entry from OS. */
static int argc = 0;

/** \brief Module-wide variable to simulate argv entry from OS. */
static char **argv = NULL;

/** \brief Module-wide variable for storing HOME variable. */
static char *home;



/**
 * \brief Test loading configuration with profile and file name specified.
 */
START_TEST(test_with_profile) {
	const ConfigStorage* conf = NULL;
	int index = 0;
	RCode result;

	char *cmdline = "sivm --config data/config/partial.conf file1 -f file2"
		" --recurse 5 -p specific file3";
	create_program_args(cmdline, &argc, &argv);

	result = config_load(argc, argv, &index);
	ck_assert_int_eq( OK, result );

	ck_assert_int_eq(8, index);
	ck_assert_str_eq("file1", argv[index]);
	ck_assert_str_eq("file2", argv[index+1]);
	ck_assert_str_eq("file3", argv[index+2]);

	conf = config_get();

	ck_assert( NULL != conf->filename );
	ck_assert_str_eq( "data/config/partial.conf" , conf->filename );

	ck_assert( NULL != conf->profile );
	ck_assert_str_eq( "specific", conf->profile );

	ck_assert( NULL != conf );

	ck_assert_int_eq( conf->fullscreen, TRUE );
	ck_assert_int_eq( 5, conf->recurse );
	ck_assert_int_eq( 2, conf->auto_zoom );
	ck_assert_int_eq( 200, conf->geometry_width );
	ck_assert_int_eq( 100, conf->geometry_height );

	result = config_remove();
	ck_assert( NULL == config_get() );

	destroy_program_args(argc, &argv);

} END_TEST

/**
 * \brief Test configuration loading without specified profile.
 *
 * The default profile 'default' should be used.
 */
START_TEST(test_without_profile) {
	const ConfigStorage* conf = NULL;
	int index = 0;
	RCode result;

	char *cmdline = "sivm --config data/config/partial.conf file1 file2 file3";
	create_program_args(cmdline, &argc, &argv);

	result = config_load(argc, argv, &index);
	ck_assert_int_eq( OK, result );

	ck_assert_int_eq(3, index);
	ck_assert_str_eq("file1", argv[index]);
	ck_assert_str_eq("file2", argv[index+1]);
	ck_assert_str_eq("file3", argv[index+2]);

	conf = config_get();

	ck_assert( NULL != conf->filename );
	ck_assert_str_eq( "data/config/partial.conf" , conf->filename );

	ck_assert( NULL == conf->profile );

	ck_assert( NULL != conf );

	ck_assert_int_eq( conf->fullscreen, FALSE );
	ck_assert_int_eq( 1, conf->recurse );
	ck_assert_int_eq( 3, conf->auto_zoom );
	ck_assert_int_eq( 100, conf->geometry_width );
	ck_assert_int_eq( 150, conf->geometry_height );

	result = config_remove();
	ck_assert( NULL == config_get() );

	destroy_program_args(argc, &argv);
} END_TEST


/**
 * \brief Test configuration loading without specified profile and file.
 *
 * The default profile 'default' should be used. File '.sivmrc' from home
 * directory should be used.
 */
START_TEST(test_without_filename) {
	const ConfigStorage* conf = NULL;
	int index = 0;
	RCode result;

	char *cmdline = "sivm file1 file2 file3";
	create_program_args(cmdline, &argc, &argv);

	result = config_load(argc, argv, &index);
	ck_assert_int_eq( OK, result );

	ck_assert_int_eq(1, index);
	ck_assert_str_eq("file1", argv[index]);
	ck_assert_str_eq("file2", argv[index+1]);
	ck_assert_str_eq("file3", argv[index+2]);

	conf = config_get();

	ck_assert( NULL == conf->filename );
	ck_assert( NULL == conf->profile );

	ck_assert( NULL != conf );

	ck_assert_int_eq( 3, conf->auto_zoom );
	ck_assert_int_eq( 100, conf->geometry_width );
	ck_assert_int_eq( 150, conf->geometry_height );

	result = config_remove();
	ck_assert( NULL == config_get() );

	destroy_program_args(argc, &argv);
} END_TEST
/**
 * \brief Setup function for setting HOME env variable
 *
 * The configuration loading can look for config file in the HOME. To simulate
 * such loading, we set home variable to point to our test data directory.
 */
static void test_setup(void) {
	COLOR_SETUP_START;

	// store original value
	home = getenv("HOME");

	// set new value
	setenv("HOME", "data/config", 1);

	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_SETUP_END;
	return;
}

/**
 * \brief Teardown function for resetting HOME variable
 */
static void test_teardown(void) {
	COLOR_TEARDOWN_START;

	// restore home variable
	setenv("HOME", home, 1);

	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_TEARDOWN_END;
	return;
}
/**
 * \brief Create test suite for config.
 * \return pointer to accept set suite
 */
Suite* create_config_suite (void) {
	Suite *s = suite_create ("Config");

	TCase *tc_loading = tcase_create ("Loading");
	tcase_add_checked_fixture (tc_loading, test_setup, test_teardown);
	tcase_add_test (tc_loading, test_with_profile);
	tcase_add_test (tc_loading, test_without_profile);
	tcase_add_test (tc_loading, test_without_filename);

	suite_add_tcase (s, tc_loading);

	return s;
}

/** \} */
