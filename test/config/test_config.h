/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \defgroup config_test Configuration Tests
 * \{
 *
 * \file test_config.h
 */

#ifndef _TEST_CONFIG_TEST_CONFIG_H_
#define _TEST_CONFIG_TEST_CONFIG_H_

#include <check.h>

/**
 * \brief Create config test suite and return pointer to it.
 */
extern Suite* create_config_suite (void);

#endif
/** \} */
