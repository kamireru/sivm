/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup config_test
 * \{
 *
 * \file test_config_file.c
 */

#include <math.h>
#include <glib.h>
#include "src/config/config_file.h"

#include "test/config/test_config_file.h"
#include "test/common/common.h"


/**
 * \brief Test loading configuration from empty file.
 */
START_TEST(test_config_empty) {
	ConfigStorage* conf;
	RCode result;

	conf = config_from_file("data/config/empty.conf", "test");

	ck_assert( NULL != conf );

	ck_assert( NULL == conf->filename );
	ck_assert( NULL == conf->profile );
	ck_assert( NULL == conf->layout );
	ck_assert( NULL == conf->exclude );
	ck_assert( NULL == conf->layout_hint );

	ck_assert( FALSE == conf->fullscreen );

	ck_assert_int_eq( 0, conf->recurse );
	ck_assert_int_eq( 1, conf->auto_zoom );
	ck_assert_int_eq( 400, conf->geometry_width );
	ck_assert_int_eq( 320, conf->geometry_height );

	// five decimal places precision for zoom comparison
	ck_assert( ZOOM_EQUAL( 1.0f, conf->zoom ) );
	ck_assert( ZOOM_EQUAL( 0.6f, conf->pan_percentage ) );

	result = config_destroy(conf);
	ck_assert_int_eq( OK, result );
} END_TEST

/**
 * \brief Test loading configuration from file which does not contain 'config'
 * section with correct profile name.
 */
START_TEST(test_no_profile) {
	ConfigStorage* conf;
	RCode result;

	conf = config_from_file("data/config/full.conf", "test");

	ck_assert( NULL != conf );

	ck_assert( NULL == conf->filename );
	ck_assert( NULL == conf->layout );
	ck_assert( NULL == conf->profile );
	ck_assert( NULL == conf->exclude );
	ck_assert( NULL == conf->layout_hint );

	ck_assert( FALSE == conf->fullscreen );

	ck_assert_int_eq( 0, conf->recurse );
	ck_assert_int_eq( 1, conf->auto_zoom );
	ck_assert_int_eq( 400, conf->geometry_width );
	ck_assert_int_eq( 320, conf->geometry_height );

	// five decimal places precision for zoom comparison
	ck_assert( ZOOM_EQUAL( 1.0f, conf->zoom ) );
	ck_assert( ZOOM_EQUAL( 0.6f, conf->pan_percentage ) );

	result = config_destroy(conf);
	ck_assert_int_eq( OK, result );
} END_TEST

/**
 * \brief Test loading configuration from file which contains correct
 * configuration section.
 */
START_TEST(test_has_profile) {
	ConfigStorage* conf;
	RCode result;
	gint i;

	conf = config_from_file("data/config/full.conf", "default");

	ck_assert( NULL != conf );

	ck_assert( NULL == conf->filename );
	ck_assert( NULL == conf->profile );

	ck_assert( TRUE == conf->fullscreen );

	ck_assert_int_eq( 5, conf->recurse );
	ck_assert_int_eq( 3, conf->auto_zoom );
	ck_assert_int_eq( 800, conf->geometry_width );
	ck_assert_int_eq( 640, conf->geometry_height );

	// five decimal places precision for zoom comparison
	ck_assert( ZOOM_EQUAL( 2.5f, conf->zoom ) );
	ck_assert( ZOOM_EQUAL( 0.25f, conf->pan_percentage ) );

	ck_assert( NULL != conf->exclude );

	// there should be 3 patterns in configuration file
	for (i = 0; i < 3; i++ ) {
		ck_assert(NULL != conf->exclude[i]);
	}

	ck_assert_str_eq( "pattern 1", conf->exclude[0] );
	ck_assert_str_eq( "pattern 2", conf->exclude[1] );
	ck_assert_str_eq( "pattern 3", conf->exclude[2] );
	ck_assert( NULL == conf->exclude[3] );

	ck_assert( NULL != conf->layout );
	ck_assert_str_eq( "simple", conf->layout);

	// there should be 2 hints in configuration file
	ck_assert( NULL != conf->layout_hint );
	ck_assert( NULL == conf->layout_hint[2] );
	for (i = 0; i < 2; i++ ) {
		ck_assert(NULL != conf->layout_hint[i]);
	}

	ck_assert_str_eq( "hint 1", conf->layout_hint[0] );
	ck_assert_str_eq( "hint 2", conf->layout_hint[1] );

	result = config_destroy(conf);
	ck_assert_int_eq( OK, result );
} END_TEST

/**
 * \brief Test loading of recurse configuration
 *
 * In this scenario, only 'recursive = true' option is present. Depth should be
 * set to -1 to show that it is not limited.
 */
START_TEST(test_recurse_flag_only) {
	ConfigStorage* conf;
	RCode result;
	gint i;

	conf = config_from_file("data/config/full.conf", "flagonly");

	ck_assert( NULL != conf );

	ck_assert_int_eq( -1, conf->recurse );

	ck_assert( NULL != conf->layout );
	ck_assert_str_eq( "minimal", conf->layout);

	result = config_destroy(conf);
	ck_assert_int_eq( OK, result );
} END_TEST


/**
 * \brief Test loading of recurse configuration
 *
 * In this scenario, only 'depth = 5' option is present. In this case the
 * configuration should be ignored as recursive loading is not enabled.
 */
START_TEST(test_recurse_depth_only) {
	ConfigStorage* conf;
	RCode result;
	gint i;

	conf = config_from_file("data/config/full.conf", "depthonly");

	ck_assert( NULL != conf );

	ck_assert_int_eq( 0, conf->recurse );

	ck_assert( NULL != conf->layout );
	ck_assert_str_eq( "minimal", conf->layout);

	result = config_destroy(conf);
	ck_assert_int_eq( OK, result );
} END_TEST

/**
 * \brief Create test suite for config file.
 * \return pointer to config file test suite
 */
Suite* create_config_file_suite (void) {

	Suite *s = suite_create ("ConfigFile");

	TCase *tc_parse = tcase_create ("Parse");
	tcase_add_checked_fixture (tc_parse, common_setup, common_teardown);
	tcase_add_test (tc_parse, test_config_empty);
	tcase_add_test (tc_parse, test_no_profile);
	tcase_add_test (tc_parse, test_has_profile);
	tcase_add_test (tc_parse, test_recurse_flag_only);
	tcase_add_test (tc_parse, test_recurse_depth_only);

	suite_add_tcase (s, tc_parse);

	return s;
}

/** \} */
