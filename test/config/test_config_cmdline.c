/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * \addtogroup config_test
 * \{
 *
 * \file test_config_cmdline.c
 */

#include <math.h>
#include <glib.h>
#include "src/config/config_cmdline.h"

#include "test/common/common.h"
#include "test/config/test_config_cmdline.h"

/** \brief Module-wide variable to simulate argc entry from OS. */
static int argc = 0;

/** \brief Module-wide variable to simulate argv entry from OS. */
static char **argv = NULL;

/**
 * Utility function for testing contents of the configuration storage.
 *
 * Both tests for testing config loading from command line arguments are using
 * same values, so the function can wrap tests and reuse them in more tests.
 *
 * \param conf pointer to storage structure
 */
static void check_config_storage(ConfigStorage* conf) {
	int i;

	ck_assert( NULL != conf );
	ck_assert( NULL != conf->filename );
	ck_assert( NULL != conf->profile );

	ck_assert_str_eq( "filename.conf" , conf->filename );
	ck_assert_str_eq( "default", conf->profile );

	ck_assert( TRUE == conf->fullscreen );

	ck_assert_int_eq( 5, conf->recurse );
	ck_assert_int_eq( 3, conf->auto_zoom );
	ck_assert_int_eq( 800, conf->geometry_width );
	ck_assert_int_eq( 640, conf->geometry_height );

	// five decimal places precision for zoom comparison
	ck_assert( ZOOM_EQUAL( 2.5f, conf->zoom ) );
	ck_assert( ZOOM_EQUAL( 0.2f, conf->pan_percentage ) );

	// there should be 3 patterns in configuration file
	ck_assert( NULL != conf->exclude );
	for (i = 0; i < 3; i++ ) {
		ck_assert(NULL != conf->exclude[i]);
	}

	ck_assert_str_eq( "pattern1", conf->exclude[0] );
	ck_assert_str_eq( "pattern2", conf->exclude[1] );
	ck_assert_str_eq( "pattern3", conf->exclude[2] );
	ck_assert( NULL == conf->exclude[3] );

	ck_assert( NULL != conf->layout );
	ck_assert_str_eq( "simple", conf->layout );

	// there should be 2 patterns in configuration file
	ck_assert( NULL != conf->layout_hint );
	for (i = 0; i < 2; i++ ) {
		ck_assert(NULL != conf->layout_hint[i]);
	}

	ck_assert_str_eq( "hint1", conf->layout_hint[0] );
	ck_assert_str_eq( "hint2", conf->layout_hint[1] );
	ck_assert( NULL == conf->layout_hint[2] );
}

/**
 * \brief Test loading configuration from command line using short options.
 */
START_TEST(test_short_opts) {
	ConfigStorage* conf = NULL;
	int flags = 0, index = 0;
	RCode result;

	char *cmdline = "sivm -c filename.conf -p default -f -a 3 -r"
		" 5 -g 800x640 -z 2.5 -e pattern1 -e pattern2 -e pattern3"
		" -l simple -L hint1 -L hint2 -P 0.2";
	create_program_args(cmdline, &argc, &argv);

	mark_point();
	conf = config_from_cmdline(argc, argv, &flags, &index);

	ck_assert_int_eq(
			CONF_FILENAME | CONF_PROFILE | CONF_FULLSCREEN | CONF_AUTO_ZOOM
			| CONF_ZOOM | CONF_GEOMETRY_WIDTH | CONF_GEOMETRY_HEIGHT
			| CONF_RECURSE | CONF_EXCLUDE | CONF_LAYOUT | CONF_LAYOUT_HINT
			| CONF_PAN_PERCENTAGE,
			flags
		);
	ck_assert_int_eq(28, index);

	check_config_storage(conf);

	result = config_destroy(conf);
	ck_assert_int_eq( OK, result );

	destroy_program_args(argc, &argv);
} END_TEST

/**
 * \brief Test loading configuration from command line using long options.
 */
START_TEST(test_long_opts) {
	ConfigStorage* conf = NULL;
	int flags = 0, index = 0;
	RCode result;

	char *cmdline = "sivm --config filename.conf --profile default"
		" --fullscreen --auto-zoom 3 --recurse 5 --geometry 800x640"
		" --zoom 2.5 --exclude pattern1 --exclude pattern2 --exclude"
		" pattern3 --layout simple --layout-hint hint1 --layout-hint hint2"
		" --pan 0.2";
	create_program_args(cmdline, &argc, &argv);

	conf = config_from_cmdline(argc, argv, &flags, &index);

	ck_assert_int_eq(
			CONF_FILENAME | CONF_PROFILE | CONF_FULLSCREEN | CONF_AUTO_ZOOM
			| CONF_ZOOM | CONF_GEOMETRY_WIDTH | CONF_GEOMETRY_HEIGHT
			| CONF_RECURSE | CONF_EXCLUDE | CONF_LAYOUT | CONF_LAYOUT_HINT
			| CONF_PAN_PERCENTAGE,
			flags
		);
	ck_assert_int_eq(28, index);

	check_config_storage(conf);

	result = config_destroy(conf);
	ck_assert_int_eq( OK, result );

	destroy_program_args(argc, &argv);
} END_TEST

/**
 * \brief Test argument reordering
 *
 * Test that all non-option arguments are at the end of the argv array after the
 * command line processing.
 */
START_TEST(test_arg_reorder) {
	ConfigStorage* conf = NULL;
	int flags = 0, index = 0;
	RCode result;

	char *cmdline = "sivm file1 -f file2 --recurse 6 file3";
	create_program_args(cmdline, &argc, &argv);

	conf = config_from_cmdline(argc, argv, &flags, &index);

	ck_assert_int_eq( CONF_FULLSCREEN | CONF_RECURSE, flags );
	ck_assert_int_eq(4, index);

	ck_assert_str_eq("file1", argv[index]);
	ck_assert_str_eq("file2", argv[index+1]);
	ck_assert_str_eq("file3", argv[index+2]);

	result = config_destroy(conf);
	ck_assert_int_eq( OK, result );

	destroy_program_args(argc, &argv);

} END_TEST

/**
 * \brief Create test suite for config cmdline.
 * \return pointer to config cmdline test suite
 */
Suite* create_config_cmdline_suite (void) {

	Suite *s = suite_create ("ConfigCmd");

	TCase *tc_parse = tcase_create ("Parse");
	tcase_add_checked_fixture (tc_parse, common_setup, common_teardown);
	tcase_add_test (tc_parse, test_short_opts);
	tcase_add_test (tc_parse, test_long_opts);
	tcase_add_test (tc_parse, test_arg_reorder);


	suite_add_tcase (s, tc_parse);

	return s;
}

/** \} */
