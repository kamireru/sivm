/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \addtogroup config_test
 * \{
 *
 * \file test_config_storage.c
 */

#include <math.h>
#include <glib.h>
#include "src/config/config_storage.h"

#include "test/config/test_config_storage.h"
#include "test/common/common.h"

#define LONGSTRING "This is a very long string that is used for checking" \
	"memory leaks in the program by duping it and not deallocating at all. The" \
	"string is so long to make sure memory loss won't be overlooked when" \
	"running the test through Valgrind or similar memory checking tool. And of" \
	"course, its definition does not need to be documented because it is" \
	"self-documenting. Yay!"
#define SHORTSTRING "Another testing string, but this one should be shorter" \
	"and thus it contains more boring text. And no explanations."

/**
 * Test helper utility for allocating exclude structures for testing purposes.
 *
 * \param count number of patterns to allocate
 * \param string what string to use for patterns
 * \return pointer to exclude structure
 */
static gchar** create_list(int count, gchar* string) {
	gchar** list = NULL;
	int i = 0;

	list = (gchar**) malloc(sizeof(gchar*) * (count + 1) );
	ck_assert( NULL != list );

	for ( i = 0; i < count;  i++ ) {
		list[i] = strdup(string);
	}
	list[count] = NULL;

	return list;
}

/**
 * \brief Test configuration storage allocation and deallocation.
 *
 * The test checks allocation with default value and dellocation. It is not
 * possible to check correct deallocation programatically (I do not know how
 * yet), so the test should be run through Valgrind or similar tool to ensure
 * correctness.
 *
 * None of the memory allocated in the test function should be freed there. It
 * is responsibility of destructor function.
 */
START_TEST(test_manage) {
	RCode result;
	ConfigStorage *conf = NULL;

	// test construction
	conf = config_create();
	ck_assert( NULL != conf );

	ck_assert( NULL == conf->filename );
	ck_assert( NULL == conf->profile );
	ck_assert( NULL == conf->exclude );
	ck_assert( NULL == conf->layout );
	ck_assert( NULL == conf->layout_hint );

	ck_assert( FALSE == conf->fullscreen );

	ck_assert_int_eq( 0, conf->recurse );
	ck_assert_int_eq( 1, conf->auto_zoom );
	ck_assert_int_eq( 400, conf->geometry_width );
	ck_assert_int_eq( 320, conf->geometry_height );

	// five decimal places precision for zoom comparison
	ck_assert( ZOOM_EQUAL( 1.0f, conf->zoom ) );

	// allocate some memory for pointer values to check they deallocate properly
	conf->filename = strdup(LONGSTRING);
	conf->profile = strdup(LONGSTRING);
	conf->exclude = create_list(2, LONGSTRING);
	conf->layout_hint = create_list(2, LONGSTRING);

	// test destruction
	result = config_destroy(conf);
	ck_assert_int_eq( OK, result );

	// set pointer to NULL to make sure above allocated memory will be lost if
	// not deallocated properly
	conf = NULL;
} END_TEST

/**
 * \brief Test configuration updating
 *
 * Test that only fields marked in option bitmap are being copied to source
 * config and that the copy of pointers is deep, not shallow (is same values,
 * different pointers.
 *
 * Also test that there is no memory leak if rewriting dynamicaly allocated
 * options.
 */
START_TEST(test_update) {
	RCode result;
	long mask, i;
	ConfigStorage *conf1, *conf2;

	// create two configurations
	conf1 = config_create();
	conf2 = config_create();
	ck_assert( NULL != conf1 );
	ck_assert( NULL != conf2 );

	// prepare data into structures and update bit mask
	conf1->fullscreen = FALSE;
	conf1->recurse = 3;
	conf1->filename = strdup(SHORTSTRING);
	conf1->profile = strdup(SHORTSTRING);
	conf1->exclude = create_list(2, SHORTSTRING);

	conf2->fullscreen = TRUE;
	conf2->recurse = 5;
	conf2->filename = strdup(LONGSTRING);
	conf2->profile = strdup(LONGSTRING);
	conf2->exclude = create_list(2, LONGSTRING);

	mask = CONF_FILENAME | CONF_PROFILE | CONF_EXCLUDE | CONF_FULLSCREEN;
	result = config_update(conf1, conf2, mask);

	// check results

	// verify fields not in bitmap were not updated
	ck_assert_int_ne( conf1->recurse, conf2->recurse );

	// fields in bitmap were updated
	ck_assert_int_eq( conf1->fullscreen, conf2->fullscreen );

	ck_assert( conf1->filename != conf2->filename );
	ck_assert( conf1->profile != conf2->profile );
	ck_assert( conf1->exclude != conf2->exclude );

	ck_assert_str_eq( conf1->filename, conf2->filename );
	ck_assert_str_eq( conf1->profile, conf2->profile );
	ck_assert( (NULL==conf1->exclude) == (NULL==conf2->exclude) );

	if ( NULL != conf1->exclude ) {
		for (i = 0; NULL != conf1->exclude[i]; i++ ) {
			// check both are NULL or not NULL
			ck_assert( (NULL==conf1->exclude[i]) == (NULL==conf2->exclude[i]) );
			ck_assert( conf1->exclude[i] != conf2->exclude[i] );
			ck_assert_str_eq( conf1->exclude[i], conf2->exclude[i] );
		}
	}

	// deallocate configuration storages and NULL them to make sure any memory
	// not deallocated is lost
	result = config_destroy(conf1);
	ck_assert_int_eq( OK, result );

	result = config_destroy(conf2);
	ck_assert_int_eq( OK, result );

	conf1 = NULL;
	conf2 = NULL;
} END_TEST

/**
 * \brief Create test suite for config storage.
 * \return pointer to config storage test suite
 */
Suite* create_config_storage_suite (void) {

	Suite *s = suite_create ("ConfigStorage");

	TCase *tc_manage = tcase_create ("Manage");
	tcase_add_checked_fixture (tc_manage, common_setup, common_teardown);
	tcase_add_test (tc_manage, test_manage);
	tcase_add_test (tc_manage, test_update);

	suite_add_tcase (s, tc_manage);

	return s;
}

/** \} */
