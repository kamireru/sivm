/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <gdk/gdk.h>
#include "src/frontend/model_generic.h"
#include "src/frontend/model_application.h"

#include "test/common/common.h"
#include "test/frontend/test_model_generic.h"


static ModelGeneric *model_generic = NULL;

/**
 * \brief Test to create application model
 */
START_TEST(test_create_application_data) {

	ModelApplication *app_data = NULL;

	app_data = model_app_create();
	fail_if( NULL == app_data, NULL);

	fail_if( sizeof(*app_data) != sizeof(ModelApplication), NULL );
	fail_if( NULL != app_data->loop, NULL );

	fail_if( OK != model_app_destroy(app_data), NULL);

} END_TEST


/**
 * The function reset error buffer before the test and creates instance of model
 * to run tests over it.
 */
static void model_setup(void) {
	COLOR_SETUP_START;
	model_generic = model_create(MODEL_APPLICATION);

	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_SETUP_END;
	return ;
}

/**
 * The function reset error buffer and deallocate instance of model used in the
 * test (so that we don't need to reset it for next test).
 */
static void model_teardown(void) {
	COLOR_TEARDOWN_START;
	fail_unless( OK == model_unref(model_generic), NULL);

	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_TEARDOWN_END;
	return ;
}

/**
 * \brief Test setup of the GLIB loop to model
 */
START_TEST(test_setup_loop) {

	// get pointer from application model and check preconditions
	ModelApplication* app_data = model_app_get_data_pointer(model_generic);
	fail_if( NULL == app_data, NULL );
	fail_if( NULL != app_data->loop, NULL );

	model_app_set_loop(model_generic, (GMainLoop*)0x01);
	fail_if( (GMainLoop*)0x01 != app_data->loop, NULL );

} END_TEST

/**
 * \brief Create test suite for application model module.
 * \return pointer to application model suite
 */
Suite* create_model_application_suite (void) {

	Suite *suite = suite_create("ModelpAplication");

	TCase *tc_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_manage, common_setup, common_teardown);
	tcase_add_test(tc_manage, test_create_application_data);

	TCase *tc_setup = tcase_create("Setup");
	tcase_add_checked_fixture(tc_setup, model_setup, model_teardown);
	tcase_add_test(tc_setup, test_setup_loop);

	suite_add_tcase(suite, tc_manage);
	suite_add_tcase(suite, tc_setup);

	return suite;
}
