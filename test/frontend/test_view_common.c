/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <check.h>
#include "test/common/common.h"
#include "test/frontend/test_view_common.h"

ViewGeneric *view_generic = NULL;


/**
 * The function reset error buffer before the test and one generic view for
 * general testing.
 */
void common_setup_view_generic(void) {
	COLOR_SETUP_START;
	view_generic = view_create(VIEW_NONE);
	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_SETUP_END;
	return ;
}

/**
 * The function reset error buffer after the test.
 */
void common_teardown_view_generic(void) {
	COLOR_TEARDOWN_START;
	ck_assert_int_eq( OK, view_unref(view_generic) );
	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_TEARDOWN_END;
	return ;
}
