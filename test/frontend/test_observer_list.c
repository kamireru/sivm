/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "src/frontend/observer_list.h"
#include "src/frontend/view_generic.h"

#include "test/frontend/test_observer_list.h"
#include "test/common/common.h"


static ViewGeneric *generic_view1 = NULL;
static ViewGeneric *generic_view2 = NULL;
static ViewGeneric *generic_view3 = NULL;
static ViewGeneric *generic_view4 = NULL;
static ViewGeneric *generic_view5 = NULL;

/**
 * The function reset error buffer before the test and allocate some views to
 * use for tests.
 */
static void observer_setup(void)
{
	COLOR_SETUP_START;
	generic_view1 = view_create(VIEW_NONE);
	generic_view2 = view_create(VIEW_NONE);
	generic_view3 = view_create(VIEW_NONE);
	generic_view4 = view_create(VIEW_NONE);
	generic_view5 = view_create(VIEW_NONE);

	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_SETUP_END;
	return ;
}

/**
 * The function reset error buffer and deallocate views created for tests.
 */
static void observer_teardown(void)
{
	COLOR_TEARDOWN_START;
	fail_unless( OK == view_unref(generic_view1), NULL);
	fail_unless( OK == view_unref(generic_view2), NULL);
	fail_unless( OK == view_unref(generic_view3), NULL);
	fail_unless( OK == view_unref(generic_view4), NULL);
	fail_unless( OK == view_unref(generic_view5), NULL);

	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_TEARDOWN_END;
	return ;
}

/**
 * \brief Test creation and descruction of the empty list.
 *
 * This test cannot check if memory was deallocated or not. It should be used
 * together with tool specialized for memory leak discovery (for example
 * Valgrind)
 */
START_TEST(test_create_empty) {

	ObserverList *list = NULL;

	list = observer_create_list();
	fail_if( 0 != list->maxSize, NULL );
	fail_if( 0 != list->currentSize, NULL );
	fail_if( NULL != list->array, NULL );

	fail_if( FAIL == observer_destroy(list), NULL );

} END_TEST

/**
 * \brief Test creation and destruction of non empty list.
 *
 * This test cannot check if memory was deallocated or not. It should be used
 * together with tool specialized for memory leak discovery (for example
 * Valgrind)
 */
START_TEST(test_create_non_empty) {

	ObserverList *list = NULL;

	list = observer_create_list();
	fail_if( 0 != list->maxSize, NULL );
	fail_if( 0 != list->currentSize, NULL );
	fail_if( 0 != observer_get_count(list), NULL );
	fail_if( NULL != list->array, NULL );

	fail_if( OK != observer_insert(list, generic_view1) );
	fail_if( OBSERVER_LIST_INCREMENT != list->maxSize, NULL);
	fail_if( 1 != list->currentSize, NULL);
	fail_if( 1 != observer_get_count(list), NULL );

	fail_if( FAIL == observer_destroy(list), NULL );

} END_TEST


/**
 * \brief Test initial allocation of the observer list.
 *
 * The memory for observer view list is allocated wWhen the first view is added.
 * Initial size of the list is equal to OBSERVER_LIST_INCREMENT.
 */
START_TEST(test_manage_initial) {

	ObserverList *list = NULL;

	list = observer_create_list();

	fail_if( OK != observer_insert(list, generic_view1) );
	fail_if( OBSERVER_LIST_INCREMENT != list->maxSize, NULL);
	fail_if( 1 != list->currentSize, NULL);
	fail_if( 1 != observer_get_count(list), NULL );

	observer_destroy(list);
} END_TEST


/**
 * \brief Test reallocation of the observer list.
 *
 * The memory for observer view list reallocated when there is no room for a
 * view that is being inserted. Each reallocation adds memory for new
 * OBSERVER_LIST_INCREMENT elements.
 */
START_TEST(test_manage_realloc) {

	ObserverList *list = NULL;

	list = observer_create_list();

	observer_insert(list, generic_view1);
	observer_insert(list, generic_view2);
	observer_insert(list, generic_view3);
	observer_insert(list, generic_view4);

	fail_if( OK != observer_insert(list, generic_view5) );
	fail_if( 2*OBSERVER_LIST_INCREMENT != list->maxSize, NULL);
	fail_if( OBSERVER_LIST_INCREMENT + 1 != list->currentSize, NULL);
	fail_if( OBSERVER_LIST_INCREMENT + 1 != observer_get_count(list), NULL );

	observer_destroy(list);
} END_TEST


/**
 * \brief Test insert new observer view into list.
 *
 * This test tests addition of new observer view into the list when the view is
 * not present yet.
 */
START_TEST(test_insert) {

	ObserverList * list = observer_create_list();

	fail_if( FAIL == observer_insert(list, generic_view1), NULL);
	fail_if ( OBSERVER_LIST_INCREMENT != list->maxSize, NULL);
	fail_if( 1 != list->currentSize, NULL);
	fail_if( 1 != observer_get_count(list), NULL );
	fail_if( generic_view1 != list->array[0], NULL);
	fail_if( NULL != list->array[1], NULL);
	fail_if( NULL != list->array[2], NULL);
	fail_if( NULL != list->array[3], NULL);

	fail_if( FAIL == observer_insert(list, generic_view2), NULL);
	fail_if ( OBSERVER_LIST_INCREMENT != list->maxSize, NULL);
	fail_if( 2 != list->currentSize, NULL);
	fail_if( 2 != observer_get_count(list), NULL );
	fail_if( generic_view1 != list->array[0], NULL);
	fail_if( generic_view2 != list->array[1], NULL);
	fail_if( NULL != list->array[2], NULL);
	fail_if( NULL != list->array[3], NULL);

	fail_if( FAIL == observer_insert(list, generic_view3), NULL);
	fail_if ( OBSERVER_LIST_INCREMENT != list->maxSize, NULL);
	fail_if( 3 != list->currentSize, NULL);
	fail_if( 3 != observer_get_count(list), NULL );
	fail_if( generic_view1 != list->array[0], NULL);
	fail_if( generic_view2 != list->array[1], NULL);
	fail_if( generic_view3 != list->array[2], NULL);
	fail_if( NULL != list->array[3], NULL);

	fail_if( FAIL == observer_insert(list, generic_view4), NULL);
	fail_if ( OBSERVER_LIST_INCREMENT != list->maxSize, NULL);
	fail_if( 4 != list->currentSize, NULL);
	fail_if( 4 != observer_get_count(list), NULL );
	fail_if( generic_view1 != list->array[0], NULL);
	fail_if( generic_view2 != list->array[1], NULL);
	fail_if( generic_view3 != list->array[2], NULL);
	fail_if( generic_view4 != list->array[3], NULL);

	fail_if( FAIL == observer_insert(list, generic_view5), NULL);
	fail_if ( 2* OBSERVER_LIST_INCREMENT != list->maxSize, NULL);
	fail_if( 5 != list->currentSize, NULL);
	fail_if( 5 != observer_get_count(list), NULL );
	fail_if( generic_view1 != list->array[0], NULL);
	fail_if( generic_view2 != list->array[1], NULL);
	fail_if( generic_view3 != list->array[2], NULL);
	fail_if( generic_view4 != list->array[3], NULL);
	fail_if( generic_view5 != list->array[4], NULL);
	fail_if( NULL != list->array[5], NULL);
	fail_if( NULL != list->array[6], NULL);
	fail_if( NULL != list->array[7], NULL);

	observer_destroy(list);
} END_TEST


/**
 * \brief Test insertion of already present observer.
 *
 * This test tests addition of new observer view into the list when the view is
 * already present there.
 */
START_TEST(test_insert_duplicate) {

	ObserverList * list = observer_create_list();

	fail_if( FAIL == observer_insert(list, generic_view1), NULL);
	fail_if( FAIL == observer_insert(list, generic_view2), NULL);
	fail_if( FAIL == observer_insert(list, generic_view3), NULL);
	fail_if ( OBSERVER_LIST_INCREMENT != list->maxSize, NULL);
	fail_if( 3 != list->currentSize, NULL);
	fail_if( 3 != observer_get_count(list), NULL );
	fail_if( generic_view1 != list->array[0], NULL);
	fail_if( generic_view2 != list->array[1], NULL);
	fail_if( generic_view3 != list->array[2], NULL);
	fail_if( NULL != list->array[3], NULL);

	fail_if( FAIL == observer_insert(list, generic_view2), NULL);
	fail_if ( OBSERVER_LIST_INCREMENT != list->maxSize, NULL);
	fail_if( 3 != list->currentSize, NULL);
	fail_if( 3 != observer_get_count(list), NULL );
	fail_if( generic_view1 != list->array[0], NULL);
	fail_if( generic_view2 != list->array[1], NULL);
	fail_if( generic_view3 != list->array[2], NULL);
	fail_if( NULL != list->array[3], NULL);

	observer_destroy(list);
} END_TEST


/**
 * \brief Test present observer removal.
 *
 * This test tests removal of observer view that is stored in the list. Test
 * both removal from list end and from list center.
 */
START_TEST(test_remove_present) {

	ObserverList * list = observer_create_list();

	observer_insert(list, generic_view1);
	observer_insert(list, generic_view2);
	observer_insert(list, generic_view3);
	observer_insert(list, generic_view4);
	observer_insert(list, generic_view5);

	fail_if( FAIL == observer_remove(list, generic_view5), NULL);
	fail_if( 4 != list->currentSize, NULL);
	fail_if( 4 != observer_get_count(list), NULL );
	fail_if( NULL != list->array[4], NULL);

	fail_if( FAIL == observer_remove(list, generic_view2), NULL);
	fail_if( 3 != list->currentSize, NULL);
	fail_if( 3 != observer_get_count(list), NULL );
	fail_if( generic_view1 != list->array[0], NULL);
	fail_if( generic_view4 != list->array[1], NULL);
	fail_if( generic_view3 != list->array[2], NULL);
	fail_if( NULL != list->array[3], NULL);

	observer_destroy(list);
} END_TEST


/**
 * \brief Test removal of observer that is not in the list.
 *
 * This test tests removal of observer view from the list when the view is not
 * present there.
 */
START_TEST(test_remove_missing) {

	ObserverList * list = observer_create_list();

	observer_insert(list, generic_view1);
	observer_insert(list, generic_view2);
	observer_insert(list, generic_view3);

	fail_if( FAIL == observer_remove(list, generic_view5), NULL);
	fail_if( 3 != list->currentSize, NULL);
	fail_if( 3 != observer_get_count(list), NULL );
	fail_if( generic_view1 != list->array[0], NULL);
	fail_if( generic_view2 != list->array[1], NULL);
	fail_if( generic_view3 != list->array[2], NULL);

	observer_destroy(list);
} END_TEST

/**
 * \brief Test update notification to the views
 */
START_TEST(test_update_notification) {

	ObserverList * list = observer_create_list();

	observer_insert(list, generic_view1);
	observer_insert(list, generic_view2);
	observer_insert(list, generic_view3);

	fail_if( 0 != generic_view1->last_sequence, NULL );
	fail_if( 0 != generic_view2->last_sequence, NULL );
	fail_if( 0 != generic_view3->last_sequence, NULL );

	observer_update(list, 1);

	fail_if( 1 != generic_view1->last_sequence, NULL );
	fail_if( 1 != generic_view2->last_sequence, NULL );
	fail_if( 1 != generic_view3->last_sequence, NULL );

	observer_destroy(list);
} END_TEST


/**
 * \brief Create test suite for generic view module.
 * \return pointer to generic view suite
 */
Suite* create_observer_list_suite (void) {

	Suite *suite = suite_create("ObserverList");

	TCase *tc_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_manage, observer_setup, observer_teardown);
	tcase_add_test(tc_manage, test_create_empty);
	tcase_add_test(tc_manage, test_create_non_empty);
	tcase_add_test(tc_manage, test_manage_initial);
	tcase_add_test(tc_manage, test_manage_realloc);

	TCase *tc_content = tcase_create("Content");
	tcase_add_checked_fixture(tc_content, observer_setup, observer_teardown);
	tcase_add_test(tc_content, test_insert);
	tcase_add_test(tc_content, test_insert_duplicate);
	tcase_add_test(tc_content, test_remove_present);
	tcase_add_test(tc_content, test_remove_missing);
	tcase_add_test(tc_content, test_update_notification);

	suite_add_tcase(suite, tc_manage);
	suite_add_tcase(suite, tc_content);

	return suite;
}
