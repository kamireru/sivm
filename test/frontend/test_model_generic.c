/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "src/frontend/view_generic.h"
#include "src/frontend/model_generic.h"
#include "src/frontend/observer_list.h"

#include "test/common/common.h"
#include "test/frontend/test_model_generic.h"


static ModelGeneric *model_generic = NULL;
static ViewGeneric *view_generic = NULL;

/**
 * \brief Test to create generic model
 *
 * The test create and destroy generic model with type MODEL_NONE - ie no specific
 * model stuff is created and destroyed.
 */
START_TEST(test_create_none) {

	ModelGeneric* model = NULL;

	model = model_create(MODEL_NONE);
	ck_assert( NULL != model );
	ck_assert( MODEL_NONE == model->type );
	ck_assert( NULL != model->view_list );
	ck_assert( NULL == model->specific_data );
	ck_assert_int_eq( 1, model->sequence );
	ck_assert_int_eq( 1, model->reference );

	fail_if( OK == model_destroy(model), NULL);
	fail_if( ! error_is_error(), NULL );
	error_reset();

	fail_if( OK != model_unref(model), NULL );

} END_TEST

/**
 * \brief Test to create generic model
 *
 * The test create and destroy generic model with type MODEL_APPLICATION
 */
START_TEST(test_create_application) {

	ModelGeneric* model = NULL;

	model = model_create(MODEL_APPLICATION);
	ck_assert( NULL != model );
	ck_assert( MODEL_APPLICATION == model->type );
	ck_assert( NULL != model->view_list );
	ck_assert( NULL != model->specific_data );
	ck_assert_int_eq( 1, model->sequence );
	ck_assert_int_eq( 1, model->reference );

	fail_if( OK == model_destroy(model), NULL);
	fail_if( ! error_is_error(), NULL );
	error_reset();

	fail_if( OK != model_unref(model), NULL );

} END_TEST


/**
 * The function reset error buffer before the test and creates instance of model
 * to run tests over it.
 */
static void model_setup(void) {
	COLOR_SETUP_START;
	model_generic = model_create(MODEL_NONE);
	view_generic = view_create(VIEW_NONE);

	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_SETUP_END;
	return ;
}

/**
 * The function reset error buffer and deallocate instance of model used in the
 * test (so that we don't need to reset it for next test).
 */
static void model_teardown(void) {
	COLOR_TEARDOWN_START;
	fail_unless( OK == model_unref(model_generic), NULL);
	fail_unless( OK == view_unref(view_generic), NULL);

	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_TEARDOWN_END;
	return ;
}

/**
 * \brief Test addition and removal of observer.
 *
 * Add and remove view to the models list of observers. Verify that the view
 * sequence number is increase when the view is added to the model.
 */
START_TEST(test_view_manage) {
	RCode result;

	ck_assert_int_eq( 0, view_generic->last_sequence );
	ck_assert_int_eq( 0, observer_get_count(model_generic->view_list) );

	result = model_insert_view(model_generic, view_generic);
	ck_assert_int_eq( OK, result );
	ck_assert_int_eq( 1, view_generic->last_sequence );
	ck_assert_int_eq( 1, observer_get_count(model_generic->view_list) );

	result = model_remove_view(model_generic, view_generic);
	ck_assert_int_eq( OK, result );
	ck_assert_int_eq( 1, view_generic->last_sequence );
	ck_assert_int_eq( 0, observer_get_count(model_generic->view_list) );

} END_TEST

/**
 * \brief Test addition and removal of observer.
 *
 * Verify that inserting view into model calls update on the view and increases
 * the sequence number. Any subsequent call to model_update also increases the
 * sequence number.
 */
START_TEST(test_view_update) {
	RCode result;

	ck_assert_int_eq( 0, view_generic->last_sequence );

	model_insert_view(model_generic, view_generic);
	ck_assert_int_eq( 1, view_generic->last_sequence );

	model_generate_sequence(model_generic);
	ck_assert_int_eq( 2, model_generic->sequence );
	ck_assert_int_eq( 1, view_generic->last_sequence );

	model_update(model_generic);
	ck_assert_int_eq( 2, view_generic->last_sequence );

} END_TEST

/**
 * \brief Test sequence number generation.
 *
 * Verify that sequence number is correctly generated.
 */
START_TEST(test_generate_sequence) {

	// simulate seed for sequence number
	model_generic->sequence = 0;
	fail_if( OK != model_generate_sequence(model_generic), NULL );
	fail_if( 1 != model_generic->sequence, NULL );

	// simulate seed for sequence number
	model_generic->sequence = 5;
	fail_if( OK != model_generate_sequence(model_generic), NULL );
	fail_if( 6 != model_generic->sequence, NULL );


	// simulate sequence number that will result in buffer overflow and reset to zero
	model_generic->sequence = 0 - 1;
	fail_if( OK != model_generate_sequence(model_generic), NULL );
	fail_if( 1 != model_generic->sequence, NULL );


} END_TEST


/**
 * \brief Create test suite for generic model module.
 * \return pointer to generic model suite
 */
Suite* create_model_generic_suite (void) {

	Suite *suite = suite_create("Model");

	// test case to hold specific view creations
	TCase *tc_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_manage, common_setup, common_teardown);
	tcase_add_test(tc_manage, test_create_none);
	tcase_add_test(tc_manage, test_create_application);

	// test case to hold generic model functionality
	TCase *tc_update = tcase_create("Internal");
	tcase_add_checked_fixture(tc_update, model_setup, model_teardown);
	tcase_add_test(tc_update, test_view_manage);
	tcase_add_test(tc_update, test_view_update);
	tcase_add_test(tc_update, test_generate_sequence);

	suite_add_tcase(suite, tc_manage);
	suite_add_tcase(suite, tc_update);

	return suite;
}
