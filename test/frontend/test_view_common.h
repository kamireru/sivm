/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _TEST_FRONTEND_TEST_VIEW_COMMON_H_
#define _TEST_FRONTEND_TEST_VIEW_COMMON_H_

#include "src/frontend/view_generic.h"

/** \brief Variable holding generic view pointer in view tests. */
extern ViewGeneric *view_generic;

/** \brief Common setup function for generic view creation. */
extern void common_setup_view_generic(void);

/** \brief Common teardown function for generic view destruction. */
extern void common_teardown_view_generic(void);

#endif
