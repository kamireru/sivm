/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>
#include "src/frontend/view_list_status.h"

#include "test/common/common.h"
#include "test/frontend/test_view_common.h"
#include "test/frontend/test_view_list_status.h"


/**
 * \brief Test creation of list status view.
 *
 * The function test creation of list status view with valid generic view as
 * parent.
 */
START_TEST(test_create_list_status_data) {

	ViewListStatus *lsv_data = NULL;

	lsv_data = view_lsv_create(view_generic);
	ck_assert( NULL != lsv_data );
	ck_assert_int_eq( sizeof(*lsv_data), sizeof(ViewListStatus) );

	ck_assert_int_eq( VIEW_STATUS_POSITION, lsv_data->status_type );

	ck_assert_int_ne( 0, lsv_data->text_size.x );
	ck_assert_int_ne( 0, lsv_data->text_size.y );
	ck_assert_int_eq( 5, lsv_data->border_size.x );
	ck_assert_int_eq( 2, lsv_data->border_size.y );

	ck_assert_int_eq( 0, lsv_data->color.bg.red );
	ck_assert_int_eq( 0, lsv_data->color.bg.green );
	ck_assert_int_eq( 0, lsv_data->color.bg.blue );

	ck_assert_int_eq( 65535, lsv_data->color.fg.red );
	ck_assert_int_eq( 65535, lsv_data->color.fg.green );
	ck_assert_int_eq( 65535, lsv_data->color.fg.blue );

	ck_assert( NULL != lsv_data->font );

	ck_assert_int_eq(  0, view_generic->region_hint.x );
	ck_assert_int_eq(
			view_generic->region_hint.y,
			lsv_data->text_size.y + 2 * lsv_data->border_size.y
		);


	fail_if( OK != view_lsv_destroy(lsv_data), NULL);

} END_TEST

/**
 * \brief Create test suite for list status view module.
 * \return pointer to generic view suite
 */
Suite* create_view_list_status_suite (void) {

	Suite *suite = suite_create("ViewListStatus");

	TCase *tc_create = tcase_create("Create");
	tcase_add_checked_fixture(tc_create, common_setup_view_generic, common_teardown_view_generic);
	tcase_add_test(tc_create, test_create_list_status_data);

	suite_add_tcase(suite, tc_create);

	return suite;
}
