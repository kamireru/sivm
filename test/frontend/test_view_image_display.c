/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>
#include "src/frontend/view_generic.h"
#include "src/frontend/view_application.h"
#include "src/frontend/view_image_display.h"

#include "test/common/common.h"
#include "test/frontend/test_view_common.h"
#include "test/frontend/test_view_image_display.h"


/**
 * \brief Test creation of image display view.
 *
 * The function test creation of image display view with valid generic view as
 * parent.
 */
START_TEST(test_create_image_display_data) {

	ViewImageDisplay *idv_data = NULL;

	idv_data = view_idv_create(view_generic);
	ck_assert( NULL != idv_data );
	ck_assert_int_eq( sizeof(*idv_data), sizeof(ViewImageDisplay) );

	ck_assert( NULL == idv_data->view_buffer );
	ck_assert( 1.0 == idv_data->zoom_factor );
	ck_assert_int_eq( ZOOM_FIT_CUSTOM, idv_data->zoom_type );

	ck_assert_int_eq( 0, idv_data->dimension.x );
	ck_assert_int_eq( 0, idv_data->dimension.y );

	ck_assert_int_eq( 0, idv_data->offset.x );
	ck_assert_int_eq( 0, idv_data->offset.y );

	ck_assert_int_eq( 0, idv_data->viewport.x );
	ck_assert_int_eq( 0, idv_data->viewport.y );
	ck_assert_int_eq( 0, idv_data->viewport.width );
	ck_assert_int_eq( 0, idv_data->viewport.height );

	fail_if( OK != view_idv_destroy(idv_data), NULL);

} END_TEST

/**
 * \brief Setup image display view for view tests
 *
 * The function creates single image display view so it can be used in the
 * tests.
 */
static void setup_image_view(void) {
	COLOR_SETUP_START;

	view_generic = view_create(VIEW_IMAGE_DISPLAY);

	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_SETUP_END;
	return ;
}

/**
 * \brief Teardown image display view after test
 *
 * The function can be used for teardown after both recomputation and view
 * management tests.
 */
static void teardown_image_view(void) {
	COLOR_TEARDOWN_START;

	ck_assert( OK == view_unref(view_generic) );

	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_TEARDOWN_END;
	return ;
}

/**
 * Structure holding input and output data for recomputation tests.
 *
 * It contains data to prepare into view structure, result values of function
 * invocations and data that should be in structure after the computation.
 *
 * The in_view serves as input for both view->region and
 * view->spec_data->viewport.
 */
struct RecomputeTestData {
		ZoomType type;
		GdkRectangle in_view;
		GdkPoint in_dimension;
		float zoom;
		gboolean zoom_result;
		gboolean viewport_result;
		GdkRectangle out_view;
	};

/*
 * Define variables holding test data for zoom and viewport recomputation
 * looping test.
 */
#define TEST_VIEWPORT_COUNT 17
struct RecomputeTestData test_viewport_data[TEST_VIEWPORT_COUNT] = {
		{ ZOOM_FIT_CUSTOM, {10, 10, 200, 300}, {640, 480}, 1.0f, FALSE, FALSE, {10, 10, 200, 300} },
		{ ZOOM_FIT_CUSTOM, {10, 10, 200, 300}, {150, 250}, 1.0f, FALSE, TRUE,  {35, 35, 150, 250} },
		{ ZOOM_FIT_CUSTOM, {10, 10, 200, 300}, {640, 480}, 2.0f, FALSE, FALSE, {10, 10, 200, 300} },
		{ ZOOM_FIT_CUSTOM, {10, 10, 200, 300}, {50,   80}, 2.0f, FALSE, TRUE,  {60, 80, 100, 160} },
		{ ZOOM_FIT_CUSTOM, {10, 10, 200, 300}, {640, 480}, 0.5f, FALSE, TRUE,  {10, 40, 200, 240} },

		{ ZOOM_FIT_WIDTH, {0, 0, 300, 200}, {750, 480}, 0.4f, TRUE, TRUE,  {0, 4, 300, 192} },
		{ ZOOM_FIT_WIDTH, {0, 0, 300, 200}, {200, 480}, 1.5f, TRUE, FALSE, {0, 0, 300, 200} },

		{ ZOOM_FIT_HEIGHT, {0, 0, 300, 200}, {640, 500}, 0.4f,  TRUE,  TRUE, {22, 0, 256, 200} },
		{ ZOOM_FIT_HEIGHT, {0, 0, 300, 200}, {640, 160}, 1.25f, TRUE, FALSE, {0,  0, 300, 200} },

		{ ZOOM_FIT_IMAGE, {0, 0, 300, 200}, {750, 400}, 0.4f,  TRUE, TRUE, {0,  20, 300, 160} },
		{ ZOOM_FIT_IMAGE, {0, 0, 300, 200}, {300, 500}, 0.4f,  TRUE, TRUE, {90,  0, 120, 200} },
		{ ZOOM_FIT_IMAGE, {0, 0, 600, 400}, {400, 200}, 1.5f,  TRUE, TRUE, {0,  50, 600, 300} },
		{ ZOOM_FIT_IMAGE, {0, 0, 600, 400}, {400, 320}, 1.25f, TRUE, TRUE, {50,  0, 500, 400} },

		// test zoom value limiting when computing initial zoom
		{ ZOOM_FIT_WIDTH,  {0, 0, 1, 50}, {1000, 50},  0.01f, TRUE, TRUE, {  0,  25,   1,   0} },
		{ ZOOM_FIT_WIDTH,  {0, 0, 1000, 50}, {1, 50}, 200.0f, TRUE, TRUE, {400,   0, 200,  50} },
		{ ZOOM_FIT_HEIGHT, {0, 0, 50, 1}, {50, 1000},  0.01f, TRUE, TRUE, { 25,   0,   0,   1} },
		{ ZOOM_FIT_HEIGHT, {0, 0, 50, 1000}, {50, 1}, 200.0f, TRUE, TRUE, {  0, 400,  50, 200} }
	};

/**
 * \brief Test region recomputation function for all zoom types
 *
 * The function is executed in loop for each record in variable
 * test_viewport_data.
 *
 * The zoom variable can be input or output variable depending on the zoom type
 * used. The fit original use zoom stored in structure and compute boundaries.
 * The other first compute zoom and then compute region boundaries.
 *
 * Since it is difficult to compate floats (due to their lack of precision), we
 * check the resulting zoom for equality on 6 decimal places only
 */
START_TEST(test_compute_viewport) {
	ViewImageDisplay *idv_data;
	gboolean result;
	int decimal = 1000000;

	// get pointer to generic view internal data
	idv_data = view_idv_get_data_pointer(view_generic);
	ck_assert( NULL != idv_data );

	// setup input values for test
	view_generic->region.x = test_viewport_data[_i].in_view.x;
	view_generic->region.y = test_viewport_data[_i].in_view.y;
	view_generic->region.width = test_viewport_data[_i].in_view.width;
	view_generic->region.height = test_viewport_data[_i].in_view.height;

	idv_data->dimension.x = test_viewport_data[_i].in_dimension.x;
	idv_data->dimension.y = test_viewport_data[_i].in_dimension.y;

	idv_data->viewport.x = test_viewport_data[_i].in_view.x;
	idv_data->viewport.y = test_viewport_data[_i].in_view.y;
	idv_data->viewport.width = test_viewport_data[_i].in_view.width;
	idv_data->viewport.height = test_viewport_data[_i].in_view.height;

	idv_data->zoom_type = test_viewport_data[_i].type;
	if ( ZOOM_FIT_CUSTOM == test_viewport_data[_i].type ) {
		idv_data->zoom_factor = test_viewport_data[_i].zoom;
	} else {
		idv_data->zoom_factor = 0.0f;
	}

	// execute recomputation
	result = view_idv_compute_factor(view_generic, idv_data);
	ck_assert_int_eq( test_viewport_data[_i].zoom_result, result );

	result = view_idv_compute_viewport(view_generic, idv_data);
	ck_assert_int_eq( test_viewport_data[_i].viewport_result, result );

	// verify output values for test
	ck_assert(
			ZOOM_EQUAL( test_viewport_data[_i].zoom, idv_data->zoom_factor )
		);

	ck_assert_int_eq( test_viewport_data[_i].out_view.x, idv_data->viewport.x);
	ck_assert_int_eq( test_viewport_data[_i].out_view.y, idv_data->viewport.y);
	ck_assert_int_eq( test_viewport_data[_i].out_view.width, idv_data->viewport.width);
	ck_assert_int_eq( test_viewport_data[_i].out_view.height, idv_data->viewport.height);

} END_TEST

/**
 * Structure holding input and output data for offset recomputation tests.
 */
struct OffsetTestData {
		ZoomType in_type;
		PanDirection in_direction;
		GdkRectangle in_view;
		GdkPoint in_dimension;
		GdkPoint in_offset;
		gdouble in_zoom;
		gboolean out_result;
		GdkPoint out_offset;
	};

/*
 * Define variables holding test data for offset computation looping
 * test.
 */
#define TEST_OFFSET_COUNT 18
struct OffsetTestData test_offset_data[TEST_OFFSET_COUNT] = {
		{ ZOOM_FIT_CUSTOM, PAN_UP, {0, 0, 300, 200}, {640, 480}, {0,   0}, 1.00f, FALSE, {0,   0} },
		{ ZOOM_FIT_CUSTOM, PAN_UP, {0, 0, 300, 200}, {640, 480}, {0, 240}, 1.00f, TRUE,  {0, 120} },

		{ ZOOM_FIT_CUSTOM, PAN_DOWN, {0, 0, 300, 200}, {640, 480}, {0,   0}, 1.00f, TRUE,  {0, 120} },
		{ ZOOM_FIT_CUSTOM, PAN_DOWN, {0, 0, 300, 200}, {640, 480}, {0, 200}, 1.00f, TRUE,  {0, 280} },
		{ ZOOM_FIT_CUSTOM, PAN_DOWN, {0, 0, 300, 200}, {640, 480}, {0, 280}, 1.00f, FALSE, {0, 280} },
		{ ZOOM_FIT_CUSTOM, PAN_DOWN, {0, 0, 300, 200}, {640, 480}, {0, 280}, 2.00f, TRUE, {0, 400} },
		{ ZOOM_FIT_CUSTOM, PAN_DOWN, {0, 0, 300, 200}, {640, 480}, {0, 280}, 0.50f, TRUE, {0,  40} },

		{ ZOOM_FIT_CUSTOM, PAN_LEFT, {0, 0, 300, 200}, {640, 480}, {  0, 0}, 1.00f, FALSE, {  0, 0} },
		{ ZOOM_FIT_CUSTOM, PAN_LEFT, {0, 0, 300, 200}, {640, 480}, {300, 0}, 1.00f, TRUE,  {120, 0} },

		{ ZOOM_FIT_CUSTOM, PAN_RIGHT, {0, 0, 300, 200}, {640, 480}, {  0, 0}, 1.00f, TRUE,  {180, 0} },
		{ ZOOM_FIT_CUSTOM, PAN_RIGHT, {0, 0, 300, 200}, {640, 480}, {300, 0}, 1.00f, TRUE,  {340, 0} },
		{ ZOOM_FIT_CUSTOM, PAN_RIGHT, {0, 0, 300, 200}, {640, 480}, {340, 0}, 1.00f, FALSE, {340, 0} },
		{ ZOOM_FIT_CUSTOM, PAN_RIGHT, {0, 0, 300, 200}, {640, 480}, {500, 0}, 2.00f, TRUE, {680, 0} },
		{ ZOOM_FIT_CUSTOM, PAN_RIGHT, {0, 0, 300, 200}, {640, 480}, {500, 0}, 0.50f, TRUE, { 20, 0} },

		// test that offset won't became negative when panning picture zoomed to fit whole image
		{ ZOOM_FIT_IMAGE, PAN_RIGHT, {0, 0, 400, 320}, {480, 640}, {0, 0}, 0.50f, FALSE, {0, 0} },
		{ ZOOM_FIT_IMAGE, PAN_LEFT,  {0, 0, 400, 320}, {480, 640}, {0, 0}, 0.50f, FALSE, {0, 0} },
		{ ZOOM_FIT_IMAGE, PAN_UP,    {0, 0, 320, 400}, {640, 480}, {0, 0}, 0.50f, FALSE, {0, 0} },
		{ ZOOM_FIT_IMAGE, PAN_DOWN,  {0, 0, 320, 400}, {640, 480}, {0, 0}, 0.50f, FALSE, {0, 0} }
	};

/**
 * \brief Test offset recomputation
 *
 * The function is executed in loop for each record in variable
 * test_offset_data. See OffsetTestData structure definition to check input and
 * output values.
 */
START_TEST(test_compute_offset) {
	ViewImageDisplay *idv_data;
	gboolean result;

	// get pointer to generic view internal data
	idv_data = view_idv_get_data_pointer(view_generic);
	ck_assert( NULL != idv_data );

	// setup input values for test
	idv_data->zoom_type = test_offset_data[_i].in_type;

	view_generic->region.x = test_offset_data[_i].in_view.x;
	view_generic->region.y = test_offset_data[_i].in_view.y;
	view_generic->region.width = test_offset_data[_i].in_view.width;
	view_generic->region.height = test_offset_data[_i].in_view.height;

	idv_data->dimension.x = test_offset_data[_i].in_dimension.x;
	idv_data->dimension.y = test_offset_data[_i].in_dimension.y;

	idv_data->offset.x = test_offset_data[_i].in_offset.x;
	idv_data->offset.y = test_offset_data[_i].in_offset.y;

	idv_data->zoom_factor = test_offset_data[_i].in_zoom;

	// execute recomputation
	result = view_idv_compute_offset(
			view_generic, idv_data, test_offset_data[_i].in_direction
		);
	ck_assert_int_eq( test_offset_data[_i].out_result, result );

	// verify output values for test
	ck_assert_int_eq( test_offset_data[_i].out_offset.x, idv_data->offset.x);
	ck_assert_int_eq( test_offset_data[_i].out_offset.y, idv_data->offset.y);

} END_TEST

/**
 * Structure holding input and output data for offset and factor recomputation
 * during zoom.
 */
struct ZoomTestData {
		ZoomDirection in_direction;
		GdkRectangle in_viewport;
		GdkPoint in_dimension;
		GdkPoint in_offset;
		gdouble in_zoom;
		gboolean out_result;
		gdouble out_zoom;
		GdkPoint out_offset;
	};

/*
 * Define variables holding test data for offset computation looping
 * test.
 */
#define TEST_ZOOM_COUNT 12
struct ZoomTestData test_zoom_data[TEST_ZOOM_COUNT] = {
		{ ZOOM_DIRECTION_IN, {0, 0, 300, 200}, {640, 480}, {  0,   0}, 1.00f, TRUE, 1.25f, {0,    0} },
		{ ZOOM_DIRECTION_IN, {0, 0, 300, 200}, {640, 480}, {100,  60}, 1.00f, TRUE, 1.25f, {125, 75} },

		{ ZOOM_DIRECTION_OUT, {0, 0, 300, 200}, {640, 480}, {  0,   0}, 1.00f, TRUE, 0.8f, {  0,   0} },
		{ ZOOM_DIRECTION_OUT, {0, 0, 300, 200}, {640, 480}, {100,  50}, 1.00f, TRUE, 0.8f, { 80,  40} },
		{ ZOOM_DIRECTION_OUT, {0, 0, 300, 200}, {640, 480}, {340, 280}, 1.00f, TRUE, 0.8f, {212, 184} },
		// when image stops filling whole viewport and starts to be centered
		{ ZOOM_DIRECTION_OUT, {0, 0, 300, 200}, {600, 400}, {  0,   0}, 0.50f, TRUE, 0.4f, {  0,   0} },

		{ ZOOM_DIRECTION_RESET, {0, 0, 300, 200}, {640, 480}, {  0,   0}, 1.00f, FALSE, 1.00f, {0,  0} },
		{ ZOOM_DIRECTION_RESET, {0, 0, 300, 200}, {640, 480}, {  0,   0}, 1.25f, TRUE , 1.00f, {0,  0} },
		{ ZOOM_DIRECTION_RESET, {0, 0, 300, 200}, {640, 480}, {100, 100}, 1.00f, TRUE,  1.00f, {0,  0} },
		{ ZOOM_DIRECTION_RESET, {0, 0, 300, 200}, {640, 480}, {100, 100}, 1.25f, TRUE,  1.00f, {0,  0} },

		// test minimum and maximum zoom value limiting
		{ ZOOM_DIRECTION_IN, {0, 0, 300, 200}, {640, 480}, {  0,   0}, 200.0f, FALSE, 200.0f, {0, 0} },
		{ ZOOM_DIRECTION_OUT, {0, 0, 300, 200}, {640, 480}, {  0,   0}, 0.01f, FALSE,  0.01f, {0, 0} }
	};

/**
 * \brief Test offset recomputation
 *
 * The function is executed in loop for each record in variable
 * test_offset_data. See OffsetTestData structure definition to check input and
 * output values.
 */
START_TEST(test_compute_zoom) {
	ViewImageDisplay *idv_data;
	gboolean result;
	int decimal = 1000000;

	// get pointer to generic view internal data
	idv_data = view_idv_get_data_pointer(view_generic);
	ck_assert( NULL != idv_data );

	// setup input values for test
	idv_data->zoom_type = ZOOM_FIT_CUSTOM;

	idv_data->viewport.x = test_zoom_data[_i].in_viewport.x;
	idv_data->viewport.y = test_zoom_data[_i].in_viewport.y;
	idv_data->viewport.width = test_zoom_data[_i].in_viewport.width;
	idv_data->viewport.height = test_zoom_data[_i].in_viewport.height;

	idv_data->dimension.x = test_zoom_data[_i].in_dimension.x;
	idv_data->dimension.y = test_zoom_data[_i].in_dimension.y;

	idv_data->offset.x = test_zoom_data[_i].in_offset.x;
	idv_data->offset.y = test_zoom_data[_i].in_offset.y;

	idv_data->zoom_factor = test_zoom_data[_i].in_zoom;

	// execute recomputation
	result = view_idv_compute_zoom(
			view_generic, idv_data, test_zoom_data[_i].in_direction
		);
	ck_assert_int_eq( test_zoom_data[_i].out_result, result );

	// verify output values for test
	ck_assert(
			ZOOM_EQUAL( test_zoom_data[_i].out_zoom, idv_data->zoom_factor )
		);
	ck_assert_int_eq( test_zoom_data[_i].out_offset.x, idv_data->offset.x);
	ck_assert_int_eq( test_zoom_data[_i].out_offset.y, idv_data->offset.y);

} END_TEST


/**
 * \brief Test view region buffer creation and resizing
 *
 * Verify the buffer is correctly created when there is no buffer yet and that
 * it is resized when dimensions change. When dimensions do not change, then it
 * is not resized.
 */
START_TEST(test_view_resize) {
	GdkRectangle region = { 5, 10, 100, 200 };
	GdkPixbuf *pointer = NULL;
	ViewImageDisplay *idv_data = NULL;

	// get pointer to internal data and verify starting conditions
	idv_data = view_idv_get_data_pointer(view_generic);
	ck_assert( NULL != idv_data );
	ck_assert( NULL == idv_data->view_buffer );
	ck_assert_int_eq( 0, view_generic->region.width );
	ck_assert_int_eq( 0, view_generic->region.height );

	// verify position and dimension is updated on resize
	// verify buffer is not null and its dimensions match region
	ck_assert_int_eq( OK, view_resize(view_generic, region) );
	ck_assert_int_eq(  5, view_generic->region.x );
	ck_assert_int_eq( 10, view_generic->region.y );
	ck_assert_int_eq( 100, view_generic->region.width );
	ck_assert_int_eq( 200, view_generic->region.height );

	ck_assert( NULL != idv_data->view_buffer );
	ck_assert_int_eq( 100, gdk_pixbuf_get_width(idv_data->view_buffer) );
	ck_assert_int_eq( 200, gdk_pixbuf_get_height(idv_data->view_buffer) );


	// verify no change to buffer if dimensions do not change
	pointer = idv_data->view_buffer;
	region.x = 20;
	region.y = 25;

	ck_assert_int_eq( OK, view_resize(view_generic, region) );
	ck_assert_int_eq( 20, view_generic->region.x );
	ck_assert_int_eq( 25, view_generic->region.y );
	ck_assert_int_eq( 100, view_generic->region.width );
	ck_assert_int_eq( 200, view_generic->region.height );

	ck_assert( NULL != idv_data->view_buffer );
	ck_assert( pointer == idv_data->view_buffer );
	ck_assert_int_eq( 100, gdk_pixbuf_get_width(idv_data->view_buffer) );
	ck_assert_int_eq( 200, gdk_pixbuf_get_height(idv_data->view_buffer) );


	// verify buffer is reallocated when dimensions differ
	pointer = idv_data->view_buffer;
	region.width = 120;
	region.height = 210;

	ck_assert_int_eq( OK, view_resize(view_generic, region) );
	ck_assert_int_eq( 20, view_generic->region.x );
	ck_assert_int_eq( 25, view_generic->region.y );
	ck_assert_int_eq( 120, view_generic->region.width );
	ck_assert_int_eq( 210, view_generic->region.height );

	ck_assert( NULL != idv_data->view_buffer );
	ck_assert( pointer != idv_data->view_buffer );
	ck_assert_int_eq( 120, gdk_pixbuf_get_width(idv_data->view_buffer) );
	ck_assert_int_eq( 210, gdk_pixbuf_get_height(idv_data->view_buffer) );

} END_TEST

/**
 * \brief Create test suite for image display view module.
 * \return pointer to generic view suite
 */
Suite* create_view_image_display_suite (void) {

	Suite *suite = suite_create("ViewImageDisplay");

	TCase *tc_create = tcase_create("Create");
	tcase_add_checked_fixture(tc_create, common_setup_view_generic, common_teardown_view_generic);
	tcase_add_test(tc_create, test_create_image_display_data);

	TCase *tc_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_manage, setup_image_view, teardown_image_view);
	tcase_add_test(tc_manage, test_view_resize);

	TCase *tc_compute = tcase_create("Recompute");
	tcase_add_checked_fixture(tc_compute, setup_image_view, teardown_image_view);
	tcase_add_loop_test(tc_compute, test_compute_viewport, 0, TEST_VIEWPORT_COUNT);
	tcase_add_loop_test(tc_compute, test_compute_offset, 0, TEST_OFFSET_COUNT);
	tcase_add_loop_test(tc_compute, test_compute_zoom, 0, TEST_ZOOM_COUNT);

	suite_add_tcase(suite, tc_create);
	suite_add_tcase(suite, tc_manage);
	suite_add_tcase(suite, tc_compute);

	return suite;
}
