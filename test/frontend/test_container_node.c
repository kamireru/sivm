/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>
#include "src/frontend/view_generic.h"
#include "src/frontend/container_node.h"

#include "test/common/common.h"
#include "test/frontend/test_container_node.h"

/**
 * \brief Test creation of container leaf.
 */
START_TEST(test_create_leaf) {
	RCode result;
	ContainerNode* node = NULL;

	node = container_node_create(CONTAINER_LEAF, NODE_HORIZONTAL);
	ck_assert( NULL != node );
	ck_assert( NULL == node->parent );
	ck_assert_int_eq( CONTAINER_LEAF, node->type );

	ck_assert( NULL == node->data.leaf.view );
	ck_assert_int_eq( FALSE, node->data.leaf.dirty );

	result = container_node_destroy(node);
	ck_assert_int_eq( OK, result );
} END_TEST

/**
 * \brief Test creation of container node.
 *
 * Test creation of single node and its destruction. The node does not have any
 * childs in this test.
 */
START_TEST(test_create_node) {
	RCode result;
	ContainerNode* node = NULL;

	node = container_node_create(CONTAINER_NODE, NODE_HORIZONTAL);
	ck_assert( NULL != node );
	ck_assert( NULL == node->parent );
	ck_assert_int_eq( CONTAINER_NODE, node->type );

	ck_assert_int_eq( NODE_HORIZONTAL, node->data.node.direction );
	ck_assert_int_eq( 0, node->data.node.size );
	ck_assert( NULL == node->data.node.list );

	result = container_node_destroy(node);
	ck_assert_int_eq( OK, result );
} END_TEST

/**
 * \brief Test deallocation of deep node structure
 *
 * Allocate at least two level deep node structure and then deallocate it. Check
 * memory allocation using valgrind.
 */
START_TEST(test_destroy_node) {
	guint i;
	RCode result;
	ContainerNode *master = NULL;
	ContainerNode *child[4] = { NULL };

	// create master node into which we will insert
	master = container_node_create(CONTAINER_NODE, NODE_HORIZONTAL);
	ck_assert( NULL != master );

	for (i = 0; i < 4; i++ ) {
		child[i] = container_node_create(CONTAINER_LEAF, NODE_HORIZONTAL);
		ck_assert( NULL != child[i] );

		result = container_node_insert(master, child[i], i);
		ck_assert_int_eq( OK, result );
	}

	// use recursive destruction of child nodes
	result = container_node_destroy(master);
	ck_assert_int_eq( OK, result );
} END_TEST

/**
 * \brief Test insertion of new child node to the container node.
 *
 * The function tests insertion into empty child list, at the start of non-empty
 * child list, at the end of child list and in the middle of child list.
 */
START_TEST(test_manage_insert) {
	RCode result;
	ContainerNode *master = NULL;
	ContainerNode *child1 = NULL, *child2 = NULL;
	ContainerNode *child3 = NULL, *child4 = NULL;

	// create master node into which we will insert
	master = container_node_create(CONTAINER_NODE, NODE_HORIZONTAL);
	ck_assert( NULL != master );

	// create and insert node to the empty list
	child2 = container_node_create(CONTAINER_LEAF, NODE_HORIZONTAL);
	ck_assert( NULL != child2 );

	result = container_node_insert(master, child2, 0);
	ck_assert_int_eq( OK, result );
	ck_assert( master == child2->parent );
	ck_assert_int_eq( 1, container_node_get_size(master) );
	ck_assert( NULL != master->data.node.list );
	ck_assert( child2 == container_node_get_child(master, 0) );

	// create and insert node to the start of the list
	child1 = container_node_create(CONTAINER_LEAF, NODE_HORIZONTAL);
	ck_assert( NULL != child1 );

	result = container_node_insert(master, child1, 0);
	ck_assert_int_eq( OK, result );
	ck_assert( master == child1->parent );
	ck_assert_int_eq( 2, container_node_get_size(master) );
	ck_assert( NULL != master->data.node.list );
	ck_assert( child1 == container_node_get_child(master, 0) );
	ck_assert( child2 == container_node_get_child(master, 1) );

	// create and insert node to the end of the list
	child4 = container_node_create(CONTAINER_LEAF, NODE_HORIZONTAL);
	ck_assert( NULL != child4 );

	result = container_node_insert(master, child4, 2);
	ck_assert_int_eq( OK, result );
	ck_assert( master == child4->parent );
	ck_assert_int_eq( 3, container_node_get_size(master) );
	ck_assert( NULL != master->data.node.list );
	ck_assert( child1 == container_node_get_child(master, 0) );
	ck_assert( child2 == container_node_get_child(master, 1) );
	ck_assert( child4 == container_node_get_child(master, 2) );

	// create and insert node to the middle of list
	child3 = container_node_create(CONTAINER_LEAF, NODE_HORIZONTAL);
	ck_assert( NULL != child3 );

	result = container_node_insert(master, child3, 2);
	ck_assert_int_eq( OK, result );
	ck_assert( master == child3->parent );
	ck_assert_int_eq( 4, container_node_get_size(master) );
	ck_assert( NULL != master->data.node.list );
	ck_assert( child1 == container_node_get_child(master, 0) );
	ck_assert( child2 == container_node_get_child(master, 1) );
	ck_assert( child3 == container_node_get_child(master, 2) );
	ck_assert( child4 == container_node_get_child(master, 3) );

	// deallocate whole structure (deallocation is recursive)
	result = container_node_destroy(master);
	ck_assert_int_eq( OK, result );
} END_TEST

/**
 * \brief Test removal of child nodes from master node.
 *
 * Check removal of node from start, end and middle of the node list.
 */
START_TEST(test_manage_remove) {
	guint i;
	RCode result;
	ContainerNode *master = NULL;
	ContainerNode *child[4] = { NULL };

	// create master node into which we will insert
	master = container_node_create(CONTAINER_NODE, NODE_HORIZONTAL);
	ck_assert( NULL != master );

	for (i = 0; i < 4; i++ ) {
		child[i] = container_node_create(CONTAINER_LEAF, NODE_HORIZONTAL);
		ck_assert( NULL != child[i] );

		result = container_node_insert(master, child[i], i);
		ck_assert_int_eq( OK, result );
		ck_assert( master == child[i]->parent );
	}

	// remove node from middle of the list
	result = container_node_remove(master, 1);
	ck_assert_int_eq( OK, result );
	ck_assert_int_eq( 3, container_node_get_size(master) );
	ck_assert( child[0] == container_node_get_child(master, 0) );
	ck_assert( child[2] == container_node_get_child(master, 1) );
	ck_assert( child[3] == container_node_get_child(master, 2) );
	ck_assert( NULL == child[1]->parent );

	// remove node from end of the list
	result = container_node_remove(master, 2);
	ck_assert_int_eq( OK, result );
	ck_assert_int_eq( 2, container_node_get_size(master) );
	ck_assert( child[0] == container_node_get_child(master, 0) );
	ck_assert( child[2] == container_node_get_child(master, 1) );
	ck_assert( NULL == child[3]->parent );

	// remove node from start of the list
	result = container_node_remove(master, 0);
	ck_assert_int_eq( OK, result );
	ck_assert_int_eq( 1, container_node_get_size(master) );
	ck_assert( child[2] == container_node_get_child(master, 0) );
	ck_assert( NULL == child[0]->parent );

	// remove remaining node from the list
	result = container_node_remove(master, 0);
	ck_assert_int_eq( OK, result );
	ck_assert( NULL == child[2]->parent );

	// since childs are not in the list, we can't use recursive destroy
	for (i = 0; i < 4; i++ ) {
		result = container_node_destroy(child[i]);
		ck_assert_int_eq( OK, result );
	}
	result = container_node_destroy(master);
	ck_assert_int_eq( OK, result );
} END_TEST

/**
 * \brief Test replacing one child node with another child node
 *
 * The function replaces one child node on specified position with different
 * child node.
 */
START_TEST(test_manage_replace) {
	RCode result;
	ContainerNode *master = NULL;
	ContainerNode *child1 = NULL, *child2 = NULL;
	ContainerNode *tmp = NULL;

	// create master node into which we will insert
	master = container_node_create(CONTAINER_NODE, NODE_HORIZONTAL);
	ck_assert( NULL != master );

	// create and insert node to the empty list
	child1 = container_node_create(CONTAINER_LEAF, NODE_HORIZONTAL);
	ck_assert( NULL != child1 );

	result = container_node_insert(master, child1, 0);
	ck_assert_int_eq( OK, result );

	// replace child1 node with child2 node in the list
	child2 = container_node_create(CONTAINER_LEAF, NODE_HORIZONTAL);
	ck_assert( NULL != child2 );

	result = container_node_set_child(master, child2, 0);
	ck_assert_int_eq( OK, result );
	ck_assert( child2 == container_node_get_child(master, 0) );
	ck_assert( NULL == child1->parent );
	ck_assert( master == child2->parent );

	// deallocate whole structure (deallocation is recursive)
	result = container_node_destroy(child1);
	ck_assert_int_eq( OK, result );

	result = container_node_destroy(master);
	ck_assert_int_eq( OK, result );
} END_TEST

/**
 * \brief Test locating position of child in parent node.
 */
START_TEST(test_manage_position) {
	guint i;
	RCode result;
	ContainerNode *master = NULL;
	ContainerNode *child[4] = { NULL };

	// create master node into which we will insert
	master = container_node_create(CONTAINER_NODE, NODE_HORIZONTAL);
	ck_assert( NULL != master );

	for (i = 0; i < 4; i++ ) {
		child[i] = container_node_create(CONTAINER_LEAF, NODE_HORIZONTAL);
		ck_assert( NULL != child[i] );

		result = container_node_insert(master, child[i], i);
		ck_assert_int_eq( OK, result );
	}

	// get locations of child nodes existing in list
	ck_assert_int_eq( 0, container_node_get_position(master, child[0]) );
	ck_assert_int_eq( 1, container_node_get_position(master, child[1]) );
	ck_assert_int_eq( 2, container_node_get_position(master, child[2]) );
	ck_assert_int_eq( 3, container_node_get_position(master, child[3]) );

	// remove node from the middle of list and deallocate it
	result = container_node_remove(master, 1);
	ck_assert_int_eq( OK, result );

	// get locations of all nodes, one would be 'not found' now
	ck_assert_int_eq( 0,  container_node_get_position(master, child[0]) );
	ck_assert_int_eq( -1, container_node_get_position(master, child[1]) );
	ck_assert_int_eq( 1,  container_node_get_position(master, child[2]) );
	ck_assert_int_eq( 2,  container_node_get_position(master, child[3]) );

	// deallocate whole structure (deallocation is recursive)
	result = container_node_destroy(child[1]);
	ck_assert_int_eq( OK, result );

	result = container_node_destroy(master);
	ck_assert_int_eq( OK, result );
} END_TEST

/**
 * \brief Test invocation of node management functions on leaf node.
 *
 * The management function should not invoke cleanly on leaf.
 */
START_TEST(test_manage_leaf) {
	RCode result;
	ContainerNode *leaf = NULL;
	ContainerNode *node = NULL;

	// create master node into which we will insert
	leaf = container_node_create(CONTAINER_LEAF, NODE_HORIZONTAL);
	ck_assert( NULL != leaf );

	node = container_node_create(CONTAINER_NODE, NODE_HORIZONTAL);
	ck_assert( NULL != node );

	// test insertion and removal from leaf (should fail)
	result = container_node_insert(leaf, node, 0);
	ck_assert_int_eq( FAIL, result );
	ck_assert_int_eq( TRUE, error_is_error() );
	error_reset();

	result = container_node_remove(leaf, 0);
	ck_assert_int_eq( FAIL, result );
	ck_assert_int_eq( TRUE, error_is_error() );
	error_reset();

	// deallocate test container nodes
	result = container_node_destroy(node);
	ck_assert_int_eq( OK, result );

	result = container_node_destroy(leaf);
	ck_assert_int_eq( OK, result );
} END_TEST

/**
 * \brief Test setup of the view into the leaf node.
 */
START_TEST(test_manage_set_view) {
	RCode result;
	ContainerNode *leaf = NULL;
	ViewGeneric *view1 = NULL, *view2 = NULL;

	leaf = container_node_create(CONTAINER_LEAF, NODE_HORIZONTAL);
	ck_assert( NULL != leaf );

	view1 = view_create(VIEW_NONE);
	ck_assert( NULL != view1 );
	ck_assert_int_eq( 1, view1->reference );

	view2 = view_create(VIEW_NONE);
	ck_assert( NULL != view2 );
	ck_assert_int_eq( 1, view2->reference );

	// set view for the first time
	result = container_node_set_view(leaf, view1);
	ck_assert_int_eq( OK, result );
	ck_assert_int_eq(2, view1->reference );
	ck_assert( view1 == container_node_get_view(leaf) );

	// overwrite view with another one
	result = container_node_set_view(leaf, view2);
	ck_assert_int_eq( OK, result );
	ck_assert_int_eq(1, view1->reference );
	ck_assert_int_eq(2, view2->reference );
	ck_assert( view2 == container_node_get_view(leaf) );

	result = container_node_set_view(leaf, NULL);
	ck_assert_int_eq( OK, result );
	ck_assert_int_eq(1, view1->reference );
	ck_assert_int_eq(1, view2->reference );
	ck_assert( NULL == container_node_get_view(leaf) );

	// deallocate test container and view structures
	view_unref(view1);
	view_unref(view2);
	container_node_destroy(leaf);
} END_TEST

/**
 * \brief Test invocation of leaf management functions over node.
 */
START_TEST(test_manage_set_view_to_node) {
	RCode result;
	ContainerNode *node = NULL;
	ViewGeneric *view = NULL;

	node = container_node_create(CONTAINER_NODE, NODE_HORIZONTAL);
	ck_assert( NULL != node );

	view = view_create(VIEW_NONE);
	ck_assert( NULL != view );
	ck_assert_int_eq( 1, view->reference );

	// set view for the first time
	result = container_node_set_view(node, view);
	ck_assert_int_eq( FAIL, result );
	ck_assert_int_eq(1, view->reference );
	ck_assert_int_eq( TRUE, error_is_error() );
	error_reset();

	// deallocate test container and view structures
	view_unref(view);
	container_node_destroy(node);
} END_TEST


/**
 * \brief Test resize of structure with only leaf node
 */
START_TEST(test_manage_leaf_resize) {
	RCode result;

	GdkRectangle region =  { 5, 10, 50, 100 };
	ContainerNode* node = container_node_create(CONTAINER_LEAF, NODE_HORIZONTAL);

	container_node_set_view(node, view_create(VIEW_NONE));

	ck_assert_int_eq( 0, node->region.x );
	ck_assert_int_eq( 0, node->region.y );
	ck_assert_int_eq( 0, node->region.width );
	ck_assert_int_eq( 0, node->region.height );
	ck_assert_int_eq( FALSE, container_node_get_flag(node) );

	result = container_node_resize(node, region);
	ck_assert_int_eq( OK, result );

	ck_assert_int_eq(   5, node->region.x );
	ck_assert_int_eq(  10, node->region.y );
	ck_assert_int_eq(  50, node->region.width );
	ck_assert_int_eq( 100, node->region.height );
	ck_assert_int_eq( TRUE, container_node_get_flag(node) );

	ck_assert_int_eq( OK, container_node_reset_flag(node) );
	ck_assert_int_eq( FALSE, container_node_get_flag(node) );

	container_node_destroy(node);
} END_TEST


struct ResizeTestData {
		ContainerNodeDirection direction;
		GdkPoint hint[3];
		GdkRectangle leaf[3];
	};

// test data in following order:
//  - all nodes greedy
//  - modest requests, one greedy node
//  - non modest requests, no greedy node
//  - non modest requests, one greedy node
#define TEST_RESIZE_COUNT 8
struct ResizeTestData resize_test_data[TEST_RESIZE_COUNT] = {
	{ NODE_HORIZONTAL, {{ 0,0}, { 0,0}, {  0,0}}, {{ 5,10,20,120}, {25,10,20,120}, {45,10,20,120}} },
	{ NODE_HORIZONTAL, {{ 5,0}, { 0,0}, { 15,0}}, {{ 5,10, 5,120}, {10,10,40,120}, {50,10,15,120}} },
	{ NODE_HORIZONTAL, {{50,0}, {50,0}, {100,0}}, {{ 5,10,15,120}, {20,10,15,120}, {35,10,30,120}} },
	{ NODE_HORIZONTAL, {{60,0}, {60,0}, {  0,0}}, {{ 5,10,24,120}, {29,10,24,120}, {53,10,12,120}} },

	{ NODE_VERTICAL,   {{0,  0}, {0,  0}, {0,  0}}, {{ 5,10,60, 40}, { 5,50,60,40}, {5, 90,60,40}} },
	{ NODE_VERTICAL,   {{0,  0}, {0, 20}, {0, 20}}, {{ 5,10,60, 80}, { 5,90,60,20}, {5,110,60,20}} },
	{ NODE_VERTICAL,   {{0, 50}, {0, 50}, {0,100}}, {{ 5,10,60, 30}, { 5,40,60,30}, {5, 70,60,60}} },
	{ NODE_VERTICAL,   {{0,120}, {0,120}, {0,  0}}, {{ 5,10,60, 48}, { 5,58,60,48}, {5,106,60,24}} }
};

/**
 * \brief Test resize of structure with one node and three leave nodes
 */
START_TEST(test_manage_node_resize) {
	RCode result;
	GdkRectangle new_region = { 5, 10, 60, 120 };
	gint i;

	ContainerNode* node = container_node_create(CONTAINER_NODE, resize_test_data[_i].direction);

	container_node_insert(node, container_node_create(CONTAINER_LEAF, NODE_HORIZONTAL), 0);
	container_node_insert(node, container_node_create(CONTAINER_LEAF, NODE_HORIZONTAL), 1);
	container_node_insert(node, container_node_create(CONTAINER_LEAF, NODE_HORIZONTAL), 2);

	// check no node has region set prior first resizing and update hints for
	// the child view
	for (i = 0; i < 3; i++ ) {
		ContainerNode* child = container_node_get_child(node, i);

		ck_assert_int_eq( 0, child->region.x );
		ck_assert_int_eq( 0, child->region.y );
		ck_assert_int_eq( 0, child->region.width );
		ck_assert_int_eq( 0, child->region.height );
		ck_assert_int_eq( FALSE, container_node_get_flag(child) );

		ViewGeneric* child_view = view_create(VIEW_NONE);
		container_node_set_view(child, child_view);

		child_view->region_hint.x = resize_test_data[_i].hint[i].x;
		child_view->region_hint.y = resize_test_data[_i].hint[i].y;
	}

	result = container_node_resize(node, new_region);
	ck_assert_int_eq( OK, result );

	ck_assert_int_eq(   5, node->region.x );
	ck_assert_int_eq(  10, node->region.y );
	ck_assert_int_eq(  60, node->region.width );
	ck_assert_int_eq( 120, node->region.height );

	ck_assert_int_eq( TRUE, container_node_get_flag(node) );

	for (i = 0; i < 3; i++ ) {
		ContainerNode* child = container_node_get_child(node, i);

		ck_assert_int_eq( resize_test_data[_i].leaf[i].x, child->region.x );
		ck_assert_int_eq( resize_test_data[_i].leaf[i].y, child->region.y );
		ck_assert_int_eq( resize_test_data[_i].leaf[i].width,  child->region.width );
		ck_assert_int_eq( resize_test_data[_i].leaf[i].height, child->region.height );

		ck_assert_int_eq( TRUE, container_node_get_flag(child) );
	}

} END_TEST

struct HintTestData {
		ContainerNodeDirection direction;
		GdkPoint hint[3];
		GdkPoint result;
	};

#define TEST_HINT_COUNT 10
struct HintTestData hint_test_data[TEST_HINT_COUNT] = {
	{ NODE_HORIZONTAL, {{  0, 0 }, {  0,  0 }, {  0,  0 }}, {  0,  0 } },
	{ NODE_HORIZONTAL, {{  0, 5 }, {  0, 10 }, {  0,  0 }}, {  0, 10 } },
	{ NODE_HORIZONTAL, {{  0, 5 }, {  0, 10 }, {  0, 15 }}, {  0, 15 } },
	{ NODE_HORIZONTAL, {{ 10, 0 }, { 15,  0 }, { 20,  0 }}, { 45,  0 } },
	{ NODE_HORIZONTAL, {{  5, 0 }, {  0,  0 }, {  5,  0 }}, {  0,  0 } },

	{ NODE_VERTICAL,   {{ 0,  0 }, {  0,  0 }, {  0,  0 }}, {  0,  0 } },
	{ NODE_VERTICAL,   {{ 5,  0 }, { 10,  0 }, {  0,  0 }}, { 10,  0 } },
	{ NODE_VERTICAL,   {{ 5,  0 }, { 10,  0 }, { 15,  0 }}, { 15,  0 } },
	{ NODE_VERTICAL,   {{ 0, 10 }, {  0, 15 }, {  0, 20 }}, {  0, 45 } },
	{ NODE_VERTICAL,   {{ 0,  5 }, {  0,  0 }, {  0,  5 }}, {  0,  0 } }
};

/**
 * \brief Test hint computation for non-leaf node
 */
START_TEST(test_manage_hint_compute) {
	RCode result;
	GdkPoint hint = { -1, -1 };
	gint i;

	// prepare container node with three childs for test
	ContainerNode* node = container_node_create(CONTAINER_NODE, hint_test_data[_i].direction);
	for (i = 0; i < 3; i++ ) {
		ContainerNode* child = container_node_create(CONTAINER_LEAF, NODE_HORIZONTAL);
		ViewGeneric* child_view = view_create(VIEW_NONE);

		child_view->region_hint.x = hint_test_data[_i].hint[i].x;
		child_view->region_hint.y = hint_test_data[_i].hint[i].y;

		container_node_insert(node, child, _i);
		container_node_set_view(child, child_view);
	}

	result = container_node_get_hint(node, &hint);
	ck_assert_int_eq( OK, result );

	ck_assert_int_eq( hint.x, hint_test_data[_i].result.x );
	ck_assert_int_eq( hint.y, hint_test_data[_i].result.y );

} END_TEST

/**
 * \brief Create test suite for container view module.
 * \return pointer to container view suite
 */
Suite* create_container_node_suite (void) {

	Suite *suite = suite_create("ContainerNode");

	TCase *tc_create = tcase_create("Create");
	tcase_add_checked_fixture(tc_create, common_setup, common_teardown);
	tcase_add_test(tc_create, test_create_node);
	tcase_add_test(tc_create, test_create_leaf);
	tcase_add_test(tc_create, test_destroy_node);

	TCase *tc_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_manage, common_setup, common_teardown);
	tcase_add_test(tc_manage, test_manage_insert);
	tcase_add_test(tc_manage, test_manage_remove);
	tcase_add_test(tc_manage, test_manage_replace);
	tcase_add_test(tc_manage, test_manage_position);

	tcase_add_test(tc_manage, test_manage_leaf);
	tcase_add_test(tc_manage, test_manage_set_view);
	tcase_add_test(tc_manage, test_manage_set_view_to_node);
	tcase_add_test(tc_manage, test_manage_leaf_resize);
	tcase_add_loop_test(tc_manage, test_manage_node_resize, 0, TEST_RESIZE_COUNT);
	tcase_add_loop_test(tc_manage, test_manage_hint_compute, 0, TEST_HINT_COUNT);

	suite_add_tcase(suite, tc_create);
	suite_add_tcase(suite, tc_manage);

	return suite;
}
