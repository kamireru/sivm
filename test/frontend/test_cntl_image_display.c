/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <gdk/gdkkeysyms.h>
#include "src/frontend/cntl_image_display.h"

#include "test/frontend/test_cntl_image_display.h"
#include "test/common/common.h"

/** \brief Static variable for holding test controller. */
static CntlGeneric *cntl_generic = NULL;

/** \brief Static variable for holding pointer to test controller data. */
static CntlImageDisplayData *cntl_data = NULL;

/**
 * \brief Test to create image display controller
 *
 * Test that specific data structure is correctly allocated and reset.
 */
START_TEST(test_create_controller) {
	RCode result;
	CntlGeneric *cntl = NULL;
	CntlImageDisplayData *cntl_data = NULL;

	cntl = cntl_create(CNTL_IMAGE_DISPLAY);
	ck_assert( NULL != cntl );
	ck_assert_int_eq( CNTL_IMAGE_DISPLAY, cntl->type );
	ck_assert_int_eq( 1, cntl->reference );

	ck_assert( NULL == cntl->view );
	ck_assert( NULL != cntl->automaton );
	ck_assert( NULL != cntl->specific_data );

	// test cntl_data data creation
	cntl_data = (CntlImageDisplayData*) cntl->specific_data;
	ck_assert_int_eq( sizeof(*cntl_data), sizeof(CntlImageDisplayData) );

	ck_assert_int_eq( 0, cntl_data->count );
	ck_assert_int_eq( MOVE_INDEX_FIRST, cntl_data->movement_type );
	ck_assert_int_eq( ZOOM_FIT_CUSTOM, cntl_data->auto_zoom );
	ck_assert_int_eq( ZOOM_DIRECTION_RESET, cntl_data->zoom_type );
	ck_assert_int_eq( PAN_UP, cntl_data->pan_direction );

	// try controller destruction
	result = cntl_destroy(cntl);
	ck_assert_int_eq( FAIL, result );
	ck_assert_int_eq( TRUE, error_is_error() );
	error_reset();

	// try controller dereference
	result = cntl_unref(cntl);
	ck_assert_int_eq( TRUE, result );
} END_TEST

/**
 * \brief Setup image display controller for parser tests.
 */
static void generic_setup(void) {
	COLOR_SETUP_START;

	cntl_generic = cntl_create(CNTL_IMAGE_DISPLAY);
	cntl_data = (CntlImageDisplayData*) cntl_generic->specific_data;

	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_SETUP_END;
	return ;
}

/**
 * \brief Teardown image display controller after test
 */
static void generic_teardown(void) {
	COLOR_TEARDOWN_START;

	ck_assert( OK == cntl_unref(cntl_generic) );
	cntl_generic = NULL;
	cntl_data = NULL;

	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_TEARDOWN_END;
	return ;
}

/**
 * \brief Test specific controller data reset.
 */
START_TEST(test_data_reset) {

	// update specific data in controller to initial values
	cntl_data->count = 0;
	cntl_data->movement_type = MOVE_INDEX_LAST;
	cntl_data->auto_zoom = ZOOM_FIT_IMAGE;
	cntl_data->zoom_type = ZOOM_DIRECTION_OUT;
	cntl_data->pan_direction = PAN_RIGHT;

	// reset test data and check results
	cntl_idv_data_reset(cntl_generic);

	ck_assert_int_eq( 0, cntl_data->count );
	ck_assert_int_eq( MOVE_INDEX_FIRST, cntl_data->movement_type );
	ck_assert_int_eq( ZOOM_FIT_CUSTOM, cntl_data->auto_zoom );
	ck_assert_int_eq( ZOOM_DIRECTION_RESET, cntl_data->zoom_type );
	ck_assert_int_eq( PAN_UP, cntl_data->pan_direction );
} END_TEST


/**
 * Structure for describing sequence of keys to supply to some parsing function
 * and resulting values (integer values - ie enums or numbers).
 */
typedef struct {
		/** \brief Index of key in structure to send to function. */
		unsigned int index;

		/** \brief Value in the specific controller structure after key press. */
		long result;
	} ParseKeyTestData;

/**
 * \brief Test parsing out numericals from keystroke sequence.
 */
START_TEST(test_parse_count) {
	GdkEventKey key_list[] = {
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_0, 0 ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_5, 0 ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_9, 0 )
		};
	ParseKeyTestData sequence[] = { {1, 5}, {0, 50}, {2, 509} };
	int i;

	// test initial state of the count
	ck_assert_int_eq( 0, cntl_data->count );

	// process sequence of keystrokes and verify results in between
	for (i = 0; i < 3; i++) {
		cntl_idv_parse_count(0, 0, CNTL_KEY(key_list, sequence[i].index), (void*)cntl_generic);
		ck_assert_int_eq( sequence[i].result, cntl_data->count );
	}
} END_TEST

/**
 * \brief Test parsing out numericals from keystroke sequence using keypad.
 */
START_TEST(test_parse_count_keypad) {
	GdkEventKey key_list[] = {
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_KP_0, 0 ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_KP_5, 0 ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_KP_9, 0 )
		};
	ParseKeyTestData sequence[] = { {1, 5}, {0, 50}, {2, 509} };
	int i;

	// test initial state of the count
	ck_assert_int_eq( 0, cntl_data->count );

	// process sequence of keystrokes and verify results in between
	for (i = 0; i < 3; i++) {
		cntl_idv_parse_count(0, 0, CNTL_KEY(key_list, sequence[i].index), (void*)cntl_generic);
		ck_assert_int_eq( sequence[i].result, cntl_data->count );
	}
} END_TEST

/**
 * \brief Test parsing out type of the movement in the image list
 */
START_TEST(test_parse_movement_type) {
	GdkEventKey key_list[] = {
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_n, 0 ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Page_Down, 0 ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_p, 0 ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Page_Up, 0 ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_P, 0 ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Home, 0 ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_N, 0 ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_End, 0 ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_M, 0 )
		};
	ParseKeyTestData sequence[] = {
			{0, MOVE_INDEX_NEXT }, {2, MOVE_INDEX_PREV}, {4, MOVE_INDEX_FIRST}, {6, MOVE_INDEX_LAST},
			{1, MOVE_INDEX_NEXT }, {3, MOVE_INDEX_PREV}, {5, MOVE_INDEX_FIRST}, {7, MOVE_INDEX_LAST},
			{8, MOVE_INDEX_INDEX}
		};
	int i;

	// test initial state of the count
	ck_assert_int_eq( MOVE_INDEX_FIRST, cntl_data->movement_type );

	// process sequence of keystrokes and verify results in between
	for (i = 0; i < 9; i++) {
		cntl_idv_parse_movement(0, 0, CNTL_KEY(key_list, sequence[i].index), (void*)cntl_generic);
		ck_assert_int_eq( sequence[i].result, cntl_data->movement_type );
	}
} END_TEST

/**
 * \brief Test parsing out type of auto-zoom configuration
 */
START_TEST(test_parse_auto_zoom) {
	GdkEventKey key_list[] = {
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_i, 0),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_w, 0),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_h, 0),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_c, 0)
		};
	ParseKeyTestData sequence[] = {
			{0, ZOOM_FIT_IMAGE}, {1, ZOOM_FIT_WIDTH}, {2, ZOOM_FIT_HEIGHT}, {3, ZOOM_FIT_CUSTOM}
		};
	int i;

	// test initial state of the count
	ck_assert_int_eq( ZOOM_FIT_CUSTOM, cntl_data->movement_type );

	// process sequence of keystrokes and verify results in between
	for (i = 0; i < 4; i++) {
		cntl_idv_parse_auto_zoom(0, 0, CNTL_KEY(key_list, sequence[i].index), (void*)cntl_generic);
		ck_assert_int_eq( sequence[i].result, cntl_data->auto_zoom );
	}
} END_TEST

/**
 * \brief Test parsing out type of auto-zoom configuration
 */
START_TEST(test_parse_zoom) {
	GdkEventKey key_list[] = {
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_i, 0),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_o, 0),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_r, 0)
		};
	ParseKeyTestData sequence[] = {
			{0, ZOOM_DIRECTION_IN}, {1, ZOOM_DIRECTION_OUT}, {2, ZOOM_DIRECTION_RESET}
		};
	int i;

	// test initial state of the count
	ck_assert_int_eq( ZOOM_DIRECTION_RESET, cntl_data->movement_type );

	// process sequence of keystrokes and verify results in between
	for (i = 0; i < 3; i++) {
		cntl_idv_parse_zoom(0, 0, CNTL_KEY(key_list, sequence[i].index), (void*)cntl_generic);
		ck_assert_int_eq( sequence[i].result, cntl_data->zoom_type );
	}
} END_TEST

/**
 * \brief Test parsing out direction of panning
 */
START_TEST(test_parse_pan_direction) {
	GdkEventKey key_list[] = {
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Up, 0),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_k, 0),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Down, 0),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_j, 0),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Left, 0),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_h, 0),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Right, 0),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_l, 0),
		};
	ParseKeyTestData sequence[] = {
			{0, PAN_UP}, {2, PAN_DOWN}, {4, PAN_LEFT}, {6, PAN_RIGHT},
			{1, PAN_UP}, {3, PAN_DOWN}, {5, PAN_LEFT}, {7, PAN_RIGHT}
		};
	int i;

	// test initial state of the count
	ck_assert_int_eq( PAN_UP, cntl_data->movement_type );

	// process sequence of keystrokes and verify results in between
	for (i = 0; i < 8; i++) {
		cntl_idv_parse_pan(0, 0, CNTL_KEY(key_list, sequence[i].index), (void*)cntl_generic);
		ck_assert_int_eq( sequence[i].result, cntl_data->pan_direction );
	}
} END_TEST

START_TEST(test_parse_action) {
	GdkEventKey key_list[] = {
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_m, 0 ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_c, 0 ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_s, 0 )
		};
	ParseKeyTestData sequence[] = { {0, GROUP_MOVE }, {1, GROUP_COPY }, {2, GROUP_SELECT } };
	int i;

	// test initial state of the count
	ck_assert_int_eq( GROUP_SELECT, cntl_data->group_action );

	// process sequence of keystrokes and verify results in between
	for (i = 0; i < 3; i++) {
		cntl_idv_parse_action(0, 0, CNTL_KEY(key_list, sequence[i].index), (void*)cntl_generic);
		ck_assert_int_eq( sequence[i].result, cntl_data->group_action );
	}
} END_TEST

START_TEST(test_parse_group) {
	GdkEventKey key_list[] = {
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_m, 0 ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_c, 0 ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_s, 0 ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_G, GDK_SHIFT_MASK ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Z, GDK_SHIFT_MASK ),
			CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_asterisk, 0 )
		};
	ParseKeyTestData sequence[] = { {0, 'm' }, {1, 'c' }, {2, 's' }, {3, 'G' }, {4, 'Z' }, {5, '*'} };
	int i;

	// test initial state of the count
	ck_assert_int_eq( 0, (int)cntl_data->group_id );

	// process sequence of keystrokes and verify results in between
	for (i = 0; i < 6; i++) {
		cntl_idv_parse_group(0, 0, CNTL_KEY(key_list, sequence[i].index), (void*)cntl_generic);
		ck_assert_int_eq( (int)sequence[i].result, (int)cntl_data->group_id );
	}
} END_TEST

/**
 * \brief Create test suite for image display controller module.
 * \return pointer to image display controller suite
 */
Suite* create_cntl_image_display_suite (void) {

	Suite *suite = suite_create("CntlImageDisplay");

	TCase *tc_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_manage, common_setup, common_teardown);
	tcase_add_test(tc_manage, test_create_controller);

	TCase *tc_parse = tcase_create("Parse");
	tcase_add_checked_fixture(tc_parse, generic_setup, generic_teardown);
	tcase_add_test(tc_parse, test_data_reset);
	tcase_add_test(tc_parse, test_parse_count);
	tcase_add_test(tc_parse, test_parse_count_keypad);
	tcase_add_test(tc_parse, test_parse_movement_type);
	tcase_add_test(tc_parse, test_parse_auto_zoom);
	tcase_add_test(tc_parse, test_parse_zoom);
	tcase_add_test(tc_parse, test_parse_pan_direction);
	tcase_add_test(tc_parse, test_parse_action);
	tcase_add_test(tc_parse, test_parse_group);

	suite_add_tcase(suite, tc_manage);
	suite_add_tcase(suite, tc_parse);

	return suite;
}
