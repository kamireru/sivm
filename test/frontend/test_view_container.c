/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>
#include "src/frontend/view_generic.h"
#include "src/frontend/view_container.h"
#include "src/frontend/container_node.h"

#include "test/common/common.h"
#include "test/frontend/test_view_common.h"
#include "test/frontend/test_view_container.h"


static ViewGeneric *view_container = NULL;

/**
 * \brief Test creation of container view.
 *
 * The function tests creation of container view specific data.
 */
START_TEST(test_create_container_data) {
	ViewContainer *cv_data = NULL;

	cv_data = view_cv_create(view_generic);
	ck_assert( NULL != cv_data );
	ck_assert_int_eq( sizeof(*cv_data), sizeof(ViewContainer) );

	ck_assert( NULL != cv_data->top_node );
	ck_assert( CONTAINER_LEAF == cv_data->top_node->type );
	ck_assert( NULL == container_node_get_view(cv_data->top_node) );

	ck_assert( NULL != cv_data->focus_node );
	ck_assert( cv_data->focus_node == cv_data->top_node );

	ck_assert_int_eq( 0, cv_data->color.bg.red );
	ck_assert_int_eq( 0, cv_data->color.bg.green );
	ck_assert_int_eq( 0, cv_data->color.bg.blue );

	ck_assert_int_eq( 0, cv_data->color.active.red );
	ck_assert_int_eq( 65535, cv_data->color.active.green );
	ck_assert_int_eq( 65535, cv_data->color.active.blue );

	ck_assert_int_eq( 0, cv_data->color.inactive.red );
	ck_assert_int_eq( 0, cv_data->color.inactive.green );
	ck_assert_int_eq( 65535, cv_data->color.inactive.blue );

	fail_if( OK != view_cv_destroy(cv_data), NULL);
} END_TEST

/**
 * \brief Setup container view for view tests
 *
 * The function creates single container view so it can be used in the
 * tests.
 */
static void setup_container(void) {
	COLOR_SETUP_START;

	view_generic = view_create(VIEW_NONE);
	view_container = view_create(VIEW_CONTAINER);

	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_SETUP_END;
	return ;
}

/**
 * \brief Teardown container view after test
 *
 * The function can be used for teardown after both recomputation and view
 * management tests.
 */
static void teardown_container(void) {
	COLOR_TEARDOWN_START;

	ck_assert( OK == view_unref(view_generic) );
	ck_assert( OK == view_unref(view_container) );

	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_TEARDOWN_END;
	return ;
}


/**
 * \brief Test setup of view into the focus node.
 */
START_TEST(test_setup_child_view) {
	RCode result;
	ViewContainer *cv_data = view_cv_get_data_pointer(view_container);

	ck_assert_int_eq( 1, view_generic->reference );

	// set a child view to the node with focus
	result = view_cv_set_view(view_container, view_generic);
	ck_assert_int_eq( OK, result );
	ck_assert( view_generic == container_node_get_view( cv_data->focus_node ) );
	ck_assert_int_eq( 2, view_generic->reference );

	// unset child view from node with focus
	result = view_cv_set_view(view_container, NULL);
	ck_assert_int_eq( OK, result );
	ck_assert( NULL == container_node_get_view( cv_data->focus_node ) );
	ck_assert_int_eq( 1, view_generic->reference );
} END_TEST

/**
 * \brief Test split of the focused node.
 *
 * This test case contains only simple scenarios with one level deep structure.
 * It tests creation of node list from simple leaf and adding several nodes to
 * the list.
 */
START_TEST(test_manage_node_simple_split) {
	RCode result;
	ViewContainer *cv_data = view_cv_get_data_pointer(view_container);

	// set view to the focused node (only one node now)
	result = view_cv_set_view(view_container, view_generic);
	ck_assert_int_eq( OK, result );

	// split original leaf node vertically
	result = view_cv_node_split(view_container, NODE_VERTICAL);
	ck_assert_int_eq( OK, result );

	ck_assert( cv_data->top_node != cv_data->focus_node );
	ck_assert( view_generic == container_node_get_view(cv_data->focus_node) );
	ck_assert_int_eq( 2, container_node_get_size(cv_data->top_node) );
	ck_assert_int_eq( 0, container_node_get_position(cv_data->top_node, cv_data->focus_node) );
	ck_assert_int_eq( CONTAINER_NODE, cv_data->top_node->type );
	ck_assert_int_eq( CONTAINER_LEAF, cv_data->focus_node->type );

	// split the original leaf node vertically again (parent node is updated)
	result = view_cv_node_split(view_container, NODE_VERTICAL);
	ck_assert_int_eq( OK, result );

	ck_assert( view_generic == container_node_get_view(cv_data->focus_node) );
	ck_assert_int_eq( 3, container_node_get_size(cv_data->top_node) );
	ck_assert_int_eq( 0, container_node_get_position(cv_data->top_node, cv_data->focus_node) );

	// change focus node and split again
	cv_data->focus_node = container_node_get_child(cv_data->top_node, 1);

	result = view_cv_node_split(view_container, NODE_VERTICAL);
	ck_assert_int_eq( OK, result );

	ck_assert_int_eq( 4, container_node_get_size(cv_data->top_node) );
	ck_assert_int_eq( 1, container_node_get_position(cv_data->top_node, cv_data->focus_node) );

} END_TEST

/**
 * \brief Test removal of leafs in simple, one level, structure
 */
START_TEST(test_manage_node_simple_remove) {
	RCode result;
	ViewContainer *cv_data = view_cv_get_data_pointer(view_container);
	ContainerNode *tmp;

	// split the node several times to have 1 node and 4 leaves in structure
	view_cv_node_split(view_container, NODE_VERTICAL);
	view_cv_node_split(view_container, NODE_VERTICAL);
	view_cv_node_split(view_container, NODE_VERTICAL);

	// set focus to last node and remove it
	cv_data->focus_node = container_node_get_child(cv_data->top_node, 2);
	tmp = cv_data->focus_node;

	result = view_cv_node_remove(view_container);
	ck_assert_int_eq( OK, result );

	ck_assert( tmp != cv_data->focus_node );
	ck_assert_int_eq( 3, container_node_get_size(cv_data->top_node) );
	ck_assert_int_eq( 1, container_node_get_position(cv_data->top_node, cv_data->focus_node) );

	// set focus to first node and remove it
	cv_data->focus_node = container_node_get_child(cv_data->top_node, 0);
	tmp = cv_data->focus_node;

	result = view_cv_node_remove(view_container);
	ck_assert_int_eq( OK, result );

	ck_assert( tmp != cv_data->focus_node );
	ck_assert_int_eq( 2, container_node_get_size(cv_data->top_node) );
	ck_assert_int_eq( 0, container_node_get_position(cv_data->top_node, cv_data->focus_node) );

	// check that last node removal resultes in node collapse
	cv_data->focus_node = container_node_get_child(cv_data->top_node, 0);
	tmp = container_node_get_child(cv_data->top_node, 1);
	ck_assert( cv_data->top_node == tmp->parent );
	ck_assert( NULL == cv_data->top_node->parent );

	result = view_cv_node_remove(view_container);
	ck_assert_int_eq( OK, result );

	ck_assert_int_eq( CONTAINER_LEAF, cv_data->top_node->type );
	ck_assert( tmp == cv_data->focus_node );
	ck_assert( tmp == cv_data->top_node );
	ck_assert( NULL == tmp->parent );

	// check that last node can't be removed from view
	result = view_cv_node_remove(view_container);
	ck_assert_int_eq( FAIL, result );
	ck_assert( error_is_error() );
	error_reset();
} END_TEST

/**
 * \brief Test advanced node management
 *
 * This test contains node spitting and removing for hierarchies deeper than one
 * level.
 */
START_TEST(test_manage_node_advanced) {
	RCode result;
	ViewContainer *cv_data = view_cv_get_data_pointer(view_container);
	ContainerNode *lvl_node1 = NULL;

	// construct more complicated hierarchy
	ck_assert_int_eq( CONTAINER_LEAF, cv_data->top_node->type );

	view_cv_node_split(view_container, NODE_VERTICAL);
	view_cv_node_split(view_container, NODE_VERTICAL);

	ck_assert_int_eq( CONTAINER_NODE, cv_data->top_node->type );
	ck_assert_int_eq( 3, container_node_get_size(cv_data->top_node) );
	ck_assert( cv_data->focus_node == container_node_get_child(cv_data->top_node, 0) );
	ck_assert_int_eq( CONTAINER_LEAF, container_node_get_child(cv_data->top_node, 0)->type );
	ck_assert_int_eq( CONTAINER_LEAF, container_node_get_child(cv_data->top_node, 1)->type );
	ck_assert_int_eq( CONTAINER_LEAF, container_node_get_child(cv_data->top_node, 2)->type );

	view_cv_node_split(view_container, NODE_HORIZONTAL);

	ck_assert_int_eq( CONTAINER_NODE, container_node_get_child(cv_data->top_node, 0)->type );

	ck_assert( cv_data->focus_node->parent == container_node_get_child(cv_data->top_node, 0) );
	ck_assert_int_eq( 0, container_node_get_position(cv_data->focus_node->parent, cv_data->focus_node) );
	ck_assert_int_eq( 2, container_node_get_size(cv_data->focus_node->parent) );

	// remove focus node to collapse one node from deeper level
	result = view_cv_node_remove(view_container);
	ck_assert_int_eq( OK, result );

	ck_assert( cv_data->focus_node == container_node_get_child(cv_data->top_node, 0) );
	ck_assert_int_eq( CONTAINER_LEAF, container_node_get_child(cv_data->top_node, 0)->type );
	ck_assert_int_eq( CONTAINER_LEAF, container_node_get_child(cv_data->top_node, 1)->type );
	ck_assert_int_eq( CONTAINER_LEAF, container_node_get_child(cv_data->top_node, 2)->type );

} END_TEST

/**
 * \brief Setting focus to different node in structure
 */
START_TEST(test_manage_node_set_focus) {
	RCode result;
	ViewContainer *cv_data = view_cv_get_data_pointer(view_container);
	ContainerNode *detached = NULL;
	ContainerNode *leaf = NULL;

	view_cv_node_split(view_container, NODE_VERTICAL);
	view_cv_node_split(view_container, NODE_VERTICAL);
	view_cv_node_split(view_container, NODE_VERTICAL);

	// test setting new focus node
	leaf = container_node_get_child(cv_data->top_node, 0);
	ck_assert( cv_data->focus_node == leaf );

	leaf = container_node_get_child(cv_data->top_node, 1);
	ck_assert( cv_data->focus_node != leaf );

	result = view_cv_node_set_focus(view_container, leaf);
	ck_assert_int_eq( OK, result );
	ck_assert( cv_data->focus_node == leaf );

	// test setting non-leaf node as focus
	result = view_cv_node_set_focus(view_container, cv_data->top_node);
	ck_assert_int_eq( FAIL, result );
	ck_assert( cv_data->focus_node == leaf );
	error_reset();


	// detach node and hack it to remove it from node hierarchy to simulate node
	// from other container view
	detached = container_node_get_child(cv_data->top_node, 3);
	detached->parent = NULL;

	result = view_cv_node_set_focus(view_container, detached);
	ck_assert_int_eq( FAIL, result );
	ck_assert( cv_data->focus_node == leaf );
	error_reset();

	// reattach node before ending the test
	detached->parent = cv_data->top_node;

} END_TEST


/**
 * \brief Create test suite for container view module.
 * \return pointer to container view suite
 */
Suite* create_view_container_suite (void) {

	Suite *suite = suite_create("ViewContainer");

	TCase *tc_create = tcase_create("Create");
	tcase_add_checked_fixture(tc_create, common_setup_view_generic, common_teardown_view_generic);
	tcase_add_test(tc_create, test_create_container_data);

	TCase *tc_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_manage, setup_container, teardown_container);
	tcase_add_test(tc_manage, test_setup_child_view);
	tcase_add_test(tc_manage, test_manage_node_simple_split);
	tcase_add_test(tc_manage, test_manage_node_simple_remove);
	tcase_add_test(tc_manage, test_manage_node_advanced);
	tcase_add_test(tc_manage, test_manage_node_set_focus);

	suite_add_tcase(suite, tc_create);
	suite_add_tcase(suite, tc_manage);

	return suite;
}
