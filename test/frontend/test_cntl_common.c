/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <gdk/gdkkeysyms.h>
#include "src/frontend/cntl_common.h"

#include "test/frontend/test_cntl_common.h"
#include "test/common/common.h"

/**
 * The storage structure for testing CNTL_KEY_STRUCTURE macro and for storing
 * some keys that will be used when testing compare function.
 */
static GdkEventKey key_list[]  = {
/* 0 */	CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_q, 0 ),
/* 1 */	CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_q, GDK_CONTROL_MASK ),
/* 2 */	CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_q, GDK_CONTROL_MASK | GDK_BUTTON1_MASK),
/* 3 */	CNTL_KEY_STRUCTURE(GDK_KEY_RELEASE, GDK_q, 0 ),
/* 4 */	CNTL_KEY_STRUCTURE(GDK_KEY_RELEASE, GDK_q, GDK_CONTROL_MASK ),
/* 5 */	CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_q, 0 ),
/* 6 */	CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Q, 0 ),
/* 7 */	CNTL_KEY_STRUCTURE(GDK_KEY_PRESS, GDK_Q, GDK_CONTROL_MASK )
	};

#define KEY(INDEX) &(key_list[(INDEX)])

/**
 * \brief Test function for comparing key events
 *
 * The test for testing comparator function used in the automatons implementing
 * logic of the controllers
 */
START_TEST(test_compare_funtion) {

	// compare null with select keys (NULL is always less)
	fail_if( FA_LESS != cntl_key_compare(NULL, KEY(5)), NULL );
	fail_if( FA_GREATER != cntl_key_compare(KEY(5), NULL), NULL );
	fail_if( FA_EQUAL != cntl_key_compare(NULL, NULL), NULL );

	// compare few same key structures
	fail_if( FA_EQUAL != cntl_key_compare(KEY(0), KEY(0)), NULL );
	fail_if( FA_EQUAL != cntl_key_compare(KEY(0), KEY(5)), NULL );

	// compare keys that are same when state mask applied
	fail_if( FA_EQUAL != cntl_key_compare(KEY(1), KEY(2)), NULL );

	// compare keys that differ in type (in both ways)
	fail_if( FA_GREATER != cntl_key_compare(KEY(0), KEY(6)), NULL );
	fail_if( FA_LESS != cntl_key_compare(KEY(6), KEY(0)), NULL );

	// compare keys that differ in key value
	fail_if( FA_GREATER != cntl_key_compare(KEY(5), KEY(6)), NULL );
	fail_if( FA_LESS != cntl_key_compare(KEY(6), KEY(5)), NULL );

	// compare keys that differ in state flags
	fail_if( FA_LESS != cntl_key_compare(KEY(0), KEY(1)), NULL );
	fail_if( FA_GREATER != cntl_key_compare(KEY(1), KEY(0)), NULL );

} END_TEST



/**
 * \brief Create test suite for common controller module.
 * \return pointer to common controller suite
 */
Suite* create_cntl_common_suite (void) {

	Suite *suite = suite_create("CntlCommon");

	TCase *tc_manage = tcase_create("Common");
	tcase_add_checked_fixture(tc_manage, NULL, NULL);
	tcase_add_test(tc_manage, test_compare_funtion);

	suite_add_tcase(suite, tc_manage);

	return suite;
}
