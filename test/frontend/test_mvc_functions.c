/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/frontend/mvc_functions.h"

#include "src/frontend/view_generic.h"
#include "src/frontend/model_generic.h"
#include "src/frontend/cntl_generic.h"

#include "test/common/common.h"
#include "test/frontend/test_mvc_functions.h"


/**
 * \brief Test creation of the MVC triad
 *
 * Test creation of model, view and controller through one function call. Make
 * sure that they are correctly linked together.
 */
START_TEST(test_create_triad) {

	ViewGeneric* view = NULL;

	view = mvc_triad_create(MODEL_NONE, VIEW_NONE, CNTL_NONE);

	fail_if( NULL == view, NULL );
	fail_if( NULL == view->model, NULL );
	fail_if( NULL == view->controller, NULL );

	fail_if( view != cntl_get_view(view->controller), NULL );
	fail_if( 1 != observer_get_count(view->model->view_list), NULL );

	// call update on the model and check view is updated
	view->model->sequence = 5;
	model_update(view->model);

	fail_if( 5 != view->last_sequence, NULL );

	fail_if ( OK != view_unref(view), NULL );

} END_TEST


/**
 * \brief Create test suite for mvc functions module.
 * \return pointer to mvc functions suite
 */
Suite* create_mvc_functions_suite (void) {

	Suite *suite = suite_create("MVC");

	TCase *tc_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_manage, common_setup, common_teardown);
	tcase_add_test(tc_manage, test_create_triad);

	suite_add_tcase(suite, tc_manage);

	return suite;
}
