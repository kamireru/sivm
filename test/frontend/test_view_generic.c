/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "src/frontend/view_generic.h"
#include "src/frontend/view_application.h"
#include "src/frontend/view_image_display.h"

#include "src/frontend/model_generic.h"
#include "src/frontend/cntl_generic.h"

#include "test/common/common.h"
#include "test/frontend/test_view_generic.h"


/**
 * The static variables here are used for the update() and redraw() tests for
 * one specific view. The view is created in setup function and destroyed in
 * teardown function.
 */
static ViewGeneric *view_generic = NULL;
static ModelGeneric *model_generic1 = NULL;
static ModelGeneric *model_generic2 = NULL;
static CntlGeneric *cntl_generic1 = NULL;
static CntlGeneric *cntl_generic2 = NULL;

static GdkRectangle test_region = {15, 15, 300, 200};

/**
 * \brief Test to create generic view
 *
 * The test create and destroy generic view with type VIEW_NONE - ie no specific
 * view stuff is created and destroyed.
 */
START_TEST(test_create_none) {

	ViewGeneric *view = NULL;

	view = view_create(VIEW_NONE);
	fail_if( NULL == view, NULL);
	fail_if( NULL != view->window, NULL );
	fail_if( NULL != view->specific_data, NULL );
	fail_if( NULL != view->model, NULL );
	fail_if( NULL != view->controller, NULL );
	fail_if( VIEW_NONE != view->type, NULL );
	fail_if( 0 != view->last_sequence, NULL );
	fail_if( 1 != view->reference, NULL );

	fail_if( 0 != view->region.x, NULL );
	fail_if( 0 != view->region.y, NULL );
	fail_if( 0 != view->region.width, NULL );
	fail_if( 0 != view->region.height, NULL );

	ck_assert_int_eq( 0, view->region_hint.x );
	ck_assert_int_eq( 0, view->region_hint.y );

	// check that simple destruction won't work
	fail_if( OK == view_destroy(view), NULL );
	fail_if( FALSE == error_is_error(), NULL );
	error_reset();

	// check that unreference work
	fail_if( OK != view_unref(view), NULL );

} END_TEST

/**
 * \brief Test to create application view.
 *
 * The test create and destroy application view. Check if everything behaves
 * correctly.
 */
START_TEST(test_create_application) {

	ViewGeneric *view = NULL;

	view  = view_create(VIEW_APPLICATION);
	fail_if( NULL == view, NULL );
	fail_if( NULL == view->specific_data, NULL );
	fail_if( NULL != view->model, NULL );
	fail_if( NULL != view->controller, NULL );
	fail_if( VIEW_APPLICATION != view->type, NULL );
	fail_if( 0 != view->last_sequence, NULL );
	fail_if( 1 != view->reference, NULL );

	// check that simple destruction won't work
	fail_if( OK == view_destroy(view), NULL );
	fail_if( ! error_is_error(), NULL );
	error_reset();

	// check that unreference work
	fail_if( OK != view_unref(view), NULL );

} END_TEST

/**
 * \brief Test to create image display view.
 *
 * The test create and destroy image display view. Check if everything behaves
 * correctly.
 */
START_TEST(test_create_image_display) {

	ViewGeneric *view = NULL;

	view  = view_create(VIEW_IMAGE_DISPLAY);
	fail_if( NULL == view, NULL );
	fail_if( NULL == view->specific_data, NULL );
	fail_if( NULL != view->model, NULL );
	fail_if( NULL != view->controller, NULL );
	fail_if( VIEW_IMAGE_DISPLAY != view->type, NULL );
	fail_if( 0 != view->last_sequence, NULL );
	fail_if( 1 != view->reference, NULL );

	// check that simple destruction won't work
	fail_if( OK == view_destroy(view), NULL );
	fail_if( ! error_is_error(), NULL );
	error_reset();

	// check that unreference work
	fail_if( OK != view_unref(view), NULL );

} END_TEST


/**
 * The function reset error buffer before the test.
 */
static void test_generic_setup(void)
{
	COLOR_SETUP_START;
	view_generic = view_create(VIEW_NONE);
	model_generic1 = model_create(MODEL_NONE);
	model_generic2 = model_create(MODEL_NONE);
	cntl_generic1 = cntl_create(CNTL_NONE);
	cntl_generic2 = cntl_create(CNTL_NONE);
	error_reset();
	COLOR_SETUP_END;
	return ;
}

/**
 * The function reset error buffer after the test.
 */
static void test_generic_teardown(void)
{
	COLOR_TEARDOWN_START;
	fail_unless( OK == view_unref(view_generic), NULL);
	fail_unless( OK == model_unref(model_generic1), NULL);
	fail_unless( OK == model_unref(model_generic2), NULL);
	fail_unless( OK == cntl_unref(cntl_generic1), NULL);
	fail_unless( OK == cntl_unref(cntl_generic2), NULL);
	error_reset();
	COLOR_TEARDOWN_END;
	return ;
}

/**
 * \brief Test call to generic update() method.
 *
 * The test checks that the view_update() function will fail if the view
 * supplied to it is generic view and not the specific view.
 */
START_TEST(test_generic_update) {

	guint sequence = view_generic->last_sequence;

	view_update(view_generic, 1);
	fail_if( sequence == view_generic->last_sequence, NULL );

} END_TEST

/**
 * \brief Test call to generic update() method.
 *
 * The test checks that the view_redraw() function fail when the view supplied
 * to it is generic view and not the specific view.
 */
START_TEST(test_generic_redraw) {

	fail_if( OK != view_redraw(view_generic, &test_region), NULL);
	fail_if( error_is_error(), NULL);
} END_TEST

/**
 * \brief Test call to generic resize() method.
 *
 * The test supplies new dimensions to the generic view and test if they were
 * updated correctly. The function shall correctly update the the dimensions.
 */
START_TEST(test_generic_resize) {

	GdkRectangle rect = { 50, 100, 150, 200 };

	fail_if( OK != view_resize(view_generic, rect), NULL);

	fail_if( rect.x != view_generic->region.x, NULL);
	fail_if( rect.y != view_generic->region.y, NULL);
	fail_if( rect.width != view_generic->region.width, NULL);
	fail_if( rect.height != view_generic->region.height, NULL);
} END_TEST

/**
 * \brief Test setup of the model to the view
 *
 * Check that the setup is propagated to the model and that it is correctly
 * cleared when the view has new model assigned.
 *
 * Check that last sequence is correctly updated when view changes.
 */
START_TEST(test_set_model) {

	// set arbitrary sequence numbers to the models
	model_generic1->sequence = 20;
	model_generic2->sequence = 10;

	// check preconditions for the test are set
	fail_if( NULL != view_generic->model, NULL );
	fail_if( 0 != observer_get_count(model_generic1->view_list), NULL) ;
	fail_if( 0 != observer_get_count(model_generic2->view_list), NULL) ;
	fail_if( 1 != model_generic1->reference, NULL );
	fail_if( 1 != model_generic2->reference, NULL );
	ck_assert_int_eq( 0, view_generic->last_sequence );

	view_set_model(view_generic, model_generic1);

	// check that the model and view are correctly linked
	fail_if( model_generic1 != view_generic->model, NULL );
	fail_if( 1 != observer_get_count(model_generic1->view_list), NULL) ;
	fail_if( 0 != observer_get_count(model_generic2->view_list), NULL) ;
	fail_if( 2 != model_generic1->reference, NULL );
	fail_if( 1 != model_generic2->reference, NULL );
	ck_assert_int_eq( 20, view_generic->last_sequence );

	// change  model assigned to the view
	view_set_model(view_generic, model_generic2);

	// check that linking of the view and model are updated
	fail_if( model_generic2 != view_generic->model, NULL );
	fail_if( 0 != observer_get_count(model_generic1->view_list), NULL) ;
	fail_if( 1 != observer_get_count(model_generic2->view_list), NULL) ;
	fail_if( 1 != model_generic1->reference, NULL );
	fail_if( 2 != model_generic2->reference, NULL );
	ck_assert_int_eq( 10, view_generic->last_sequence );

	// delete the link completely
	view_set_model(view_generic, NULL);

	// check that view is not linked to any model anymore
	fail_if( NULL != view_generic->model, NULL );
	fail_if( 0 != observer_get_count(model_generic1->view_list), NULL) ;
	fail_if( 0 != observer_get_count(model_generic2->view_list), NULL) ;
	fail_if( 1 != model_generic1->reference, NULL );
	fail_if( 1 != model_generic2->reference, NULL );
	ck_assert_int_eq( 0, view_generic->last_sequence );

} END_TEST

/**
 * \brief Test setup of the controller to the view
 *
 * Check that the setup is propagated to the controller and that it is correctly
 * cleared when the view has new controller assigned.
 */
START_TEST(test_set_controller) {

	// preconditions for the test
	fail_if( NULL != view_generic->controller, NULL );
	fail_if( NULL != cntl_generic1->view, NULL );
	fail_if( NULL != cntl_generic2->view, NULL );
	fail_if( 1 != cntl_generic1->reference, NULL );
	fail_if( 1 != cntl_generic2->reference, NULL );

	// check assignenment of new controller
	view_set_controller(view_generic, cntl_generic1);

	fail_if( cntl_generic1 != view_generic->controller, NULL );
	fail_if( view_generic != cntl_generic1->view, NULL );
	fail_if( NULL != cntl_generic2->view, NULL );
	fail_if( 2 != cntl_generic1->reference, NULL );
	fail_if( 1 != cntl_generic2->reference, NULL );

	// check changing controller
	view_set_controller(view_generic, cntl_generic2);

	fail_if( cntl_generic2 != view_generic->controller, NULL );
	fail_if( NULL != cntl_generic1->view, NULL );
	fail_if( view_generic != cntl_generic2->view, NULL );
	fail_if( 1 != cntl_generic1->reference, NULL );
	fail_if( 2 != cntl_generic2->reference, NULL );

	// check removing controller link completely
	view_set_controller(view_generic, NULL);

	fail_if( NULL != view_generic->controller, NULL );
	fail_if( NULL != cntl_generic1->view, NULL );
	fail_if( NULL != cntl_generic2->view, NULL );
	fail_if( 1 != cntl_generic1->reference, NULL );
	fail_if( 1 != cntl_generic2->reference, NULL );

} END_TEST


/**
 * \brief Create test suite for generic view module.
 * \return pointer to generic view suite
 */
Suite* create_view_generic_suite (void) {

	Suite *suite = suite_create("View");

	/**
	 * The test case groups test for creation and destruction of generic and
	 * specific views. These really do not have a common setup or teardown
	 * functionality.
	 */
	TCase *tc_view_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_view_manage, common_setup, common_teardown);
	tcase_add_test(tc_view_manage, test_create_none);
	tcase_add_test(tc_view_manage, test_create_application);
	tcase_add_test(tc_view_manage, test_create_image_display);

	/**
	 * The test cases from here further group update() and redraw() test for one
	 * kind of view (generic, image display, ...) and thus some kind of common
	 * setup ant teardown can be utilized.
	 */
	TCase *tc_view_generic = tcase_create("Generic");
	tcase_add_checked_fixture(tc_view_generic, test_generic_setup, test_generic_teardown);
	tcase_add_test(tc_view_generic, test_generic_update);
	tcase_add_test(tc_view_generic, test_generic_redraw);
	tcase_add_test(tc_view_generic, test_generic_resize);
	tcase_add_test(tc_view_generic, test_set_model);
	tcase_add_test(tc_view_generic, test_set_controller);

	suite_add_tcase(suite, tc_view_manage);
	suite_add_tcase(suite, tc_view_generic);

	return suite;
}
