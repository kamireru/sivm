/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "src/frontend/view_generic.h"
#include "src/frontend/view_application.h"
#include "src/frontend/view_image_display.h"

#include "test/common/common.h"
#include "test/frontend/test_view_common.h"
#include "test/frontend/test_view_application.h"

/**
 * Variable holding for holding prepared application view for more complex
 * tests. This is a separate variable to increase readability of the code.
 */
static ViewGeneric *application_view = NULL;

/**
 * \brief Test creation of application view structure.
 *
 * Test function that creates specific data structure for application view. This
 * call should never be used directly.
 */
START_TEST(test_create_application_data) {

	ViewApplication *app_data = NULL;

	app_data = view_app_create(view_generic);
	fail_if( NULL == app_data, NULL);

	fail_if( sizeof(*app_data) != sizeof(ViewApplication), NULL );
	fail_if( NULL != app_data->child, NULL );

	fail_if( OK != view_app_destroy(app_data), NULL);

} END_TEST

/**
 * \brief Test setup of the application view child.
 *
 * Create application view, verify the child is set to null and then change the
 * child view setting two times to check both arbitrary pointer setup and NULL
 * pointer setup.
 */
START_TEST(test_child_view_setup) {

	ViewGeneric *view = NULL;
	ViewApplication *app_data = NULL;

	// create application view and get pointer to its data
	view  = view_create(VIEW_APPLICATION);
	fail_if( NULL == view, NULL );

	app_data = view_app_get_data_pointer(view);
	fail_if( NULL != app_data->child, NULL );

	// precondition check
	fail_if( 1 != view_generic->reference, NULL );

	// test setup of the child view
	fail_if( FAIL == view_app_set_child(view, view_generic), NULL );
	fail_if( view_generic != app_data->child );
	fail_if( app_data->child != view_app_get_child(view), NULL );
	fail_if( 2 != view_generic->reference, NULL );

	// test removing of the child
	fail_if( FAIL == view_app_set_child(view, NULL), NULL );
	fail_if( NULL != app_data->child );
	fail_if( NULL != view_app_get_child(view), NULL );
	fail_if( 1 != view_generic->reference, NULL );

	fail_if( OK != view_unref(view), NULL );

} END_TEST


/**
 * The function reset error buffer before the test and setup application view
 * with one child view for main functionality testing used for testing
 * application view creation function.
 */
static void setup_application(void)
{
	COLOR_SETUP_START;
	view_generic = view_create(VIEW_NONE);
	application_view = view_create(VIEW_APPLICATION);

	view_app_set_child( application_view, view_generic);
	view_unref(view_generic);

	error_reset();
	COLOR_SETUP_END;
	return ;
}

/**
 * The function reset error buffer after the test.
 */
static void teardown_application(void)
{
	COLOR_TEARDOWN_START;
	fail_unless( OK == view_unref(view_generic), NULL);
	fail_unless( OK == view_unref(application_view), NULL);
	error_reset();
	COLOR_TEARDOWN_END;
	return ;
}

/**
 * \brief Test new region propagation to child
 */
START_TEST(test_main_resize) {
	GdkRectangle rect = { 50, 100, 150, 200 };

	fail_if( OK != view_resize(application_view, rect), NULL);

	// the application view will store the region info
	fail_if( rect.x != application_view->region.x, NULL);
	fail_if( rect.y != application_view->region.y, NULL);
	fail_if( rect.width != application_view->region.width, NULL);
	fail_if( rect.height != application_view->region.height, NULL);

	// the child view will have same region info (view_generic variable is used
	// for the check since it is faster and more readable)
	fail_if( rect.x != view_generic->region.x, NULL);
	fail_if( rect.y != view_generic->region.y, NULL);
	fail_if( rect.width != view_generic->region.width, NULL);
	fail_if( rect.height != view_generic->region.height, NULL);

} END_TEST


/**
 * \brief Create test suite for application view module.
 * \return pointer to generic view suite
 */
Suite* create_view_application_suite (void) {

	Suite *suite = suite_create("ViewApplication");

	TCase *tc_view_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_view_manage, common_setup_view_generic, common_teardown_view_generic);
	tcase_add_test(tc_view_manage, test_create_application_data);
	tcase_add_test(tc_view_manage, test_child_view_setup);

	TCase *tc_view_main = tcase_create("Main");
	tcase_add_checked_fixture(tc_view_main, setup_application, teardown_application);
	tcase_add_test(tc_view_main, test_main_resize);

	suite_add_tcase(suite, tc_view_manage);
	suite_add_tcase(suite, tc_view_main);

	return suite;
}
