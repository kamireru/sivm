/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <gdk/gdkkeysyms.h>
#include "src/frontend/cntl_application.h"

#include "test/frontend/test_cntl_application.h"
#include "test/common/common.h"

/**
 * \brief Test to create application controller
 *
 * Test that specific data structure is correctly allocated and reset.
 */
START_TEST(test_create_controller) {
	RCode result;
	CntlGeneric *cntl = NULL;

	cntl = cntl_create(CNTL_APPLICATION);
	ck_assert( NULL != cntl );
	ck_assert_int_eq( CNTL_APPLICATION, cntl->type );
	ck_assert_int_eq( 1, cntl->reference );

	ck_assert( NULL == cntl->view );
	ck_assert( NULL != cntl->automaton );
	ck_assert( NULL == cntl->specific_data );

	// try controller destruction
	result = cntl_destroy(cntl);
	ck_assert_int_eq( FAIL, result );
	ck_assert_int_eq( TRUE, error_is_error() );
	error_reset();

	// try controller dereference
	result = cntl_unref(cntl);
	ck_assert_int_eq( TRUE, result );
} END_TEST

/**
 * \brief Create test suite for application controller module.
 * \return pointer to application controller suite
 */
Suite* create_cntl_application_suite (void) {

	Suite *suite = suite_create("CntlApplication");

	TCase *tc_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_manage, common_setup, common_teardown);
	tcase_add_test(tc_manage, test_create_controller);

	suite_add_tcase(suite, tc_manage);

	return suite;
}
