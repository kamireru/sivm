/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <gdk/gdk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include "src/backend/group_list.h"
#include "src/backend/image_group.h"
#include "src/frontend/model_generic.h"
#include "src/frontend/model_image_list.h"

#include "test/common/common.h"
#include "test/frontend/test_model_generic.h"

/**
 * \brief List of image objects that will be used through testing.
 *
 * We need real image instances to test image group properly. This list contains
 * predefined images that can be used for it.
 *
 * The first there image files are real images, next two files are real files
 * which are not images and the last two files does not exist in the test data.
 */
static gchar*	image_name_list[] = {
		"data/unit/4x4_black.png",
		"data/unit/4x4_white.png",
		"data/unit/lvl1_dir3/4x4_brown.png",
		"data/unit/not_image.txt",
		"data/unit/not_image.txt",
		"data/unit/not_found.txt",
		"data/unit/not_exist.txt",
		"data/unit/lvl1_dir2/4x4_blue.png",
		"data/unit/lvl1_dir2/4x4_blue.png",
		"data/unit/lvl1_dir2/4x4_blue.png",
		"data/unit/lvl1_dir2/4x4_blue.png",
		"data/unit/lvl1_dir2/4x4_blue.png",
		"data/unit/lvl1_dir2/4x4_blue.png",
		"data/unit/lvl1_dir2/4x4_blue.png"
	};

/**
 * \brief Array with pointers to images for group creation.
 *
 * Array of image pointers that is used for fast creation of image group
 * content.
 */
static Image* pointer_list[14] = { NULL };

/**
 * Variable for storing pointer to pre-allocated model used in the tests.
 */
static ModelGeneric *model_generic = NULL;

/**
 * Variable for storing pointer to pre-allocated image group used in the tests.
 */
static ImageGroup *image_group = NULL;
static ImageGroup *source_group = NULL;
static ImageGroup *target_group = NULL;

/**
 * \brief Test to create image list model
 */
START_TEST(test_create) {

	ModelGeneric* model = NULL;
	ModelImageList* app_data = NULL;

	model = model_create(MODEL_IMAGE_LIST);
	ck_assert( NULL != model);
	ck_assert( NULL != model->view_list);
	ck_assert( NULL != model->specific_data);

	ck_assert( MODEL_IMAGE_LIST == model->type);
	ck_assert_int_eq( 1, model->sequence);

	app_data = model_iml_get_data_pointer(model);
	ck_assert( NULL != app_data);
	ck_assert( NULL == app_data->image_group );
	ck_assert( NULL == app_data->image_data );
	ck_assert_int_eq( -1, app_data->image_index);

	ck_assert( OK != model_destroy(model));
	ck_assert( error_is_error());
	error_reset();

	ck_assert( OK == model_unref(model));

} END_TEST

/**
 * \brief Verify function for loading image from filesystem
 *
 * Try to load existing image, file that is not and image and file that does not
 * exists in the filesystem.
 */
START_TEST(test_load_image) {
	GdkPixbufAnimation* image = NULL;

	image = model_iml_load_image("data/unit/4x4_black.png");
	ck_assert( NULL != image );
	ck_assert( ! error_is_error() );
	g_object_unref( (gpointer)image );

	image = model_iml_load_image("data/unit//not_image.txt");
	ck_assert( NULL == image );
	ck_assert( ! error_is_error() );

	image = model_iml_load_image("data/unit/not_found.txt");
	ck_assert( NULL == image );
	ck_assert( ! error_is_error() );

} END_TEST

/**
 * \brief Verify retrieval of image group from group list and its creation if it
 * is not present yet.
 */
START_TEST(test_load_group) {
	ImageGroup* group_a = NULL;
	ImageGroup* group_b = NULL;
	RCode result;

	result = glist_create();
	ck_assert_int_eq(OK, result );
	ck_assert_int_eq(0, glist_get_size() );

	// there is no group yet, so one will be created
	group_a = model_iml_load_group('a');
	ck_assert( NULL != group_a );
	ck_assert_int_eq( 'a', group_a->tag );
	ck_assert_int_eq( 1, glist_get_size() );

	group_b = model_iml_load_group('b');
	ck_assert( NULL != group_b );
	ck_assert_int_eq( 'b', group_b->tag );
	ck_assert_int_eq( 2, glist_get_size() );

	// there already should be group 'a'
	group_b = model_iml_load_group('a');
	ck_assert( group_a == group_b );

	result = glist_destroy();
	ck_assert_int_eq( OK, result );

} END_TEST



/* ========================================================================= */
static void model_setup(void) {
	int i;
	COLOR_SETUP_START;
	model_generic = model_create(MODEL_IMAGE_LIST);

	for (i = 0; i < 14; i++) {
		pointer_list[i] = image_create(image_name_list[i]);
	}

	image_group = group_create("test_name", 't');

	if ( error_is_error() ) ERROR_LOG();
	COLOR_SETUP_END;
}

/**
 * The function reset error buffer and deallocate instance of model used in the
 * test (so that we don't need to reset it for next test).
 */
static void model_teardown(void) {
	int i;
	COLOR_TEARDOWN_START;

	model_unref(model_generic);
	group_destroy(image_group);

	for (i = 0; i < 14; i++) {
		image_unref(pointer_list[i]);
		pointer_list[i] = NULL;
	}

	if ( error_is_error() ) ERROR_LOG();
	COLOR_TEARDOWN_END;
}

/**
 * \brief Verify setup of image group when first file is valid image
 *
 * Setup an image group to the model and make sure that the first file in the
 * group is existing, valid image
 */
START_TEST(test_setup_group_first_valid) {
	ModelImageList* app_data = NULL;

	// add test data into image group
	ck_assert( OK == group_insert(image_group, 0, pointer_list, 7) );
	ck_assert_int_eq( 7, group_get_size(image_group) );

	app_data = model_iml_get_data_pointer(model_generic);
	ck_assert( NULL != app_data);
	ck_assert( NULL == app_data->image_group );
	ck_assert_int_eq( -1, app_data->image_index );

	ck_assert( OK == model_iml_set_image_group(model_generic, image_group) );
	ck_assert( image_group == app_data->image_group );
	ck_assert_int_eq( 0, app_data->image_index );
	ck_assert_int_eq( 7, group_get_size(image_group) );

	// unset the image group
	ck_assert( OK == model_iml_set_image_group(model_generic, NULL) );
	ck_assert( NULL == app_data->image_group );
	ck_assert_int_eq( -1, app_data->image_index );

} END_TEST

/**
 * \brief Verify setup of image group when first file(s) are not valid images
 *
 * Setup an image group to the model and make sure that the first file or more in the
 * group does not exists or are not valid images. There is at least one valid
 * image file in the group.
 */
START_TEST(test_setup_group_first_invalid) {
	ModelImageList* app_data = NULL;
	Image* test_data[4] = {
			pointer_list[4], // not valid image
			pointer_list[6], // does not exists
			pointer_list[0], pointer_list[1] // valid images
		};

	// add test data into image group
	ck_assert( OK == group_insert(image_group, 0, test_data, 4) );
	ck_assert_int_eq( 4, group_get_size(image_group) );

	app_data = model_iml_get_data_pointer(model_generic);
	ck_assert( NULL != app_data);
	ck_assert( NULL == app_data->image_group );

	ck_assert( OK == model_iml_set_image_group(model_generic, image_group) );
	ck_assert( image_group == app_data->image_group );
	ck_assert_int_eq( 0, app_data->image_index );
	ck_assert_int_eq( 2, group_get_size(image_group) );

	// unset the image group
	ck_assert( OK == model_iml_set_image_group(model_generic, NULL) );
	ck_assert( NULL == app_data->image_group );
	ck_assert_int_eq( -1, app_data->image_index );

} END_TEST

/**
 * \brief Verify setup of image group when no file is valid
 *
 * Setup an image group to the model and make sure that there is no valid file
 * in the image group at all.
 */
START_TEST(test_setup_group_none_valid) {
	ModelImageList* app_data = NULL;
	Image* test_data[4] = {
			pointer_list[3], pointer_list[4], // not valid image
			pointer_list[5], pointer_list[6], // does not exists
		};

	// add test data into image group
	ck_assert( OK == group_insert(image_group, 0, test_data, 4) );
	ck_assert_int_eq( 4, group_get_size(image_group) );

	app_data = model_iml_get_data_pointer(model_generic);
	ck_assert( NULL != app_data);
	ck_assert( NULL == app_data->image_group );
	ck_assert_int_eq( -1, app_data->image_index );

	ck_assert( OK == model_iml_set_image_group(model_generic, image_group) );
	ck_assert( image_group == app_data->image_group );
	ck_assert_int_eq( -1, app_data->image_index );
	ck_assert_int_eq( 0, group_get_size(image_group) );

	// unset the image group
	ck_assert( OK == model_iml_set_image_group(model_generic, NULL) );
	ck_assert( NULL == app_data->image_group );
	ck_assert_int_eq( -1, app_data->image_index );

} END_TEST

/**
 * \brief Check updating position in the group list
 *
 * Verify that the image index moving work correctly, especially when the index
 * points to first or last image in the group. In this test use image group with
 * valid images only
 */
START_TEST(test_change_image_index) {
	ModelImageList* app_data = NULL;
	Image* test_data[8] = {
			pointer_list[0], pointer_list[1], pointer_list[2], pointer_list[7],
			pointer_list[8], pointer_list[9], pointer_list[10], pointer_list[11],
		};

	// add test data into image group
	ck_assert( OK == group_insert(image_group, 0, test_data, 8) );
	ck_assert_int_eq( 8, group_get_size(image_group) );

	app_data = model_iml_get_data_pointer(model_generic);
	ck_assert( NULL != app_data);

	// verify image index is set to 0 when group is assigned
	ck_assert( OK == model_iml_set_image_group(model_generic, image_group) );
	ck_assert_int_eq( 0, app_data->image_index );

	// verify set index lower than image group bound
	ck_assert( OK == model_iml_change_image(model_generic, -10, TRUE) );
	ck_assert_int_eq( 0, app_data->image_index );

	// verify set index higher than image group bound
	ck_assert( OK == model_iml_change_image(model_generic, 10, TRUE) );
	ck_assert_int_eq( 7, app_data->image_index );

	// verify set index into image group bounds
	ck_assert( OK == model_iml_change_image(model_generic, 4, TRUE) );
	ck_assert_int_eq( 4, app_data->image_index );

} END_TEST

/**
 * \brief Check position update in image group when new index fails
 *
 * Verify that the image index moving work correctly when the new position is on
 * unloadable image and fallback must be used.
 *
 * The data used in the test are invalid image surrounded by valid images.
 */
START_TEST(test_change_image_index_fail) {
	ModelImageList* app_data = NULL;
	Image* test_data[4] = {
			pointer_list[0], pointer_list[3], pointer_list[4], pointer_list[7]
		};

	// add test data into image group
	ck_assert( OK == group_insert(image_group, 0, test_data, 4) );
	ck_assert_int_eq( 4, group_get_size(image_group) );

	app_data = model_iml_get_data_pointer(model_generic);
	ck_assert( NULL != app_data);

	// verify image index is set to 0 when group is assigned
	ck_assert( OK == model_iml_set_image_group(model_generic, image_group) );
	ck_assert_int_eq( 0, app_data->image_index );

	// verify with index forwarding
	ck_assert( OK == model_iml_change_image(model_generic, 1, FALSE) );
	ck_assert_int_eq( 0, app_data->image_index );
	ck_assert_int_eq( 3, group_get_size(image_group) );

	// verify without index forwarding
	ck_assert( OK == model_iml_change_image(model_generic, 1, TRUE) );
	ck_assert_int_eq( 1, app_data->image_index );
	ck_assert_int_eq( 2, group_get_size(image_group) );

} END_TEST

/**
 * \brief Check position update in image group when multiple indexes fails
 *
 * Verify that the image index moving work correctly when the new position is on
 * the start of several unloadable images. Make sure that all unloadable images
 * in sequence are immediately removed.
 *
 * The data used in the test are invalid image surrounded by valid images.
 */
START_TEST(test_change_image_next_multi_fail) {
	ModelImageList* app_data = NULL;
	Image* test_data[7] = {
			pointer_list[0], // valid image
			pointer_list[3], pointer_list[4], pointer_list[5], pointer_list[6],
			pointer_list[1], pointer_list[2] // valid image
		};

	// add test data into image group
	ck_assert( OK == group_insert(image_group, 0, test_data, 7) );
	ck_assert_int_eq( 7, group_get_size(image_group) );

	app_data = model_iml_get_data_pointer(model_generic);
	ck_assert( NULL != app_data);

	// verify image index is set to 0 when group is assigned
	ck_assert( OK == model_iml_set_image_group(model_generic, image_group) );
	ck_assert_int_eq( 0, app_data->image_index );
	ck_assert_int_eq( 7, group_get_size(image_group) );

	// go to first invalid image of four
	ck_assert( OK == model_iml_move_index(model_generic, MOVE_INDEX_NEXT, 1) );
	ck_assert_int_eq( 1, app_data->image_index );
	ck_assert_int_eq( 3, group_get_size(image_group) );

} END_TEST

/**
 * \brief Check position update in image group when last indexe fails
 *
 * Verify that the image index moving work correctly when the new position is on
 * the last index in the group and the index is not valid image.
 */
START_TEST(test_change_image_next_last_fail) {
	ModelImageList* app_data = NULL;
	Image* test_data[7] = {
			pointer_list[0], pointer_list[1], pointer_list[2], // valid image
			pointer_list[3] // invalid image
		};

	// add test data into image group
	ck_assert( OK == group_insert(image_group, 0, test_data, 4) );
	ck_assert_int_eq( 4, group_get_size(image_group) );

	app_data = model_iml_get_data_pointer(model_generic);
	ck_assert( NULL != app_data);

	// verify image index is set to 0 when group is assigned
	ck_assert( OK == model_iml_set_image_group(model_generic, image_group) );
	ck_assert_int_eq( 0, app_data->image_index );
	ck_assert_int_eq( 4, group_get_size(image_group) );

	// go to first invalid image of four
	ck_assert( OK == model_iml_move_index(model_generic, MOVE_INDEX_NEXT, 3) );
	ck_assert_int_eq( 2, app_data->image_index );
	ck_assert_int_eq( 3, group_get_size(image_group) );

} END_TEST

/**
 * \brief Check position update in image group when multiple indexes fails
 *
 * Verify that the image index moving work correctly when the new position is on
 * the start of several unloadable images. Make sure that all unloadable images
 * in sequence are immediately removed.
 *
 * The data used in the test are invalid image surrounded by valid images.
 */
START_TEST(test_change_image_prev_multi_fail) {
	ModelImageList* app_data = NULL;
	Image* test_data[7] = {
			pointer_list[0], pointer_list[3], pointer_list[4], pointer_list[5],
			pointer_list[6], pointer_list[1], pointer_list[2]
		};

	// add test data into image group
	ck_assert( OK == group_insert(image_group, 0, test_data, 7) );
	ck_assert_int_eq( 7, group_get_size(image_group) );

	app_data = model_iml_get_data_pointer(model_generic);
	ck_assert( NULL != app_data);

	// verify image index is set to 0 when group is assigned
	ck_assert( OK == model_iml_set_image_group(model_generic, image_group) );
	ck_assert( OK == model_iml_change_image(model_generic, 5, TRUE) );
	ck_assert_int_eq( 5, app_data->image_index );
	ck_assert_int_eq( 7, group_get_size(image_group) );

	// go to first invalid image of four
	ck_assert( OK == model_iml_move_index(model_generic, MOVE_INDEX_PREV, 1) );
	ck_assert_int_eq( 0, app_data->image_index );
	ck_assert_int_eq( 3, group_get_size(image_group) );

} END_TEST

/**
 * \brief Check moving pointer forward and backward
 *
 * Verify that the image index moving work correctly, especially when the index
 * points to first or last image in the group. In this test use image group with
 * valid images only
 */
START_TEST(test_change_image_move) {
	ModelImageList* app_data = NULL;
	Image* test_data[8] = {
			pointer_list[0], pointer_list[1], pointer_list[2], pointer_list[7],
			pointer_list[8], pointer_list[9], pointer_list[10], pointer_list[11],
		};

	// add test data into image group
	ck_assert( OK == group_insert(image_group, 0, test_data, 8) );
	ck_assert_int_eq( 8, group_get_size(image_group) );

	app_data = model_iml_get_data_pointer(model_generic);
	ck_assert( NULL != app_data);

	// verify image index is set to 0 when group is assigned
	ck_assert( OK == model_iml_set_image_group(model_generic, image_group) );
	ck_assert_int_eq( 0, app_data->image_index );

	// verify calling 'prev' on first image
	ck_assert( OK == model_iml_move_index(model_generic, MOVE_INDEX_PREV, 1) );
	ck_assert_int_eq( 0, app_data->image_index );

	// verify calling 'next' on image that is not last
	ck_assert( OK == model_iml_move_index(model_generic, MOVE_INDEX_NEXT, 5) );
	ck_assert_int_eq( 5, app_data->image_index );

	// verify calling 'prev' on image that is not first
	ck_assert( OK == model_iml_move_index(model_generic, MOVE_INDEX_PREV, 2) );
	ck_assert_int_eq( 3, app_data->image_index );

	// verify calling 'prev' that goes out of bound
	ck_assert( OK == model_iml_move_index(model_generic, MOVE_INDEX_PREV, 20) );
	ck_assert_int_eq( 0, app_data->image_index );

	// verify calling 'next' that goes out of bound
	ck_assert( OK == model_iml_move_index(model_generic, MOVE_INDEX_NEXT, 20) );
	ck_assert_int_eq( 7, app_data->image_index );

	// verify calling 'next' on last image in group
	ck_assert( OK == model_iml_move_index(model_generic, MOVE_INDEX_NEXT, 1) );
	ck_assert_int_eq( 7, app_data->image_index );

	// verify calling 'first'
	ck_assert( OK == model_iml_move_index(model_generic, MOVE_INDEX_FIRST, 0) );
	ck_assert_int_eq( 0, app_data->image_index );

	// verify calling 'last'
	ck_assert( OK == model_iml_move_index(model_generic, MOVE_INDEX_LAST, 0) );
	ck_assert_int_eq( 7, app_data->image_index );

	// verify calling 'index'
	ck_assert( OK == model_iml_move_index(model_generic, MOVE_INDEX_INDEX, 3) );
	ck_assert_int_eq( 3, app_data->image_index );

} END_TEST



/* ========================================================================= */

static void transfer_setup(void) {
	int i;
	COLOR_SETUP_START;
	model_generic = model_create(MODEL_IMAGE_LIST);

	for (i = 0; i < 7; i++) {
		// fill group with seven images, but use only valid ones
		pointer_list[i] = image_create(image_name_list[0]);
	}

	source_group = group_create("source", 's');
	target_group = group_create("target", 't');
	group_insert(source_group, 0, pointer_list, 7);

	if ( error_is_error() ) ERROR_LOG();
	COLOR_SETUP_END;
	return ;
}

static void transfer_teardown(void) {
	int i;
	COLOR_TEARDOWN_START;

	group_destroy(source_group);
	group_destroy(target_group);

	for (i = 0; i < 7; i++) {
		image_unref(pointer_list[i]);
		pointer_list[i] = NULL;
	}

	if ( error_is_error() ) ERROR_LOG();
	COLOR_TEARDOWN_END;
	return ;
}

/**
 * \brief Verify handling of situations where there is nothing to transfer.
 */
START_TEST(test_transfer_no_action) {
	ModelImageList* iml_data = NULL;
	RCode result;

	iml_data = model_iml_get_data_pointer(model_generic);
	ck_assert( NULL != iml_data );

	iml_data->image_index = 0;

	// test invocation with count equal to zero
	iml_data->image_group = source_group;
	model_iml_transfer_image(model_generic, target_group, 0, FALSE);
	ck_assert_int_eq(0, group_get_size(target_group));

	// test invocation with source group equals to target group
	iml_data->image_group = source_group;
	ck_assert_int_eq(7, group_get_size(source_group));
	model_iml_transfer_image(model_generic, source_group, 0, FALSE);
	ck_assert_int_eq(7, group_get_size(source_group));

	// test invocation with empty source group
	iml_data->image_group = target_group;
	ck_assert_int_eq(7, group_get_size(source_group));
	model_iml_transfer_image(model_generic, source_group, 0, FALSE);
	ck_assert_int_eq(7, group_get_size(source_group));

	// test invocation with NULL source group
	iml_data->image_group = NULL;
	model_iml_transfer_image(model_generic, target_group, 0, FALSE);
	ck_assert_int_eq(0, group_get_size(target_group));

	// test invocation with NULL target group
	model_iml_transfer_image(model_generic, NULL, 0, FALSE);
} END_TEST

/**
 * Structure holding input and output data for copy tests between image groups.
 */
struct TestCopyData {
		int index;
		int count;
		int copy_count;
	};

/*
 * Define variable holding test data for copy transfer test. Source group has 7
 * images in it.
 */
#define TEST_COPY_COUNT 4
struct TestCopyData test_copy_data[TEST_COPY_COUNT] = {
		{ 0, 1, 1 },
		{ 5, 2, 2 },
		{ 0, 10, 7 },
		{ 3, 10, 4 }
	};

/**
 * \brief Verify copying a list of images between groups
 */
START_TEST(test_transfer_copy) {
	ModelImageList* iml_data = NULL;
	RCode result;

	iml_data = model_iml_get_data_pointer(model_generic);
	ck_assert( NULL != iml_data );

	iml_data->image_index = test_copy_data[_i].index;
	iml_data->image_group = source_group;
	model_iml_transfer_image(model_generic, target_group, test_copy_data[_i].count, FALSE);

	ck_assert_int_eq(test_copy_data[_i].copy_count, group_get_size(target_group));

} END_TEST

/**
 * Structure holding input and output data for move tests between image groups.
 */
struct TestMoveData {
		int index;
		int size;
		int count;
		int source_index;
		int source_size;
		int target_size;
	};

/*
 * Define variable holding test data for move transfer test. Source group has 7
 * images in it.
 */
#define TEST_MOVE_COUNT 5
struct TestMoveData test_move_data[TEST_MOVE_COUNT] = {
		{ 0, 7, 7,  -1, 0, 7 },
		{ 0, 7, 10,  -1, 0, 7 },
		{ 0, 7, 1,  0, 6, 1 },
		{ 1, 7, 2,  1, 5, 2 },
		{ 3, 7, 5,  2, 3, 4 }
	};

/**
 * \brief Verify moveing a list of images between groups
 */
START_TEST(test_transfer_move) {
	ModelImageList* iml_data = NULL;
	RCode result;

	iml_data = model_iml_get_data_pointer(model_generic);
	ck_assert( NULL != iml_data );

	iml_data->image_index = test_move_data[_i].index;
	iml_data->image_group = source_group;

	ck_assert_int_eq( test_move_data[_i].size, group_get_size(source_group) );

	model_iml_transfer_image(model_generic, target_group, test_move_data[_i].count, TRUE);

	ck_assert_int_eq( test_move_data[_i].source_index, iml_data->image_index );
	ck_assert_int_eq( test_move_data[_i].source_size, group_get_size(source_group) );
	ck_assert_int_eq( test_move_data[_i].target_size, group_get_size(target_group) );
} END_TEST




/* ========================================================================= */

/**
 * \brief Verify retrieval of group information (group name, position, total)
 *
 * Setup an image group to the model and make sure that the first file in the
 * group is existing, valid image
 */
START_TEST(test_query_group_info) {
	ModelImageList* app_data = NULL;
	gchar* result = NULL;
	Image* test_data_1[3] = { pointer_list[0], pointer_list[1], pointer_list[2] };
	Image* test_data_2[7] = {
			pointer_list[7], pointer_list[8], pointer_list[9], pointer_list[10],
			pointer_list[11], pointer_list[12], pointer_list[13]
		};

	// test with no image group associated with model
	result = model_iml_query_group_info(model_generic);
	ck_assert( NULL != result );
	ck_assert_str_eq( "n/a", result );
	g_free( (gpointer)result );

	group_insert(image_group, 0, test_data_1, 3);
	model_iml_set_image_group(model_generic, image_group);

	result = model_iml_query_group_info(model_generic);
	ck_assert( NULL != result );
	ck_assert_str_eq( "t 1/3", result );
	g_free( (gpointer)result );

	group_insert(image_group, 0, test_data_2, 7);
	model_iml_move_index(model_generic, MOVE_INDEX_NEXT, 1);

	result = model_iml_query_group_info(model_generic);
	ck_assert( NULL != result );
	ck_assert_str_eq( "t 2/10", result );
	g_free( (gpointer)result );
} END_TEST

/**
 * \brief Verify retrieval of current image filename
 */
START_TEST(test_query_image_name) {
	ModelImageList* app_data = NULL;
	gchar* result = NULL;

	// test with no image group associated with model
	result = model_iml_query_image_name(model_generic);
	ck_assert( NULL != result );
	ck_assert_str_eq( "n/a", result );
	g_free( (gpointer)result );

	group_insert(image_group, 0, pointer_list, 3);
	model_iml_set_image_group(model_generic, image_group);

	result = model_iml_query_image_name(model_generic);
	ck_assert( NULL != result );
	ck_assert_str_eq( image_name_list[0], result );
	g_free( (gpointer)result );

	model_iml_move_index(model_generic, MOVE_INDEX_NEXT, 1);

	result = model_iml_query_image_name(model_generic);
	ck_assert( NULL != result );
	ck_assert_str_eq( image_name_list[1], result );
	g_free( (gpointer)result );
} END_TEST


/**
 * \brief Create test suite for image list model module.
 * \return pointer to image list model suite
 */
Suite* create_model_image_list_suite (void) {

	Suite *suite = suite_create("ModelImageList");

	TCase *tc_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_manage, common_setup, common_teardown);
	tcase_add_test(tc_manage, test_create);
	tcase_add_test(tc_manage, test_load_image);
	tcase_add_test(tc_manage, test_load_group);

	TCase *tc_setup = tcase_create("Setup");
	tcase_add_checked_fixture(tc_setup, model_setup, model_teardown);
	tcase_add_test(tc_setup, test_setup_group_first_valid);
	tcase_add_test(tc_setup, test_setup_group_first_invalid);
	tcase_add_test(tc_setup, test_setup_group_none_valid);

	TCase *tc_change = tcase_create("Change");
	tcase_add_checked_fixture(tc_change, model_setup, model_teardown);
	tcase_add_test(tc_change, test_change_image_index);
	tcase_add_test(tc_change, test_change_image_index_fail);
	tcase_add_test(tc_change, test_change_image_move);
	tcase_add_test(tc_change, test_change_image_next_multi_fail);
	tcase_add_test(tc_change, test_change_image_next_last_fail);
	tcase_add_test(tc_change, test_change_image_prev_multi_fail);

	TCase *tc_transfer = tcase_create("Transfer");
	tcase_add_checked_fixture(tc_transfer, transfer_setup, transfer_teardown);
	tcase_add_test(tc_transfer, test_transfer_no_action);
	tcase_add_loop_test(tc_transfer, test_transfer_copy, 0, TEST_COPY_COUNT);
	tcase_add_loop_test(tc_transfer, test_transfer_move, 0, TEST_MOVE_COUNT);

	TCase *tc_query = tcase_create("Query");
	tcase_add_checked_fixture(tc_query, model_setup, model_teardown);
	tcase_add_test(tc_query, test_query_group_info);
	tcase_add_test(tc_query, test_query_image_name);

	suite_add_tcase(suite, tc_manage);
	suite_add_tcase(suite, tc_setup);
	suite_add_tcase(suite, tc_change);
	suite_add_tcase(suite, tc_transfer);
	suite_add_tcase(suite, tc_query);

	return suite;
}
