/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "src/frontend/cntl_generic.h"

#include "test/frontend/test_cntl_generic.h"
#include "test/common/common.h"

/**
 * \brief Test to create generic cntl
 *
 * The test create and destroy generic cntl with type CNTL_NONE - ie no specific
 * cntl stuff is created and destroyed.
 */
START_TEST(test_create_none) {

	int i;
	CntlGeneric *cntl = NULL;

	cntl = cntl_create(CNTL_NONE);
	fail_if( NULL == cntl, NULL);
	fail_if( CNTL_NONE != cntl->type, NULL );
	fail_if( NULL != cntl->view, NULL );
	fail_if( NULL == cntl->automaton, NULL );
	fail_if( 1 != cntl->reference, NULL );

	fail_if( OK == cntl_destroy(cntl), NULL );
	fail_if( ! error_is_error(), NULL );
	error_reset();

	fail_if ( OK != cntl_unref(cntl), NULL);

} END_TEST



/**
 * \brief Test management of the controller automatons.
 *
 * The test get pointer to automaton (several times) and then unreferences it to
 * test that only one instance of automaton is created.
 *
 * The test is run in the loop and internal variable _i is filled with value of
 * the CntlType to test (need to be cast).
 */
START_TEST(test_automaton_manage) {
	RCode result;
	CntlType type = (CntlType)_i;
	FaAutomaton *automaton_ptr = NULL;

	// verify there is 0 references at the start
	ck_assert_int_eq(0, cntl_automaton_count(type) );

	// verify reference decrease to zero and not further
	result = cntl_automaton_destroy(type);
	ck_assert_int_eq( FAIL, result );
	ck_assert_int_eq( FALSE, error_is_error() );

	// create first reference (and automaton)
	automaton_ptr = cntl_automaton_create(type);
	ck_assert( NULL != automaton_ptr );
	ck_assert_int_eq( 1, cntl_automaton_count(type) );

	// verify automaton is created only once and references increase
	ck_assert( automaton_ptr == cntl_automaton_create(type) );
	ck_assert_int_eq( 2, cntl_automaton_count(type) );

	// verify references decrease
	result = cntl_automaton_destroy(type);
	ck_assert_int_eq( OK, result );
	ck_assert_int_eq( 1, cntl_automaton_count(type) );

	// remove final reference and check that count is 0
	// (use valgrind to verify deallocation of the automaton structure)
	result = cntl_automaton_destroy(type);
	ck_assert_int_eq( OK, result );
	ck_assert_int_eq( 0, cntl_automaton_count(type) );

} END_TEST

/**
 * \brief Create test suite for generic controller module.
 * \return pointer to generic controller suite
 */
Suite* create_cntl_generic_suite (void) {

	Suite *suite = suite_create("Cntl");

	TCase *tc_manage = tcase_create("Manage");
	tcase_add_checked_fixture(tc_manage, common_setup, common_teardown);
	tcase_add_test(tc_manage, test_create_none);

	TCase *tc_automaton = tcase_create("Automaton");
	tcase_add_checked_fixture(tc_automaton, common_setup, common_teardown);
	tcase_add_test(tc_automaton, test_automaton_manage);
	tcase_add_loop_test(tc_automaton, test_automaton_manage, CNTL_NONE, CNTL_LAST - 1);

	suite_add_tcase(suite, tc_manage);
	suite_add_tcase(suite, tc_automaton);

	return suite;
}
