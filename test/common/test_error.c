/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <check.h>
#include <stdio.h>
#include <string.h>

#include "test/common/test_error.h"
#include "src/common/error.h"

ERROR_DECLARE_START
	ERROR_DECLARE(SOME)
	ERROR_DECLARE(OTHER)
ERROR_DECLARE_END

ERROR_DEFINE_START(ELIST)
	ERROR_DEFINE(SOME, "Some error"),
	ERROR_DEFINE(OTHER, "Other error")
ERROR_DEFINE_END

/**
 * Check if error types are corretly created
 */
START_TEST(test_error_type)
{
	fail_unless( 0 == strcmp(ELIST.SOME.name, "SOME"), NULL);
	fail_unless( 0 == strcmp(ELIST.SOME.title, "Some error"), NULL);

	fail_unless( 0 == strcmp(ELIST.OTHER.name, "OTHER"), NULL);
	fail_unless( 0 == strcmp(ELIST.OTHER.title, "Other error"), NULL);

}
END_TEST

/**
 * Check if error is really empty after call to reset
 */
START_TEST(test_no_error)
{
	char*	result = "";


	fail_unless( error_get_info()->error == NULL, NULL);
	fail_unless( error_get_info()->file == NULL, NULL);
	fail_unless( error_get_info()->line == 0, NULL);
	fail_unless( error_get_info()->truncated == 0, NULL);
	fail_unless( error_get_info()->note_size == strlen(error_get_info()->note_buffer) );
	fail_unless( 0 == strcmp(error_get_info()->note_buffer, result),
				"\nRESULT='%s'\nEXPECTED='%s'\n", error_get_info()->note_buffer, result
				);
	fail_unless( 0 == error_get_discarded());
}
END_TEST


/**
 * Only few basic items should be filled when error is only set
 */
#line 1 "test/common/test_error.c"
START_TEST(test_set_error) {

	char* result1 = "test/common/test_error.c";
	char* result2 = "";

	ERROR_SET(ELIST.SOME);
	fail_unless( error_get_info()->error == &ELIST.SOME, NULL);
	fail_unless( 0 == strcmp(error_get_info()->file, result1),
				"\nRESULT='%s'\nEXPECTED='%s'\n", error_get_info()->file, result1
				);
	fail_unless( error_get_info()->line == 6, NULL);
	fail_unless( error_get_info()->truncated == 0, NULL);
	fail_unless( error_get_info()->note_size == strlen(error_get_info()->note_buffer) );
	fail_unless( 0 == strcmp(error_get_info()->note_buffer, result2),
				"\nRESULT='%s'\nEXPECTED='%s'\n", error_get_info()->note_buffer, result2
				);
	fail_unless( 0 == error_get_discarded());
}
END_TEST

/**
 * Test adding notes to the error
 */
#line 1 "test/common/test_error.c"
void function1() {

	ERROR_SET(ELIST.SOME);
	ERROR_NOTE("Note 1");
	return;
}

START_TEST(test_simple_note) {

	char* result1 = "test/common/test_error.c";
	char* result2 = "* function1(): Note 1 [test/common/test_error.c:4]\n"
					"* test_simple_note(): Note 2 [test/common/test_error.c:15]\n";

	function1();
	ERROR_NOTE("Note 2");
	fail_unless( error_get_info()->error == &ELIST.SOME, NULL);
	fail_unless( 0 == strcmp(error_get_info()->file, result1),
				"\nRESULT='%s'\nEXPECTED='%s'\n", error_get_info()->file, result1
				);
	fail_unless( error_get_info()->line == 3, NULL);
	fail_unless( error_get_info()->truncated == 0, NULL);
	fail_unless( error_get_info()->note_size == strlen(error_get_info()->note_buffer) );
	fail_unless( 0 == strcmp(error_get_info()->note_buffer, result2),
				"\nRESULT='%s'\nEXPECTED='%s'\n", error_get_info()->note_buffer, result2
				);
	fail_unless( 0 == error_get_discarded());
}
END_TEST

/**
 * Complicated message in note
 */
#line 1 "test/common/test_error.c"
START_TEST(test_complicated_note) {

	char* result1 = "test/common/test_error.c";
	char* result2 = "* test_complicated_note(): This is 3 times more complicated "
					"[test/common/test_error.c:8]\n";

	ERROR_SET(ELIST.SOME);
	ERROR_NOTE("This %s %d times more complicated", "is", 3);
	fail_unless( error_get_info()->error == &ELIST.SOME, NULL);
	fail_unless( 0 == strcmp(error_get_info()->file, result1),
			"\nRESULT='%s'\nEXPECTED='%s'\n", error_get_info()->file, result1
		);
	fail_unless( error_get_info()->line == 7, NULL);
	fail_unless( error_get_info()->truncated == 0, NULL);
	fail_unless( error_get_info()->note_size == strlen(error_get_info()->note_buffer) );
	fail_unless( 0 == strcmp(error_get_info()->note_buffer, result2),
			"\nRESULT='%s'\nEXPECTED='%s'\n", error_get_info()->note_buffer, result2
		);
	fail_unless( 0 == error_get_discarded());
}
END_TEST

/**
 * Discarded messages
 */
START_TEST(test_discarded_error)
{

	ERROR_SET(ELIST.SOME);
	ERROR_SET(ELIST.SOME);
	ERROR_SET(ELIST.SOME);
	fail_unless( 2 == error_get_discarded());
	error_reset();
	fail_unless( 0 == error_get_discarded());
}
END_TEST

/**
 * \brief Test storage of too long message.
 *
 * Add more notes that the size of the error buffer to produce truncated error
 * notes.
 */
#line 1 "test/common/test_error.c"
START_TEST(test_truncated_note) {

	int i, written;
	char* result_file = "test/common/test_error.c";
	char result_text[ERROR_BUFFER_SIZE];
	char* note = "This message is to test truncation of notes";
	const char* note_text = "* test_truncated_note(): This message is to test truncation "
			"of notes [test/common/test_error.c:15]\n";
	char* text_ptr = result_text;

	ERROR_SET(ELIST.SOME);
	for (i = 0; i <= ERROR_BUFFER_SIZE / strlen(note_text); i++) {
		int string_limit = ERROR_BUFFER_SIZE - i*strlen(note_text);

		ERROR_NOTE(note);
		written = snprintf(text_ptr, string_limit, "%s", note_text);
		text_ptr = (written > string_limit) ? NULL:  text_ptr + written;
	}

	fail_unless( error_get_info()->error == &ELIST.SOME, NULL);
	fail_unless( 0 == strcmp(error_get_info()->file, result_file),
			"\nRESULT='%s'\nEXPECTED='%s'\n",
			error_get_info()->file, result_file
		);
	fail_unless( error_get_info()->line == 11,
			"EXPECTED=%d, ACTUAL=%d",
			11, error_get_info()->line
		);
	fail_unless( error_get_info()->truncated == 1, NULL);
	fail_unless( 0 == strcmp(error_get_info()->note_buffer, result_text),
			"\nRESULT='%s'\nEXPECTED='%s'\n",
			error_get_info()->note_buffer, result_text
		);
	fail_unless( error_get_info()->note_size > strlen(error_get_info()->note_buffer),
			"EXPECTED: %d >= %d\n",
			error_get_info()->note_size, strlen(error_get_info()->note_buffer)
		);
	fail_unless( error_get_info()->note_size >= ERROR_BUFFER_SIZE,
			"EXPECTED: %d >= %d",
			error_get_info()->note_size, ERROR_BUFFER_SIZE
		);
	fail_unless( 0 == error_get_discarded());
}
END_TEST


/**
 * result of error_check() method
 */
START_TEST(test_check)
{
	// result when there is an error
	ERROR_SET(ELIST.SOME);
	fail_unless( &ELIST.SOME == error_get_info()->error, NULL);
	fail_unless( error_is_error(), NULL);

	// result when there is no error
	error_reset();
	fail_unless( NULL == error_get_info()->error, NULL);
	fail_unless( ! error_is_error(), NULL);
}
END_TEST


/**
 * Function for setting up environment before tests
 */
void test_error_setup(void)
{
	error_reset();
	return ;
}

void test_error_teardown(void)
{
	error_reset();
	return ;
}

/**
 * \brief Create test suite for error handling.
 * \return pointer to error test suite
 */
Suite* create_error_suite (void)
{
  Suite *s = suite_create ("Error");

  TCase *tc_display = tcase_create ("Display");
  tcase_add_checked_fixture (tc_display, test_error_setup, test_error_teardown);
  tcase_add_test (tc_display, test_error_type);
  tcase_add_test (tc_display, test_no_error);
  tcase_add_test (tc_display, test_set_error);
  tcase_add_test (tc_display, test_simple_note);
  tcase_add_test (tc_display, test_complicated_note);
  tcase_add_test (tc_display, test_check);

  TCase *tc_limits = tcase_create ("Limits");
  tcase_add_checked_fixture (tc_limits, test_error_setup, test_error_teardown);
  tcase_add_test (tc_limits, test_discarded_error);
  tcase_add_test (tc_limits, test_truncated_note);


  suite_add_tcase (s, tc_display);
  suite_add_tcase (s, tc_limits);

  return s;
}
