/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "src/config.h"

#include <string.h>
#include <malloc.h>

#include "src/common/error.h"
#include "test/common/common.h"

/**
 * The function will check number of arguments and will create argv array of
 * appropriate length. The array will be filled by pointers to original string
 * and the string will be updated with '\\0' characters at the end of arguments.
 *
 * Limitations: The function is really simple, it does not handle parameters
 * enclosed in parentheses (ie params with spaces) and trailing spaces.
 *
 * \param command command line string to parse and update
 * \param argc return parameter with number of parameter
 * \param argv return parameter with string array
 */
void create_program_args(char  *command, int *argc, char ***argv) {

	int i, count=0;
	char **array = NULL;

	// check number of arguments
	for ( i = 0; i <= strlen(command); i++ ) {
		if ( 0 == i || ' ' != command[i-1] ) {
			count = (' ' == command[i] || '\0' == command[i]) ? count + 1 : count;
		}
	}

	array = (char**) malloc( (count+1) * sizeof(char*) );
	if ( NULL != array ) {
		i = 0;
		char *end_ptr = NULL;
		char *start_ptr = command;
		do {
			// find argument endpoints
			while ( ' ' == *start_ptr ) start_ptr++;
			end_ptr = strchrnul(start_ptr, ' ');

			// duplicate the argument and assign it
			array[i++] = strndup(start_ptr, end_ptr - start_ptr);

			if ( '\0' != *end_ptr ) {
				start_ptr = end_ptr;
			} else {
				array[i] = NULL;
			}
		} while ( '\0' != *end_ptr );
	}

	*argv = array;
	*argc = count;
	return;
}

/**
 * Deallocate argv array created by create_program_args() function and '\\0'
 * characters back to spaces.
 *
 * \param argc number of parameters
 * \param argv list of parameters
 */
void destroy_program_args(int argc, char ***argv) {

	int i;

	// deallocate memory
	for (i=0; NULL != (*argv)[i]; i++) {
		free( (void*)((*argv)[i]) );
	}

	free( (void*)(*argv) );
	*argv = NULL;
	return;
}

/**
 * Common setup() function that setups trace coloring. It is to be used when
 * tests contain functinality that emits traces, but does not require
 * setup/teardown.
 */
void common_setup(void) {
	COLOR_SETUP_START;

	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_SETUP_END;
	return;
}

/**
 * Common teardown() function that returns color setup to original setting. It
 * is to be used when tests contain functinality that emits traces, but does not
 * require setup/teardown.
 */
void common_teardown(void) {
	COLOR_TEARDOWN_START;

	if ( error_is_error() ) {
		ERROR_LOG();
	}

	COLOR_TEARDOWN_END;
	return;
}
