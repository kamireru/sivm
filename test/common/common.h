/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file common.h
 *
 * Defines global variables and functions used in unit test suites.
 */
#ifndef TEST_COMMON_COMMON_H
#define TEST_COMMON_COMMON_H

#include <stdio.h>
#include "src/config.h"

/**
 * \brief Return string or "NULL".
 *
 * The macro will serve on places where we need to print out string and it can
 * be NULL. The g_print() function can do that, but we do not want to use
 * glib in unit tests too much
 */
#define STRINGIFY(STRING) ( NULL == (STRING) ? "NULL" : (STRING))


/** \brief Create argv and argc arguments from command line string. */
extern void create_program_args(char *command, int *argc, char ***argv);

/** \brief The function will deallocate memory and revert updates to the input string. */
extern void destroy_program_args(int argc, char ***argv);

/** \brief Reusable setup function that only colors output. */
extern void common_setup(void);

/** \brief Reusable teardown function that disables output coloring. */
extern void common_teardown(void);


/**
 * \brief Start coloring of setup() unit test function.
 *
 * The macro will output ANSI color sequence to start colorization of traces
 * from setup() function. The color of traces from setup block is green.
 *
 * The colorization is turned on/off by UNIT_COLOR_ENABLE macro
 */
#define COLOR_SETUP_START \
do { if ( 1 == UNIT_COLOR_ENABLE ) { printf("[32m"); } } while (0)


/**
 * \brief End coloring of setup() unit test function.
 *
 * The macro will output ANSI color sequence to end colorization of traces from
 * setup() function and will turn on color for test function traces. The color
 * of traces from test block is blue.
 *
 * The colorization is turned on/off by UNIT_COLOR_ENABLE macro
 */
#define COLOR_SETUP_END \
do { if ( 1 == UNIT_COLOR_ENABLE ) { printf("[34m"); } } while (0)


/**
 * \brief Start coloring of teardown() unit test function.
 *
 * The macro will output ANSI color sequence to start colorization of traces
 * from teardown() function. The color of traces from teardown() block is red.
 *
 * The colorization is turned on/off by UNIT_COLOR_ENABLE macro
 */
#define COLOR_TEARDOWN_START \
do { if ( 1 == UNIT_COLOR_ENABLE ) { printf("[31m"); } } while (0)


/**
 * \brief End coloring of teardown() unit test function.
 *
 * The macro will output ANSI color sequence to stop all colorization (ie revert
 * back to default color.
 *
 * The colorization is turned on/off by UNIT_COLOR_ENABLE macro
 */
#define COLOR_TEARDOWN_END \
do { if ( 1 == UNIT_COLOR_ENABLE ) { printf("[0m"); } } while (0)

#endif

