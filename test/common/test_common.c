/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <malloc.h>
#include <string.h>

#include "test/common/test_common.h"
#include "test/common/common.h"

/**
 * \brief Check creation and destruction of command line arguments from string.
 *
 * We will need to test several different strings to verify that the parsing is
 * correct. This function wrap common functionality in the tests. The test
 * function will only prepare string to parse and data to use for check.
 *
 * \param cmdline string to parse
 * \param count number of parameters to expect on output
 * \param result array of parameters to expect on output
 */
void test_common_cmdarg_generic(char *cmdline, int count, char *result[]) {

	char *command = NULL;
	int i;
	int argc = 0;
	char **argv = NULL;

	command = strdup(cmdline);

	create_program_args(command, &argc, &argv);

	ck_assert_int_ne( 0, argc );
	ck_assert_int_eq( count, argc );
	ck_assert( NULL != argv );

	for (i=0; i < count; i++) {
		ck_assert( NULL != argv[i] );
		ck_assert_str_eq( argv[i], result[i] );
	}

	ck_assert( NULL == argv[count] );

	destroy_program_args(argc, &argv);

	ck_assert( NULL == argv );
	ck_assert_str_eq( command, cmdline );

	free((void*)command);
}

/**
 * \brief Check creation and destruction of command line arguments from string.
 *
 * Check simple scenario with only one argument on command line.
 */
START_TEST(test_common_cmdarg_single) {

	char command[] = "progname";
	char *result[] = { "progname", NULL };

	test_common_cmdarg_generic(command, 1, result);
}
END_TEST

/**
 * \brief Check creation and destruction of command line arguments from string.
 *
 * Check simple scenario with three parameters divided by single space - ie
 * ideal configuration.
 */
START_TEST(test_common_cmdarg_simple) {

	char command[] = "progname first second";
	char *result[] = { "progname", "first", "second", NULL };

	test_common_cmdarg_generic(command, 3, result);
}
END_TEST

/**
 * \brief Check creation and destruction of command line arguments from string.
 *
 * Check scenario where parameters are divided with arbitrary number of spaces.
 * But there are no spaces before first and after last argument.
 */
START_TEST(test_common_cmdarg_space) {

	char command[] = "progname   first  second     third";
	char *result[] = { "progname", "first", "second", "third", NULL };

	test_common_cmdarg_generic(command, 4, result);
}
END_TEST

/**
 * \brief Check function of STRINGIFY macro
 */
START_TEST(test_common_stringify) {

	char *null_ptr = NULL;

	fail_unless(0 == strcmp("NULL", STRINGIFY(NULL)));
	fail_unless(0 == strcmp("NULL", STRINGIFY(null_ptr)));
	fail_unless(0 == strcmp("String", STRINGIFY("String")));

}
END_TEST

/**
 * Create test suite for common utilities used in other test suites.
 *
 * \return common test suite pointer
 */
Suite* create_common_suite (void) {
  Suite *suite = suite_create ("Common");

  TCase *tc_macro = tcase_create ("Macro");
  tcase_add_checked_fixture (tc_macro, NULL, NULL);
  tcase_add_test (tc_macro, test_common_stringify);

  TCase *tc_cmdline = tcase_create ("CmdLine");
  tcase_add_checked_fixture (tc_cmdline, NULL, NULL);
  tcase_add_test (tc_cmdline, test_common_cmdarg_single);
  tcase_add_test (tc_cmdline, test_common_cmdarg_simple);
  tcase_add_test (tc_cmdline, test_common_cmdarg_space);

  suite_add_tcase (suite, tc_macro);
  suite_add_tcase (suite, tc_cmdline);

  return suite;
}
