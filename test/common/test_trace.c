/*
 * Copyright (C) 2010 Zdenek Crha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <check.h>

#include "test/common/test_trace.h"
#include "src/common/trace.h"


// we only need to define trace areas here, no need to declare them since we
// won't be exporting them to different module
TRACE_DEFINE(MAIN, 0x1 << 0, TL_DEBUG);
TRACE_DEFINE(LIB1, 0x1 << 1, TL_DEBUG);
TRACE_DEFINE(LIB2, 0x1 << 2, TL_DEBUG);
TRACE_DEFINE(UTIL, 0x1 << 3, TL_DEBUG);

/**
 * \brief Function for setting up environment before tests.
 *
 * Reset global trace mask to disable all traces and reset all test trace areas
 * to the debug level.
 */
static void test_setup(void) {
	trace_global_mask = 0;
	MAIN->severity = TL_DEBUG;
	LIB1->severity = TL_DEBUG;
	LIB2->severity = TL_DEBUG;
	UTIL->severity = TL_DEBUG;

	trace_init(MAIN, LIB1, LIB2, UTIL, NULL);
}

/**
 * \brief Function for cleanup of trace setup
 *
 * This function removes trace configuration. This is mandatory when the tests
 * are not run in separate processes as the trace setup here will influence
 * emitting of the traces from the other test suites (it is global variable).
 */
static void test_teardown(void) {
	trace_global_mask = 0;
}

/**
 * \brief Test trace level setup
 *
 * Test function for setting list of trace areas to desired trace level.
 */
START_TEST(test_set_severity) {

	trace_set_threshold(TL_MINOR, MAIN, UTIL, NULL);
	fail_if( TL_MINOR != MAIN->severity, NULL );
	fail_if( TL_DEBUG != LIB2->severity, NULL );
	fail_if( TL_DEBUG != LIB2->severity, NULL );
	fail_if( TL_MINOR != UTIL->severity, NULL );
} END_TEST

/**
 * \brief Test trace toggling
 *
 * Test function testing enable, disable and toggle of the trace areas
 */
START_TEST(test_toggle) {

	// first test function call that does no update
	trace_toggle(TL_ENABLE, NULL);
	fail_if( 0 != (MAIN->mask & trace_global_mask), NULL );
	fail_if( 0 != (LIB1->mask & trace_global_mask), NULL );
	fail_if( 0 != (LIB2->mask & trace_global_mask), NULL );
	fail_if( 0 != (UTIL->mask & trace_global_mask), NULL );

	trace_toggle(TL_ENABLE, MAIN, UTIL, LIB1, NULL);
	fail_if( 0 == (MAIN->mask & trace_global_mask), NULL );
	fail_if( 0 == (LIB1->mask & trace_global_mask), NULL );
	fail_if( 0 != (LIB2->mask & trace_global_mask), NULL );
	fail_if( 0 == (UTIL->mask & trace_global_mask), NULL );

	trace_toggle(TL_DISABLE, MAIN, NULL);
	fail_if( 0 != (MAIN->mask & trace_global_mask), NULL );
	fail_if( 0 == (LIB1->mask & trace_global_mask), NULL );
	fail_if( 0 != (LIB2->mask & trace_global_mask), NULL );
	fail_if( 0 == (UTIL->mask & trace_global_mask), NULL );

	trace_toggle(TL_TOGGLE, MAIN, LIB1, LIB2, UTIL, NULL);
	fail_if( 0 == (MAIN->mask & trace_global_mask), NULL );
	fail_if( 0 != (LIB1->mask & trace_global_mask), NULL );
	fail_if( 0 == (LIB2->mask & trace_global_mask), NULL );
	fail_if( 0 != (UTIL->mask & trace_global_mask), NULL );
} END_TEST


/**
 * \brief test setup of single tracing area using textual representation.
 *
 * The test uses UTIL trace area in order to not use first trace area in the
 * array.
 */
START_TEST(test_setup_single_area) {

	trace_setup_area("UTIL", "ENABLE");
	fail_if( 0 == (UTIL->mask & trace_global_mask), NULL );

	trace_setup_area("UTIL", "disable");
	fail_if( 0 != (UTIL->mask & trace_global_mask), NULL );

	trace_setup_area("util", "CRITICAL");
	fail_if( TL_CRITICAL != UTIL->severity, NULL );

	trace_setup_area("util", "debug");
	fail_if( TL_DEBUG != UTIL->severity, NULL );
} END_TEST

/**
 * \brief test setup of multiple areas using setup string.
 */
START_TEST(test_setup_multiple_area) {

	trace_setup("MAIN=ENABLE,MAIN=MINOR,UTIL=ENABLE,UTIL=MAJOR");
	fail_if( 0 == (MAIN->mask & trace_global_mask), NULL );
	fail_if( 0 == (UTIL->mask & trace_global_mask), NULL );
	fail_if( TL_MINOR != MAIN->severity, NULL );
	fail_if( TL_MAJOR != UTIL->severity, NULL );

} END_TEST


/**
 * \brief Create test suite for trace handling.
 * \return pointer to trace test suite
 */
Suite* create_trace_suite (void) {

  Suite *s = suite_create ("Trace");

  TCase *tc_manage = tcase_create ("Manage");
  tcase_add_checked_fixture (tc_manage, test_setup, test_teardown);
  tcase_add_test (tc_manage, test_set_severity);
  tcase_add_test (tc_manage, test_toggle);
  tcase_add_test (tc_manage, test_setup_single_area);
  tcase_add_test (tc_manage, test_setup_multiple_area);

  suite_add_tcase (s, tc_manage);

  return s;
}
