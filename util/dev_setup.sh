#!/bin/bash

# Copyright (C) 2010 Zdenek Crha
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
#
# -----------------------------------------------------------------------------
# A script for setting up environment for development:
#
# * set check environment variables to ease unit test debugging
# * set compiller flags to not optimalize code
# * enable traces in sivm program
#

PWD=`pwd`
if [ $(basename $PWD) = "sivm" ]; then
	export SIVM_HOME=$PWD
	export SIVM_TRACE="GUI=ENABLE,MFUN=ENABLE,VIEW=ENABLE,MODL=ENABLE,CNTL=ENABLE"
	export CSCOPE_DB=$SIVM_HOME/cscope.out
	export CK_VERBOSITY=normal
	export CK_FORK=no
	export COLOR=yes
	export CFLAGS="-g -O0"
	ulimit -c 2048

	# setup gtk debug variables to ease valgrind usage
	export G_SLICE=always-malloc
	export G_DEBUG=gc-friendly
else
	echo "Source the script from SIVM root directory"
fi

