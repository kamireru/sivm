#!/usr/bin/perl -w

# Copyright (C) 2010 Zdenek Crha
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License


# -----------------------------------------------------------------------------
# Script used for converting information about commits and authors into GNU
# ChangeLog format
#
# The script expects on list of  [date, name, email, subject] tupples on
# standard input. The list must be sorted from newest to oldest entry and fields
# must be pipe-delimited.
#
# Such input can be retrieved by executing:
# 	git log --format="%ai|%an|%ae|s" | sort --reverse
#

use strict;

my @list = ();
my $old = "";
while ( defined(my $data = <stdin> ) ) {
	chomp $data;
	my ($date, $name, $email, $subject) = split /\|/, $data;
	($date) = split / /, $date;

	my $line = "$date\t$name\t<$email>";

	if ( $old ne $line ) {
		map { print $_; } sort @list;
		@list = ();

		print "\n$line\n\n";
		$old = $line;
	}
	push @list, "\t* $subject\n";
}

map { print $_; } sort @list;
