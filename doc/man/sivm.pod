=head1 NAME

sivm - simple image viewer and manager

=head1 SYNOPSIS

sivm [OPTIONS] FILE...

=head1 DESCRIPTION

Lightweight program for displaying images and organizing them in filesystem.


=head1 OPTIONS

=head2 B<--config FILE, -c FILE>

Name of the configuration file to use. If none is present, then the program
looks for I<.sivmrc> in home directory.

=head2 B<--profile NAME, -p NAME>

Set name of configuration profile to look for in config file.

=head2 B<--layout NAME, -l NAME>

Name of the gui layout to use for application. Supported values are:

=over

=item I<minimal> display only image

=item I<simple> display image and status bar (default)

=back

=head2 B<--fullscreen, -f>

Start program in fullscreen mode

=head2 B<--geometry WIDTHxHEIGHT, -g WIDTHxHEIGHT>

Set initial geometry of the program window.

=head2 B<--auto-zoom TYPE, -a TYPE>

Configure auto-zoom type to be used by program. The type is integer value from 0
to 3:

=over

=item value 0 (no auto zooming)

=item value 1 (zoom to fit whole image)

=item value 2 (zoom to fit image width)

=item value 3 (zoom to fit image height)

=back

=head2 B<--zoom ZOOM, -z ZOOM>

Set initial zoom value. The zoom is set as multiplier and default value is
I<1.0>.

=head2 B<--pan PERCENTAGE, -P PERCENTAGE>

Set percentage of the view screen to be used when panning the image.  Value
should be in interval <0.1, 1.0>. Default value is I<0.6>.

=head2 B<--recurse DEPTH, -r DEPTH>

Recurse into subdirectories when constructing list of images to display.
Parameter specifies maximum depth of recursion.

=head2 B<--exclude PATTERN, -e PATTERN>

Configure one or more glob I<PATTERN>s used for excluding files during image
list construction on program start.

=head2 B<--trace STRING>

Configure internal tracing module to display more detailed information about
program workflow. This option is used mainly for debugging purposes.

See program documentation for more detailed explanation of tracing and possible
options.

=head2 B<--version, -v>

Display program version to the standard output.

=head2 B<--help, -h>

Display usage information to the standard output.


=head1 NAVIGATION IN PROGRAM

The program is controller by key sequences (inspiration taken from Vim text
editor). Most of the sequences are one keystroke long, but others can be longer.

=over

=item B<q>

Quit the program

=item B<p,n> and B<Page Up, Page Down>

backward in the current image list.

If preceded by keystrokes forming number, then the number indicates how many
images will be skipped in the move. Otherwise, move only to the next or previous
image.

=item B<h,j,k,l> and B<left,up,down,right>

Pan the displayed area of image in chosen direction if it is larger than program
screen. If image is smaller than screen, then nothing will happen.

=item B<P,N> and B<Home,End>

Move to the first or last image in the image list.

=item B<M>

If preceded by keystrokes forming number, move to the index specified by that
number. Otherwise, move to the first image in list.

=item B<f>

Toggle program between window and fullscreen mode

=back


=head1 TRACE CONFIGURATION

The inbuild tracing module allows to set different log level to different areas
of program code. Thus it is possible to look for program errors without being
overwhelmed by detailed logging from whole program.

For example, it is possible to display debugging message from configuration
loading part of the program and nothing at all from gui part of the program.

Setup of enabled tracing areas and their message severity threshold is done by
providin setup string either by environment variable or by command line option.

The setup string contains B<key=value pairs> delimited by comma characters. The
B<key> is always upper case name of the program area. The B<value> is either
on/off or threshold level indicator.

=over

=item Trace areas

I<MFUN>, I<CONF>, I<STRG>, I<PILE>, I<IGRP>, I<GREC>, I<GUI>, I<VIEW>, I<CNTL>,
I<MODL>, I<LAY>, I<CONT>

=item On/off indicators

I<ENABLE>, I<DISABLE>

=item Threshold level indicators

I<DEBUG>, I<INFO>, I<MINOR>, I<MAJOR>, I<CRITICAL>

=back

B<For example:>

	CONF=ENABLE,CONF=MINOR,GUI=DISABLE

Will enable tracing from configuration program area, but only messages with
minor serverity and higher. And at the same time it disable all tracing from gui
program area.


=head1 FILES

=over

=item I<~/.sivmrc>

Default configuration file.

=back


=head1 LICENSE

This program is distributed the terms of the GNU General Public License v3.0 as
published by the Free Software Foundation.

=head1 AUTHOR

Zdenek Crha <zdenek.crha@gmail.com>

=head1 BUGS

Probably many.

If you want to report a problem with program, please send an email to
zdenek.crha@gmail.com.
